(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.inotify = {};
  Drupal.behaviors.inotify.RefreshNotificationsView = function(){
    // Find the notification view.
    $.each(Drupal.views.instances ,function getInstance(index, element) {
      if(element.settings.view_name == 'notifications') {
        element.refreshViewAjax.progress = {type:'none'};
        $('.js-view-dom-id-' + element.settings.view_dom_id).trigger('RefreshView');
        $( document ).ajaxStop(function() {
          $('.inotify-opener .count').html($('.inotify-block-content .unread').length);
          $('.inotify-opener .count').show();
        });
      }
    });
  };

  Drupal.behaviors.NotificationsBlockEvents = {
    attach: function (context, settings) {
      $('.inotify-opener').once().click(function() {
        $('.inotify-block-content').toggleClass('active');
        $('.inotify-opener .count').hide();
      });
      $('body').click(function(evt) {
        if($(evt.target).closest('.inotify-block').length) {
          return;
        }
        else {
          if ($('.inotify-block-content').hasClass('active')) {
            $('.inotify-block-content').toggleClass('active')
          }
        }
      });
    }
  };

  Drupal.Nodejs.callbacks.RefreshNotificationsViewCallBack = {
    callback: function (message) {
    	if (message.callback == 'RefreshNotificationsViewCallBack') {
        Drupal.behaviors.inotify.RefreshNotificationsView();
    	}
    }
  };
})(jQuery, Drupal);
