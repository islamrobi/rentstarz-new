<?php

namespace Drupal\inotify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class InotifySettingsForm.
 */
class InotifySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'inotify.inotifysettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inotify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('inotify.inotifysettings');
    $form['delete_after'] = [
      '#type' => 'number',
      '#title' => $this->t('Delete notifications after (days)'),
      '#description' => $this->t('Delete notfications after X days.'),
      '#default_value' => $config->get('delete_after'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('inotify.inotifysettings')
      ->set('delete_after', $form_state->getValue('delete_after'))
      ->save();
  }

}
