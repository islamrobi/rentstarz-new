<?php

namespace Drupal\inotify\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\node\Entity\Node;
use Drupal\inotify\Entity\InotifyNotification;

/**
 * Provides a 'Create Inotify Notification' action.
 *
 * @RulesAction(
 *   id = "create_inotify_notification",
 *   label = @Translation("Create iNotify Notification"),
 *   category = @Translation("iNotify"),
 *   context = {
 *     "uid" = @ContextDefinition("string",
 *       label = @Translation("UID"),
 *       description = @Translation("Specifies the uid of the receiver of the notification.")
 *     ),
 *     "title" = @ContextDefinition("string",
 *       label = @Translation("Title"),
 *       description = @Translation("Specifies the title of the notification.")
 *     ),
 *     "description" = @ContextDefinition("string",
 *       label = @Translation("Description"),
 *       description = @Translation("Specifies the description of the notification.")
 *     ),
 *     "target_link" = @ContextDefinition("link",
 *       label = @Translation("Target Link"),
 *       description = @Translation("Specifies the URL the should open when the notification is clicked.")
 *     )
 *   }
 * )
 */
class CreateNotification extends RulesActionBase {

  /**
   * Create Notification.
   *
   * {@inheritdoc}
   */
  protected function doExecute($uid, $title, $description, $target_link) {
    // Create notification entity.
    $notification = InotifyNotification::create([
      'title' => $title,
      'description' => $description,
      'target_link' => $target_link,
      'uid' => $uid,
    ]);
    $notification->save();
  }

}
