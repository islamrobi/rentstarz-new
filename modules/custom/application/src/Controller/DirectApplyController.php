<?php

namespace Drupal\application\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for the Example module.
 */
class DirectApplyController extends ControllerBase
{
    /**
     * Returns a simple page.
     *
     * @return array
     *   A simple renderable array.
     */

    public $access_key;
    public $property_id;
    public $property;
    public $applicant;
    public $co_applicant;
    public $user;
    public $co_applicant_email;
    public $mail_manager;
    public $co_applicant_exist;
    public $co_applicant_update_form;

    public function directApply($propertyid)
    {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        // Common Variables
        $this->property_id = $propertyid;
        $this->user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $this->access_key = md5(time() + $this->property_id);
        $this->mail_manager = \Drupal::service('plugin.manager.mail');
        $this->co_applicant_update_form = false;

        if (isset($_GET['co_applicant_update_form']) && $_GET['co_applicant_update_form']) {
            $this->co_applicant_update_form = true;
        }

        // Check application exist with the co-applicant email
        $this->co_applicant_exist = false;
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'application')
            ->condition('uid', $this->user->id())
            ->condition('field_co_applicant_email', 'saimazerin7@gmail.com');

        $results = $query->execute();
        foreach ($results as $result) {
            $application_id = $result;
        }

        if (isset($application_id)) {
            $this->co_applicant_exist = true;
        }

        // Redirect users to the login page if not authenticated
        if (!$this->user->isAuthenticated()) {
            $response = new RedirectResponse('/user/login');
            return $response->send();
        }

        // Redirect to the listing page if applicant data doesn't exist
        if (empty($this->user->field_applicant_id->getvalue()[0]['value'])) {
            $response = new RedirectResponse('/property-listing');
            return $response->send();
        }

        // Check is user applied on this property or not
        $application_exist = false;
        $property_application_nids = \Drupal::entityQuery('node')->condition('type', 'application')->execute();
        $property_applications =  \Drupal\node\Entity\Node::loadMultiple($property_application_nids);

        foreach ($property_applications as $property_application) {
            $property_applicant_id = $property_application->getOwnerId();
            $property_id_on_application = $property_application->get('field_property')->getValue()[0]['target_id'];

            if (($property_applicant_id == $this->user->id()) && $this->property_id == $property_id_on_application) {
                $application_exist = true;
            }
        }

        if ($application_exist) {
            $response = new RedirectResponse('/property-listing');
            drupal_set_message($this->t('You already applied on this property before'), 'warning', TRUE);
            return $response->send();
        }

        // Load all the nodes
        $this->applicant = Node::load($this->user->field_applicant_id->getvalue()[0]['value']);
        $this->co_applicant = Node::load($this->user->field_co_applicant_id->getvalue()[0]['value']);
        $this->property = Node::load($this->property_id);

        // get all the information from applicant and co-applicant and create application with that information
        //Applicant - Basic Information
        $field_about_me = $this->getParagraph($this->applicant, 'field_about_me');
        $about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $this->getParagraphFieldValue($field_about_me, 'field_first_name'),
            'field_last_name' => $this->getParagraphFieldValue($field_about_me, 'field_last_name'),
            'field_email' => $this->getParagraphFieldValue($field_about_me, 'field_email'),
            'field_driving_license' => $this->getParagraphFieldValue($field_about_me, 'field_driving_license'),
            'field_birth_date' => $this->getParagraphFieldValue($field_about_me, 'field_birth_date'),
            'field_mobile' => $this->getParagraphFieldValue($field_about_me, 'field_mobile'),
        ]);
        $about_me->save();
        $about_me_id = $about_me->id();

        //Applicant - Residence
        $field_residence = $this->getParagraph($this->applicant, 'field_residence');
        $residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $this->getParagraphFieldValue($field_residence, 'field_residence_type'),
            'field_address' => $this->getParagraphFieldValue($field_residence, 'field_address'),
            'field_mov_in_date' => $this->getParagraphFieldValue($field_residence, 'field_mov_in_date'),
            'field_city' => $this->getParagraphFieldValue($field_residence, 'field_city'),
            'field_state' => $this->getParagraphFieldValue($field_residence, 'field_state'),
            'field_zip_code' => $this->getParagraphFieldValue($field_residence, 'field_zip_code'),
            'field_rent' => $this->getParagraphFieldValue($field_residence, 'field_rent'),
            'field_landlord_name' => $this->getParagraphFieldValue($field_residence, 'field_landlord_name'),
            'field_reason_for_leaving' => $this->getParagraphFieldValue($field_residence, 'field_reason_for_leaving'),
        ]);
        $residence->save();
        $residence_id = $residence->id();

        //Occupation
        $field_occupation = $this->getParagraph($this->applicant, 'field_occupation');
        $occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $this->getParagraphFieldValue($field_occupation, 'field_occupation_type'),
            'field_employer_name' => $this->getParagraphFieldValue($field_occupation, 'field_employer_name'),
            'field_job_title' => $this->getParagraphFieldValue($field_occupation, 'field_job_title'),
            'field_monthly_income' => $this->getParagraphFieldValue($field_occupation, 'field_monthly_income'),
            'field_work_address' => $this->getParagraphFieldValue($field_occupation, 'field_work_address'),
            'field_city' => $this->getParagraphFieldValue($field_occupation, 'field_city'),
            'field_zip_code' => $this->getParagraphFieldValue($field_occupation, 'field_zip_code'),
            'field_work_type' => $this->getParagraphFieldValue($field_occupation, 'field_work_type'),
            'field_start_date' => $this->getParagraphFieldValue($field_occupation, 'field_start_date'),
            'field_supervisor_name' => $this->getParagraphFieldValue($field_occupation, 'field_supervisor_name'),
            'field_mobile' => $this->getParagraphFieldValue($field_occupation, 'field_mobile'),
        ]);
        $occupation->save();
        $occupation_id = $occupation->id();

        //Previous Occupation
        $field_previous_occupation = $this->getParagraph($this->applicant, 'field_previous_occupation');
        $previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $this->getParagraphFieldValue($field_previous_occupation, 'field_employer_name'),
            'field_job_title' => $this->getParagraphFieldValue($field_previous_occupation, 'field_job_title'),
            'field_monthly_income' => $this->getParagraphFieldValue($field_previous_occupation, 'field_monthly_income'),
            'field_work_address' => $this->getParagraphFieldValue($field_previous_occupation, 'field_work_address'),
            'field_city' => $this->getParagraphFieldValue($field_previous_occupation, 'field_city'),
            'field_zip_code' => $this->getParagraphFieldValue($field_previous_occupation, 'field_zip_code'),
            'field_work_type' => $this->getParagraphFieldValue($field_previous_occupation, 'field_work_type'),
            'field_start_date' => $this->getParagraphFieldValue($field_previous_occupation, 'field_start_date'),
            'field_supervisor_name' => $this->getParagraphFieldValue($field_previous_occupation, 'field_supervisor_name'),
            'field_mobile' => $this->getParagraphFieldValue($field_previous_occupation, 'field_mobile'),
        ]);
        $previous_occupation->save();
        $previous_occupation_id = $previous_occupation->id();

        //Financial
        $field_financial = $this->getParagraph($this->applicant, 'field_financial');
        $financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $this->getParagraphFieldValue($field_financial, 'field_bank_name'),
            'field_bank_address' => $this->getParagraphFieldValue($field_financial, 'field_bank_address'),
            'field_account_type' => $this->getParagraphFieldValue($field_financial, 'field_account_type'),
            'field_landlord_name' => $this->getParagraphFieldValue($field_financial, 'field_landlord_name'),
        ]);
        $financial->save();
        $financial_id = $financial->id();

        //Reference
        $field_reference = $this->getParagraph($this->applicant, 'field_reference');
        $reference = Paragraph::create([
            'type' => 'reference',
            'field_full_name' => $this->getParagraphFieldValue($field_reference, 'field_full_name'),
            'field_relationship' => $this->getParagraphFieldValue($field_reference, 'field_relationship'),
            'field_address' => $this->getParagraphFieldValue($field_reference, 'field_address'),
            'field_mobile' => $this->getParagraphFieldValue($field_reference, 'field_mobile'),
            'field_landlord_name' => $this->getParagraphFieldValue($field_reference, 'field_landlord_name'),
        ]);
        $reference->save();
        $reference_id = $reference->id();

        //Emergency Contact
        $field_emergency_contact = $this->getParagraph($this->applicant, 'field_emergency_contact');
        $emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $this->getParagraphFieldValue($field_emergency_contact, 'field_full_name'),
            'field_relationship' => $this->getParagraphFieldValue($field_emergency_contact, 'field_relationship'),
            'field_address' => $this->getParagraphFieldValue($field_emergency_contact, 'field_address'),
            'field_mobile' => $this->getParagraphFieldValue($field_emergency_contact, 'field_mobile'),
        ]);
        $emergency_contact->save();
        $emergency_contact_id = $emergency_contact->id();

        //Rental Documents
        $rental_doc_fids = array();
        foreach ($this->applicant->field_rental_document as $item) {
            if ($item->entity) {
                $rental_doc_fids[] = $item->entity->id();
            }
        }

        //Questioneer
        $field_questioneer = $this->getParagraph($this->applicant, 'field_questioneer');
        $questioneer = Paragraph::create([
            'type' => 'questioneer',
            'field_do_you' => $this->getParagraphFieldValue($field_questioneer, 'field_do_you_smoke'),
            'field_illegal_drugs' => $this->getParagraphFieldValue($field_questioneer, 'field_have_you_ever_been_convict'),
            'field_evicted' => $this->getParagraphFieldValue($field_questioneer, 'field_have_you_ever_been_evicted'),
            'field_babankruptcy' => $this->getParagraphFieldValue($field_questioneer, 'field_have_you_ever_filed_for_ba'),
            'field_bedbugs' => $this->getParagraphFieldValue($field_questioneer, 'field_have_you_ever_had_bedbugs_'),
            'field_lequid_filled_furniture' => $this->getParagraphFieldValue($field_questioneer, 'field_lequid_filled_furnit'),
            'field_pets' => $this->getParagraphFieldValue($field_questioneer, 'field_will_you_have_pets_'),
        ]);
        $questioneer->save();
        $questioneer_id = $questioneer->id();

        //Co-Applicant - Basic Information
        $co_applicant_type = $this->co_applicant->field_co_applicant_type->getValue()[0]['value'];
        $co_field_about_me = $this->getParagraph($this->co_applicant, 'field_about_co_applicant');
        $this->co_applicant_email = $this->getParagraphFieldValue($co_field_about_me, 'field_email');
        $co_about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $this->getParagraphFieldValue($co_field_about_me, 'field_first_name'),
            'field_last_name' => $this->getParagraphFieldValue($co_field_about_me, 'field_last_name'),
            'field_email' => $this->co_applicant_email,
            'field_driving_license' => $this->getParagraphFieldValue($co_field_about_me, 'field_driving_license'),
            'field_birth_date' => $this->getParagraphFieldValue($co_field_about_me, 'field_birth_date'),
            'field_mobile' => $this->getParagraphFieldValue($co_field_about_me, 'field_mobile'),
        ]);
        $co_about_me->save();
        $co_about_me_id = $co_about_me->id();

        //Co-Applicant - Residence
        $co_field_residence = $this->getParagraph($this->co_applicant, 'field_residence');
        $co_residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $this->getParagraphFieldValue($co_field_residence, 'field_residence_type'),
            'field_address' => $this->getParagraphFieldValue($co_field_residence, 'field_address'),
            'field_mov_in_date' => $this->getParagraphFieldValue($co_field_residence, 'field_mov_in_date'),
            'field_city' => $this->getParagraphFieldValue($co_field_residence, 'field_city'),
            'field_state' => $this->getParagraphFieldValue($co_field_residence, 'field_state'),
            'field_zip_code' => $this->getParagraphFieldValue($co_field_residence, 'field_zip_code'),
            'field_rent' => $this->getParagraphFieldValue($co_field_residence, 'field_rent'),
            'field_landlord_name' => $this->getParagraphFieldValue($co_field_residence, 'field_landlord_name'),
            'field_reason_for_leaving' => $this->getParagraphFieldValue($co_field_residence, 'field_reason_for_leaving'),
        ]);
        $co_residence->save();
        $co_residence_id = $co_residence->id();

        //Co-Applicant - Occupation
        $co_field_occupation = $this->getParagraph($this->co_applicant, 'field_occupation');
        $co_occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $this->getParagraphFieldValue($co_field_occupation, 'field_occupation_type'),
            'field_employer_name' => $this->getParagraphFieldValue($co_field_occupation, 'field_employer_name'),
            'field_job_title' => $this->getParagraphFieldValue($co_field_occupation, 'field_job_title'),
            'field_monthly_income' => $this->getParagraphFieldValue($co_field_occupation, 'field_monthly_income'),
            'field_work_address' => $this->getParagraphFieldValue($co_field_occupation, 'field_work_address'),
            'field_city' => $this->getParagraphFieldValue($co_field_occupation, 'field_city'),
            'field_zip_code' => $this->getParagraphFieldValue($co_field_occupation, 'field_zip_code'),
            'field_work_type' => $this->getParagraphFieldValue($co_field_occupation, 'field_work_type'),
            'field_start_date' => $this->getParagraphFieldValue($co_field_occupation, 'field_start_date'),
            'field_supervisor_name' => $this->getParagraphFieldValue($co_field_occupation, 'field_supervisor_name'),
            'field_mobile' => $this->getParagraphFieldValue($co_field_occupation, 'field_mobile'),
        ]);
        $co_occupation->save();
        $co_occupation_id = $co_occupation->id();

        //Co-Applicant - Previous Occupation
        $co_field_previous_occupation = $this->getParagraph($this->co_applicant, 'field_previous_occupation');
        $co_previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_employer_name'),
            'field_job_title' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_job_title'),
            'field_monthly_income' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_monthly_income'),
            'field_work_address' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_work_address'),
            'field_city' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_city'),
            'field_zip_code' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_zip_code'),
            'field_work_type' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_work_type'),
            'field_start_date' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_start_date'),
            'field_supervisor_name' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_supervisor_name'),
            'field_mobile' => $this->getParagraphFieldValue($co_field_previous_occupation, 'field_mobile'),
        ]);
        $co_previous_occupation->save();
        $co_previous_occupation_id = $co_previous_occupation->id();

        //Co-Applicant - Financial
        $co_field_financial = $this->getParagraph($this->co_applicant, 'field_financial');
        $co_financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $this->getParagraphFieldValue($co_field_financial, 'field_bank_name'),
            'field_bank_address' => $this->getParagraphFieldValue($co_field_financial, 'field_bank_address'),
            'field_account_type' => $this->getParagraphFieldValue($co_field_financial, 'field_account_type'),
            'field_landlord_name' => $this->getParagraphFieldValue($co_field_financial, 'field_landlord_name'),
        ]);
        $co_financial->save();
        $co_financial_id = $co_financial->id();

        //Co-Applicant - Reference
        $co_field_reference = $this->getParagraph($this->co_applicant, 'field_reference');
        $co_reference = Paragraph::create([
            'type' => 'reference',
            'field_full_name' => $this->getParagraphFieldValue($co_field_reference, 'field_full_name'),
            'field_relationship' => $this->getParagraphFieldValue($co_field_reference, 'field_relationship'),
            'field_address' => $this->getParagraphFieldValue($co_field_reference, 'field_address'),
            'field_mobile' => $this->getParagraphFieldValue($co_field_reference, 'field_mobile'),
            'field_landlord_name' => $this->getParagraphFieldValue($co_field_reference, 'field_landlord_name'),
        ]);
        $co_reference->save();
        $co_reference_id = $co_reference->id();

        //Co-Applicant - Emergency Contact
        $co_field_emergency_contact = $this->getParagraph($this->co_applicant, 'field_emergency_contact');
        $co_emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $this->getParagraphFieldValue($co_field_emergency_contact, 'field_full_name'),
            'field_relationship' => $this->getParagraphFieldValue($co_field_emergency_contact, 'field_relationship'),
            'field_address' => $this->getParagraphFieldValue($co_field_emergency_contact, 'field_address'),
            'field_mobile' => $this->getParagraphFieldValue($co_field_emergency_contact, 'field_mobile'),
        ]);
        $co_emergency_contact->save();
        $co_emergency_contact_id = $co_emergency_contact->id();


        $co_applicant_name = 'No';

        if ($this->co_applicant_exist) {
            if (!empty($this->getParagraphFieldValue($co_field_about_me, 'field_first_name'))) {
                $co_applicant_name = $this->getParagraphFieldValue($co_field_about_me, 'field_first_name') . ' ' . $this->getParagraphFieldValue($co_field_about_me, 'field_last_name');
            }
        }

        $application = Node::create([
            'type' => 'application',
            'title' => $this->property->getTitle(),
            'field_property' => $this->property_id,
            'field_access_key' => $this->access_key,
            'field_co_applicant_email' => $this->co_applicant_email,
            'field_co_applicant_name' => $co_applicant_name,
            'field_application_completed' => true,

            //Applicant Data
            'field_about_me' => array(
                array(
                    'target_id' => $about_me_id,
                    'target_revision_id' => $about_me->getRevisionId(),
                )
            ),

            'field_residence' => array(
                array(
                    'target_id' => $residence_id,
                    'target_revision_id' => $residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $occupation_id,
                    'target_revision_id' => $occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $previous_occupation_id,
                    'target_revision_id' => $previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $financial_id,
                    'target_revision_id' => $financial->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $emergency_contact_id,
                    'target_revision_id' => $emergency_contact->getRevisionId(),
                )
            ),

            'field_rental_document' => $rental_doc_fids,

            'field_questioneer' => array(
                array(
                    'target_id' => $questioneer_id,
                    'target_revision_id' => $questioneer->getRevisionId(),
                )
            ),
            'field_reference' => array(
                array(
                    'target_id' => $reference_id,
                    'target_revision_id' => $reference->getRevisionId(),
                )
            ),

            //Co-Applicant Data
            'field_co_applicant_type' => $co_applicant_type,
            'field_about_co_applicant' => array(
                array(
                    'target_id' => $co_about_me_id,
                    'target_revision_id' => $co_about_me->getRevisionId(),
                )
            ),

            'field_co_applicant_residence' => array(
                array(
                    'target_id' => $co_residence_id,
                    'target_revision_id' => $co_residence->getRevisionId(),
                )
            ),

            'field_co_applicant_occupation' => array(
                array(
                    'target_id' => $co_occupation_id,
                    'target_revision_id' => $co_occupation->getRevisionId(),
                )
            ),

            'field_coapp_previous_occupation' => array(
                array(
                    'target_id' => $co_previous_occupation_id,
                    'target_revision_id' => $co_previous_occupation->getRevisionId(),
                )
            ),

            'field_co_applicant_financial' => array(
                array(
                    'target_id' => $co_financial_id,
                    'target_revision_id' => $co_financial->getRevisionId(),
                )
            ),

            'field_co_applicant_reference' => array(
                array(
                    'target_id' => $co_reference_id,
                    'target_revision_id' => $co_reference->getRevisionId(),
                )
            ),

            'field_coapp_emergency_contact' => array(
                array(
                    'target_id' => $co_emergency_contact_id,
                    'target_revision_id' => $co_emergency_contact->getRevisionId(),
                )
            ),
        ]);

        $application->save();

        //Create notification
        /*
    $notification = Node::create([
        'type' => 'notification',
        'title' => $this->getParagraphFieldValue($field_about_me, 'field_first_name') . ' ' . $this->getParagraphFieldValue($field_about_me, 'field_last_name') . ' applied on property ' . $this->property->getTitle(),
        'field_from' => $this->user->id(),
        'field_to' => $this->property->getOwnerId(),
        'field_property' => $this->property_id
    ]);
    $notification->save();
    */
        // Sending email to co-applicant and property owner
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $applicant_name = $this->getParagraphFieldValue($field_about_me, 'field_first_name') . ' ' . $this->getParagraphFieldValue($field_about_me, 'field_last_name');
        $co_applicant_name = $this->getParagraphFieldValue($co_field_about_me, 'field_first_name') . ' ' . $this->getParagraphFieldValue($co_field_about_me, 'field_last_name');

        // Send email to Co-Applicant
        $co_applicant_data['message']['sign_up_required'] = true;
        $co_applicant_data['message']['co_applicant_update_form'] = false;

        if ($this->co_applicant_exist) {
            $co_applicant_data['message']['sign_up_required'] = false;
        }

        if ($this->co_applicant_update_form) {
            $co_applicant_data['message']['co_applicant_update_form'] = true;
        }

        // Create Notification Node
        $notification_agent = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
            'title' => 'You have an application',
            'description' =>  $applicant_name . ' has applied to ' . $this->property->getTitle(),
            'target_link' => 'internal:/dashboard/applications-on-property/' . $this->property->id(),
            'uid' => $this->property->getOwnerId(), // The user that should recieve this notification.
        ]);
        $notification_agent->save();

        if (!empty($this->co_applicant_email)) {
            $co_applicant_data['message']['application_id'] = $application->id();
            $co_applicant_data['message']['applicant_name'] = $applicant_name;
            $co_applicant_data['message']['applicant_email'] = $this->getParagraphFieldValue($field_about_me, 'field_email');
            $co_applicant_data['message']['co_applicant_name'] = $co_applicant_name;
            $co_applicant_data['message']['co_applicant_email'] = $this->co_applicant_email;
            $co_applicant_data['message']['access_key'] = $this->access_key;
            $co_applicant_data['message']['property_id'] = $this->property_id;
            $co_applicant_data['message']['property_name'] = $this->property->getTitle();
            $co_applicant_data['message']['property_zip'] = $this->property->field_property_address->getValue()[0]['postal_code'];
            $co_applicant_data['message']['property_state'] = $this->property->field_property_address->getValue()[0]['administrative_area'];
            $co_applicant_data['message']['property_city'] = $this->property->field_property_address->getValue()[0]['locality'];
            $co_applicant_data['message']['property_address_line1'] = $this->property->field_property_address->getValue()[0]['address_line1'];
            $co_applicant_data['message']['from_email'] = "noreply@rentstarz.com";
            $co_applicant_data['message']['subject'] = t('You have invitation from ' . $applicant_name);
            $co_applicant_data['message']['theme'] = 'co-applicant-email';
            $email_to_co_applicant = $this->mail_manager->mail('application', 'email_to_co_applicant', $this->co_applicant_email, $langcode, $co_applicant_data, NULL, true);

            if ($email_to_co_applicant['result'] !== true) {
                drupal_set_message(t('There was a problem sending email to Co-Applicant and it was not sent.'), 'error');
            } else {
                drupal_set_message(t('Email has been sent to Co-Applicant with the Registration link.'));
            }
        }

        // Send email to Property Owner
        $property_owner_data['message']['applicant_name'] = $applicant_name;
        $property_owner_data['message']['applicant_email'] = $this->getParagraphFieldValue($field_about_me, 'field_email');
        $property_owner_data['message']['co_applicant_name'] = $co_applicant_name;
        //$property_owner_data['message']['co_applicant_email'] = $this->co_applicant_email;
        $property_owner_data['message']['access_key'] = $this->access_key;
        $property_owner_data['message']['property_id'] = $this->property_id;
        $property_owner_data['message']['property_name'] = $this->property->getTitle();
        $property_owner_data['message']['property_zip'] = $this->property->field_property_address->getValue()[0]['postal_code'];
        $property_owner_data['message']['property_state'] = $this->property->field_property_address->getValue()[0]['administrative_area'];
        $property_owner_data['message']['property_city'] = $this->property->field_property_address->getValue()[0]['locality'];
        $property_owner_data['message']['property_address_line1'] = $this->property->field_property_address->getValue()[0]['address_line1'];
        $property_owner_data['message']['from_email'] = "noreply@rentstarz.com";
        $property_owner_data['message']['subject'] = t($applicant_name . ' applied on your property ' . $this->property->getTitle());
        $property_owner_data['message']['theme'] = 'property-owner-email';
        $email_to_property_owner = $this->mail_manager->mail('application', 'email_to_property_owner', $this->property->getOwner()->getEmail(), $langcode, $property_owner_data, NULL, true);

        if ($email_to_property_owner['result'] !== true) {
            drupal_set_message(t('There was a problem sending email to property owner and it was not sent.'), 'error');
        }

        $response = new RedirectResponse('/property-listing');
        drupal_set_message($this->t('Your application has been submitted for the property "' . $this->property->getTitle() . '"'), 'status', TRUE);
        return $response->send();
    }


    /**
     * Getter method for paragraph
     */
    public function getParagraph($entity, $field_name)
    {
        return Paragraph::load($entity->get($field_name)->getValue()[0]['target_id']);
    }

    public function getParagraphFieldValue($paragraph, $name)
    {
        return $paragraph->$name->value;
    }
}
