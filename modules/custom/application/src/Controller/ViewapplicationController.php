<?php
namespace Drupal\application\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides route responses for the Example module.
 */
class ViewapplicationController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function viewApplicationPage() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $current_user = \Drupal::currentUser();
    $user = User::load($current_user->id());

    //Check the user is authenticated
    if($current_user->isAuthenticated() === false){
      $response = new RedirectResponse('/user/login');
      $response->send();
      exit;
    }

    $applicant_id = $user->field_applicant_id->getValue()[0]['value'];
    $co_applicant_id = $user->field_co_applicant_id->getValue()[0]['value'];

    //Check the user does have application exist
    if(!isset($applicant_id) && !isset($co_applicant_id)){
      $response = new RedirectResponse('/property-listing');
      $response->send();
      drupal_set_message($this->t('You haven\'t applied to any property yet.'), 'warning', TRUE);
      exit;
    }

    //Get applicant and co-applicant node
    $applicant = Node::load($applicant_id);
    $co_applicant = Node::load($co_applicant_id);

    /**
     * Applicant Information
     */
    //Applicant - About Me
    $applicant_about = $this->getParagraph($applicant, 'field_about_me');
    
    $application['profile_photo'] = NULL;
    if(!empty($user->user_picture->entity)){
      $application['profile_photo'] = file_create_url($user->user_picture->entity->getFileUri());
    }else{
      $application['profile_photo'] = '/sites/default/files/default_images/user123.png';
    }

    //$application['profile_photo'] = file_create_url($user->user_picture->entity->getFileUri());
    $application['first_name'] = $this->getParagraphFieldValue($applicant_about, 'field_first_name');
    $application['last_name'] = $this->getParagraphFieldValue($applicant_about, 'field_last_name');
    $application['full_name'] = $application['first_name'].' '.$application['last_name'];
    $application['date_of_birth'] = $this->getParagraphFieldValue($applicant_about, 'field_birth_date');
    $application['phone_number'] = $this->getParagraphFieldValue($applicant_about, 'field_mobile');
    $application['email'] = $this->getParagraphFieldValue($applicant_about, 'field_email');
    $application['field_driving_license'] = $this->getParagraphFieldValue($applicant_about, 'field_driving_license');
    

    //Applicant - Residence
    $applicant_residence = $this->getParagraph($applicant, 'field_residence');

    $application['residence_type'] = $this->getParagraphFieldValue($applicant_residence, 'field_residence_type');
    $application['residence_address'] = $this->getParagraphFieldValue($applicant_residence, 'field_address');
    $application['residence_mov_in_date'] = $this->getParagraphFieldValue($applicant_residence, 'field_mov_in_date');
    $application['residence_city'] = $this->getParagraphFieldValue($applicant_residence, 'field_city');
    $application['residence_state'] = $this->getParagraphFieldValue($applicant_residence, 'field_state');
    $application['residence_zip_code'] = $this->getParagraphFieldValue($applicant_residence, 'field_zip_code');
    $application['residence_rent'] = $this->getParagraphFieldValue($applicant_residence, 'field_rent');
    $application['residence_landlord_name'] = $this->getParagraphFieldValue($applicant_residence, 'field_landlord_name');
    $application['residence_reason_for_leaving'] = $this->getParagraphFieldValue($applicant_residence, 'field_reason_for_leaving');

    //Applicant - Occupation
    $applicant_occupation = $this->getParagraph($applicant, 'field_occupation');

    $application['occupation_type'] = $this->getParagraphFieldValue($applicant_occupation, 'field_occupation_type');
    $application['occupation_employer_name'] = $this->getParagraphFieldValue($applicant_occupation, 'field_employer_name');
    $application['occupation_job_title'] = $this->getParagraphFieldValue($applicant_occupation, 'field_job_title');
    $application['occupation_monthly_income'] = $this->getParagraphFieldValue($applicant_occupation, 'field_monthly_income');
    $application['occupation_work_address'] = $this->getParagraphFieldValue($applicant_occupation, 'field_work_address');
    $application['occupation_city'] = $this->getParagraphFieldValue($applicant_occupation, 'field_city');
    $application['occupation_zip_code'] = $this->getParagraphFieldValue($applicant_occupation, 'field_zip_code');
    $application['occupation_work_type'] = $this->getParagraphFieldValue($applicant_occupation, 'field_work_type');
    $application['occupation_start_date'] = $this->getParagraphFieldValue($applicant_occupation, 'field_start_date');
    $application['occupation_supervisor_name'] = $this->getParagraphFieldValue($applicant_occupation, 'field_supervisor_name');
    $application['occupation_phone_number'] = $this->getParagraphFieldValue($applicant_occupation, 'field_mobile');

    //Applicant - Previous Occupation
    $applicant_prev_occupation = $this->getParagraph($applicant, 'field_previous_occupation');

    $application['prev_occupation_employer_name'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_employer_name');
    $application['prev_occupation_job_title'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_job_title');
    $application['prev_occupation_monthly_income'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_monthly_income');
    $application['prev_occupation_work_address'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_work_address');
    $application['prev_occupation_city'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_city');
    $application['prev_occupation_zip_code'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_zip_code');
    $application['prev_occupation_work_type'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_work_type');
    $application['prev_occupation_start_date'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_start_date');
    $application['prev_occupation_supervisor_name'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_supervisor_name');
    $application['prev_occupation_phone_number'] = $this->getParagraphFieldValue($applicant_prev_occupation, 'field_mobile');

    //Applicant - Financial
    $applicant_financial = $this->getParagraph($applicant, 'field_financial');

    $application['financial_bank_name'] = $this->getParagraphFieldValue($applicant_financial, 'field_bank_name');
    $application['financial_bank_address'] = $this->getParagraphFieldValue($applicant_financial, 'field_bank_address');
    $application['financial_account_type'] = $this->getParagraphFieldValue($applicant_financial, 'field_account_type');
    $application['financial_landlord_name'] = $this->getParagraphFieldValue($applicant_financial, 'field_landlord_name');

    //Applicant - Reference
    $applicant_reference = $this->getParagraph($applicant, 'field_reference');

    $application['reference_full_name'] = $this->getParagraphFieldValue($applicant_reference, 'field_full_name');
    $application['reference_relationship'] = $this->getParagraphFieldValue($applicant_reference, 'field_relationship');
    $application['reference_address'] = $this->getParagraphFieldValue($applicant_reference, 'field_address');
    $application['reference_mobile'] = $this->getParagraphFieldValue($applicant_reference, 'field_mobile');
    $application['reference_landlord_name'] = $this->getParagraphFieldValue($applicant_reference, 'field_landlord_name');

    //Applicant - Emergency Contact
    $applicant_contact = $this->getParagraph($applicant, 'field_emergency_contact');

    $application['contact_full_name'] = $this->getParagraphFieldValue($applicant_contact, 'field_full_name');
    $application['contact_relationship'] = $this->getParagraphFieldValue($applicant_contact, 'field_relationship');
    $application['contact_address'] = $this->getParagraphFieldValue($applicant_contact, 'field_address');
    $application['contact_mobile'] = $this->getParagraphFieldValue($applicant_contact, 'field_mobile');

    //Applicant - Rental Documents
    $application['rental_documents'] = $this->get_files($applicant->field_rental_document);

    //Applicant - Questioneer
    $applicant_questioneer = $this->getParagraph($applicant, 'field_questioneer');
    $application['applicant_questioneer'] = array();
    $questioneer_fields = array('field_do_you', 'field_illegal_drugs', 'field_evicted', 'field_babankruptcy', 'field_bedbugs', 'field_lequid_filled_furniture', 'field_pets');
    
    for($i = 0; $i < count($questioneer_fields); $i++){
      $questioneer = array(
        "label" => $applicant_questioneer->get($questioneer_fields[$i])->getFieldDefinition()->getLabel(),
        "value" => $this->getParagraphFieldValue($applicant_questioneer, $questioneer_fields[$i])
      );

      array_push($application['applicant_questioneer'], $questioneer);
    }
    
    
    
    /**
     * Co-Applicant Information
     */
    //Applicant - About Me
    $co_applicant_about = $this->getParagraph($co_applicant, 'field_about_co_applicant');
    
    $application['co']['first_name'] = $this->getParagraphFieldValue($co_applicant_about, 'field_first_name');
    $application['co']['last_name'] = $this->getParagraphFieldValue($co_applicant_about, 'field_last_name');
    $application['co']['full_name'] = $application['co']['first_name'].' '.$application['co']['last_name'];
    $application['co']['date_of_birth'] = $this->getParagraphFieldValue($co_applicant_about, 'field_birth_date');
    $application['co']['phone_number'] = $this->getParagraphFieldValue($co_applicant_about, 'field_mobile');
    $application['co']['email'] = $this->getParagraphFieldValue($co_applicant_about, 'field_email');
    $application['co']['field_driving_license'] = $this->getParagraphFieldValue($co_applicant_about, 'field_driving_license');

    //Applicant - Residence
    $co_applicant_residence = $this->getParagraph($co_applicant, 'field_residence');

    $application['co']['residence_type'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_residence_type');
    $application['co']['residence_address'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_address');
    $application['co']['residence_mov_in_date'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_mov_in_date');
    $application['co']['residence_city'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_city');
    $application['co']['residence_state'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_state');
    $application['co']['residence_zip_code'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_zip_code');
    $application['co']['residence_rent'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_rent');
    $application['co']['residence_landlord_name'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_landlord_name');
    $application['co']['residence_reason_for_leaving'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_reason_for_leaving');

    //Applicant - Occupation
    $co_applicant_occupation = $this->getParagraph($co_applicant, 'field_occupation');

    $application['co']['occupation_type'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_occupation_type');
    $application['co']['occupation_employer_name'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_employer_name');
    $application['co']['occupation_job_title'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_job_title');
    $application['co']['occupation_monthly_income'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_monthly_income');
    $application['co']['occupation_work_address'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_work_address');
    $application['co']['occupation_city'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_city');
    $application['co']['occupation_zip_code'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_zip_code');
    $application['co']['occupation_work_type'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_work_type');
    $application['co']['occupation_start_date'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_start_date');
    $application['co']['occupation_supervisor_name'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_supervisor_name');
    $application['co']['occupation_phone_number'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_mobile');

    //Applicant - Previous Occupation
    $co_applicant_prev_occupation = $this->getParagraph($co_applicant, 'field_previous_occupation');

    $application['co']['prev_occupation_employer_name'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_employer_name');
    $application['co']['prev_occupation_job_title'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_job_title');
    $application['co']['prev_occupation_monthly_income'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_monthly_income');
    $application['co']['prev_occupation_work_address'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_work_address');
    $application['co']['prev_occupation_city'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_city');
    $application['co']['prev_occupation_zip_code'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_zip_code');
    $application['co']['prev_occupation_work_type'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_work_type');
    $application['co']['prev_occupation_start_date'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_start_date');
    $application['co']['prev_occupation_supervisor_name'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_supervisor_name');
    $application['co']['prev_occupation_phone_number'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_mobile');

    //Applicant - Financial
    $co_applicant_financial = $this->getParagraph($co_applicant, 'field_financial');

    $application['co']['financial_bank_name'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_bank_name');
    $application['co']['financial_bank_address'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_bank_address');
    $application['co']['financial_account_type'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_account_type');
    $application['co']['financial_landlord_name'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_landlord_name');

    //Applicant - Emergency Contact
    $co_applicant_contact = $this->getParagraph($co_applicant, 'field_emergency_contact');

    $application['co']['contact_full_name'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_full_name');
    $application['co']['contact_relationship'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_relationship');
    $application['co']['contact_address'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_address');
    $application['co']['contact_mobile'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_mobile');

    
    //kint($application['first_name']);
    //exit;

    return [
      '#theme' => 'view_application',
      '#application' => $application,
    ];

    /*
    $nid = $applicationid;
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node = $node_storage->load($nid);
    $application['first_name'] = $node->get('field_first_name')->value; 
    $application['last_name'] = $node->get('field_last_name')->value; 
    $application['email'] = $node->get('field_email')->value; 
    $application['drivers_licence'] = $node->get('field_drivers_licence')->value; 
    $application['birth_date'] = $node->get('field_birth_date')->value; 
    $application['cell_phone'] = $node->get('field_phone')->value; 

    $family = $node->field_family_members->getValue();
    // Loop through the result set.
    foreach ( $family as $member ) {
      
      $mem_ar = Paragraph::load($member['target_id']);
      $target_id = $member['target_id'];
      $mem_ar_info[$target_id]['age'] = $mem_ar->get('field_age')->value;
      $mem_ar_info[$target_id]['name'] = $mem_ar->get('field_full_name')->value;
      $mem_ar_info[$target_id]['relation'] = $mem_ar->get('field_relationship')->value;
    }
    */

  }

  /**
   * Getter method for paragraph
   */
  public function getParagraph($node, $name)
  {
      return Paragraph::load($node->get($name)->getValue()[0]['target_id']);
  }

  public function getParagraphFieldValue($paragraph, $name)
  {
      return $paragraph->$name->value;
  }

  function get_files($files){
    $img_urls = array();

    foreach ($files as $item) {
        if ($item->entity) {
            $img_urls[] = $item->entity->url();
        }
    }

    return $img_urls;
  }

}