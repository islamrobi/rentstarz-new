<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\user\Kernel;
use \Drupal\node\Entity\Node;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\Component\Utility\Random;
use Drupal\user\Entity\User;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class AdditionalInfoForm extends FormBase
{
    public $user;
    public $application;
    public $pay_stubs;
    public $tax_return;
    public $bank_statement;
    public $property;
    public $mail_manager;


    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
       return 'additional_info_form';
    }


    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $application_id = NULL)
    {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        $this->pay_stubs = false;
        $this->tax_return = false;
        $this->bank_statement = false;

        $this->user = User::load(\Drupal::currentUser()->id());

        $this->mail_manager = \Drupal::service('plugin.manager.mail');

        //Check the user is authenticated
        if($this->user->isAuthenticated() === false){
            $this->errorHandler('warning', true, 'Login required', '/user/login?destination=/modal/application/update/additional_info/'.$application_id);
        }

        //Load Target Application
        if(isset($application_id) && is_numeric($application_id)){
            $this->application =  Node::load($application_id);
        }else{
            $this->errorHandler('warning', true, 'Application doesn\'t exist or Ivalid', '/modal/my-applications');
        }

        if(is_object($this->application)){
            $this->property = Node::load($this->application->field_property->getValue()[0]['target_id']);

            if($this->application->field_required_pay_stubs->getValue()[0]['value'] == TRUE){
                $this->pay_stubs = true;
            }

            if($this->application->field_required_tax_return->getValue()[0]['value'] == TRUE){
                $this->tax_return = true;
            }

            if($this->application->field_required_bank_statement->getValue()[0]['value'] == TRUE){
                $this->bank_statement = true;
            }

            if($this->application->getOwnerId() == $this->user->id()){

                if($this->pay_stubs){
                    $field_pay_stubs = '';
                    if(isset($this->application->field_pay_stubs->getValue()['0']['target_id'])){
                        $field_pay_stubs = $this->application->field_pay_stubs->getValue()['0']['target_id'];
                    }
                    $form['field_pay_stubs'] = array(
                        '#type' => 'managed_file',
                        '#title' => $this->t('<p class="text-center">Upload Document of Pay Stubs</p>'),
                        '#upload_location' => 'private://additional-info/pay-stubs/'.date('Y').'-'. date('m').'/',
                        '#upload_validators' => array(
                            'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
                        ),
                        '#default_value' => array($field_pay_stubs),
                        '#required' => true,
                        '#multiple' => TRUE,
                        '#extended' => TRUE,
                    );
                }

                if($this->tax_return){
                    $field_tax_return = '';
                    if(isset($this->application->field_tax_return->getValue()['0']['target_id'])){
                        $field_tax_return = $this->application->field_tax_return->getValue()['0']['target_id'];
                    }
                    $form['field_tax_return'] = array(
                        '#type' => 'managed_file',
                        '#title' => $this->t('<p class="text-center">Upload Document of Tax Return</p>'),
                        '#upload_location' => 'private://additional-info/tax-return/'.date('Y').'-'. date('m').'/',
                        '#upload_validators' => array(
                            'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
                        ),
                        '#default_value' => array($field_tax_return),
                        '#required' => true,
                        '#multiple' => TRUE,
                        '#extended' => TRUE,
                    );
                }

                if($this->bank_statement){
                    $field_bank_statement = '';
                    if(isset($this->application->field_bank_statement->getValue()['0']['target_id'])){
                        $field_bank_statement = $this->application->field_bank_statement->getValue()['0']['target_id'];
                    }
                    $form['field_bank_statement'] = array(
                        '#type' => 'managed_file',
                        '#title' => $this->t('<p class="text-center">Upload Document of Bank Statement</p>'),
                        '#upload_location' => 'private://additional-info/bank-statement/'.date('Y').'-'. date('m').'/',
                        '#upload_validators' => array(
                            'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
                        ),
                        '#default_value' => array($field_bank_statement),
                        '#required' => true,
                        '#multiple' => TRUE,
                        '#extended' => TRUE,
                    );
                }

                $form['field_current_landlord'] = [
                    '#type' => 'textarea',
                    '#title' => $this->t('Current Landlord'),
                    '#default_value' => $this->application->field_current_landlord->getValue()[0]['value']
                ];

                // Submit Button
                $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Send Documents')];
                $form['data']['application_title'] = $this->application->getTitle();
                $form['data']['message_from_agent'] = $this->application->field_message->getValue()[0]['value'];
                $form['#theme'] = 'submit_additional_info_form';
                return $form;
            }else{
                $this->errorHandler('warning', true, 'You are not the owner of this application', '/modal/my-applications');
            }
        }else{
            $this->errorHandler('warning', true, 'Application doesn\'t exist or Ivalid', '/modal/my-applications');
        }

    }


    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function validateForm(array & $form, FormStateInterface $form_state) {
        //Check the agent is eligible to request for additional information
        if($this->application->field_status->getValue()[0]['value'] != 'requestinginfo'){
            $this->errorHandler('warning', true, 'Update Failed! Agent/Landloard doesn\'t request for the additional documents', '/modal/my-applications');
        }
    }


    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
  	public function submitForm(array & $form, FormStateInterface $form_state) {
        if($this->pay_stubs){
            $this->application->set('field_pay_stubs', $form_state->getValue('field_pay_stubs')['fids']);
        }

        if($this->tax_return){
            $this->application->set('field_tax_return', $form_state->getValue('field_tax_return')['fids']);
        }

        if($this->bank_statement){
            $this->application->set('field_bank_statement', $form_state->getValue('field_bank_statement')['fids']);
        }

        $this->application->set('field_current_landlord', $form_state->getValue('field_current_landlord'));
        $this->application->set('field_status', 'pending');

        $this->application->save();

        $applicant_name = $this->user->field_first_name->getValue()[0]['value'] . ' ' . $this->user->field_last_name->getValue()[0]['value'];

        // Create Notification Node
        $notification_agent = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
            'title' => 'Additional information available',
            'description' =>  $applicant_name .' has completed the Additional Information Request, please click here to view.',
            'target_link' => 'internal:/dashboard/applications-on-property/' . $this->property->id(),
            'uid' => $this->property->getOwnerId(), // The user that should recieve this notification.
        ]);
        $notification_agent->save();

        //Send email to property owner to inform the additional document has been submitted
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $applicant_data['message']['applicant_name'] = $applicant_name;
        $applicant_data['message']['application_id'] = $this->application->id();
        $applicant_data['message']['application_name'] = $this->application->title->value;
        $applicant_data['message']['property_name'] = $this->property->title->value;
        $applicant_data['message']['from_email'] = "noreply@rentstarz.com";
        $applicant_data['message']['subject'] = t($applicant_name.' has submitted additional documents for the application '. $this->application->title->value);
        $applicant_data['message']['theme'] = 'submit-additional-info-email';
        $submit_additional_info_email = $this->mail_manager->mail('application', 'submit_additional_info_email', $this->property->getOwner()->getEmail(), $langcode, $applicant_data, NULL, true);

        if ($submit_additional_info_email['result'] !== true) {
            drupal_set_message(t('There was a problem sending email to property owner and it was not sent.'), 'error');
        }

        $response = new RedirectResponse('/modal/my-applications');
        $response->send();
        drupal_set_message($this->t('Your documents has been submitted successfully'), 'status', TRUE);
    }



    public function errorHandler($error_type = 'status', $exit = true, $error_message = NULL, $url = ''){
        $response = new RedirectResponse($url);
        $response->send();

        if(isset($error_message)){
            drupal_set_message($this->t($error_message), $error_type, TRUE);
        }

        if($exit){
            exit;
        }
    }
}
