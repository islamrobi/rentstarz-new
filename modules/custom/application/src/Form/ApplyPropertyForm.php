<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\user\Kernel;
use \Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\Component\Utility\Random;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ApplyPropertyForm extends FormBase
{
    public $user;
    public $access_key;
    public $property_id;
    public $property;
    public $applicant_name;
    public $co_applicant_name;
    public $applicant_email;
    public $co_applicant_email;
    public $co_applicant_signup_required;
    public $application_status;
    public $mail_manager;

    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $propertyid = NULL)
    {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        // Common Variables
        $this->property_id = $propertyid;
        $this->property = Node::load($this->property_id);
        $this->user = User::load(\Drupal::currentUser()->id());
        $this->access_key = md5(time()+$this->property_id);
        $this->mail_manager = \Drupal::service('plugin.manager.mail');

        // Redirect users to the login page if not authenticated
        if(!$this->user->isAuthenticated()){
            $response = new RedirectResponse('/user/login');
            return $response->send();
        }

        // Redirect user to the direct application porcess if Application is exist
        $application_data_exist = false || $this->user->field_application_data_exist->getValue()[0]['value'];
        if($application_data_exist){
            $response = new RedirectResponse('/application/apply/direct/' . $this->property_id);
            return $response->send();
        }

        //User Photo
        if(!$this->user->user_picture->isEmpty()){
            $default_user_picture = $this->user->user_picture->entity->id();
            $user_picture_title = 'Update profile picture';
        }else{
            $default_user_picture = '';
            $user_picture_title = 'Upload profile picture';
        }

        // Build Form
        // About me - Form Section
        $form['new_photo'] = [
            '#type' => 'fieldset',
            '#title' => $this->t($user_picture_title),
        ];

        $form['new_photo']['image_file'] = [
            '#title' => t($user_picture_title),
            '#title_display' => 'invisible',
            '#type' => 'managed_file',
            '#theme' => 'image_widget',
            '#preview_image_style' => 'medium',
            '#upload_location' => 'public://pictures/'.date('Y-m'),
            '#multiple' => FALSE,
            '#description' => t('Allowed extensions: <em>gif, png, jpg, jpeg</em>'),
            '#upload_validators' => [
              'file_validate_is_image' => [],
              'file_validate_extensions' => ['gif png jpg jpeg'],
              'file_validate_size' => [10485760],
            ],
            '#default_value' => array($default_user_picture),
        ];

        $form['f_name'] = array(
            '#type' => 'textfield',
            '#title' => t('First Name:'),
            '#required' => true,
            '#default_value' => $this->user->field_first_name->getvalue()[0]['value'],
            '#disabled' => true,
        );

        $form['l_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Last Name:'),
            '#required' => false,
            '#default_value' => $this->user->field_last_name->getvalue()[0]['value'],
            '#disabled' => true,
        );

        $form['birth_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Birth date'),
            '#description' => $this->t(''),
        );

        $form['cell_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Cell phone'),
            '#description' => $this->t(''),
        );

        $form['email'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Email'),
            '#size' => 60,
            '#maxlength' => USERNAME_MAX_LENGTH,
            '#description' => $this->t(''),
            '#required' => true,
            '#default_value' => $this->user->getEmail(),
            '#disabled' => TRUE,
        );

        $form['driver_license'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Driver license'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        // Residence - Form Section
        $form['residence_type'] = array(
            '#type' => 'radios',
            '#options' => array(
                'own' => $this->t('Own'),
                'rent' => $this->t('Rent')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['movin_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Movin date'),
            '#description' => $this->t('.'),
        );

        $form['city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['state'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('State'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['zip_code'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip Code'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['rent'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Rent'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landlord Name'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['reason_for_leaving'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Reason for leaving'),
        ];

        // Occupation
        $form['occupation'] = array(
            '#type' => 'radios',
            '#options' => array(
                'employed' => $this->t('Employed'),
                'self-employed' => $this->t('Self-employed'),
                'student' => $this->t('Student'),
                'unemployed' => $this->t('Unemployed'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
        );

        $form['work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        // Previous Occupation
        $form['previous_employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['previous_work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
        );

        $form['previous_work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        $form['previous_work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
        );

        // Financial
        $form['financial_bank_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Name'),
        );

        $form['financial_bank_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Address'),
        );

        $form['financial_account_type'] = array(
            '#type' => 'select',
            '#title' => 'Account Type',
            '#options' => array(
                'savings' => $this->t('Savings'),
                'current' => $this->t('Current'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        //Reference
        $form['field_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
            '#default_value' => ''
        );

        $form['field_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
            '#default_value' => ''
        );

        $form['field_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#default_value' => ''
        );

        $form['field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#default_value' => ''
        );

        // Emergency Contact
        $form['contact_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
        );

        $form['contact_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
        );

        $form['contact_address'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Adresss'),
        );

        $form['contact_phone_number'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
        );

        //Rental Document
        /*
        $form['field_rental_document'] = array(
            '#type' => 'managed_file',
            '#title' => $this->t('<p class="text-center">Upload your rental documents securely</p>'),
            '#upload_location' => 'private://rental-docs',
            '#progress_message' => $this->t('Please wait...'),
            //'#extended' => TRUE,
            '#size' => 13,
            '#multiple' => TRUE,
            '#upload_validators' => array(
                'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
            )
        );
        */

        $form['field_rental_document'] = array(
            '#type' => 'managed_file',
            '#title' => $this->t('<p class="text-center">Upload your rental documents securely</p>'),
            '#upload_location' => 'private://rental-docs',
            '#extended' => TRUE,
            '#upload_validators' => array(
                'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
            ),
            //'#default_value' => array($field_pay_stubs),
            '#multiple' => TRUE,
        );

        /**
         * Questioneer
         */
        $form['field_do_you'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_illegal_drugs'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_evicted'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_babankruptcy'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_bedbugs'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_lequid_filled_furniture'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['field_pets'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        /**
         * Co-Application
         */
        // About Co-Applicant
        $form['co_applicant_type'] = array(
            '#type' => 'radios',
            '#options' => array(
                'no' => $this->t('No'),
                'co-applicant' => $this->t('Co-Applicant'),
                'guarantor' => $this->t('Guarantor')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#required' => false,
            '#default_value' => 'no'
        );

        $form['co_f_name'] = array(
            '#type' => 'textfield',
            '#title' => t('First Name:'),
            '#required' => false
        );

        $form['co_l_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Last Name:'),
            '#required' => false
        );

        $form['co_full_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Full Name:'),
            '#required' => false
        );

        $form['co_birth_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Birth date'),
            '#required' => false
        );

        $form['co_email'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Email'),
            '#size' => 60,
            '#maxlength' => USERNAME_MAX_LENGTH,
            '#required' => false,
        );

        $form['co_email_2'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Email'),
            '#size' => 60,
            '#maxlength' => USERNAME_MAX_LENGTH,
            '#required' => false,
        );

        $form['co_driving_license'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Driver license'),
            '#required' => false
        );

        $form['co_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Mobile'),
            '#default_value' => (!empty($mobile)) ? $mobile[0]['value'] : ''
        );

        $form['sign_up_required'] = array(
            '#type' => 'radios',
            '#options' => array(
                'on' => $this->t('Send request to co-applicant or guarantor'),
                'off' => $this->t('Complete the co-applicant or guarantor form now')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => 'off'
        );

        // Residence - Form Section
        $form['co_residence_type'] = array(
            '#type' => 'radios',
            '#options' => array(
                'own' => $this->t('Own'),
                'rent' => $this->t('Rent')
            ),
            '#prefixes' => '',
            '#suffixes' => ''
        );

        $form['co_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_movin_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Movin date'),
            '#description' => $this->t('.')
        );

        $form['co_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_state'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('State'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_zip_code'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip Code'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_rent'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Rent'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landlord Name'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_reason_for_leaving'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Reason for leaving')
        ];

        // Occupation
        $form['co_occupation'] = array(
            '#type' => 'radios',
            '#options' => array(
                'employed' => $this->t('Employed'),
                'self-employed' => $this->t('Self-employed'),
                'student' => $this->t('Student'),
                'unemployed' => $this->t('Unemployed')
            ),
            '#prefixes' => '',
            '#suffixes' => ''
        );

        $form['co_employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['co_work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t('')
        );

        $form['co_work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false
        );

        // Previous Occupation
        $form['co_previous_employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['co_previous_work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t('')
        );

        $form['co_previous_work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false
        );

        $form['co_previous_work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false
        );

        // Financial
        $form['co_financial_bank_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Name')
        );

        $form['co_financial_bank_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Address')
        );

        $form['co_financial_account_type'] = array(
            '#type' => 'select',
            '#title' => 'Account Type',
            '#options' => array(
                'savings' => $this->t('Savings'),
                'current' => $this->t('Current'),
            ),
            '#prefixes' => '',
            '#suffixes' => '',
        );

        $form['co_financial_landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landloar Name')
        );

        // Reference
        $form['co_field_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
            '#default_value' => ''
        );

        $form['co_field_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
            '#default_value' => ''
        );

        $form['co_field_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#default_value' => ''
        );

        $form['co_field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#default_value' => ''
        );

        // Emergency Contact
        $form['co_contact_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name')
        );

        $form['co_contact_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship')
        );

        $form['co_contact_address'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Adresss')
        );

        $form['co_contact_phone_number'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number')
        );
        /** EOF: Co-Applicant Form */

        $form['access_key'] = ['#type' => 'value', '#value' => $this->access_key];
        $form['propertyid'] = ['#type' => 'value', '#value' => $propertyid];

        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Apply')];

        // Call the custom template (application-form.html.twig) for this form
        $form['#theme'] = 'application_form';
        return $form;
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $first_name = $form_state->getValue('f_name');
        $last_name = $form_state->getValue('l_name');
        $this->applicant_email = $form_state->getValue('email');

        // sign_up_required variable being used to check the co-applicant registration required to complete the application
        if($form_state->getValue('sign_up_required') == 'on'){
            $this->co_applicant_signup_required = true;
            $this->application_status = 'copending';
        }else{
            $this->co_applicant_signup_required = false;
            $this->application_status = 'pending';
        }

        if($this->co_applicant_signup_required){
            $this->co_applicant_email = $form_state->getValue('co_email');
        }else{
            $this->co_applicant_email = $form_state->getValue('co_email_2');
        }

        if (strlen($first_name) > 80)
        {
            $form_state->setErrorByName('f_name', $this->t('The first name is too long above 80 chasracters.'));
        }

        if (strlen($last_name) > 80)
        {
            $form_state->setErrorByName('l_name', $this->t('The last name is too long above 80 characters.'));
        }

        if (strlen($this->applicant_email) > 200)
        {
            $form_state->setErrorByName('email', $this->t('The email id is too long above 200 characters.'));
        }
        else if (!valid_email_address($this->applicant_email))
        {
            $form_state->setErrorByName('email', $this->t('The email id is not valid.'));
        }
    }

    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $this->applicant_name = $form_state->getValue('f_name') . ' ' . $form_state->getValue('l_name');
        $this->co_applicant_name = $form_state->getValue('co_f_name') . ' ' . $form_state->getValue('co_l_name');

        //Applicant - Basic Information
        $about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $form_state->getValue('f_name'),
            'field_last_name' => $form_state->getValue('l_name'),
            'field_email' => $this->applicant_email,
            'field_driving_license' => $form_state->getValue('driver_license'),
            'field_birth_date' => $form_state->getValue('birth_date'),
            'field_mobile' => $form_state->getValue('cell_phone'),
        ]);
        $about_me->save();
        $about_me_id = $about_me->id();

        //Applicant - Residence
        $residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $form_state->getValue('residence_type'),
            'field_address' => $form_state->getValue('address'),
            'field_mov_in_date' => $form_state->getValue('movin_date'),
            'field_city' => $form_state->getValue('city'),
            'field_state' => $form_state->getValue('state'),
            'field_zip_code' => $form_state->getValue('zip_code'),
            'field_rent' => $form_state->getValue('rent'),
            'field_landlord_name' => $form_state->getValue('landlord_name'),
            'field_reason_for_leaving' => $form_state->getValue('reason_for_leaving'),
        ]);
        $residence->save();
        $residence_id = $residence->id();

        //Occupation
        $occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $form_state->getValue('occupation'),
            'field_employer_name' => $form_state->getValue('employer_name'),
            'field_job_title' => $form_state->getValue('job_title'),
            'field_monthly_income' => $form_state->getValue('total_montyly_income'),
            'field_work_address' => $form_state->getValue('work_address'),
            'field_city' => $form_state->getValue('work_city'),
            'field_zip_code' => $form_state->getValue('work_zip'),
            'field_work_type' => $form_state->getValue('work_type'),
            'field_start_date' => $form_state->getValue('work_start_date'),
            'field_supervisor_name' => $form_state->getValue('work_supervisor'),
            'field_mobile' => $form_state->getValue('work_phone'),
        ]);
        $occupation->save();
        $occupation_id = $occupation->id();

        //Previous Occupation
        $previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $form_state->getValue('previous_employer_name'),
            'field_job_title' => $form_state->getValue('previous_job_title'),
            'field_monthly_income' => $form_state->getValue('previous_total_montyly_income'),
            'field_work_address' => $form_state->getValue('previous_work_address'),
            'field_city' => $form_state->getValue('previous_work_city'),
            'field_zip_code' => $form_state->getValue('previous_work_zip'),
            'field_work_type' => $form_state->getValue('previous_work_type'),
            'field_start_date' => $form_state->getValue('previous_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('previous_work_supervisor'),
            'field_mobile' => $form_state->getValue('previous_work_phone'),
        ]);
        $previous_occupation->save();
        $previous_occupation_id = $previous_occupation->id();

        //Financial
        $financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $form_state->getValue('financial_bank_name'),
            'field_bank_address' => $form_state->getValue('financial_bank_address'),
            'field_account_type' => $form_state->getValue('financial_account_type'),
            'field_landlord_name' => $form_state->getValue('financial_landlord_name'),
        ]);
        $financial->save();
        $financial_id = $financial->id();

        //Reference
        $reference = Paragraph::create([
            'type' => 'reference',
            'field_full_name' => $form_state->getValue('field_full_name'),
            'field_relationship' => $form_state->getValue('field_relationship'),
            'field_address' => $form_state->getValue('field_address'),
            'field_mobile' => $form_state->getValue('field_mobile')
        ]);
        $reference->save();
        $reference_id = $reference->id();

        //Emergency Contact
        $emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $form_state->getValue('contact_full_name'),
            'field_relationship' => $form_state->getValue('contact_relationship'),
            'field_address' => $form_state->getValue('contact_address'),
            'field_mobile' => $form_state->getValue('contact_phone_number'),
        ]);
        $emergency_contact->save();
        $emergency_contact_id = $emergency_contact->id();

        //$field_rental_document = $form_state->getValue('field_rental_document')['fids'];

        //Questioneer
        $questioneer = Paragraph::create([
            'type' => 'questioneer',
            'field_do_you' => $form_state->getValue('field_do_you'),
            'field_illegal_drugs' => $form_state->getValue('field_illegal_drugs'),
            'field_evicted' => $form_state->getValue('field_evicted'),
            'field_babankruptcy' => $form_state->getValue('field_babankruptcy'),
            'field_bedbugs' => $form_state->getValue('field_bedbugs'),
            'field_lequid_filled_furniture' => $form_state->getValue('field_lequid_filled_furniture'),
            'field_pets' => $form_state->getValue('field_pets'),
        ]);
        $questioneer->save();
        $questioneer_id = $questioneer->id();


        //Co-Applicant - Basic Information
        $co_about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $form_state->getValue('co_f_name'),
            'field_last_name' => $form_state->getValue('co_l_name'),
            'field_email' => $this->co_applicant_email,
            'field_birth_date' => $form_state->getValue('co_birth_date'),
            'field_driving_license' => $form_state->getValue('co_driving_license'),
            'field_mobile' => $form_state->getValue('co_mobile'),
        ]);
        $co_about_me->save();
        $co_about_me_id = $co_about_me->id();

        //Co-Applicant - Residence
        $co_residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $form_state->getValue('co_residence_type'),
            'field_address' => $form_state->getValue('co_address'),
            'field_mov_in_date' => $form_state->getValue('co_movin_date'),
            'field_city' => $form_state->getValue('co_city'),
            'field_state' => $form_state->getValue('co_state'),
            'field_zip_code' => $form_state->getValue('co_zip_code'),
            'field_rent' => $form_state->getValue('co_rent'),
            'field_landlord_name' => $form_state->getValue('co_landlord_name'),
            'field_reason_for_leaving' => $form_state->getValue('co_reason_for_leaving'),
        ]);
        $co_residence->save();
        $co_residence_id = $co_residence->id();

        //Co-Applicant - Occupation
        $co_occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $form_state->getValue('co_occupation'),
            'field_employer_name' => $form_state->getValue('co_employer_name'),
            'field_job_title' => $form_state->getValue('co_job_title'),
            'field_monthly_income' => $form_state->getValue('co_total_montyly_income'),
            'field_work_address' => $form_state->getValue('co_work_address'),
            'field_city' => $form_state->getValue('co_work_city'),
            'field_zip_code' => $form_state->getValue('co_work_zip'),
            'field_work_type' => $form_state->getValue('co_work_type'),
            'field_start_date' => $form_state->getValue('co_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('co_work_supervisor'),
            'field_mobile' => $form_state->getValue('co_work_phone'),
        ]);
        $co_occupation->save();
        $co_occupation_id = $co_occupation->id();

        //Co-Applicant - Previous Occupation
        $co_previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $form_state->getValue('co_previous_employer_name'),
            'field_job_title' => $form_state->getValue('co_previous_job_title'),
            'field_monthly_income' => $form_state->getValue('co_previous_total_montyly_income'),
            'field_work_address' => $form_state->getValue('co_previous_work_address'),
            'field_city' => $form_state->getValue('co_previous_work_city'),
            'field_zip_code' => $form_state->getValue('co_previous_work_zip'),
            'field_work_type' => $form_state->getValue('co_previous_work_type'),
            'field_start_date' => $form_state->getValue('co_previous_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('co_previous_work_supervisor'),
            'field_mobile' => $form_state->getValue('co_previous_work_phone'),
        ]);
        $co_previous_occupation->save();
        $co_previous_occupation_id = $co_previous_occupation->id();

        //Co-Applicant - Financial
        $co_financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $form_state->getValue('co_financial_bank_name'),
            'field_bank_address' => $form_state->getValue('co_financial_bank_address'),
            'field_account_type' => $form_state->getValue('co_financial_account_type'),
            'field_landlord_name' => $form_state->getValue('co_financial_landlord_name'),
        ]);
        $co_financial->save();
        $co_financial_id = $co_financial->id();

        //Co-Applicant Reference
        $co_reference = Paragraph::create([
            'type' => 'reference',
            'field_full_name' => $form_state->getValue('co_field_full_name'),
            'field_relationship' => $form_state->getValue('co_field_relationship'),
            'field_address' => $form_state->getValue('co_field_address'),
            'field_mobile' => $form_state->getValue('co_field_mobile')
        ]);
        $co_reference->save();
        $co_reference_id = $co_reference->id();

        //Co-Applicant - Emergency Contact
        $co_emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $form_state->getValue('co_contact_full_name'),
            'field_relationship' => $form_state->getValue('co_contact_relationship'),
            'field_address' => $form_state->getValue('co_contact_address'),
            'field_mobile' => $form_state->getValue('co_contact_phone_number'),
        ]);
        $co_emergency_contact->save();
        $co_emergency_contact_id = $co_emergency_contact->id();

        $co_applicant_name = 'No';

        if($this->co_applicant_signup_required){
            if(!empty($form_state->getValue('co_full_name'))){
                $co_applicant_name = $form_state->getValue('co_full_name');
            }

            $notification_co_app_pending = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
                'title' => 'You have an application with a co-applicant pending',
                'description' => 'We’re waiting for the co-applicant to complete their application then it will be available.',
                'target_link' => 'internal:/dashboard/applications-on-property/' . $this->property->id(),
                'uid' => $this->property->getOwnerId(), // The user that should recieve this notification.
            ]);
            $notification_co_app_pending->save();
        }else{
            $field_application_completed = true;

            if(!empty($form_state->getValue('co_f_name')) || !empty($form_state->getValue('co_l_name'))){
                $co_applicant_name = $form_state->getValue('co_f_name') . ' ' . $form_state->getValue('co_l_name');
            }
        }


        /**
         * Create new application
         */
        $application = Node::create([
            'type' => 'application',
            'title' => $this->property->title->value,
            'field_access_key' => $this->access_key,
            'field_property' => $this->property_id,
            'field_co_applicant_email' => $this->co_applicant_email,
            'field_co_applicant_name' => $co_applicant_name,
            'field_status' => $this->application_status,
            'field_application_completed' => $field_application_completed,

            // Applicant Data
            'field_applicant_photo' => $form_state->getValue('image_file'),

            'field_about_me' => array(
                array(
                    'target_id' => $about_me_id,
                    'target_revision_id' => $about_me->getRevisionId(),
                )
            ),

            'field_residence' => array(
                array(
                    'target_id' => $residence_id,
                    'target_revision_id' => $residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $occupation_id,
                    'target_revision_id' => $occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $previous_occupation_id,
                    'target_revision_id' => $previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $financial_id,
                    'target_revision_id' => $financial->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $emergency_contact_id,
                    'target_revision_id' => $emergency_contact->getRevisionId(),
                )
            ),

            'field_questioneer' => array(
                array(
                    'target_id' => $questioneer_id,
                    'target_revision_id' => $questioneer->getRevisionId(),
                )
            ),

            'field_reference' => array(
                array(
                    'target_id' => $reference_id,
                    'target_revision_id' => $reference->getRevisionId(),
                )
            ),

            'field_rental_document' => $form_state->getValue('field_rental_document')['fids'],

            // Co-Applicant Data
            'field_about_co_applicant' => array(
                array(
                    'target_id' => $co_about_me_id,
                    'target_revision_id' => $co_about_me->getRevisionId(),
                )
            ),

            'field_co_applicant_residence' => array(
                array(
                    'target_id' => $co_residence_id,
                    'target_revision_id' => $co_residence->getRevisionId(),
                )
            ),

            'field_co_applicant_occupation' => array(
                array(
                    'target_id' => $co_occupation_id,
                    'target_revision_id' => $co_occupation->getRevisionId(),
                )
            ),

            'field_coapp_previous_occupation' => array(
                array(
                    'target_id' => $co_previous_occupation_id,
                    'target_revision_id' => $co_previous_occupation->getRevisionId(),
                )
            ),

            'field_co_applicant_financial' => array(
                array(
                    'target_id' => $co_financial_id,
                    'target_revision_id' => $co_financial->getRevisionId(),
                )
            ),

            'field_co_applicant_reference' => array(
                array(
                    'target_id' => $co_reference_id,
                    'target_revision_id' => $co_reference->getRevisionId(),
                )
            ),

            'field_coapp_emergency_contact' => array(
                array(
                    'target_id' => $co_emergency_contact_id,
                    'target_revision_id' => $co_emergency_contact->getRevisionId(),
                )
            )
        ]);


        /**
         * Create new applicant
         */
        $applicant = Node::create([
            'type' => 'applicant',
            'title' => 'Application Form - '. $form_state->getValue('f_name') . ' ' . $form_state->getValue('l_name'),
            'field_co_applicant_email' => $this->co_applicant_email,
            'field_applicant_photo' => $form_state->getValue('image_file'),

            'field_about_me' => array(
                array(
                    'target_id' => $about_me_id,
                    'target_revision_id' => $about_me->getRevisionId(),
                )
            ),

            'field_residence' => array(
                array(
                    'target_id' => $residence_id,
                    'target_revision_id' => $residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $occupation_id,
                    'target_revision_id' => $occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $previous_occupation_id,
                    'target_revision_id' => $previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $financial_id,
                    'target_revision_id' => $financial->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $emergency_contact_id,
                    'target_revision_id' => $emergency_contact->getRevisionId(),
                )
            ),

            'field_questioneer' => array(
                array(
                    'target_id' => $questioneer_id,
                    'target_revision_id' => $questioneer->getRevisionId(),
                )
            ),

            'field_reference' => array(
                array(
                    'target_id' => $reference_id,
                    'target_revision_id' => $reference->getRevisionId(),
                )
            ),

            'field_rental_document' => $form_state->getValue('field_rental_document')['fids'],
        ]);


        /**
         * Create new Co-Applicant
         */
        $co_applicant = Node::create([
            'type' => 'co_applicant',
            'title' => 'Co-Applicant on property "'. $this->property->title->value. '" added by ' . $form_state->getValue('f_name'),
            'field_co_applicant_type' => $form_state->getValue('co_applicant_type'),

            'field_about_co_applicant' => array(
                array(
                    'target_id' => $co_about_me_id,
                    'target_revision_id' => $co_about_me->getRevisionId(),
                )
            ),

            'field_residence' => array(
                array(
                    'target_id' => $co_residence_id,
                    'target_revision_id' => $co_residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $co_occupation_id,
                    'target_revision_id' => $co_occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $co_previous_occupation_id,
                    'target_revision_id' => $co_previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $co_financial_id,
                    'target_revision_id' => $co_financial->getRevisionId(),
                )
            ),

            'field_reference' => array(
                array(
                    'target_id' => $co_reference_id,
                    'target_revision_id' => $co_reference->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $co_emergency_contact_id,
                    'target_revision_id' => $co_emergency_contact->getRevisionId(),
                )
            )
        ]);

        // Create Notification Node
        if ($this->co_applicant_signup_required == false) {
            $notification_agent = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
                'title' => 'You have an application',
                'description' =>  $this->applicant_name . ' has applied to ' . $this->property->title->value,
                'target_link' => 'internal:/dashboard/applications-on-property/' . $this->property->id(),
                'uid' => $this->property->getOwnerId(), // The user that should recieve this notification.
            ]);
            $notification_agent->save();
        }

        /*
        $notification = Node::create([
            'type' => 'notification',
            'title' => $form_state->getValue('f_name') . ' ' . $form_state->getValue('f_name') . ' applied on property ' . $this->property->title->value,
            'field_from' => $this->user->id(),
            'field_to' => $this->property->getOwnerId(),
            'field_property' => $this->property_id
        ]);
        $notification->save();
        */

        // Save All nodes
        $application->save();
        $applicant->save();
        $co_applicant->save();

        //Update User Information
        $user_image = $form_state->getValue('image_file');

        if (!empty($user_image)) {
            $file = File::load($user_image[0]);
            /** @var \Drupal\user\Entity\User $user */
            //$user = $form_state->get('user');
            $this->user->set('user_picture', [
                'target_id' => $file->id(),
                'alt' => $this->t('Member photo'),
            ]);
        }

        $this->user->set('field_application_data_exist', true);
        $this->user->set('field_applicant_id', $applicant->id());
        $this->user->set('field_co_applicant_id', $co_applicant->id());
        $this->user->set('field_first_name', $form_state->getValue('f_name'));
        $this->user->set('field_last_name', $form_state->getValue('l_name'));
        $this->user->save();

        // Send emails to Applicant, Co-Applicant, Property Woner
        $langcode = \Drupal::currentUser()->getPreferredLangcode();

        if(!empty($this->co_applicant_email)){
            // Send an email to co-application to create account and fill-up the form
            $co_applicant_data['message']['sign_up_required'] = $this->co_applicant_signup_required;
            $co_applicant_data['message']['applicant_name'] = $this->applicant_name;
            $co_applicant_data['message']['applicant_email'] = $this->applicant_email;
            $co_applicant_data['message']['co_applicant_name'] = $this->co_applicant_name;
            $co_applicant_data['message']['co_applicant_email'] = $this->co_applicant_email;
            $co_applicant_data['message']['co_applicant_email_base64'] = base64_encode($this->co_applicant_email);
            $co_applicant_data['message']['access_key'] = $this->access_key;
            $co_applicant_data['message']['property_id'] = $this->property_id;
            $co_applicant_data['message']['property_name'] = $this->property->title->value;
            $co_applicant_data['message']['property_zip'] = $this->property->field_property_address->getValue()[0]['postal_code'];
            $co_applicant_data['message']['property_state'] = $this->property->field_property_address->getValue()[0]['administrative_area'];
            $co_applicant_data['message']['property_city'] = $this->property->field_property_address->getValue()[0]['locality'];
            $co_applicant_data['message']['property_address_line1'] = $this->property->field_property_address->getValue()[0]['address_line1'];
            $co_applicant_data['message']['from_email'] = "noreply@rentstarz.com";
            $co_applicant_data['message']['subject'] = t('You have invitation from ' . $this->applicant_name);
            $co_applicant_data['message']['theme'] = 'co-applicant-email';
            $email_to_co_applicant = $this->mail_manager->mail('application', 'email_to_co_applicant', $this->co_applicant_email, $langcode, $co_applicant_data, NULL, true);

            if ($email_to_co_applicant['result'] !== true) {
                drupal_set_message(t('There was a problem sending email to Co-Applicant and it was not sent.'), 'error');
            }
            else {
                drupal_set_message(t('Email has been sent to Co-Applicant with the Registration link.'));
            }
        }

        // Send email to Property Owner
        $property_owner_data['message']['sign_up_required'] = $this->co_applicant_signup_required;
        $property_owner_data['message']['co_applicant_registraion_complete'] = false;
        $property_owner_data['message']['applicant_name'] = $this->applicant_name;
        $property_owner_data['message']['applicant_email'] = $this->applicant_email;
        $property_owner_data['message']['co_applicant_name'] = $this->co_applicant_name;
        $property_owner_data['message']['co_applicant_email'] = $this->co_applicant_email;
        $property_owner_data['message']['access_key'] = $this->access_key;
        $property_owner_data['message']['property_id'] = $this->property_id;
        $property_owner_data['message']['property_name'] = $this->property->title->value;
        $property_owner_data['message']['property_zip'] = $this->property->field_property_address->getValue()[0]['postal_code'];
        $property_owner_data['message']['property_state'] = $this->property->field_property_address->getValue()[0]['administrative_area'];
        $property_owner_data['message']['property_city'] = $this->property->field_property_address->getValue()[0]['locality'];
        $property_owner_data['message']['property_address_line1'] = $this->property->field_property_address->getValue()[0]['address_line1'];
        $property_owner_data['message']['from_email'] = "noreply@rentstarz.com";
        $property_owner_data['message']['subject'] = t($this->applicant_name . ', applied on your property '. $this->property->title->value);
        $property_owner_data['message']['theme'] = 'property-owner-email';
        $email_to_property_owner = $this->mail_manager->mail('application', 'email_to_property_owner', $this->property->getOwner()->getEmail(), $langcode, $property_owner_data, NULL, true);

        if ($email_to_property_owner['result'] !== true) {
            drupal_set_message(t('There was a problem sending email to property owner and it was not sent.'), 'error');
        }

        // Redirect user to the property map page with status message
        $response = new RedirectResponse('/property-listing');
        drupal_set_message($this->t('Your application has been submitted for the property "'. $this->property->title->value . '"'), 'status', TRUE);
        return $response->send();
    }

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
        return 'application_form';
    }

}
