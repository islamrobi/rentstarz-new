<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class EditApplicantForm extends FormBase {
    public $applicant;
    public $user;

    public $field_about_me;
    public $field_residence;
    public $field_occupation;
    public $field_previous_occupation;
    public $field_financial;
    public $field_reference;
    public $field_emergency_contact;
    public $field_questioneer;

	/**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
        return 'edit_applicant_form';
	}

  	/**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
  	public function buildForm(array $form, FormStateInterface $form_state, $propertyid = NULL) {
        \Drupal::service('page_cache_kill_switch')->trigger();

        $this->user = User::load(\Drupal::currentUser()->id());

        //Check the user is authenticated
        if($this->user->isAuthenticated() === false){
            $response = new RedirectResponse('/user/login');
            $response->send();
            exit;
        }

        $applicant_id = $this->user->field_applicant_id->getValue()[0]['value'];

        //Check the user does have application exist
        if(!isset($applicant_id)){
            $response = new RedirectResponse('/property-listing');
            $response->send();
            drupal_set_message($this->t('You haven\'t applied to any property yet.'), 'warning', TRUE);
            exit;
        }

        $this->applicant =  Node::load($applicant_id);

        if(!$this->applicant->field_applicant_photo->isEmpty()){
            $default_image_id = $this->applicant->field_applicant_photo->entity->id();
            $user_picture_title = 'Update profile picture';
        }else{
            $default_image_id = '';
            $user_picture_title = 'Upload profile picture';
        }

        // About me - Form Section
        $form['image_file'] = [
            '#title' => t($user_picture_title),
            '#type' => 'managed_file',
            '#theme' => 'image_widget',
            '#preview_image_style' => 'medium',
            '#upload_location' => 'public://applicant/'.date('Y-m'),
            '#multiple' => FALSE,
            '#upload_validators' => [
              'file_validate_is_image' => [],
              'file_validate_extensions' => ['gif png jpg jpeg'],
              'file_validate_size' => [10485760],
            ],
            '#default_value' => array($default_image_id),
        ];

        $this->field_about_me = $this->getParagraph('field_about_me');
        $form['field_first_name'] = array(
            '#type' => 'textfield',
            '#title' => t('First Name:'),
            '#required' => true,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_first_name')
        );

        $form['field_last_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Last Name:'),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_last_name')
        );

        $form['field_birth_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Birth date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_birth_date')
        );

        $form['field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Cell phone'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_mobile')
        );

        $form['field_email'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Email'),
            '#size' => 60,
            '#maxlength' => USERNAME_MAX_LENGTH,
            '#description' => $this->t(''),
            '#required' => true,
            '#disabled' => true,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_email')
        );

        $form['field_driving_license'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Driver license'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_driving_license')
        );

        // Residence - Form Section
        $this->field_residence = $this->getParagraph('field_residence');
        $form['residence_type'] = array(
            '#type' => 'radios',
            '#options' => array(
                'own' => $this->t('Own'),
                'rent' => $this->t('Rent')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_residence_type')
        );

        $form['address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_address')
        );

        $form['movin_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Movin date'),
            '#description' => $this->t('.'),
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_mov_in_date')
        );

        $form['city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_city')
        );

        $form['state'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('State'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_state')
        );

        $form['zip_code'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip Code'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_zip_code')
        );

        $form['rent'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Rent'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_rent')
        );

        $form['landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landlord Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_landlord_name')
        );

        $form['reason_for_leaving'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Reason for leaving'),
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_reason_for_leaving')
        ];

        // Occupation
        $this->field_occupation = $this->getParagraph('field_occupation');
        $form['occupation'] = array(
            '#type' => 'radios',
            '#options' => array(
                'employed' => $this->t('Employed'),
                'self-employed' => $this->t('Self-employed'),
                'student' => $this->t('Student'),
                'unemployed' => $this->t('Unemployed')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_occupation_type')
        );

        $form['employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_employer_name')
        );

        $form['job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_job_title')
        );

        $form['total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_monthly_income')
        );

        $form['work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_address')
        );

        $form['work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_city')
        );

        $form['work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_zip_code')
        );

        $form['work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_type')
        );

        $form['work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_start_date')
        );

        $form['work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_supervisor_name')
        );

        $form['work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_mobile')
        );

        // Previous Occupation
        $this->field_previous_occupation = $this->getParagraph('field_previous_occupation');
        $form['previous_employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_employer_name')
        );

        $form['previous_job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_job_title')
        );

        $form['previous_total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_monthly_income')
        );

        $form['previous_work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_address')
        );

        $form['previous_work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_city')
        );

        $form['previous_work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_zip_code')
        );

        $form['previous_work_type'] = array(
            '#type' => 'select',
            '#title' => 'Work Type',
            '#options' => array(
                'full-time' => $this->t('Full Time'),
                'part-time' => $this->t('Part Time'),
                'freelance' => $this->t('Freelance'),
            ),
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_type')
        );

        $form['previous_work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_start_date')
        );

        $form['previous_work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_supervisor_name')
        );

        $form['previous_work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_mobile')
        );

        // Financial
        $this->field_financial = $this->getParagraph('field_financial');
        $form['financial_bank_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_name')
        );

        $form['financial_bank_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Address'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_address')
        );

        $form['financial_account_type'] = array(
            '#type' => 'select',
            '#title' => $this->t('Account Type'),
            '#options' => [
                'current' => $this->t('Current'),
                'savings' => $this->t('Savings'),
            ],
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_account_type')
        );

        $form['financial_landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landloar Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_landlord_name')
        );

        //Reference
        $this->field_reference = $this->getParagraph('field_reference');
        $form['field_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_full_name')
        );

        $form['field_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
            '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_relationship')
        );

        $form['field_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_address')
        );

        $form['field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_mobile')
        );

        // Emergency Contact
        $this->field_emergency_contact = $this->getParagraph('field_emergency_contact');
        $form['contact_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_full_name')
        );

        $form['contact_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_relationship')
        );

        $form['contact_address'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Adresss'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_address')
        );

        $form['contact_phone_number'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_mobile')
        );

        //Rental Document
        $form['field_rental_document'] = array(
            '#type' => 'managed_file',
            '#title' => $this->t('<p class="text-center">Upload your rental documents securely</p>'),
            '#upload_location' => 'private://rental-docs',
            '#progress_message' => $this->t('Please wait...'),
            '#extended' => TRUE,
            '#size' => 13,
            '#multiple' => TRUE,
            '#upload_validators' => array(
                'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
            )
        );

        // Questioneer
        $this->field_questioneer = $this->getParagraph('field_questioneer');
        $form['field_do_you'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_do_you'),
        );

        $form['field_illegal_drugs'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_illegal_drugs'),
        );

        $form['field_evicted'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_evicted'),
        );

        $form['field_babankruptcy'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_babankruptcy'),
        );

        $form['field_bedbugs'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_bedbugs'),
        );

        $form['field_lequid_filled_furniture'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_lequid_filled_furniture'),
        );

        $form['field_pets'] = array(
            '#type' => 'radios',
            '#options' => array(
                'yes' => $this->t('Yes'),
                'no' => $this->t('No')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_questioneer, 'field_pets'),
        );

        // Submit Button
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Save')];

        $form['#theme'] = 'edit_applicant_form';
        return $form;
	}

	/**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function validateForm(array & $form, FormStateInterface $form_state) {

  	}

	/**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function submitForm(array & $form, FormStateInterface $form_state) {
        //Applicant - Residence
        $this->field_about_me->set('field_birth_date', $form_state->getValue('field_birth_date'));
        $this->field_about_me->set('field_driving_license', $form_state->getValue('field_driving_license'));
        $this->field_about_me->set('field_email', $form_state->getValue('field_email'));
        $this->field_about_me->set('field_first_name', $form_state->getValue('field_first_name'));
        $this->field_about_me->set('field_last_name', $form_state->getValue('field_last_name'));
        $this->field_about_me->set('field_mobile', $form_state->getValue('field_mobile'));
        $this->field_about_me->save();

        //Applicant - Residence
        $this->field_residence->set('field_residence_type', $form_state->getValue('residence_type'));
        $this->field_residence->set('field_address', $form_state->getValue('address'));
        $this->field_residence->set('field_mov_in_date', $form_state->getValue('movin_date'));
        $this->field_residence->set('field_city', $form_state->getValue('city'));
        $this->field_residence->set('field_state', $form_state->getValue('state'));
        $this->field_residence->set('field_zip_code', $form_state->getValue('zip_code'));
        $this->field_residence->set('field_rent', $form_state->getValue('rent'));
        $this->field_residence->set('field_landlord_name', $form_state->getValue('landlord_name'));
        $this->field_residence->set('field_reason_for_leaving', $form_state->getValue('reason_for_leaving'));
        $this->field_residence->save();

        //Applicant - Occupation
        $this->field_occupation->set('field_occupation_type', $form_state->getValue('occupation'));
        $this->field_occupation->set('field_employer_name', $form_state->getValue('employer_name'));
        $this->field_occupation->set('field_job_title', $form_state->getValue('job_title'));
        $this->field_occupation->set('field_monthly_income', $form_state->getValue('total_montyly_income'));
        $this->field_occupation->set('field_work_address', $form_state->getValue('work_address'));
        $this->field_occupation->set('field_city', $form_state->getValue('work_city'));
        $this->field_occupation->set('field_zip_code', $form_state->getValue('work_zip'));
        $this->field_occupation->set('field_work_type', $form_state->getValue('work_type'));
        $this->field_occupation->set('field_start_date', $form_state->getValue('work_start_date'));
        $this->field_occupation->set('field_supervisor_name', $form_state->getValue('work_supervisor'));
        $this->field_occupation->set('field_mobile', $form_state->getValue('work_phone'));
        $this->field_occupation->save();

        //Applicant - Previous Occupation
        $this->field_previous_occupation->set('field_employer_name', $form_state->getValue('previous_employer_name'));
        $this->field_previous_occupation->set('field_job_title', $form_state->getValue('previous_job_title'));
        $this->field_previous_occupation->set('field_monthly_income', $form_state->getValue('previous_total_montyly_income'));
        $this->field_previous_occupation->set('field_work_address', $form_state->getValue('previous_work_address'));
        $this->field_previous_occupation->set('field_city', $form_state->getValue('previous_work_city'));
        $this->field_previous_occupation->set('field_zip_code', $form_state->getValue('previous_work_zip'));
        $this->field_previous_occupation->set('field_work_type', $form_state->getValue('previous_work_type'));
        $this->field_previous_occupation->set('field_start_date', $form_state->getValue('previous_work_start_date'));
        $this->field_previous_occupation->set('field_supervisor_name', $form_state->getValue('previous_work_supervisor'));
        $this->field_previous_occupation->set('field_mobile', $form_state->getValue('previous_work_phone'));
        $this->field_previous_occupation->save();

        //Applicant - Financial
        $this->field_financial->set('field_bank_name', $form_state->getValue('financial_bank_name'));
        $this->field_financial->set('field_bank_address', $form_state->getValue('financial_bank_address'));
        $this->field_financial->set('field_account_type', $form_state->getValue('financial_account_type'));
        $this->field_financial->set('field_landlord_name', $form_state->getValue('financial_landlord_name'));
        $this->field_financial->save();

        //Applicant - Reference
        $this->field_reference->set('field_full_name', $form_state->getValue('field_full_name'));
        $this->field_reference->set('field_relationship', $form_state->getValue('field_relationship'));
        $this->field_reference->set('field_address', $form_state->getValue('field_address'));
        $this->field_reference->set('field_mobile', $form_state->getValue('field_mobile'));
        $this->field_reference->save();

        //Applicant - Emergency Contact
        $this->field_emergency_contact->set('field_full_name', $form_state->getValue('contact_full_name'));
        $this->field_emergency_contact->set('field_relationship', $form_state->getValue('contact_relationship'));
        $this->field_emergency_contact->set('field_address', $form_state->getValue('contact_address'));
        $this->field_emergency_contact->set('field_mobile', $form_state->getValue('contact_phone_number'));
        $this->field_emergency_contact->save();

        //Applicant - Questioneer
        $this->field_questioneer->set('field_do_you', $form_state->getValue('field_do_you'));
        $this->field_questioneer->set('field_illegal_drugs', $form_state->getValue('field_illegal_drugs'));
        $this->field_questioneer->set('field_evicted', $form_state->getValue('field_evicted'));
        $this->field_questioneer->set('field_babankruptcy', $form_state->getValue('field_babankruptcy'));
        $this->field_questioneer->set('field_bedbugs', $form_state->getValue('field_bedbugs'));
        $this->field_questioneer->set('field_lequid_filled_furniture', $form_state->getValue('field_lequid_filled_furniture'));
        $this->field_questioneer->set('field_pets', $form_state->getValue('field_pets'));
        $this->field_questioneer->save();

        $this->applicant->set('field_applicant_photo', $form_state->getValue('image_file'));
        $this->applicant->set('field_rental_document', $form_state->getValue('field_rental_document')['fids']);

        $this->applicant->save();

        //Update User Information
        $user_image = $form_state->getValue('image_file');

        if (!empty($user_image)) {
            $file = File::load($user_image[0]);
            /** @var \Drupal\user\Entity\User $user */
            //$user = $form_state->get('user');
            $this->user->set('user_picture', [
                'target_id' => $file->id(),
                'alt' => $this->t('Profile Picture'),
            ]);
        }

        $this->user->set('field_first_name', $form_state->getValue('field_first_name'));
        $this->user->set('field_last_name', $form_state->getValue('field_last_name'));
        $this->user->save();

        //Redirect map page and return status message
        $response = new RedirectResponse('/modal/my-applications');
        $response->send();
        drupal_set_message($this->t('Application data has been updated.'), 'status', TRUE);
	}

	/**
     * Getter method for paragraph
     */
    public function getParagraph($name)
    {
        return Paragraph::load($this->applicant->get($name)->getValue()[0]['target_id']);
    }

    public function getParagraphFieldValue($paragraph, $name)
    {
        return $paragraph->$name->value;
    }
}
