<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class EditCoApplicantForm extends FormBase {
     public $co_applicant;
     public $user;

     public $field_about_me;
     public $field_residence;
     public $field_occupation;
     public $field_previous_occupation;
     public $field_financial;
     public $field_reference;
     public $field_emergency_contact;

	/**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
        return 'edit_co_applicant_form';
	}
	
  	/**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
	public function buildForm(array $form, FormStateInterface $form_state, $propertyid = NULL) {
		\Drupal::service('page_cache_kill_switch')->trigger();
        
          $this->user = User::load(\Drupal::currentUser()->id());
          
          //Check the user is authenticated
          if($this->user->isAuthenticated() === false){
               $response = new RedirectResponse('/user/login');
               $response->send();
               exit;
          }

          $co_applicant_id = $this->user->field_co_applicant_id->getValue()[0]['value'];
          
          //Check the user does have application exist
          if(!isset($co_applicant_id)){
               $response = new RedirectResponse('/property-listing');
               $response->send();
               drupal_set_message($this->t('You haven\'t applied to any property yet.'), 'warning', TRUE);
               exit;
          }

          $this->co_applicant =  Node::load($co_applicant_id);
          
          // About Co-Applicant
          $this->field_about_me = $this->getParagraph('field_about_co_applicant');
          $form['co_applicant_type'] = array(
               '#type' => 'radios',
               '#options' => array(
                    'co-applicant' => $this->t('Co-Applicant'),
                    'guarantor' => $this->t('Guarantor')
               ),
               '#prefixes' => '',
               '#suffixes' => '',
               '#required' => false,
               '#default_value' => $this->co_applicant->field_co_applicant_type->getValue()[0]['value']
          );

          $form['co_f_name'] = array(
               '#type' => 'textfield',
               '#title' => t('First Name:'),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_first_name')
          );

          $form['co_l_name'] = array(
               '#type' => 'textfield',
               '#title' => t('Last Name:'),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_last_name')
          );

          $form['co_birth_date'] = array(
               '#type' => 'date',
               '#title' => $this->t('Birth date'),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_birth_date')
          );

          $form['co_email'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Email'),
               '#size' => 60,
               '#maxlength' => USERNAME_MAX_LENGTH,
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_email'),
          );

          $form['co_driving_license'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Driver license'),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_driving_license')
          );

          $form['co_mobile'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Mobile'),
               '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_mobile')
          );

          // Residence - Form Section
          $this->field_residence = $this->getParagraph('field_residence');
          $form['co_residence_type'] = array(
               '#type' => 'radios',
               '#options' => array(
                    'own' => $this->t('Own'),
                    'rent' => $this->t('Rent')
               ),
               '#prefixes' => '',
               '#suffixes' => '',
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_residence_type')
          );

          $form['co_address'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Address'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_address')
          );

          $form['co_movin_date'] = array(
               '#type' => 'date',
               '#title' => $this->t('Movin date'),
               '#description' => $this->t('.'),
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_mov_in_date')
          );

          $form['co_city'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('City'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_city')
          );

          $form['co_state'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('State'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_state')
          );

          $form['co_zip_code'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Zip Code'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_zip_code')
          );

          $form['co_rent'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Rent'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_rent')
          );

          $form['co_landlord_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Landlord Name'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_landlord_name')
          );

          $form['co_reason_for_leaving'] = [
               '#type' => 'textarea',
               '#title' => $this->t('Reason for leaving'),
               '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_reason_for_leaving')
          ];
          
          // Occupation
          $this->field_occupation = $this->getParagraph('field_occupation');
          $form['co_occupation'] = array(
               '#type' => 'radios',
               '#options' => array(
                    'employed' => $this->t('Employed'),
                    'self-employed' => $this->t('Self-employed'),
                    'student' => $this->t('Student'),
                    'unemployed' => $this->t('Unemployed')
               ),
               '#prefixes' => '',
               '#suffixes' => '',
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_occupation_type')
          );

          $form['co_employer_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Employer Name'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_employer_name')
          );

          $form['co_job_title'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Job Title'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_job_title')
          );

          $form['co_total_montyly_income'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Total Monthly Income'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_monthly_income')
          );

          $form['co_work_address'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Work Address'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_address')
          );

          $form['co_work_city'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('City'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_city')
          );

          $form['co_work_zip'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Zip'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_zip_code')
          );

          $form['co_work_type'] = array(
               '#type' => 'select',
               '#title' => 'Work Type',
               '#options' => array(
                   'full-time' => $this->t('Full Time'),
                   'part-time' => $this->t('Part Time'),
                   'freelance' => $this->t('Freelance'),
               ),
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_type')
           );

          $form['co_work_start_date'] = array(
               '#type' => 'date',
               '#title' => $this->t('Start Date'),
               '#description' => $this->t(''),
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_start_date')
          );

          $form['co_work_supervisor'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Supervisor Name'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_supervisor_name')
          );

          $form['co_work_phone'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Phone Number'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_mobile')
          );

          // Previous Occupation
          $this->field_previous_occupation = $this->getParagraph('field_previous_occupation');
          $form['co_previous_employer_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Employer Name'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_employer_name')
          );

          $form['co_previous_job_title'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Job Title'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_job_title')
          );

          $form['co_previous_total_montyly_income'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Total Monthly Income'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_monthly_income')
          );

          $form['co_previous_work_address'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Work Address'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_address')
          );

          $form['co_previous_work_city'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('City'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_city')
          );

          $form['co_previous_work_zip'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Zip'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_zip_code')
          );

          $form['co_previous_work_type'] = array(
               '#type' => 'select',
               '#title' => 'Work Type',
               '#options' => array(
                   'full-time' => $this->t('Full Time'),
                   'part-time' => $this->t('Part Time'),
                   'freelance' => $this->t('Freelance'),
               ),
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_type')
           );

          $form['co_previous_work_start_date'] = array(
               '#type' => 'date',
               '#title' => $this->t('Start Date'),
               '#description' => $this->t(''),
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_start_date')
          );

          $form['co_previous_work_supervisor'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Supervisor Name'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_supervisor_name')
          );

          $form['co_previous_work_phone'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Phone Number'),
               '#description' => $this->t(''),
               '#required' => false,
               '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_mobile')
          );

          // Financial
          $this->field_financial = $this->getParagraph('field_financial');
          $form['co_financial_bank_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Bank Name'),
               '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_name')
          );

          $form['co_financial_bank_address'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Bank Address'),
               '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_address')
          );

          $form['co_financial_account_type'] = array(
               '#type' => 'select',
               '#title' => $this->t('Account Type'),
               '#options' => [
                    'current' => $this->t('Current'),
                    'savings' => $this->t('Savings'),
               ],
               '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_account_type')
          );

          $form['co_financial_landlord_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Landloar Name'),
               '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_landlord_name')
          );

          // Reference
          $this->field_reference = $this->getParagraph('field_reference');
          $form['field_full_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Full Name'),
               '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_full_name')
          );

          $form['field_relationship'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Relationship'),
               '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_relationship')
          );

          $form['field_address'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Address'),
               '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_address')
          );

          $form['field_mobile'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Phone Number'),
               '#default_value' => $this->getParagraphFieldValue($this->field_reference, 'field_mobile')
          );

          // Emergency Contact
          $this->field_emergency_contact = $this->getParagraph('field_emergency_contact');
          $form['co_contact_full_name'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Full Name'),
               '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_full_name')
          );

          $form['co_contact_relationship'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Relationship'),
               '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_relationship')
          );

          $form['co_contact_address'] = array(
               '#type' => 'textarea',
               '#title' => $this->t('Adresss'),
               '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_address')
          );

          $form['co_contact_phone_number'] = array(
               '#type' => 'textfield',
               '#title' => $this->t('Phone Number'),
               '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_mobile')
          );

          // Submit Button
          $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Save')];

          $form['#theme'] = 'edit_co_applicant_form';
          return $form;
	}

	/**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function validateForm(array & $form, FormStateInterface $form_state) {

  	}

	/**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
  	public function submitForm(array & $form, FormStateInterface $form_state) {
          // Co-Applicant - About me
          $this->field_about_me->set('field_first_name', $form_state->getValue('co_f_name'));
          $this->field_about_me->set('field_last_name', $form_state->getValue('co_l_name'));
          $this->field_about_me->set('field_email', $form_state->getValue('co_email'));
          $this->field_about_me->set('field_birth_date', $form_state->getValue('co_birth_date'));
          $this->field_about_me->set('field_driving_license', $form_state->getValue('co_driving_license'));
          $this->field_about_me->set('field_mobile', $form_state->getValue('co_mobile'));
          $this->field_about_me->save();
          
          // Co-Applicant - Residence
          $this->field_residence->set('field_residence_type', $form_state->getValue('co_residence_type'));
          $this->field_residence->set('field_address', $form_state->getValue('co_address'));
          $this->field_residence->set('field_mov_in_date', $form_state->getValue('co_movin_date'));
          $this->field_residence->set('field_city', $form_state->getValue('co_city'));
          $this->field_residence->set('field_state', $form_state->getValue('co_state'));
          $this->field_residence->set('field_zip_code', $form_state->getValue('co_zip_code'));
          $this->field_residence->set('field_rent', $form_state->getValue('co_rent'));
          $this->field_residence->set('field_landlord_name', $form_state->getValue('co_landlord_name'));
          $this->field_residence->set('field_reason_for_leaving', $form_state->getValue('co_reason_for_leaving'));
          $this->field_residence->save();
          
          // Co-Applicant - Occupation
          $this->field_occupation->set('field_occupation_type', $form_state->getValue('co_occupation'));
          $this->field_occupation->set('field_employer_name', $form_state->getValue('co_employer_name'));
          $this->field_occupation->set('field_job_title', $form_state->getValue('co_job_title'));
          $this->field_occupation->set('field_monthly_income', $form_state->getValue('co_total_montyly_income'));
          $this->field_occupation->set('field_work_address', $form_state->getValue('co_work_address'));
          $this->field_occupation->set('field_city', $form_state->getValue('co_work_city'));
          $this->field_occupation->set('field_zip_code', $form_state->getValue('co_work_zip'));
          $this->field_occupation->set('field_work_type', $form_state->getValue('co_work_type'));
          $this->field_occupation->set('field_start_date', $form_state->getValue('co_work_start_date'));
          $this->field_occupation->set('field_supervisor_name', $form_state->getValue('co_work_supervisor'));
          $this->field_occupation->set('field_mobile', $form_state->getValue('co_work_phone'));
          $this->field_occupation->save();

          // Co-Applicant - Previous Occupation
          $this->field_previous_occupation->set('field_employer_name', $form_state->getValue('co_previous_employer_name'));
          $this->field_previous_occupation->set('field_job_title', $form_state->getValue('co_previous_job_title'));
          $this->field_previous_occupation->set('field_monthly_income', $form_state->getValue('co_previous_total_montyly_income'));
          $this->field_previous_occupation->set('field_work_address', $form_state->getValue('co_previous_work_address'));
          $this->field_previous_occupation->set('field_city', $form_state->getValue('co_previous_work_city'));
          $this->field_previous_occupation->set('field_zip_code', $form_state->getValue('co_previous_work_zip'));
          $this->field_previous_occupation->set('field_work_type', $form_state->getValue('co_previous_work_type'));
          $this->field_previous_occupation->set('field_start_date', $form_state->getValue('co_previous_work_start_date'));
          $this->field_previous_occupation->set('field_supervisor_name', $form_state->getValue('co_previous_work_supervisor'));
          $this->field_previous_occupation->set('field_mobile', $form_state->getValue('co_previous_work_phone'));
          $this->field_previous_occupation->save();

          // Co-Applicant - Financial
          $this->field_financial->set('field_bank_name', $form_state->getValue('co_financial_bank_name'));
          $this->field_financial->set('field_bank_address', $form_state->getValue('co_financial_bank_address'));
          $this->field_financial->set('field_account_type', $form_state->getValue('co_financial_account_type'));
          $this->field_financial->set('field_landlord_name', $form_state->getValue('co_financial_landlord_name'));
          $this->field_financial->save();

          // Co-Applicant - Reference
          $this->field_reference->set('field_full_name', $form_state->getValue('field_full_name'));
          $this->field_reference->set('field_relationship', $form_state->getValue('field_relationship'));
          $this->field_reference->set('field_address', $form_state->getValue('field_address'));
          $this->field_reference->set('field_mobile', $form_state->getValue('field_mobile'));
          $this->field_reference->save();

          // Co-Applicant - Emergency Contact
          $this->field_emergency_contact->set('field_full_name', $form_state->getValue('co_contact_full_name'));
          $this->field_emergency_contact->set('field_relationship', $form_state->getValue('co_contact_relationship'));
          $this->field_emergency_contact->set('field_address', $form_state->getValue('co_contact_address'));
          $this->field_emergency_contact->set('field_mobile', $form_state->getValue('co_contact_phone_number'));
          $this->field_emergency_contact->save();

          // Redirect map page and return status message
          $response = new RedirectResponse('/modal/my-applications');
          $response->send();
          drupal_set_message($this->t('Co-Applicant Data has been updated.'), 'status', TRUE);
     }
     
     /**
     * Getter method for paragraph
     */
     public function getParagraph($name)
     {
          return Paragraph::load($this->co_applicant->get($name)->getValue()[0]['target_id']);
     }

     public function getParagraphFieldValue($paragraph, $name)
     {
          return $paragraph->$name->value;
     }
}