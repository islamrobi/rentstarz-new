<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\user\Kernel;
use \Drupal\node\Entity\Node;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\Component\Utility\Random;
use Drupal\user\Entity\User;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class RequestAdditionalInfoForm extends FormBase
{
    public $user;
    public $application;
    public $applicant;
    public $property;
    public $mail_manager;


    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
       return 'request_additional_info_form';
    }


    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $application_id = NULL)
    {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        $this->user = User::load(\Drupal::currentUser()->id());
        $this->mail_manager = \Drupal::service('plugin.manager.mail');

        //Check the user is authenticated
        if($this->user->isAuthenticated() === false){
            $this->errorHandler('warning', true, 'Login required', '/user/login?destination=/modal/application/update/additional_info/'.$application_id);
        }

        //Load Target Application
        if(isset($application_id) && is_numeric($application_id)){
            $this->application =  Node::load($application_id);
        }else{
            $this->errorHandler('warning', true, 'Application doesn\'t exist or Ivalid', '/modal/my-applications');
        }

        if(is_object($this->application)){
            $this->property = Node::load($this->application->field_property->getValue()[0]['target_id']);
            $this->applicant = User::load($this->application->getOwnerId());

            $form['field_required_pay_stubs'] = array(
                '#type' => 'checkbox',
                '#title' => $this->t('Request for the documents of Pay Stubs'),
                '#default_value' =>  $this->application->field_required_pay_stubs->getValue()[0]['value'],
            );

            $form['field_required_tax_return'] = array(
                '#type' => 'checkbox',
                '#title' => $this->t('Request for the documents of Tax Return'),
                '#default_value' =>  $this->application->field_required_tax_return->getValue()[0]['value'],
            );

            $form['field_required_bank_statement'] = array(
                '#type' => 'checkbox',
                '#title' => $this->t('Request for the documents of Bank Statement'),
                '#default_value' =>  $this->application->field_required_bank_statement->getValue()[0]['value'],
            );

            $form['field_message'] = [
                '#type' => 'textarea',
                '#title' => $this->t('Message to Applicant'),
                '#default_value' => $this->application->field_message->getValue()[0]['value']
            ];

            $form['data']['application_title'] = $this->application->getTitle();
            $form['data']['applicant_name'] = $this->applicant->field_first_name->getValue()[0]['value'] . ' ' . $this->applicant->field_last_name->getValue()[0]['value'];
            $form['data']['applicant_email'] = $this->applicant->getEmail();

            // Submit Button
            $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Send Request')];

            $form['#theme'] = 'request_additional_info_form';
            return $form;
        }else{
            $this->errorHandler('warning', true, 'Application doesn\'t exist or Ivalid', '/modal/my-applications');
        }

    }


    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function validateForm(array & $form, FormStateInterface $form_state) {
        //Check the agent is eligible to request for additional information
        if($this->application->field_status->getValue()[0]['value'] != 'pending'){
            $this->errorHandler('warning', true, 'Request Faild! Application need to be completed to request for the additional information', '/dashboard/applications-on-property/'.$this->property->id());
        }
    }


    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
  	public function submitForm(array & $form, FormStateInterface $form_state) {
        $this->application->set('field_required_pay_stubs', $form_state->getValue('field_required_pay_stubs'));
        $this->application->set('field_required_tax_return', $form_state->getValue('field_required_tax_return'));
        $this->application->set('field_required_bank_statement', $form_state->getValue('field_required_bank_statement'));
        $this->application->set('field_message', $form_state->getValue('field_message'));
        $this->application->set('field_status', 'requestinginfo');

        $this->application->save();

        $property_owner_name = $this->user->field_first_name->getValue()[0]['value'] . ' ' . $this->user->field_last_name->getValue()[0]['value'];

        // Create Notification Node
        $notification_rentor = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
            'title' => 'Additional information is needed',
            'description' =>  'Agent '. $property_owner_name .' is requesting additional information for the ' . $this->property->title->value . ', please click here to start sending',
            'target_link' => 'internal:/modal/application/update/additional_info/' . $this->application->id(),
            'uid' => $this->application->getOwnerId(), // The user that should recieve this notification.
        ]);
        $notification_rentor->save();

        //Send email to renter to submit additional documents
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $property_owner_data['message']['property_owner_name'] = $property_owner_name;
        $property_owner_data['message']['property_id'] = $this->property->id();
        $property_owner_data['message']['application_id'] = $this->application->id();
        $property_owner_data['message']['application_name'] = $this->application->title->value;
        $property_owner_data['message']['property_name'] = $this->property->title->value;
        $property_owner_data['message']['property_zip'] = $this->property->field_property_address->getValue()[0]['postal_code'];
        $property_owner_data['message']['property_state'] = $this->property->field_property_address->getValue()[0]['administrative_area'];
        $property_owner_data['message']['property_city'] = $this->property->field_property_address->getValue()[0]['locality'];
        $property_owner_data['message']['property_address_line1'] = $this->property->field_property_address->getValue()[0]['address_line1'];
        $property_owner_data['message']['from_email'] = "noreply@rentstarz.com";
        $property_owner_data['message']['subject'] = t($property_owner_name.' requesting to upload additional documents for the application '. $this->application->title->value);
        $property_owner_data['message']['theme'] = 'request-additonal-info-email';
        $request_additonal_info_email = $this->mail_manager->mail('application', 'request_additonal_info_email', $this->application->getOwner()->getEmail(), $langcode, $property_owner_data, NULL, true);

        if ($request_additonal_info_email['result'] !== true) {
            drupal_set_message(t('There was a problem sending email to property owner and it was not sent.'), 'error');
        }

        $response = new RedirectResponse('/dashboard/applications-on-property/'.$this->property->id());
        $response->send();
        drupal_set_message($this->t('Your request has been submitted successfully'), 'status', TRUE);
    }



    public function errorHandler($error_type = 'status', $exit = true, $error_message = NULL, $url = ''){
        $response = new RedirectResponse($url);
        $response->send();

        if(isset($error_message)){
            drupal_set_message($this->t($error_message), $error_type, TRUE);
        }

        if($exit){
            exit;
        }
    }
}
