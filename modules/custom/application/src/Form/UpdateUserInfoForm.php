<?php
namespace Drupal\application\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;
use CommerceGuys\Addressing\AddressFormat\AddressField;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class UpdateUserInfoForm extends FormBase {
    public $user;

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
       return 'update_user_info_form';
    }

    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
	public function buildForm(array $form, FormStateInterface $form_state, $propertyid = NULL) {
        \Drupal::service('page_cache_kill_switch')->trigger();

        $this->user = User::load(\Drupal::currentUser()->id());

        //Check the user is authenticated
        if($this->user->isAuthenticated() === false){
            $response = new RedirectResponse('/user/login');
            $response->send();
            exit;
        }

        $form['field_profile_type'] = array(
            '#type' => 'select',
            '#title' => t('Profile Type'),
            '#options' => array(
                'agent' => $this->t('Agent'),
                'landlord' => $this->t('Landlord'),
            ),
            '#required' => false
        );

        $form['field_company_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Company Name'),
            '#required' => false,
            '#states' => [
                'visible' => [
                  ':input[name="field_profile_type"]' => ['value' => 'agent'],
                ],
            ],
        );

        $form['field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => t('Mobile'),
            '#required' => false
        );

        $form['field_address'] = array(
            '#type' => 'address',
            '#title' => t('Address'),
            '#default_value' => ['country_code' => 'US'],
            '#used_fields' => [
                AddressField::ADDRESS_LINE1,
                AddressField::ADMINISTRATIVE_AREA,
                AddressField::LOCALITY,
                AddressField::POSTAL_CODE,
            ],
            '#available_countries' => ['US'],
        );

        $form['field_real_estate_id'] = array(
            '#type' => 'textfield',
            '#title' => t('Real estate ID'),
            '#required' => false,
            '#states' => [
                'visible' => [
                  ':input[name="field_profile_type"]' => ['value' => 'agent'],
                ],
            ],
        );

        // Submit Button
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Save')];

        $form['#theme'] = 'update_user_info_form';
        return $form;
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
	public function validateForm(array & $form, FormStateInterface $form_state) {

    }

    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
  	public function submitForm(array & $form, FormStateInterface $form_state) {
        // Update user data
        $this->user->set('field_profile_type', $form_state->getValue('field_profile_type'));
        $this->user->set('field_company_name', $form_state->getValue('field_company_name'));
        $this->user->set('field_mobile', $form_state->getValue('field_mobile'));
        $this->user->set('field_address', $form_state->getValue('field_address'));
        $this->user->set('field_real_estate_id', $form_state->getValue('field_real_estate_id'));
        //$this->user->set('field_verified', true);
        $this->user->addRole('renter');
        $this->user->addRole($form_state->getValue('field_profile_type'));
        $this->user->save();

        //Redirect map page and return status message
        $response = new RedirectResponse('/property-listing');
        $response->send();
        drupal_set_message($this->t('Your Agent or Landloard information has been updated. We will check your information and verify your account.'), 'status', TRUE);
    }
}
