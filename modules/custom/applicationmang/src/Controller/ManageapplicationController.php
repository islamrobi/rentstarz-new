<?php
namespace Drupal\applicationmang\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides route responses for the Example module.
 */
class ManageapplicationController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *  A simple renderable array.
   */
  public function manageApplicationPage() {
    $uid = \Drupal::currentUser()->id();
    //$uid = 6;
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'property');
    $query->condition('status', '1');
    $query->condition('uid', $uid);
    $ids = $query->execute();
    $applied_ids = array();
    foreach ($ids as $key => $value) {
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      $node = $node_storage->load($value);
      $applied_ids[$value] = array('title' => $node->get('title')->value); 


      $apply_query = \Drupal::entityQuery('node');
      $apply_query->condition('type', 'property_application');
      $apply_query->condition('status', '1');
      $apply_query->condition('field_reference',  $value, 'CONTAINS');
      $applied = $apply_query->execute();  
      if(!empty($applied)) { 
        foreach ($applied as $appkey => $appvalue) {
          $applied_node_storage = \Drupal::entityTypeManager()->getStorage('node');
          $applied_node = $applied_node_storage->load($appvalue);
          $application_status = $applied_node->get('field_status')->getValue();
          $author_id = $applied_node->getOwner()->get('uid')->value;
          $author_name = $applied_node->getOwner()->get('field_first_name')->value;
          $author_name .= ' '.$applied_node->getOwner()->get('field_last_name')->value;
          $author_address = $applied_node->getOwner()->get('field_address')->getValue();
          $author_address_string = $author_address[0]['address_line1'].' '.$author_address[0]['address_line2'].', '.$author_address[0]['locality'];   
          $author_picture = $applied_node->getOwner()->get('user_picture')->entity->url();
          
          $application_link = base_path().'view-application/'.$appvalue;
          $shortlist_link = base_path().'shortlist/'.$appvalue;

          $term = Term::load($application_status[0]['target_id']);
          $name = strtolower($term->getName());
          $application_ar = array(
            'author_name' => $author_name, 
            'author_address' => $author_address_string, 
            'author_picture' => $author_picture ,
            'author_id' => $author_id, 
            'application_link' => $application_link, 
            'shortlist_link' => $shortlist_link , 
            'application_status' => $application_status, );  
          $applied_info[$name][$appvalue] = $application_ar;
        }
      $applied_ids[$value]['nid'] = $applied_info;
      }
    } 

  return [
      '#theme' => 'applications_received',
      '#applications' => $applied_ids,
    ];
  }

}