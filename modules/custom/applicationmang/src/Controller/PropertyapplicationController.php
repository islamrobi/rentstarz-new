<?php
namespace Drupal\applicationmang\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for the Example module.
 */
class PropertyapplicationController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function propertyApplicationPage($propertyid) {  
    $pid = $propertyid;
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $user_application = $user->get('field_application')->getValue();
    //echo '<pre>'; print_r($user_application); echo '</pre>';exit;
    if (!empty($user_application)) {
      $application_id = $user_application[0]['target_id'];

        $node = Node::load($application_id);
        if ($node === null) {
            drupal_set_message(t('Technical error occured make sure you have filled one time link properly.'), 'error');
        } else {

            $nodeDuplicate = $node->createDuplicate();
            //edit title or something so you can find cloned
            foreach ($nodeDuplicate->field_current_residence as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            foreach ($nodeDuplicate->field_previous_residence as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            foreach ($nodeDuplicate->field_employment as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            foreach ($nodeDuplicate->field_reference as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            foreach ($nodeDuplicate->field_family_members as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            foreach ($nodeDuplicate->field_income as $field) {
              $field->entity = $field->entity->createDuplicate();
            }
            $nodeDuplicate->field_property = $pid;
            $nodeDuplicate->created->value = time();
            $nodeDuplicate->changed->value = time();
            $nodeDuplicate->save();

            drupal_set_message(
              t("Applied successfully!"), 'status');
        }
        $link = base_path().'node/'.$pid;
        return new RedirectResponse($link);


    } else {
      $link = base_path().'apply/'.$pid;
      return new RedirectResponse($link);     
    }
    
    return $nid;

  }

}