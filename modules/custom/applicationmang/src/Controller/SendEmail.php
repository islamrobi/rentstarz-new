<?php
namespace Drupal\applicationmang\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SendEmail extends ControllerBase {

    public function call_ajax(Request $request){
        $params = array();
        $content = $request->getContent();
        
        if (!empty($content)) {
            // 2nd param to get as array
            $params = json_decode($content, TRUE);
        }
        
        // Process $params...
        return new JsonResponse($params);
    }

}