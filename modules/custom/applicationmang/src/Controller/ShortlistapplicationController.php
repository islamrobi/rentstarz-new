<?php
namespace Drupal\applicationmang\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for the Example module.
 */
class ShortlistapplicationController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function shortlistApplicationPage($applicationid) {  
    $name = 'Shortlisted';
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    }
    $terms = \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties($properties);
    $term = reset($terms);
    $tid = !empty($term) ? $term->get('tid')->value : 0;


    $nid = $applicationid;
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node = $node_storage->load($nid);
    //Taxonomy
    $node->field_status->target_id = $tid;
    //save to update node
    $node->save();

    $link = base_path().'manage/application';
    $response = new RedirectResponse($link);
    $response->send();
    return $response;

  }

}