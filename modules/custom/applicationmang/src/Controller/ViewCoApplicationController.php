<?php
namespace Drupal\applicationmang\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides route responses for the Example module.
 */
class ViewCoApplicationController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function viewApplicationPage() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $current_user = \Drupal::currentUser();
    $user = User::load($current_user->id());

    //Check the user is authenticated
    if($current_user->isAuthenticated() === false){
      $response = new RedirectResponse('/user/login');
      $response->send();
      exit;
    }

    $application_id = NULL;
    //$co_applicant_id = NULL;
    $co_applicant_user = NULL;

    //Query the Application Entity for current user
    $application_query = \Drupal::entityQuery('node')
      ->condition('type', 'application')
      ->condition('field_co_applicant_user', $current_user->id());
    $application_results = $application_query->execute();

    foreach($application_results as $result){
      $application_id = $result;
    }
    
    //Check the user does have application exist
    if(!isset($application_id)){
        $response = new RedirectResponse('/property-listing');
        $response->send();
        exit;
    }

    $application_node = Node::load($application_id);
    //$co_applicant_id = $application_node->field_co_applicant->getValue()[0]['target_id'];
    //$co_applicant = Node::load($co_applicant_id);
    
    /*
    //Query the Co-Applicant Entity for current user
    $co_applicant_query = \Drupal::entityQuery('node')
      ->condition('type', 'co_applicant')
      ->condition('uid', $current_user->id());
    $co_applicant_results = $co_applicant_query->execute();

    foreach($co_applicant_results as $result){
      $co_applicant_id = $result;
    }

    //Check the user does have application exist
    if(!isset($co_applicant_id)){
      $response = new RedirectResponse('/property-listing');
      $response->send();
      drupal_set_message($this->t('You haven\'t applied to any property yet.'), 'warning', TRUE);
      exit;
    }

    //Get applicant and co-applicant node
    $co_applicant = Node::load($co_applicant_id);
    */

    /**
     * Co-Applicant Information
     */
    //Applicant - About Me
    
    $co_applicant_about = $this->getParagraph($application_node, 'field_about_co_applicant');
    
    //$application['profile_photo'] = file_create_url($user->user_picture->entity->getFileUri());
    
    $application['first_name'] = $this->getParagraphFieldValue($co_applicant_about, 'field_first_name');
    $application['last_name'] = $this->getParagraphFieldValue($co_applicant_about, 'field_last_name');
    $application['full_name'] = $application['first_name'].' '.$application['last_name'];
    $application['date_of_birth'] = $this->getParagraphFieldValue($co_applicant_about, 'field_birth_date');
    $application['phone_number'] = $this->getParagraphFieldValue($co_applicant_about, 'field_mobile');
    $application['email'] = $this->getParagraphFieldValue($co_applicant_about, 'field_email');
    $application['field_driving_license'] = $this->getParagraphFieldValue($co_applicant_about, 'field_driving_license');

    //Applicant - Residence
    $co_applicant_residence = $this->getParagraph($application_node, 'field_co_applicant_residence');

    $application['residence_type'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_residence_type');
    $application['residence_address'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_address');
    $application['residence_mov_in_date'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_mov_in_date');
    $application['residence_city'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_city');
    $application['residence_state'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_state');
    $application['residence_zip_code'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_zip_code');
    $application['residence_rent'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_rent');
    $application['residence_landlord_name'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_landlord_name');
    $application['residence_reason_for_leaving'] = $this->getParagraphFieldValue($co_applicant_residence, 'field_reason_for_leaving');

    //Applicant - Occupation
    $co_applicant_occupation = $this->getParagraph($application_node, 'field_co_applicant_occupation');

    $application['occupation_type'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_occupation_type');
    $application['occupation_employer_name'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_employer_name');
    $application['occupation_job_title'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_job_title');
    $application['occupation_monthly_income'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_monthly_income');
    $application['occupation_work_address'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_work_address');
    $application['occupation_city'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_city');
    $application['occupation_zip_code'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_zip_code');
    $application['occupation_work_type'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_work_type');
    $application['occupation_start_date'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_start_date');
    $application['occupation_supervisor_name'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_supervisor_name');
    $application['occupation_phone_number'] = $this->getParagraphFieldValue($co_applicant_occupation, 'field_mobile');

    //Applicant - Previous Occupation
    $co_applicant_prev_occupation = $this->getParagraph($application_node, 'field_coapp_previous_occupation');

    $application['prev_occupation_employer_name'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_employer_name');
    $application['prev_occupation_job_title'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_job_title');
    $application['prev_occupation_monthly_income'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_monthly_income');
    $application['prev_occupation_work_address'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_work_address');
    $application['prev_occupation_city'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_city');
    $application['prev_occupation_zip_code'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_zip_code');
    $application['prev_occupation_work_type'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_work_type');
    $application['prev_occupation_start_date'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_start_date');
    $application['prev_occupation_supervisor_name'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_supervisor_name');
    $application['prev_occupation_phone_number'] = $this->getParagraphFieldValue($co_applicant_prev_occupation, 'field_mobile');

    //Applicant - Financial
    $co_applicant_financial = $this->getParagraph($application_node, 'field_co_applicant_financial');

    $application['financial_bank_name'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_bank_name');
    $application['financial_bank_address'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_bank_address');
    $application['financial_account_type'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_account_type');
    $application['financial_landlord_name'] = $this->getParagraphFieldValue($co_applicant_financial, 'field_landlord_name');

    //Applicant - Emergency Contact
    $co_applicant_contact = $this->getParagraph($application_node, 'field_coapp_emergency_contact');

    $application['contact_full_name'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_full_name');
    $application['contact_relationship'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_relationship');
    $application['contact_address'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_address');
    $application['contact_mobile'] = $this->getParagraphFieldValue($co_applicant_contact, 'field_mobile');

    
    //kint($application['first_name']);
    //exit;

    return [
      '#theme' => 'co_application',
      '#application' => $application,
    ];

    /*
    $nid = $applicationid;
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node = $node_storage->load($nid);
    $application['first_name'] = $node->get('field_first_name')->value; 
    $application['last_name'] = $node->get('field_last_name')->value; 
    $application['email'] = $node->get('field_email')->value; 
    $application['drivers_licence'] = $node->get('field_drivers_licence')->value; 
    $application['birth_date'] = $node->get('field_birth_date')->value; 
    $application['cell_phone'] = $node->get('field_phone')->value; 

    $family = $node->field_family_members->getValue();
    // Loop through the result set.
    foreach ( $family as $member ) {
      
      $mem_ar = Paragraph::load($member['target_id']);
      $target_id = $member['target_id'];
      $mem_ar_info[$target_id]['age'] = $mem_ar->get('field_age')->value;
      $mem_ar_info[$target_id]['name'] = $mem_ar->get('field_full_name')->value;
      $mem_ar_info[$target_id]['relation'] = $mem_ar->get('field_relationship')->value;
    }
    */

  }

  /**
   * Getter method for paragraph
   */
  public function getParagraph($node, $name)
  {
      return Paragraph::load($node->get($name)->getValue()[0]['target_id']);
  }

  public function getParagraphFieldValue($paragraph, $name)
  {
      return $paragraph->$name->value;
  }

  function get_files($files){
    $img_urls = array();

    foreach ($files as $item) {
        if ($item->entity) {
            $img_urls[] = $item->entity->url();
        }
    }

    return $img_urls;
  }

}