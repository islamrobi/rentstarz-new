<?php
namespace Drupal\applicationmang\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\user\Kernel;
use \Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ApplyPropertyForm extends FormBase
{
    public $access_key;
    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $propertyid = NULL)
    {
        \Drupal::service('page_cache_kill_switch')->trigger();
        $current_user = \Drupal::currentUser();
        $current_user_id = \Drupal::currentUser()->id();
        $has_application_form = false;
        
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'applicant')
            ->condition('uid', $current_user_id);
        $application_form = $query->execute();
        
        foreach($application_form as $form){
            $application_form_id = $form[0];
        }

        $has_application_form = count($application_form);

        $this->access_key = md5(date(now)+time(now)+$propertyid);
        
        // Check weather the user allready have an application or not and user is loged in
        if($current_user->isAuthenticated()) {
            $form['#tree'] = TRUE;
            $user = \Drupal\user\Entity\User::load($current_user_id);
            //$user_application = $user->get('field_application')->getValue();
            $field_first_name = $user->get('field_first_name')->getValue();
            $field_last_name = $user->get('field_last_name')->getValue();
            $dob = $user->get('field_date_of_birth')->getValue();
            $mobile = $user->get('field_mobile')->getValue();
            $email = $user->getEmail();
            $application_exist = false;
            
            /**
             * If application already exist
             */
            if($has_application_form){
                // Check is user applied on this property or not
                $property_application_nids = \Drupal::entityQuery('node')->condition('type','application')->execute();
                $property_applications =  \Drupal\node\Entity\Node::loadMultiple($property_application_nids);
                
                foreach ($property_applications as $property_application) {
                    
                    $property_applicant_id = $property_application->getOwnerId();
                    $property_id_on_application = $property_application->get('field_property')->getValue()[0]['target_id'];
                    
                    if(($property_applicant_id == $current_user_id) && $propertyid == $property_id_on_application){
                        $application_exist = true;
                    }
                }

                if(!$application_exist){
                    // Create Propery Application
                    $property_storage = \Drupal::entityTypeManager()->getStorage('node');
                    $selected_property = $property_storage->load($propertyid);
                    
                    $node = Node::create([
                        'type' => 'application',
                        'title' => $selected_property->getTitle(),
                        'field_applicant' => $application_form_id,
                        'field_property' => $propertyid
                    ]);
                    $node->save();

                    //Create notification
                    $notification = Node::create([
                        'type' => 'notification',
                        'title' => $field_first_name[0]['value'] . ' ' . $field_last_name[0]['value'] . ' applied on property ' . $selected_property->getTitle(),
                        'field_from' => $current_user->id(),
                        'field_to' => $selected_property->getOwnerId(),
                        'field_property' => $propertyid
                    ]);
                    $notification->save();

                    $response = new RedirectResponse('/property-listing');
                    $response->send();
                    drupal_set_message($this->t('Your application has been submitted for the property "'. $selected_property->getTitle() . '"'), 'status', TRUE);
                    return;
                } else {
                    $response = new RedirectResponse('/property-listing');
                    $response->send();
                    drupal_set_message($this->t('You already applied on this property before'), 'warning', TRUE);
                    return;
                }

                return;
            }

            $config = $this->config('system.site');

            // About me - Form Section
            $form['f_name'] = array(
                '#type' => 'textfield',
                '#title' => t('First Name:'),
                '#required' => true,
                '#default_value' => (!empty($field_first_name)) ? $field_first_name[0]['value'] : ''
            );

            $form['l_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Last Name:'),
                '#required' => false,
                '#default_value' => (!empty($field_last_name)) ? $field_last_name[0]['value'] : ''
            );

            $form['birth_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Birth date'),
                '#description' => $this->t(''),
                '#default_value' => (!empty($dob)) ? $dob[0]['value'] : ''
            );

            $form['cell_phone'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Cell phone'),
                '#description' => $this->t(''),
                '#default_value' => (!empty($mobile)) ? $mobile[0]['value'] : ''
            );

            $form['email'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Email'),
                '#size' => 60,
                '#maxlength' => USERNAME_MAX_LENGTH,
                '#description' => $this->t(''),
                '#required' => true,
                '#value' => (!empty($email)) ? $email : ''
            );
            
            $form['driver_license'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Driver license'),
                '#description' => $this->t(''),
                '#required' => false
            );

            // Residence - Form Section
            $form['residence_type'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'own' => $this->t('Own'),
                    'rent' => $this->t('Rent')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['movin_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Movin date'),
                '#description' => $this->t('.')
            );

            $form['city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['state'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('State'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['zip_code'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip Code'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['rent'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Rent'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['landlord_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Landlord Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['reason_for_leaving'] = [
                '#type' => 'textarea',
                '#title' => $this->t('Reason for leaving')
            ];
            
            // Occupation
            $form['occupation'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'employed' => $this->t('Employed'),
                    'self-employed' => $this->t('Self-employed'),
                    'student' => $this->t('Student'),
                    'unemployed' => $this->t('Unemployed'),
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['employer_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Employer Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['job_title'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Job Title'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['total_montyly_income'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Total Monthly Income'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_zip'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Type'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_start_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Start Date'),
                '#description' => $this->t('e')
            );

            $form['work_supervisor'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Supervisor Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['work_phone'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number'),
                '#description' => $this->t(''),
                '#required' => false
            );

            // Previous Occupation
            $form['previous_employer_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Employer Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_job_title'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Job Title'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_total_montyly_income'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Total Monthly Income'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_zip'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Type'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_start_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Start Date'),
                '#description' => $this->t('e')
            );

            $form['previous_work_supervisor'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Supervisor Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['previous_work_phone'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number'),
                '#description' => $this->t(''),
                '#required' => false
            );

            // Financial
            $form['financial_bank_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Bank Name')
            );

            $form['financial_bank_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Bank Address')
            );

            $form['financial_account_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Account Type')
            );

            $form['financial_landlord_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Landloar Name')
            );

            /**
             * Reference - (Repeatable)
             */
            $reference = 0;
            $reference_count = $form_state->get('reference_count');
            $form['reference_fieldset'] = array(
                '#type' => 'fieldset',
                '#prefix' => '<div id="reference-fieldset-wrapper" class="col-md-12">',
                '#suffix' => '</div>'
            );

            if (empty($reference_count))
            {
                if (!empty($reference))
                {
                    $reference_count = sizeof($reference);
                    $form_state->set('reference_count', $reference_count);
                }
                else
                {
                    $reference_count = $form_state->set('reference_count', 1);
                }
            }

            for ($reference = 0; $reference < $reference_count; $reference++)
            {
                $form['reference_fieldset'][$reference]['reference_full_name'] = array(
                    '#type' => 'textfield',
                    '#title' => $this->t('Full Name'),
                    '#prefix' => '<div class="row"><div class="col-md-4">',
                    '#suffix' => '</div>',
                    '#default_value' => ''
                );

                $form['reference_fieldset'][$reference]['reference_relationship'] = array(
                    '#type' => 'textfield',
                    '#title' => $this->t('Relationship'),
                    '#prefix' => '<div class="col-md-4">',
                    '#suffix' => '</div>',
                    '#default_value' => ''
                );

                $form['reference_fieldset'][$reference]['reference_address'] = array(
                    '#type' => 'textfield',
                    '#title' => $this->t('Address'),
                    '#prefix' => '<div class="col-md-4">',
                    '#suffix' => '</div></div>',
                    '#default_value' => ''
                );

                $form['reference_fieldset'][$reference]['reference_phone_number'] = array(
                    '#type' => 'textfield',
                    '#title' => $this->t('Phone Number'),
                    '#prefix' => '<div class="row"><div class="col-md-4">',
                    '#suffix' => '</div>',
                    '#default_value' => ''
                );

                $form['reference_fieldset'][$reference]['reference_landlord_name'] = array(
                    '#type' => 'textfield',
                    '#title' => $this->t('Landloar Name'),
                    '#prefix' => '<div class="col-md-4">',
                    '#suffix' => '</div><div class="col-md-4"></div></div><hr/>',
                    '#default_value' => ''
                );
            }

            $form['reference_fieldset']['actions']['add_reference'] = array(
                '#type' => 'submit',
                '#value' => t('Add More'),
                '#prefix' => '<div class="col-md-6">',
                '#suffix' => '</div>',
                '#submit' => array('callback' => '::referenceAddOne'),
                '#ajax' => array(
                    'callback' => '::referenceAddMoreCallback',
                    'wrapper' => 'reference-fieldset-wrapper'
                )
            );

            if ($reference_count > 1)
            {
                $form['reference_fieldset']['actions']['remove_income'] = array(
                    '#type' => 'submit',
                    '#value' => t('Remove Reference'),
                    '#prefix' => '<div class="col-md-6">',
                    '#suffix' => '</div>',
                    '#submit' => array('callback' => '::referenceRemoveCallback'),
                    '#ajax' => array(
                        'callback' => '::referenceAddMoreCallback',
                        'wrapper' => 'reference-fieldset-wrapper'
                    )
                );
            }

            // Emergency Contact
            $form['contact_full_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Full Name')
            );

            $form['contact_relationship'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Relationship')
            );

            $form['contact_address'] = array(
                '#type' => 'textarea',
                '#title' => $this->t('Adresss')
            );

            $form['contact_phone_number'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number')
            );

            //Rental Document
            $form['nested']['file'] = array(
                '#type' => 'managed_file',
                '#title' => $this->t('<p class="text-center">Upload your rental documents securely</p>'),
                '#upload_location' => 'public://rental-docs',
                '#progress_message' => $this->t('Please wait...'),
                '#extended' => TRUE,
                '#size' => 13,
                '#multiple' => TRUE,
                '#upload_validators' => array(
                    'file_validate_extensions' => array('txt pdf doc docx xls xlsx ppt pptx jpg jpeg gif png')
                ),
                '#default_value' => ''
            );

            if ($default_fids) {
                $default_fids = explode(',', $default_fids);
                $form['nested']['file']['#default_value'] = $extended ? array(
                    'fids' => $default_fids,
                ) : $default_fids;
            }

            /**
             * Questioneer
             */
            $form['field_do_you_smoke'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_have_you_ever_been_convict'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_have_you_ever_been_evicted'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_have_you_ever_filed_for_ba'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_have_you_ever_had_bedbugs_'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_lequid_filled_furnit'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['field_will_you_have_pets_'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'yes' => $this->t('Yes'),
                    'no' => $this->t('No')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            /**
             * Co-Application
             */
            // About Co-Applicant
            $form['co_applicant_type'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'co-applicant' => $this->t('Co-Applicant'),
                    'guarantor' => $this->t('Guarantor')
                ),
                '#prefixes' => '',
                '#suffixes' => '',
                '#required' => true
            );

            $form['co_f_name'] = array(
                '#type' => 'textfield',
                '#title' => t('First Name:'),
                '#required' => true
            );

            $form['co_l_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Last Name:'),
                '#required' => true
            );

            $form['co_birth_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Birth date'),
                '#required' => true
            );

            $form['co_email'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Email'),
                '#size' => 60,
                '#maxlength' => USERNAME_MAX_LENGTH,
                '#required' => true,
            );

            $form['co_driving_license'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Driver license'),
                '#required' => false
            );

            $form['co_mobile'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Mobile'),
                '#default_value' => (!empty($mobile)) ? $mobile[0]['value'] : ''
            );
            
             // Residence - Form Section
            $form['co_residence_type'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'own' => $this->t('Own'),
                    'rent' => $this->t('Rent')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['co_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_movin_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Movin date'),
                '#description' => $this->t('.')
            );

            $form['co_city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_state'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('State'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_zip_code'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip Code'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_rent'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Rent'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_landlord_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Landlord Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_reason_for_leaving'] = [
                '#type' => 'textarea',
                '#title' => $this->t('Reason for leaving')
            ];
            
            // Occupation
            $form['co_occupation'] = array(
                '#type' => 'radios',
                '#options' => array(
                    'employed' => $this->t('Employed'),
                    'self-employed' => $this->t('Self-employed'),
                    'student' => $this->t('Student'),
                    'unemployed' => $this->t('Unemployed')
                ),
                '#prefixes' => '',
                '#suffixes' => ''
            );

            $form['co_employer_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Employer Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_job_title'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Job Title'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_total_montyly_income'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Total Monthly Income'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_zip'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Type'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_start_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Start Date'),
                '#description' => $this->t('e')
            );

            $form['co_work_supervisor'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Supervisor Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_work_phone'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number'),
                '#description' => $this->t(''),
                '#required' => false
            );

            // Previous Occupation
            $form['co_previous_employer_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Employer Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_job_title'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Job Title'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_total_montyly_income'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Total Monthly Income'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Address'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_city'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('City'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_zip'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Zip'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Work Type'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_start_date'] = array(
                '#type' => 'date',
                '#title' => $this->t('Start Date'),
                '#description' => $this->t('e')
            );

            $form['co_previous_work_supervisor'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Supervisor Name'),
                '#description' => $this->t(''),
                '#required' => false
            );

            $form['co_previous_work_phone'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number'),
                '#description' => $this->t(''),
                '#required' => false
            );

            // Financial
            $form['co_financial_bank_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Bank Name')
            );

            $form['co_financial_bank_address'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Bank Address')
            );

            $form['co_financial_account_type'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Account Type')
            );

            $form['co_financial_landlord_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Landloar Name')
            );

            // Emergency Contact
            $form['co_contact_full_name'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Full Name')
            );

            $form['co_contact_relationship'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Relationship')
            );

            $form['co_contact_address'] = array(
                '#type' => 'textarea',
                '#title' => $this->t('Adresss')
            );

            $form['co_contact_phone_number'] = array(
                '#type' => 'textfield',
                '#title' => $this->t('Phone Number')
            );
            /** EOF: Co-Applicant Form */

        }else{
            $form['login'] = ['#markup' => '<h3>Please login to get in touch!</h3>'];
        }

        $form['access_key'] = ['#type' => 'value', '#value' => $this->access_key];

        $form['propertyid'] = ['#type' => 'value', '#value' => $propertyid];
        // Submit Button
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Apply')];

        $form['#theme'] = 'apply_form';
        return $form;
    }

    /*
     * Reference add more
    */
    public function referenceAddOne(array & $form, FormStateInterface $form_state)
    {
        $reference_count = $form_state->get('reference_count');
        $reference_add_button = $reference_count + 1;
        $form_state->set('reference_count', $reference_add_button);
        $form_state->setRebuild();
    }

    public function referenceAddMoreCallback(array & $form, FormStateInterface $form_state)
    {
        //$reference_count = $form_state->get('reference_count');
        return $form['reference_fieldset'];
    }

    public function referenceRemoveCallback(array & $form, FormStateInterface $form_state)
    {
        $reference_count = $form_state->get('reference_count');
        if ($reference_count > 1)
        {
            $reference_remove_button = $reference_count - 1;
            $form_state->set('reference_count', $reference_remove_button);
        }
        $form_state->setRebuild();
    }

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
        return 'apply_property_form';
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $f_name = $form_state->getValue('f_name');
        $l_name = $form_state->getValue('l_name');
        $email = $form_state->getValue('email');
        $co_email = $form_state->getValue('co_email');
        
        if (strlen($f_name) > 80)
        {
            $form_state->setErrorByName('f_name', $this->t('The first name is too long above 80 chasracters.'));
        }
        
        if (strlen($l_name) > 80)
        {
            $form_state->setErrorByName('l_name', $this->t('The last name is too long above 80 characters.'));
        }

        if (strlen($email) > 200)
        {
            $form_state->setErrorByName('email', $this->t('The email id is too long above 200 characters.'));
        }
        else if (!valid_email_address($email))
        {
            $form_state->setErrorByName('email', $this->t('The email id is not valid.'));
        }

        if (strlen($co_email) > 200)
        {
            $form_state->setErrorByName('email', $this->t('The email id is too long above 200 characters.'));
        }
        else if (!valid_email_address($co_email))
        {
            $form_state->setErrorByName('email', $this->t('The email id is not valid.'));
        }

    }

    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $propertyid = $form_state->getValue('propertyid');
        $associated_property = \Drupal\node\Entity\Node::load($propertyid);
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $f_name = $form_state->getValue('f_name');
        $l_name = $form_state->getValue('l_name');
        $co_applicant_email = $form_state->getValue('co_email');
        $cell_phone = $form_state->getValue('cell_phone');

        $co_applicant_type = $form_state->getValue('co_applicant_type');

        //Co-Applicant - Basic Information
        $co_about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $form_state->getValue('co_f_name'),
            'field_last_name' => $form_state->getValue('co_l_name'),
            'field_email' => $co_applicant_email,
            'field_birth_date' => $form_state->getValue('co_birth_date'),
            'field_driving_license' => $form_state->getValue('co_driving_license'),
            'field_mobile' => $form_state->getValue('co_mobile'),
        ]);
        $co_about_me->save();
        $co_about_me_id = $co_about_me->id();

        //Co-Applicant - Residence
        $co_residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $form_state->getValue('co_residence_type'),
            'field_address' => $form_state->getValue('co_address'),
            'field_mov_in_date' => $form_state->getValue('co_movin_date'),
            'field_city' => $form_state->getValue('co_city'),
            'field_state' => $form_state->getValue('co_state'),
            'field_zip_code' => $form_state->getValue('co_zip_code'),
            'field_rent' => $form_state->getValue('co_rent'),
            'field_landlord_name' => $form_state->getValue('co_landlord_name'),
            'field_reason_for_leaving' => $form_state->getValue('co_reason_for_leaving'),
        ]);
        $co_residence->save();
        $co_residence_id = $co_residence->id();

        //Co-Applicant - Occupation
        $co_occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $form_state->getValue('co_occupation'),
            'field_employer_name' => $form_state->getValue('co_employer_name'),
            'field_job_title' => $form_state->getValue('co_job_title'),
            'field_monthly_income' => $form_state->getValue('co_total_montyly_income'),
            'field_work_address' => $form_state->getValue('co_work_address'),
            'field_city' => $form_state->getValue('co_work_city'),
            'field_zip_code' => $form_state->getValue('co_work_zip'),
            'field_work_type' => $form_state->getValue('co_work_type'),
            'field_start_date' => $form_state->getValue('co_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('co_work_supervisor'),
            'field_mobile' => $form_state->getValue('co_work_phone'),
        ]);
        $co_occupation->save();
        $co_occupation_id = $co_occupation->id();

        //Co-Applicant - Previous Occupation
        $co_previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $form_state->getValue('co_previous_employer_name'),
            'field_job_title' => $form_state->getValue('co_previous_job_title'),
            'field_monthly_income' => $form_state->getValue('co_previous_total_montyly_income'),
            'field_work_address' => $form_state->getValue('co_previous_work_address'),
            'field_city' => $form_state->getValue('co_previous_work_city'),
            'field_zip_code' => $form_state->getValue('co_previous_work_zip'),
            'field_work_type' => $form_state->getValue('co_previous_work_type'),
            'field_start_date' => $form_state->getValue('co_previous_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('co_previous_work_supervisor'),
            'field_mobile' => $form_state->getValue('co_previous_work_phone'),
        ]);
        $co_previous_occupation->save();
        $co_previous_occupation_id = $co_previous_occupation->id();

        //Co-Applicant - Financial
        $co_financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $form_state->getValue('co_financial_bank_name'),
            'field_bank_address' => $form_state->getValue('co_financial_bank_address'),
            'field_account_type' => $form_state->getValue('co_financial_account_type'),
            'field_landlord_name' => $form_state->getValue('co_financial_landlord_name'),
        ]);
        $co_financial->save();
        $co_financial_id = $co_financial->id();

        //Co-Applicant - Emergency Contact
        $co_emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $form_state->getValue('co_contact_full_name'),
            'field_relationship' => $form_state->getValue('co_contact_relationship'),
            'field_address' => $form_state->getValue('co_contact_address'),
            'field_mobile' => $form_state->getValue('co_contact_phone_number'),
        ]);
        $co_emergency_contact->save();
        $co_emergency_contact_id = $co_emergency_contact->id();
        
        /**
         * Create new Co-Applicant
         */
        $co_applicant_node = Node::create([
            'type' => 'co_applicant',
            'title' => 'Co-Applicant on property "'. $associated_property->title->value. '" added by ' . $f_name,
            'field_co_applicant_type' => $co_applicant_type,
            
            'field_about_co_applicant' => array(
                array(
                    'target_id' => $co_about_me_id,
                    'target_revision_id' => $co_about_me->getRevisionId(),
                )
            ),
            
            'field_residence' => array(
                array(
                    'target_id' => $co_residence_id,
                    'target_revision_id' => $co_residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $co_occupation_id,
                    'target_revision_id' => $co_occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $co_previous_occupation_id,
                    'target_revision_id' => $co_previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $co_financial_id,
                    'target_revision_id' => $co_financial->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $co_emergency_contact_id,
                    'target_revision_id' => $co_emergency_contact->getRevisionId(),
                )
            )
        ]);
        
        //Create Co-Applicant Node
        $co_applicant_node->save();
        

        //Applicant - Basic Information
        $about_me = Paragraph::create([
            'type' => 'basic_info',
            'field_first_name' => $form_state->getValue('f_name'),
            'field_last_name' => $form_state->getValue('l_name'),
            'field_email' => $form_state->getValue('email'),
            'field_driving_license' => $form_state->getValue('driver_license'),
            'field_birth_date' => $form_state->getValue('birth_date'),
            'field_mobile' => $form_state->getValue('cell_phone'),
        ]);
        $about_me->save();
        $about_me_id = $about_me->id();

        //Applicant - Residence
        $residence = Paragraph::create([
            'type' => 'residence',
            'field_residence_type' => $form_state->getValue('residence_type'),
            'field_address' => $form_state->getValue('address'),
            'field_mov_in_date' => $form_state->getValue('movin_date'),
            'field_city' => $form_state->getValue('city'),
            'field_state' => $form_state->getValue('state'),
            'field_zip_code' => $form_state->getValue('zip_code'),
            'field_rent' => $form_state->getValue('rent'),
            'field_landlord_name' => $form_state->getValue('landlord_name'),
            'field_reason_for_leaving' => $form_state->getValue('reason_for_leaving'),
        ]);
        $residence->save();
        $residence_id = $residence->id();
        
        //Occupation
        $occupation = Paragraph::create([
            'type' => 'occupation',
            'field_occupation_type' => $form_state->getValue('occupation'),
            'field_employer_name' => $form_state->getValue('employer_name'),
            'field_job_title' => $form_state->getValue('job_title'),
            'field_monthly_income' => $form_state->getValue('total_montyly_income'),
            'field_work_address' => $form_state->getValue('work_address'),
            'field_city' => $form_state->getValue('work_city'),
            'field_zip_code' => $form_state->getValue('work_zip'),
            'field_work_type' => $form_state->getValue('work_type'),
            'field_start_date' => $form_state->getValue('work_start_date'),
            'field_supervisor_name' => $form_state->getValue('work_supervisor'),
            'field_mobile' => $form_state->getValue('work_phone'),
        ]);
        $occupation->save();
        $occupation_id = $occupation->id();

        //Previous Occupation
        $previous_occupation = Paragraph::create([
            'type' => 'previous_occupation',
            'field_employer_name' => $form_state->getValue('previous_employer_name'),
            'field_job_title' => $form_state->getValue('previous_job_title'),
            'field_monthly_income' => $form_state->getValue('previous_total_montyly_income'),
            'field_work_address' => $form_state->getValue('previous_work_address'),
            'field_city' => $form_state->getValue('previous_work_city'),
            'field_zip_code' => $form_state->getValue('previous_work_zip'),
            'field_work_type' => $form_state->getValue('previous_work_type'),
            'field_start_date' => $form_state->getValue('previous_work_start_date'),
            'field_supervisor_name' => $form_state->getValue('previous_work_supervisor'),
            'field_mobile' => $form_state->getValue('previous_work_phone'),
        ]);
        $previous_occupation->save();
        $previous_occupation_id = $previous_occupation->id();

        //Financial
        $financial = Paragraph::create([
            'type' => 'financial',
            'field_bank_name' => $form_state->getValue('financial_bank_name'),
            'field_bank_address' => $form_state->getValue('financial_bank_address'),
            'field_account_type' => $form_state->getValue('financial_account_type'),
            'field_landlord_name' => $form_state->getValue('financial_landlord_name'),
        ]);
        $financial->save();
        $financial_id = $financial->id();

        //Reference with multiple entries
        $reference_fieldset = $form_state->getValue('reference_fieldset');
        foreach ($reference_fieldset as $reference_item)
        {
            $ref_paragraph = Paragraph::create([
                'type' => 'reference',
                'field_full_name' => array("value" => $reference_item['reference_full_name']),
                'field_relationship' => array("value" => $reference_item['reference_relationship']),
                'field_address' => array("value" => $reference_item['reference_address']),
                'field_mobile' => array("value" => $reference_item['reference_phone_number']),
                'field_landlord_name' => array("value" => $reference_item['reference_landlord_name'])
            ]);

            $ref_paragraph->save();
            $ref_targets[] = array(
                'target_id' => $ref_paragraph->id(),
                'target_revision_id' => $ref_paragraph->getRevisionId(),
            );
        }

        //Emergency Contact
        $emergency_contact = Paragraph::create([
            'type' => 'contact',
            'field_full_name' => $form_state->getValue('contact_full_name'),
            'field_relationship' => $form_state->getValue('contact_relationship'),
            'field_address' => $form_state->getValue('contact_address'),
            'field_mobile' => $form_state->getValue('contact_phone_number'),
        ]);
        $emergency_contact->save();
        $emergency_contact_id = $emergency_contact->id();
        
        //Rental Documents
        if ($form['#tree']) {
            $uploads = $form_state->getValue(array('nested', 'file'));
        } else {
            $uploads = $form_state->getValue('file');
        }
        
        if ($form['nested']['file']['#extended']) {
            $uploads = $uploads['fids'];
        }
        $fids = array();
        foreach ($uploads as $fid) {
            $fids[] = $fid;
        }

        //Questioneer
        $questioneer = Paragraph::create([
            'type' => 'questioneer',
            'field_do_you' => $form_state->getValue('field_do_you_smoke'),
            'field_illegal_drugs' => $form_state->getValue('field_have_you_ever_been_convict'),
            'field_evicted' => $form_state->getValue('field_have_you_ever_been_evicted'),
            'field_babankruptcy' => $form_state->getValue('field_have_you_ever_filed_for_ba'),
            'field_bedbugs' => $form_state->getValue('field_have_you_ever_had_bedbugs_'),
            'field_lequid_filled_furniture' => $form_state->getValue('field_lequid_filled_furnit'),
            'field_pets' => $form_state->getValue('field_will_you_have_pets_'),
        ]);
        $questioneer->save();
        $questioneer_id = $questioneer->id();

        /**
         * Create new applicant
         */
        $node = Node::create([
            'type' => 'applicant',
            'title' => 'Application Form - '. $f_name . ' ' . $l_name,
            'field_co_applicant_email' => $co_applicant_email,

            'field_about_me' => array(
                array(
                    'target_id' => $about_me_id,
                    'target_revision_id' => $about_me->getRevisionId(),
                )
            ),

            'field_residence' => array(
                array(
                    'target_id' => $residence_id,
                    'target_revision_id' => $residence->getRevisionId(),
                )
            ),

            'field_occupation' => array(
                array(
                    'target_id' => $occupation_id,
                    'target_revision_id' => $occupation->getRevisionId(),
                )
            ),

            'field_previous_occupation' => array(
                array(
                    'target_id' => $previous_occupation_id,
                    'target_revision_id' => $previous_occupation->getRevisionId(),
                )
            ),

            'field_financial' => array(
                array(
                    'target_id' => $financial_id,
                    'target_revision_id' => $financial->getRevisionId(),
                )
            ),

            'field_emergency_contact' => array(
                array(
                    'target_id' => $emergency_contact_id,
                    'target_revision_id' => $emergency_contact->getRevisionId(),
                )
            ),

            'field_questioneer' => array(
                array(
                    'target_id' => $questioneer_id,
                    'target_revision_id' => $questioneer->getRevisionId(),
                )
            ),
            'field_reference' => $ref_targets,
            'field_rental_document' => $fids
        ]);

        //Create Application Node
        $node->save();
        
        //Update User Information
        $user->set('field_first_name', $f_name);
        $user->set('field_last_name', $l_name);
        $user->set('field_mobile', $cell_phone);
        $user->save();
        
        //Apply To property
        $application = Node::create([
            'type' => 'application',
            'title' => $associated_property->title->value,
            'field_property' => $propertyid,
            'field_applicant' => $node->id(),
            'field_co_applicant' => $co_applicant_node->id(),
            'field_access_key' => $this->access_key
        ]);

        $application->save();
        
        $notification = Node::create([
            'type' => 'notification',
            'title' => $f_name . ' ' . $l_name . ' applied on property ' . $associated_property->title->value,
            'field_from' => $user->id(),
            'field_to' => $associated_property->getOwnerId(),
            'field_property' => $propertyid
        ]);

        $notification->save();

        //Redirect map page and return status message
        $response = new RedirectResponse('/property-listing');
        $response->send();
        drupal_set_message($this->t('Your application has been submitted for the property "'. $associated_property->title->value . '"'), 'status', TRUE);
    }
}
