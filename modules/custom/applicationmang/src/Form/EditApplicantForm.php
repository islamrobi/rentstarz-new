<?php
namespace Drupal\applicationmang\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class EditApplicantForm extends FormBase
{

    public $node;
    public $current_user;

    public $field_about_me;
    public $field_residence;
    public $field_occupation;
    public $field_previous_occupation;
    public $field_financial;
    public $field_emergency_contact;

    /**
     * Build the simple form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array $form
     *   Default form array structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        \Drupal::service('page_cache_kill_switch')->trigger();
        
        $this->current_user = \Drupal::currentUser();
        $user = User::load($this->current_user->id());

        //Check the user is authenticated
        if($this->current_user->isAuthenticated() === false){
            $response = new RedirectResponse('/user/login');
            $response->send();
            exit;
        }

        $applicant_id = NULL;

        
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'applicant')
            ->condition('uid', $this->current_user->id());
        
        $results = $query->execute();
        foreach($results as $result){
            $applicant_id = $result;
        }
        
        //Check the user does have application exist
        if(!isset($applicant_id)){
            $response = new RedirectResponse('/property-listing');
            $response->send();
            drupal_set_message($this->t('You haven\'t applied to any property yet.'), 'warning', TRUE);
            exit;
        }

        $this->node =  Node::load($applicant_id);

        // About me - Form Section
        $this->field_about_me = $this->getParagraph('field_about_me');
        $form['field_first_name'] = array(
            '#type' => 'textfield',
            '#title' => t('First Name:'),
            '#required' => true,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_first_name')
        );

        $form['field_last_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Last Name:'),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_last_name')
        );

        $form['field_birth_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Birth date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_birth_date')
        );

        $form['field_mobile'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Cell phone'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_mobile')
        );

        $form['field_email'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Email'),
            '#size' => 60,
            '#maxlength' => USERNAME_MAX_LENGTH,
            '#description' => $this->t(''),
            '#required' => true,
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_email')
        );
        
        $form['field_driving_license'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Driver license'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_about_me, 'field_driving_license')
        );

        // Residence - Form Section
        $this->field_residence = $this->getParagraph('field_residence');
        $form['residence_type'] = array(
            '#type' => 'radios',
            '#options' => array(
                'own' => $this->t('Own'),
                'rent' => $this->t('Rent')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_residence_type')
        );

        $form['address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_address')
        );

        $form['movin_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Movin date'),
            '#description' => $this->t('.'),
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_mov_in_date')
        );

        $form['city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_city')
        );

        $form['state'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('State'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_state')
        );

        $form['zip_code'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip Code'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_zip_code')
        );

        $form['rent'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Rent'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_rent')
        );

        $form['landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landlord Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_landlord_name')
        );

        $form['reason_for_leaving'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Reason for leaving'),
            '#default_value' => $this->getParagraphFieldValue($this->field_residence, 'field_reason_for_leaving')
        ];
        
        // Occupation
        $this->field_occupation = $this->getParagraph('field_occupation');
        $form['occupation'] = array(
            '#type' => 'radios',
            '#options' => array(
                'employed' => $this->t('Employed'),
                'self-employed' => $this->t('Self-employed'),
                'student' => $this->t('Student'),
                'unemployed' => $this->t('Unemployed')
            ),
            '#prefixes' => '',
            '#suffixes' => '',
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_occupation_type')
        );

        $form['employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_employer_name')
        );

        $form['job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_job_title')
        );

        $form['total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_monthly_income')
        );

        $form['work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_address')
        );

        $form['work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_city')
        );

        $form['work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_zip_code')
        );

        $form['work_type'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Type'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_work_type')
        );

        $form['work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_start_date')
        );

        $form['work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_supervisor_name')
        );

        $form['work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_occupation, 'field_mobile')
        );

        // Previous Occupation
        $this->field_previous_occupation = $this->getParagraph('field_previous_occupation');
        $form['previous_employer_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Employer Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_employer_name')
        );

        $form['previous_job_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Job Title'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_job_title')
        );

        $form['previous_total_montyly_income'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Total Monthly Income'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_monthly_income')
        );

        $form['previous_work_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Address'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_address')
        );

        $form['previous_work_city'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('City'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_city')
        );

        $form['previous_work_zip'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Zip'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_zip_code')
        );

        $form['previous_work_type'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Work Type'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_work_type')
        );

        $form['previous_work_start_date'] = array(
            '#type' => 'date',
            '#title' => $this->t('Start Date'),
            '#description' => $this->t(''),
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_start_date')
        );

        $form['previous_work_supervisor'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Supervisor Name'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_supervisor_name')
        );

        $form['previous_work_phone'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#description' => $this->t(''),
            '#required' => false,
            '#default_value' => $this->getParagraphFieldValue($this->field_previous_occupation, 'field_mobile')
        );

        // Financial
        $this->field_financial = $this->getParagraph('field_financial');
        $form['financial_bank_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_name')
        );

        $form['financial_bank_address'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Bank Address'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_bank_address')
        );

        $form['financial_account_type'] = array(
            '#type' => 'select',
            '#title' => $this->t('Account Type'),
            '#options' => [
                'current' => $this->t('Current'),
                'savings' => $this->t('Savings'),
            ],
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_account_type')
        );

        $form['financial_landlord_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Landloar Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_financial, 'field_landlord_name')
        );

        // Emergency Contact
        $this->field_emergency_contact = $this->getParagraph('field_emergency_contact');
        $form['contact_full_name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Full Name'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_full_name')
        );

        $form['contact_relationship'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Relationship'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_relationship')
        );

        $form['contact_address'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Adresss'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_address')
        );

        $form['contact_phone_number'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Phone Number'),
            '#default_value' => $this->getParagraphFieldValue($this->field_emergency_contact, 'field_mobile')
        );

        // Submit Button
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Save')];

        $form['#theme'] = 'edit_applicant_form';
        return $form;
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        
    }


    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array $form
     *   The render array of the currently built form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        //Applicant - Residence
        $this->field_about_me->set('field_birth_date', $form_state->getValue('field_birth_date'));
        $this->field_about_me->set('field_driving_license', $form_state->getValue('field_driving_license'));
        $this->field_about_me->set('field_email', $form_state->getValue('field_email'));
        $this->field_about_me->set('field_first_name', $form_state->getValue('field_first_name'));
        $this->field_about_me->set('field_last_name', $form_state->getValue('field_last_name'));
        $this->field_about_me->set('field_mobile', $form_state->getValue('field_mobile'));
        $this->field_about_me->save();
        
        //Applicant - Residence
        $this->field_residence->set('field_residence_type', $form_state->getValue('residence_type'));
        $this->field_residence->set('field_address', $form_state->getValue('address'));
        $this->field_residence->set('field_mov_in_date', $form_state->getValue('movin_date'));
        $this->field_residence->set('field_city', $form_state->getValue('city'));
        $this->field_residence->set('field_state', $form_state->getValue('state'));
        $this->field_residence->set('field_zip_code', $form_state->getValue('zip_code'));
        $this->field_residence->set('field_rent', $form_state->getValue('rent'));
        $this->field_residence->set('field_landlord_name', $form_state->getValue('landlord_name'));
        $this->field_residence->set('field_reason_for_leaving', $form_state->getValue('reason_for_leaving'));
        $this->field_residence->save();
        
        //Applicant - Occupation
        $this->field_occupation->set('field_occupation_type', $form_state->getValue('occupation'));
        $this->field_occupation->set('field_employer_name', $form_state->getValue('employer_name'));
        $this->field_occupation->set('field_job_title', $form_state->getValue('job_title'));
        $this->field_occupation->set('field_monthly_income', $form_state->getValue('total_montyly_income'));
        $this->field_occupation->set('field_work_address', $form_state->getValue('work_address'));
        $this->field_occupation->set('field_city', $form_state->getValue('work_city'));
        $this->field_occupation->set('field_zip_code', $form_state->getValue('work_zip'));
        $this->field_occupation->set('field_work_type', $form_state->getValue('work_type'));
        $this->field_occupation->set('field_start_date', $form_state->getValue('work_start_date'));
        $this->field_occupation->set('field_supervisor_name', $form_state->getValue('work_supervisor'));
        $this->field_occupation->set('field_mobile', $form_state->getValue('work_phone'));
        $this->field_occupation->save();

        //Applicant - Previous Occupation
        $this->field_previous_occupation->set('field_employer_name', $form_state->getValue('previous_employer_name'));
        $this->field_previous_occupation->set('field_job_title', $form_state->getValue('previous_job_title'));
        $this->field_previous_occupation->set('field_monthly_income', $form_state->getValue('previous_total_montyly_income'));
        $this->field_previous_occupation->set('field_work_address', $form_state->getValue('previous_work_address'));
        $this->field_previous_occupation->set('field_city', $form_state->getValue('previous_work_city'));
        $this->field_previous_occupation->set('field_zip_code', $form_state->getValue('previous_work_zip'));
        $this->field_previous_occupation->set('field_work_type', $form_state->getValue('previous_work_type'));
        $this->field_previous_occupation->set('field_start_date', $form_state->getValue('previous_work_start_date'));
        $this->field_previous_occupation->set('field_supervisor_name', $form_state->getValue('previous_work_supervisor'));
        $this->field_previous_occupation->set('field_mobile', $form_state->getValue('previous_work_phone'));
        $this->field_previous_occupation->save();

        //Applicant - Financial
        $this->field_financial->set('field_bank_name', $form_state->getValue('financial_bank_name'));
        $this->field_financial->set('field_bank_address', $form_state->getValue('financial_bank_address'));
        $this->field_financial->set('field_account_type', $form_state->getValue('financial_account_type'));
        $this->field_financial->set('field_landlord_name', $form_state->getValue('financial_landlord_name'));
        $this->field_financial->save();

        //Applicant - Emergency Contact
        $this->field_emergency_contact->set('field_full_name', $form_state->getValue('contact_full_name'));
        $this->field_emergency_contact->set('field_relationship', $form_state->getValue('contact_relationship'));
        $this->field_emergency_contact->set('field_address', $form_state->getValue('contact_address'));
        $this->field_emergency_contact->set('field_mobile', $form_state->getValue('contact_phone_number'));
        $this->field_emergency_contact->save();
        
        //Redirect map page and return status message
        $response = new RedirectResponse('/modal/view-application');
        $response->send();
        drupal_set_message($this->t('Application data has been updated.'), 'status', TRUE);
    }


    /*
     * Reference add more
    */
    public function referenceAddOne(array & $form, FormStateInterface $form_state)
    {
        $reference_count = $form_state->get('reference_count');
        $reference_add_button = $reference_count + 1;
        $form_state->set('reference_count', $reference_add_button);
        $form_state->setRebuild();
    }

    public function referenceAddMoreCallback(array & $form, FormStateInterface $form_state)
    {
        //$reference_count = $form_state->get('reference_count');
        return $form['reference_fieldset'];
    }

    public function referenceRemoveCallback(array & $form, FormStateInterface $form_state)
    {
        $reference_count = $form_state->get('reference_count');
        if ($reference_count > 1)
        {
            $reference_remove_button = $reference_count - 1;
            $form_state->set('reference_count', $reference_remove_button);
        }
        $form_state->setRebuild();
    }

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId()
    {
        return 'apply_property_form';
    }

    /**
     * Getter method for paragraph
     */
    public function getParagraph($name)
    {
        return Paragraph::load($this->node->get($name)->getValue()[0]['target_id']);
    }

    public function getParagraphFieldValue($paragraph, $name)
    {
        //return $paragraph->get($name)->getValue()[0]['value'];
        return $paragraph->$name->value;
    }
}