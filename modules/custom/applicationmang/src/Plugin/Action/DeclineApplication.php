<?php

namespace Drupal\applicationmang\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
// use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
// use Drupal\Core\Plugin\PluginFormInterface;
// use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * An example action covering most of the possible options.
 *
 * If type is left empty, action will be selectable for all
 * entity types.
 *
 * @Action(
 *   id = "applicationmang_decline_application",
 *   label = @Translation("Decline Application"),
 *   type = ""
 * )
 */

class DeclineApplication extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /*
     * All config resides in $this->configuration.
     * Passed view rows will be available in $this->context.
     * Data about the view used to select results and optionally
     * the batch context are available in $this->context or externally
     * through the public getContext() method.
     * The entire ViewExecutable object  with selected result
     * rows is available in $this->view or externally through
     * the public getView() method.
     */

    
    $entity->set('field_status', 'declined');
    $entity->save();
    
    //$this->messenger()->addMessage($entity->label() . ' has been canceled');
    drupal_set_message(t('Selected Application has been declined'), 'status');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }

}
