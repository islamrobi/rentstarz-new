<?php

namespace Drupal\applicationmang\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Entity\Index;
use Drupal\webprofiler\Config\ConfigFactoryWrapper;
use Drupal\Core\Config\Config;
use Drupal\paragraphs\Entity\Paragraph;


/**
 * ViewPropertyApplication REST API - GET
 *
 * @RestResource(
 *   id = "view-property-application",
 *   label = @Translation("View Property Application"),
 *   uri_paths = {
 *     "canonical" = "/rest/api/view-property-application/{nid}",
 *     "https://www.drupal.org/link-relations/create" = "/rest/api/view-property-application/{id}"
 *   }
 * )
 */

class ViewPropertyApplication extends ResourceBase {
    /**
     * A current user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;

    /**
     * The request object that contains the parameters.
    *
    * @var \Symfony\Component\HttpFoundation\Request
    */
    protected $request;

    /**
     * Drupal\webprofiler\Config\ConfigFactoryWrapper definition.
     *
     * @var \Drupal\webprofiler\Config\ConfigFactoryWrapper
     */
    protected $configFactory;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request object.
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     *   A current user instance.
     */

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user,
        Request $request, $config_factory) {
            parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
            $this->request = $request;
            $this->currentUser = $current_user;
            $this->configFactory = $config_factory;
        }
    
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('view-property-application'),
            $container->get('current_user'),
            $container->get('request_stack')->getCurrentRequest(),
            $container->get('config.factory')
        );
    }

    /**
     * Responds to GET requests.
     *
     * Returns a list of bundles for specified entity.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function get($nid) {
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $node = $node_storage->load($nid);

        $application = [];

        if(is_object($node)){
            $application['nid'] = $node->id();
            $application['created'] = \Drupal::service('date.formatter')->formatInterval(REQUEST_TIME - $node->created->value, 3).' ago';
            $application['title'] = $node->getTitle();
            
            // Personal Information
            $application['first_name'] = $node->get('field_first_name')->value; 
            $application['last_name'] = $node->get('field_last_name')->value; 
            $application['email'] = $node->get('field_email')->value; 
            $application['drivers_licence'] = $node->get('field_drivers_licence')->value; 
            $application['birth_date'] = $node->get('field_birth_date')->value; 
            $application['cell_phone'] = $node->get('field_phone')->value;
            
            // Current Residence
            /*
            $current_residence = $node->field_current_residence->getValue();
            foreach ( $current_residence as $cur_residence ) {
                $current = Paragraph::load($cur_residence['target_id']);
                $current_info['landlord_name'] = $current->get('field_landlord_name')->value;
                $current_info['phone_number'] = $current->get('field_phone_number')->value;
                $current_info['reason'] = $current->get('field_reason_for_leaving')->value;
                $current_info['rent'] = $current->get('field_rent')->value;
                $current_info['address'] = $current->get('field_address')->value;
                $current_info['city'] = $current->get('field_city')->value;
                $current_info['state'] = $current->get('field_state')->value;
                $current_info['zip'] = $current->get('field_zip')->value;
                $current_info['in_date'] = $current->get('field_move_in_date')->value;
            }
            $application['current_residence'] = $current_info;
            */
            // Previous Residence
            /*
            $previous_residence = $node->field_previous_residence->getValue();
            foreach ( $previous_residence as $pre_residence ) {
                $previous = Paragraph::load($pre_residence['target_id']);
                $previous_info['landlord_name'] = $previous->get('field_landlord_name')->value;
                $previous_info['phone_number'] = $previous->get('field_phone_number')->value;
                $previous_info['reason'] = $previous->get('field_reason_for_leaving')->value;
                $previous_info['rent'] = $previous->get('field_rent')->value;
                $previous_info['address'] = $previous->get('field_address')->value;
                $previous_info['city'] = $previous->get('field_city')->value;
                $previous_info['state'] = $previous->get('field_state')->value;
                $previous_info['zip'] = $previous->get('field_zip')->value;
                $previous_info['in_date'] = $previous->get('field_move_in_date')->value;
                $previous_info['out_date'] = $previous->get('field_move_out_date')->value;
            }
            $application['previous_residence'] = $previous_info;
            */

            //Residence
            $residence = $node->field_residence->getValue();
            $residence_info = Paragraph::load($residence[0]['target_id']);
            $application['residence']['type'] = $node->field_residence_type->getValue()[0]['value'];
            $application['residence']['address'] = $residence_info->get('field_address')->value;
            $application['residence']['movin_date'] = $residence_info->get('field_move_in_date')->value;
            $application['residence']['city'] = $residence_info->get('field_city')->value;
            $application['residence']['state'] = $residence_info->get('field_state')->value;
            $application['residence']['zip_code'] = $residence_info->get('field_zip')->value;
            $application['residence']['rent'] = $residence_info->get('field_rent')->value;
            $application['residence']['landlord_name'] = $residence_info->get('field_landlord_name')->value;
            $application['residence']['reason_for_leaving'] = $residence_info->get('field_reason_for_leaving')->value;
            
            // Occupation
            $application['occupation'] = $node->field_occupation_type->getValue()[0]['value'];
            $current_occupation = $node->field_occupation->getValue();
            $occupation_info = Paragraph::load($current_occupation[0]['target_id']);
            $application['current_occupation']['employer_name'] = $occupation_info->get('field_employer_name')->value;
            $application['current_occupation']['job_title'] = $occupation_info->get('field_job_title')->value;
            $application['current_occupation']['total_monthly_income'] = $occupation_info->get('field_total_monthly_income')->value;
            $application['current_occupation']['work_address'] = $occupation_info->get('field_work_address')->value;
            $application['current_occupation']['city'] = $occupation_info->get('field_work_city')->value;
            $application['current_occupation']['zip'] = $occupation_info->get('field_work_zip')->value;
            $application['current_occupation']['work_type'] = $occupation_info->get('field_work_type')->value;
            $application['current_occupation']['start_date'] = $occupation_info->get('field_work_start_date')->value;
            $application['current_occupation']['supervisor_name'] = $occupation_info->get('field_supervisor_name')->value;
            $application['current_occupation']['phone_number'] = $occupation_info->get('field_phone_number')->value;

            // Previous Occupation
            $previous_occupation = $node->field_previous_occupation->getValue();
            $previous_occupation_info = Paragraph::load($previous_occupation[0]['target_id']);
            $application['previous_occupation']['employer_name'] = $previous_occupation_info->get('field_employer_name')->value;
            $application['previous_occupation']['job_title'] = $previous_occupation_info->get('field_job_title')->value;
            $application['previous_occupation']['total_monthly_income'] = $previous_occupation_info->get('field_total_monthly_income')->value;
            $application['previous_occupation']['work_address'] = $previous_occupation_info->get('field_work_address')->value;
            $application['previous_occupation']['city'] = $previous_occupation_info->get('field_work_city')->value;
            $application['previous_occupation']['zip'] = $previous_occupation_info->get('field_work_zip')->value;
            $application['previous_occupation']['work_type'] = $previous_occupation_info->get('field_work_type')->value;
            $application['previous_occupation']['start_date'] = $previous_occupation_info->get('field_work_start_date')->value;
            $application['previous_occupation']['supervisor_name'] = $previous_occupation_info->get('field_supervisor_name')->value;
            $application['previous_occupation']['phone_number'] = $previous_occupation_info->get('field_phone_number')->value;

            // Financial
            $financial = $node->field_financial->getValue();
            $financial_info = Paragraph::load($financial[0]['target_id']);
            $application['financial']['bank_name'] = $financial_info->get('field_bank_name')->value;
            $application['financial']['bank_address'] = $financial_info->get('field_bank_address')->value;
            $application['financial']['account_type'] = $financial_info->get('field_account_type')->value;
            $application['financial']['landloar_name'] = $financial_info->get('field_financial_landlord_name')->value;

            // Reference
            $references = $node->field_reference->getValue();
            $reference_info = array();
            foreach ( $references as $key => $reference ) {
                $reference_ar = Paragraph::load($reference['target_id']);
                $target_id = $reference['target_id'];
                $reference_info[$key]['name'] = $reference_ar->get('field_full_name')->value;
                $reference_info[$key]['phone_number'] = $reference_ar->get('field_phone_number')->value;
                $reference_info[$key]['relationship'] = $reference_ar->get('field_relationship')->value;
                $reference_info[$key]['email'] = $reference_ar->get('field_email')->value;
            }
            $application['reference'] = $reference_info;

            // Emergency Contact
            $emergency_contact = $node->field_emergency_contact->getValue();
            $emergency_contact_info = Paragraph::load($emergency_contact[0]['target_id']);
            $application['emergency_contact']['full_name'] = $emergency_contact_info->get('field_emc_full_name')->value;
            $application['emergency_contact']['relationship'] = $emergency_contact_info->get('field_emc_relationship')->value;
            $application['emergency_contact']['adresss'] = $emergency_contact_info->get('field_emc_address')->value;
            $application['emergency_contact']['phone_number'] = $emergency_contact_info->get('field_emc_phone_number')->value;

            // Rental Documents
            $file_urls = array();
            foreach ($node->field_rental_document as $index => $document) {
                if ($document->entity) {
                    $file_urls[$index]['name'] = $document->entity->getFilename();
                    $file_urls[$index]['url'] = $document->entity->url();
                }
            }
            $application['rental_documents'] = $file_urls;

            // Job
            /*
            $jobs = $node->field_employment->getValue();
            foreach ( $jobs as $job ) {
                $job_ar = Paragraph::load($job['target_id']);
                $job_info['employer_name'] = $job_ar->get('field_employer_name')->value;
                $job_info['phone_number'] = $job_ar->get('field_phone_number')->value;
                $job_info['years'] = $job_ar->get('field_years_of_employment')->value;
            }
            $application['job'] = $job_info;

            // Income
            $income = $node->field_income->getValue();
            foreach ( $income as $inc ) {
                $inc_ar = Paragraph::load($inc['target_id']);
                $target_id = $inc['target_id'];
                $inc_info[$target_id]['amount'] = $inc_ar->get('field_amount')->value;
                $inc_info[$target_id]['source'] = $inc_ar->get('field_income_source')->value;
            }
            $application['income_info'] = $inc_info;
            */
            // Family
            /*
            $family = $node->field_family_members->getValue();
            foreach ( $family as $member ) {
                $mem_ar = Paragraph::load($member['target_id']);
                $target_id = $member['target_id'];
                $mem_ar_info[$target_id]['age'] = $mem_ar->get('field_age')->value;
                $mem_ar_info[$target_id]['name'] = $mem_ar->get('field_full_name')->value;
                $mem_ar_info[$target_id]['relation'] = $mem_ar->get('field_relationship')->value;
            }
            $application['family_info'] = $mem_ar_info;
            */
        }

        $response = new ResourceResponse($application);
        $response->addCacheableDependency($application);
                
        return $response;
    }

    function get_files($files){
        $img_urls = array();

        foreach ($files as $item) {
            if ($item->entity) {
                $img_urls[] = $item->entity->url();
            }
        }

        return $img_urls;
    }
}