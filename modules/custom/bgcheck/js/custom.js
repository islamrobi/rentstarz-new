(function($) {

    /*	$(document).ready( function () {
    	    $('#data-table').DataTable();
    	} );*/

    $('.screening .btn.loader, .screening .link.loader').on('click', function() {
        if ($('.loading').length) {
            $('.loading').fadeIn('fast');
        }
    });

    // PACKAGES SELECTION FOR THE SCREENING FORM
    $('.screening .packages .package').on('click', function() {
        $('.screening .packages, .package').removeClass('active');
        $(this).addClass('active');
        $('#edit-field-package-' + $(this).data('value')).prop("checked", true);

        // screening data form select package
        $('#edit-field-report-types').val($(this).data('value'));

        // $('.pricing .amount').attr('data-amount', $(this).data('amount'));
        // calcPricing();
    });


    // Calculate Pricing
    function calcPricing() {
        var pricingDiv = $('.pricing');
        var amount = pricingDiv.find('.amount').attr('data-amount');
        //console.log(amount);
        pricingDiv.find('.amount span').html('$' + parseFloat(amount).toFixed(2));

        var tax = pricingDiv.find('.tax').attr('data-tax');
        taxAmount = (tax / 100) * amount;
        //console.log(taxAmount);
        pricingDiv.find('.tax span').html('$' + parseFloat(taxAmount).toFixed(2));
        var total = +taxAmount + +amount;
        pricingDiv.find('.total span').html('$' + parseFloat(total).toFixed(2));
    }

    $('#reqtable').DataTable({
        "pageLength": 20,
        "ordering": false
    });

    if ($('input#edit-field-email-0-value').length) {
        $('.screening #req-email .badge').html($('#edit-field-email-0-value').val());
    }

    // REPORT TABS SHOW/HIDE
    if ($('.screening .reports').length) {
        $('.screening .reports-link').on('click', function(e) {
            e.preventDefault();
            $('.screening .reports-link').removeClass('active');
            $(this).addClass('active');
            $('.screening .reports-tabs .reports-tab').fadeOut();
            $('.screening .reports-tabs').find('[data-type="' + $(this).data('type') + '"]').fadeIn();
        });
    }

    // MENU ITEM HIDE FOR LANDLORD/AGENT
    if ($('.block').length) {
        var isAgentorLandlord = false;
        $('.block .nav .nav-item').each(function() {
            if ($(this).find('a.nav-link[href="/screening/landlord"]').length) {
                isAgentorLandlord = true;
            }
        });
        if (isAgentorLandlord) {
            $('.block .nav .nav-item a.nav-link[href="/screening/renter"]').each(function() {
                $(this).closest('li.nav-item').hide();
            });
        }
    }

    // CHANGE PACKAGE ACCORDING TO REPORT TYPES
    if ($('.screening #edit-field-report-types').length) {
        changePackage();
        $('.screening #edit-field-report-types').on('change',function(){
            changePackage();
        });
    }

    function changePackage(){
        if ($('.package.card').length) {
            var v = $('.screening #edit-field-report-types').val();
            $('.package.card[data-value="' + v + '"]').trigger('click');
        }        
    }

    // ADD EMAIL VALIDATION PATTERN ON SCR REQ FORM
    /*	var inp = $('.node-screqs-renter-form').find('input#edit-field-email-0-value');
    	if(inp.length){
    		inp.attr('pattern','[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$');
    	}*/

    // RESTRICT DATE FIELD TO 10 CHAR MAX
    // if($('.screening input[type="date"]').length){
    //     $('.screening input[type="date"]').on('keyup',function(){
    //         if($(this).val().length > 10){
    //             $(this).val($(this).val().substr(0,10));
    //         }
    //     });
    // }


    if($('.btn-print').length){
        $('.btn-print').on('click',function(e){
            e.preventDefault();
            window.print();
            //var printElm = $('.reports .reports-tab.active');
            //printDiv('.reports .reports-tab[data-type="Criminal"]');
        });
    }

    function printDiv(elm) {
        var divToPrint = document.querySelectorAll(elm)[0];
        console.log(divToPrint);
        var newWin = window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
        //newWin.document.close();
        //setTimeout(function(){ newWin.close();},10 );
    }


}(jQuery));