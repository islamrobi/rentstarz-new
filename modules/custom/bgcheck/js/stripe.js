/**
 * @file
 * Contains js 
 */

(function ($, Drupal, drupalSettings) {  

	//console.log(drupalSettings);

	if($('.screening .payment #stripe_card_element').length > 0){

		// CONFIGURE & GENERATE THE UI
		//var stripe = Stripe('pk_test_LdDQeZA7C2svmafjzisKhX2Y00VhN6NH5e');
		var stripe = Stripe(drupalSettings.published_key);
		var elements = stripe.elements();
		// Set up Stripe.js and Elements to use in checkout form
		var style = {
		    base: {
		        color: '#32325d',
		        fontFamily: 'Cairo, sans-serif',
		        fontSmoothing: 'antialiased',
		        border: '1px solid #eee',
		        fontSize: '18px',
		        '::placeholder': {
		            color: '#aab7c4'
		        }
		    },
		    invalid: {
		        color: '#fa755a',
		        iconColor: '#fa755a'
		    }
		};
		var card = elements.create("card", { style: style });
		card.mount("#stripe_card_element");

		// ERROR HANDLING
		card.addEventListener('change', function(event) {
			var displayError = document.getElementById('stripe_card_errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});

		// SUBMIT ACTION
		var allDone = false;
		var errs = $('.errs');
		//var form = document.getElementById('payment-form');
		var form = $('.payment-form');
		form.on('submit', function(ev) {
			if(!allDone){
				ev.preventDefault();
				return false;
			}
		});

		// Custom Submit Button Action
		var payBtn = $('.stripe_pay_btn');
		var frmValidated = true;
		var emailKey = $('#email_key').val();
		//console.log(emailKey);
		payBtn.on('click', function(e) {
			e.preventDefault();

			// CHECK STRIPE CARD VALIDATION
			if(!$('#stripe_card_element').hasClass('StripeElement--complete')){
				$('#stripe_card_errors').html('Please enter card information properly');
				frmValidated = false;
			}

			if(frmValidated){

				$('.loading').fadeIn('fast');

				// GET THE CLIENT SECRET
		        $.ajax({
		            //url: Drupal.url('api/stripe/payment'), 
		            //url: Drupal.url(drupalSettings.stripe_rest_url+'?package='+package), 
					url: Drupal.url(drupalSettings.stripe_rest_url+'?key='+emailKey), 
					//url: Drupal.url(drupalSettings.stripe_rest_url), 
		            method: 'GET',
		            //data: { tokenId: token.id, amount: amount },
		            //dataType: "json",
		            success: function(response) {

		            	//console.log(response); return;
		                var clientSecret = response.data.client_secret;

		                if(clientSecret.length > 0){

			                // SEND THE CARD PAYMENT
				  			stripe.confirmCardPayment(clientSecret, {
								payment_method: {
									card: card,
									billing_details: {
				        				//user_id: response.data.client_id,
				        				name: response.data.name,
				        				email: response.data.email,
				        				//billing_name: form.find('.given-name').val() + ' ' + form.find('.family-name').val(),
				        				//billing_address: form.find('.address-line1').val() + '' + form.find('.postal-code').val() + ', ' + form.find('.locality').val() + ' ' + form.find('.administrative-area').val(),
				      				}
				    			}
							}).then(function(result) {
								//console.log(result);
					    		if (result.error) {
					      			// Show error to your customer (e.g., insufficient funds)
					      			//console.log(result.error);
					      			errs.fadeIn('fast');
					      			errs.html('Cannot process payment ('+ result.error.message +')');
					      			$('.loading').hide();

					    		} else {
						      		// The payment has been processed!
						      		if (result.paymentIntent.status == "succeeded" || result.paymentIntent.status == "requires_capture") {
								        // Show a success message to your customer
								        // There's a risk of the customer closing the window before callback
								        // execution. Set up a webhook or plugin to listen for the
								        // payment_intent.succeeded event that handles any business critical
								        // post-payment actions.
								        //console.log('Success Response:',result);

								        $('#edit-metadata').val(JSON.stringify(result));

								        // REDIRECT TO POST PROCESSING URL
		 						        //window.location.replace(Drupal.url(drupalSettings.payment_post_process_url));
		 						        allDone = true; form.submit();
									}
					    		}
				  			});
						}

		            },
		            error: function(err){
		            	//console.log('Error:',err);
		            	errs.fadeIn('fast');
		            	errs.html('Cannot process payment, Please contact administrator');
		            	$('.loading').hide();
		            }
		        });
		    }

		});
	}

})(jQuery, Drupal, drupalSettings)