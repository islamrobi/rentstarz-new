<?php

namespace Drupal\bgcheck\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bgcheck\Helpers\DBHelper;

/**
 * Defines APITestController class.
 */
class APITestController extends ControllerBase {

    public $API_BASE_URL;
    public $PARTNER_ID;
    public $API_KEY;
    public $CLIENT_ID;

    public function __construct(){

/*        $this->PARTNER_ID = '1027';
        $this->CLIENT_ID = 'ujEaNRJvZr1UAb-zQYZ-Q9F0D0N1va';
        $this->API_KEY = 'k5dVIemRsGP*0CKBJY&ppoXSE*lrOx3qwc$SgdyqZ';
        $this->API_BASE_URL = 'https://rentals-api-ext.shareable.com/v1/';      */

        $bgcheckConfig = \Drupal::config('bgcheck.api_settings'); 
        $this->PARTNER_ID = $bgcheckConfig->get('bgc_partner_id');
        $this->CLIENT_ID = $bgcheckConfig->get('bgc_client_id');
        $this->API_KEY = $bgcheckConfig->get('bgc_api_key');
        $this->API_BASE_URL = $bgcheckConfig->get('bgc_api_base_url');   

    }


    public function debugObj($obj){
        echo '<pre>' . var_export($obj,true) . '</pre>'; die();
        //kint($obj);
    }


    // ---------------------------------------------------------------
    // GET THE SECURITY TOKEN
    // ---------------------------------------------------------------
    public function getSequrityToken(){

        // Pass the necessary credentials
        $authData = [ "ClientId" => $this->CLIENT_ID, "apiKey" => $this->API_KEY ]; 
        //$this->debugObj($authData);

        // MAKE THE HTTP CALL FOR TOKEN
        $client = \Drupal::httpClient();
        $res = $client->request('POST',$this->API_BASE_URL."Tokens", [
        	'http_errors' => false,
            'json' => $authData,
            'Content-type' => 'application/json',
        ]);

        // GET AND RETURN THE TOKEN
        $token = null;
        if($res->getStatusCode() == 200 || $res->getStatusCode() == 201){
            $tokenArr = json_decode($res->getBody());
            $token = $tokenArr->token;
        }
        return $token;      

    }

    // ---------------------------------------------------------------
    // GET A RENTER DATA
    // ---------------------------------------------------------------
    function getLookupData($enumerationName){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Lookups", [
            	'http_errors' => false,
            	'query' => [ 'enumerationName' => $enumerationName ],
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }

    // ---------------------------------------------------------------
    // GET A LOANDLORD DATA
    // ---------------------------------------------------------------
    function getLandlord($landlordId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }


    // ---------------------------------------------------------------
    // CREATE A NEW LANDLORD DATA
    // ---------------------------------------------------------------
    function createLandlord(){

        $sequrityToken = $this->getSequrityToken();
        $landlord = [
            //"landlordId" => 0,
            "emailAddress" => "mike@example.com",
            "firstName" => "Michael",
            "lastName" => "Bertand",
            "phoneNumber" => "3037777777",
            "phoneType" => "Mobile",
            "businessName" => "business",
            "businessAddress" => [
                "addressLine1" => "51 West 52nd Street",
                "addressLine2" => "",
                "addressLine3" => "",
                "addressLine4" => "",
                "locality" => "local",
                "region" => "AZ",
                "postalCode" => "12345",
                "country" => "US"
            ],
            "acceptedTermsAndConditions" => true
        ];

        try {

             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."Landlords", [
            	'http_errors' => false,
                'json' => $landlord,
                //'debug' => true,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
            //$response = $client->send($request);
            //$request->getHeader('Authorization');
        }
        catch (BadResponseException $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        //return $response;
        //'landlordId' => 4077,

    }


   // ---------------------------------------------------------------
    // CREATE A NEW PROPERTY DATA
    // ---------------------------------------------------------------
    function createProperty($landlordId){

        //echo $this->API_BASE_URL."Landlords/{$landlordId}/Properties"; die();

        $sequrityToken = $this->getSequrityToken();
        $property = [
            //"propertyId" => 0,
            "propertyName" => "the boston",
            "rent" => 2000,
            "deposit" => 3000,
            "isActive" => true,
            "addressLine1" => "165 cambridge street",
            "addressLine2" => "",
            "addressLine3" => "",
            "addressLine4" => "",
            "locality" => "brooklyn",
            "region" => "AZ",
            "postalCode" => "12345",
            "country" => "US",
            "bankruptcyCheck" => true,
            "bankruptcyTimeFrame" => 120,
            "incomeToRentRatio" => 0
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."Landlords/{$landlordId}/Properties", [
            	'http_errors' => false,
                'json' => $property,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // propertyId -> integer 2444

    }

    // ---------------------------------------------------------------
    // GET A PROPERTY DATA
    // ---------------------------------------------------------------
    function getProperty($landlordId,$propertyId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}/Properties/{$propertyId}", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }

    // ---------------------------------------------------------------
    // GET A PROPERTY DATA
    // ---------------------------------------------------------------
    function getProperties($landlordId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}/Properties", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }

    // ---------------------------------------------------------------
    // GET BUNDLE DATA
    // ---------------------------------------------------------------
    function getBundle(){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Bundles", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        // GET AND RETURN THE TOKEN
        $bundle = null;
        if($res->getStatusCode() == 200){
            $bundleArr = json_decode($res->getBody());
            $bundle = $bundleArr->bundle;
        }
        return $bundle;    

    }

    // ---------------------------------------------------------------
    // CREATE A NEW SCREENING REQUEST DATA
    // ---------------------------------------------------------------
    function createScrRequest($landlordId,$propertyId){

        //echo $this->API_BASE_URL."Landlords/{$landlordId}/Properties"; die();

        $sequrityToken = $this->getSequrityToken();
        $scrReq = [
            //"screeningRequestId": 0,
            "landlordId" => $landlordId,
            "propertyId" => $propertyId,
            "initialBundleId" => 3, // 2 - Basic // 3 - Plus
            //"createdOn" => "2020-02-01T10:29:11.473Z",
            //"modifiedOn" => "2020-02-01T10:29:11.473Z",
            //"propertyName" => "Test Property",
            //"propertySummaryAddress" => "Test Property Address",
            "screeningRequestRenters" => []
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."ScreeningRequests", [
            	'http_errors' => false,
                'json' => $scrReq,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // scrReqId -> integer 102140

    }

    // ---------------------------------------------------------------
    // GET SCRENNING REQUESTS FOR A RENTER
    // ---------------------------------------------------------------
    public function getLandlordScrReqs($landlordId){

    	$sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}/ScreeningRequests", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        } 

    }


    // ---------------------------------------------------------------
    // GET SCRENNING REQUESTS FOR A RENTER
    // ---------------------------------------------------------------
    public function getRenterScrReqs($renterId){

    	$sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Renters/{$renterId}/ScreeningRequests", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        } 

    }



    // ---------------------------------------------------------------
    // GET A RENTER DATA
    // ---------------------------------------------------------------
    function getRenter($renterId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Renters/{$renterId}", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }

    // ---------------------------------------------------------------
    // CREATE A NEW RENTER DATA
    // ---------------------------------------------------------------
    function createRenter(){

        $sequrityToken = $this->getSequrityToken();
        $renter = [
            "person" => [
                //"personId" => 0,
                "emailAddress" => "person1@example.com",
                "firstName" => "Anna",
                "middleName" => "Jane",
                "lastName" => "Doe",
                "phoneNumber" => "3711111111",
                "phoneType" => "Home",
                "socialSecurityNumber" => "345124578----",
                "dateOfBirth" => "1990-02-01T00:00:00",
                "homeAddress" => [
                    "addressLine1" => "51 West 52nd Street",
                    "addressLine2" => "",
                    "addressLine3" => "",
                    "addressLine4" => "",
                    "locality" => "local",
                    "region" => "AZ",
                    "postalCode" => "12345",
                    "country" => "US",
                ],
                "acceptedTermsAndConditions" => true
            ],
            "income" => 10000,
            "incomeFrequency" => "PerMonth",
            "otherIncome" => 5000,
            "otherIncomeFrequency" => "PerMonth",
            "assets" => 200000,
            "employmentStatus" => "Employed"
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."Renters", [
                'json' => $renter,
                'http_errors' => false,
                //'debug' => true,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            //kint($res);
            //$statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody(true));
            // If Any Client Erros
            if($res->getStatusCode() == 422 && $response->errors){
                foreach ($response->errors as $err) {
                    echo('<div class="error"><pre>'.$err->message.'</pre></div>');
                }       
            }else if($res->getStatusCode() == 200 || $res->getStatusCode() == 201){
                $this->debugObj($response);
                //return $response->something;
            }
            //$this->debugObj($res->getStatusCode());
        }
        catch (GuzzleException $error) {
            //$response = $ex->getResponse();
            //$jsonBody = (string) $response->getBody();
            // $logger = \Drupal::logger('HTTP Client error');
            // $logger->error($error->getMessage());
            //kint($error->getResponse()->getBody()->getContents());
            //throw $ex;
        }        

        //return $response;
        //'renter' => 4081,

    }


    // ---------------------------------------------------------------
    // CREATE A NEW RENTER DATA
    // ---------------------------------------------------------------
    function editRenter($renterId){

        $sequrityToken = $this->getSequrityToken();
        $renter = [
            "person" => [
                "personId" => $renterId,
                "emailAddress" => "lara.sergio@example.com",
                "firstName" => "SERGIO",
                "middleName" => "",
                "lastName" => "LARA",
                "phoneNumber" => "1234567890",
                "phoneType" => "Mobile",
                "socialSecurityNumber" => "666827448",
                "dateOfBirth" => "1981-01-01T00:00:00",
                "homeAddress" => [
                    "addressLine1" => "1815 E WATERBERRY DR",
                    "addressLine2" => "",
                    "addressLine3" => "",
                    "addressLine4" => "",
                    "locality" => "HURON",
                    "region" => "OH",
                    "postalCode" => "44839",
                    "country" => "US",
                ],
                "acceptedTermsAndConditions" => true
            ],
		   'income' => 10000.0,
		   'incomeFrequency' => 'PerMonth',
		   'otherIncome' => 6000.0,
		   'otherIncomeFrequency' => 'PerMonth',
		   'assets' => 20000.0,
		   'employmentStatus' => 'Employed',
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('PUT',$this->API_BASE_URL."Renters", [
                'json' => $renter,
                'http_errors' => false,
                //'debug' => true,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            //kint($res);
            echo $res->getStatusCode();
            $response = json_decode($res->getBody(true));
            // If Any Client Erros
            if($res->getStatusCode() == 422 && $response->errors){
                foreach ($response->errors as $err) {
                    echo('<div class="error"><pre>'.$err->message.'</pre></div>');
                }       
            }else if($res->getStatusCode() == 200 || $res->getStatusCode() == 201){
                $this->debugObj($response);
                //return $response->something;
            }
            //$this->debugObj($res->getStatusCode());
        }
        catch (GuzzleException $error) {

        }        

        //return $response;
        //'renter' => 4081,

    }


    // ---------------------------------------------------------------
    // GET A RENTER DATA
    // ---------------------------------------------------------------
    function getScrReqRenter($screeningRequestRenterId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }

    // ---------------------------------------------------------------
    // CREATE A NEW SCREENING REQUEST RENTER DATA
    // ---------------------------------------------------------------
    function createScrReqRenter($screeningRequestId,$landlordId,$renterId){

        $sequrityToken = $this->getSequrityToken();
        $scrReqRenter = [
            //"screeningRequestRenterId": 0,
            "landlordId" => $landlordId,
            "renterId" => $renterId,
            "bundleId" => 5003,
            "renterRole" => "Applicant", // Applicant, CoSigner
            "renterStatus" => "IdentityVerificationPending", // IdentityVerificationPending, ScreeningRequestCanceled, ReadyForReportRequest, PaymentFailure, ReportsDeliveryInProgress, ReportsDeliveryFailed, ReportsDeliverySuccess, RetryLimitExceeded, ScreeningRequestExpired
            //"createdOn": "2020-02-01T10:29:11.473Z",
            //"modifiedOn": "2020-02-01T10:29:11.473Z",
            //"renterFirstName": "",
            //"renterLastName": "",
            //"renterMiddleName": "",
            //"reportsExpireNumberOfDays": 0
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."ScreeningRequests/{$screeningRequestId}/ScreeningRequestRenters", [
            	'http_errors' => false,
                'json' => $scrReqRenter,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // screeningRequestRenterId -> integer 1589

    }


    // ---------------------------------------------------------------
    // VALIDATE A SCREENING REQ RENTER DATA
    // ---------------------------------------------------------------
    function validateScrReqRenter($screeningRequestRenterId){

        $sequrityToken = $this->getSequrityToken();
        $personData = [
            "personId" => 0,
            "emailAddress" => "renter@example.com",
            "firstName" => "Anna",
            "middleName" => "Jane",
            "lastName" => "Doe",
            "phoneNumber" => "3711111111",
            "phoneType" => "Home",
            "socialSecurityNumber" => "345124578",
            "dateOfBirth" => "1990-02-01T00:00:00",
            "homeAddress" => [
                "addressLine1" => "51 West 52nd Street",
                "addressLine2" => "",
                "addressLine3" => "",
                "addressLine4" => "",
                "locality" => "local",
                "region" => "AZ",
                "postalCode" => "12345",
                "country" => "US",
            ],
            "acceptedTermsAndConditions" => true
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Validate", [
                'json' => $personData,
                'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // examId -> integer 8869

    }

    // ---------------------------------------------------------------
    // GET getScrReqRenterReportNames
    // ---------------------------------------------------------------
    function getScrReqRenterReportNames($screeningRequestRenterId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports/Names", [
            	'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());

    }


    // ---------------------------------------------------------------
    // CREATE A NEW EXAM DATA
    // ---------------------------------------------------------------
    function createExam($screeningRequestRenterId){

        $sequrityToken = $this->getSequrityToken();
        $examData = [
            "person" => [
                //"personId" => $renterId,
                "emailAddress" => "lara.sergio@example.com",
                "firstName" => "SERGIO",
                "middleName" => "",
                "lastName" => "LARA",
                "phoneNumber" => "1234567890",
                "phoneType" => "Mobile",
                "socialSecurityNumber" => "666827448",
                "dateOfBirth" => "1981-01-01T00:00:00",
                "homeAddress" => [
                    "addressLine1" => "1815 E WATERBERRY DR",
                    "addressLine2" => "",
                    "addressLine3" => "",
                    "addressLine4" => "",
                    "locality" => "HURON",
                    "region" => "OH",
                    "postalCode" => "44839",
                    "country" => "US",
                ],
                "acceptedTermsAndConditions" => true
            ],
            "externalReferenceNumber" => "string"
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Exams", [
            	'http_errors' => false,
                'json' => $examData,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // examId -> integer 8869

    }


    // ---------------------------------------------------------------
    // CREATE A NEW EXAM DATA
    // ---------------------------------------------------------------
    function createAnswers($screeningRequestRenterId,$examId){

        $sequrityToken = $this->getSequrityToken();
        $answers = [
            "answers" => [
                [ "questionKeyName" => "ACX_RCR_REL_CITY_DYNAMIC", "selectedChoiceKeyName" => "Alfred Ingram" ],
                [ "questionKeyName" => "ACX_ID2_ASSOC_ST_NAME", "selectedChoiceKeyName" => "123rd" ],
                [ "questionKeyName" => "ACX_RCR_HUNT_LIC_TYPE", "selectedChoiceKeyName" => "Deer" ]
            ]
        ];

        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Exams/{$examId}/Answers", [
                'json' => $answers,
                'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            echo $res->getStatusCode();
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // examId -> integer 8869

    }

    // ---------------------------------------------------------------
    // CREATE A NEW EXAM DATA
    // ---------------------------------------------------------------
    function createReport($screeningRequestRenterId,$renterId){

        $sequrityToken = $this->getSequrityToken();
        $personData = [
            "person" => [
                "personId" => $renterId,
                "emailAddress" => "lara.sergio@example.com",
                "firstName" => "SERGIO",
                "middleName" => "",
                "lastName" => "LARA",
                "phoneNumber" => "1234567890",
                "phoneType" => "Mobile",
                "socialSecurityNumber" => "666827448",
                "dateOfBirth" => "1981-01-01T00:00:00",
                "homeAddress" => [
                    "addressLine1" => "1815 E WATERBERRY DR",
                    "addressLine2" => "",
                    "addressLine3" => "",
                    "addressLine4" => "",
                    "locality" => "HURON",
                    "region" => "OH",
                    "postalCode" => "44839",
                    "country" => "US",
                ],
                "acceptedTermsAndConditions" => true
            ],
        ];
		
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports", [
                'json' => $personData,
                'http_errors' => false,
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json' ]                
            ]);
            echo $res->getStatusCode();
            $this->debugObj(json_decode($res->getBody()));
        }
        catch (Exception $ex) {
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            $this->debugObj($jsonBody);
        }        

        // examId -> integer 8869

    }


    // ---------------------------------------------------------------
    // GET A REPORT DATA
    // ---------------------------------------------------------------
    function getReport($screeningRequestRenterId){

        $sequrityToken = $this->getSequrityToken();
        try {
             // MAKE THE HTTP CALL
            $client = \Drupal::httpClient();
            $res = $client->request('GET',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports", [
                'http_errors' => false,
                'query' => [ 'requestedProduct' => 'criminal' ],
                'headers' => [ 'authorization' => $sequrityToken,'Content-type' => 'application/json', ]                
            ]);
            $this->debugObj( json_decode( $res->getBody() ) );
            // $response = json_decode( $res->getBody() );
            // $reportArr = $response->reportResponseModelDetails; 
            // return $reportArr[0]->reportData;

        }
        catch (Exception $ex) {
        }        
        //return json_decode($res->getBody());
    }

    /**
    * Display the markup.
    *
    * @return array
    *   Return markup array.
    */
    public function content() {

    	//$this->getLookupData('ScreeningRequestInvitationStatus'); 
    	// AddressType, CardType, PaymentType, PhoneType, RoleType, SalaryRange, ScreeningRequestInvitationStatus, ScreeningRequestRenterStatus
        
        //$this->createLandlord(); // 4077 // 4178
        //$this->getLandlord(102418); //81450 // 4255 // 4413
        //$this->createProperty(4178); // 2444 // 2543
        //$this->getProperty(102418,53057);
        //$this->getProperties(4413);
        $this->getBundle();
        //$this->createScrRequest(4296,2625); // 102140 // 102217 // 102473
        //$this->getLandlordScrReqs(102418); //101623 //87087 //82913 //81589 //74088 // 5600 // 4413 // 5600
        //$this->getRenterScrReqs(6795); // 6795
        //$this->getScrReqRenterReportNames(1779);
        //$this->createRenter(); // 4081 
        //$this->getRenter(4470); // 5754 // 4479 // 5754 // 6795 // 5354
        //$this->editRenter(4470);
        //$this->createScrReqRenter(103156,4413,5354); // 1589 // 102140,4077,4081
       	//$this->getScrReqRenter(38413); // 1903 // 10048 // 28042 // 36937 // 37767
        //$this->validateScrReqRenter(1589);
        //$this->createExam(1980); // 8871 // 1589 // 2708 // 2490
        //$this->createAnswers(1980,85810); 
        //$this->createReport(1980,4470); // 1589 // 4081
        //$this->getReport(2708);
        //$reportData = $this->getReport(2708);
        die();

        return [
            '#type' => 'markup',
            '#markup' => $this->t('<div style="padding: 100px 0;">'.$reportData.'</div>'),
        ];
    }

}