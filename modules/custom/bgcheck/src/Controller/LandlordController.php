<?php

namespace Drupal\bgcheck\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines HelloController class.
 */
class LandlordController extends ControllerBase {

    public function __construct(){

    }

	// ------------------------------------------------------------
	// LANDLORD DASHBOARD
	// ------------------------------------------------------------
    public function dashboard() {
		//$package = Helper::getPackageInfo('renter2s@yopmail.com'); kint($package); die();
        $landlordUserId = \Drupal::currentUser()->id();

        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $query = \Drupal::entityQuery('node')->condition('type', 'screqs')
        	->condition('uid', $landlordUserId)
			->condition('status', 1)
			->sort('field_updated' , 'DESC')
			->sort('changed' , 'DESC');
		$scrReqNodeIds = $query->execute();
		$scrReqNodeIds = array_values($scrReqNodeIds);

		$renters = [];
		foreach ($scrReqNodeIds as $scrReqNodeId) {

			$scrReqNode = Node::load($scrReqNodeId);
			$renterUserId = Helper::getUserByEmail($scrReqNode->field_email->value);
			$reportShared = $scrReqNode->field_updated->getValue()[0]['value'];
			$scrReqRenterId = 0;
			if(isset($scrReqNode->field_api_entity_id->getValue()[4]['value'])){
				$scrReqRenterId = $scrReqNode->field_api_entity_id->getValue()[4]['value'];						
			}
			$emailKey = base64_encode($scrReqNode->field_email->value);
			$renters[] = [
				'id' => $renterUserId,
				'scrReqNodeId' => $scrReqNode->id(),
				'name' => $scrReqNode->field_first_name->value,
				'email' => $scrReqNode->field_email->value,
				'date' => explode('-',format_date($scrReqNode->changed->value,'short'))[0],
				'payer' => $scrReqNode->field_payer->value,
				'payment' => $scrReqNode->field_payment_status->value,
				'emailKey' => $emailKey,
				'reportShared' => $reportShared,
				'scrReqRenterId' => $scrReqRenterId,
			];
		}
		//kint($renters); die();
        // RETURN THE THEME WITH DATA
        return [
            '#theme' => 'landlord_dashboard',
            '#data' => ['renters' => $renters],
        ]; 
    }


	// ------------------------------------------------------------
	// RENTER VIEW/CREATE REPORTS DASHBOARD
	// ------------------------------------------------------------
    public function viewReports($scrReqNodeId) {

    	if(!Helper::isLandlordorAgent()){
    		return new RedirectResponse(\Drupal::url('bgcheck.renter-home', [] ));
    	}

		$reportStatus = 'NotInitialized';
		$data = [];

		// GET THE REPORTS TO VIEW
		$scrReqNode = Node::load($scrReqNodeId);

		// CHECK IF LANDLORD IS VIEWING HIS REQUESTED REPORTS ONLY
		$landlordUserId = \Drupal::currentUser()->id();
		if($scrReqNode->getOwnerId() != $landlordUserId){
			drupal_set_message('You are not allowed to view this report','error');
			return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
		}

		$isShared = $scrReqNode->field_updated->value;
		if(!$isShared){
			drupal_set_message('Report is not available','error');
			return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
		}
		$reportTypes = Helper::getReportTypes($scrReqNode);
		//kint($reportTypes); die();
		$renterId = $scrReqNode->field_api_entity_id->getValue()[3]['value'];
		$scrReqRenterId = $scrReqNode->field_api_entity_id->getValue()[4]['value'];

	    // -----------------------------------------------
	    // REPORT STATUS UPDATE ON EVERY REFRESH
	    // -----------------------------------------------
        $theAPI = new ShareAbleAPI();
        $scrReqRenterRsp = $theAPI->getScrReqRenter($scrReqRenterId);
        if(!empty($scrReqRenterRsp['error'])){
            Helper::buildRspErrors($scrReqRenterRsp);
            return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
        } 			
        $reportType = $scrReqRenterRsp['data']->bundleId;
	    
	    // -----------------------------------------------
        // GET ALL REPORTS
	    // -----------------------------------------------
	    $reports = [];
	    foreach ($reportTypes as $reportType) {
	        $reportRsp = $theAPI->getReport($scrReqRenterId,$reportType);
	        // IF ANY VALIDATION ERRORS
	        if(!empty($reportRsp['error'])){
	        	Helper::buildRspErrors($reportRsp);
	        	//return new RedirectResponse(\Drupal::url('bgcheck.renter-home', [] ));
	        }
	        $reports[$reportType] = $reportRsp['data'];
	        //kint($reports[$reportType]);
	    }
	    //die();

		$data = [
			'nid' => $scrReqRenterId,
			'renterId' => $renterId,
			'scrReqRenterId' => $scrReqRenterId,
			'reportTypes' => $reportTypes,
			'reports' => $reports
		];				
		
		//kint($data); die();
        return [ '#theme' => 'landlord_view_reports', '#data' => $data ];
	}


	// ------------------------------------------------------------
	// LANDLORD CACNEL INVITATION (BEFORE PAYMENT)
	// ------------------------------------------------------------
    public function cancelInvitation($reqNodeId){
	
		$reqNode = Node::load($reqNodeId);
		//kint($reqNode->getOwnerId()); die();
		if($reqNode){
			if($reqNode->getOwnerId() != \Drupal::currentUser()->id()){
				drupal_set_message('Cannot complete the action!','error');
				return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
			} 
			$reqNode->delete();
		}
		drupal_set_message('Screening invitation canceled');
		return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
	}


	// ------------------------------------------------------------
	// LANDLORD SENDS REQUEST TO RENTER
	// ------------------------------------------------------------
    public function sendRequest($appNodeId){

		$appData = $this->getAppData($appNodeId);

		//kint($appData); die();

		// CHECK IF REQUEST ALREADY SENT
		if($this->checkIfReqExisits($appData['renter']['id'])){
			drupal_set_message('You already sent a request to this Applicant','error');
			return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
		}

	    // INITIALIZE THE API
		$theAPI = new ShareAbleAPI();

		// -----------------------------------------------------------------------
		// GET THE LANDLORD or CREATE IF NOT EXISTS
		// -----------------------------------------------------------------------
		$curUser = User::load(\Drupal::currentUser()->id());
		$landlordId = $curUser->field_api_entity_id->value;
		if(empty($landlordId) || $landlordId == 0 || $landlordId == null){ 
			// CHECK AND CREATE A LANDLORD
			$scrReqNode = Helper::getUserScrReqNode();
			if($scrReqNode == null){
				$landlord = Helper::buildLandlord();
				//kint($landlord); die();
				$landlordRsp = $theAPI->createLandlord($landlord);
				if(!empty($landlordRsp['error'])){
					Helper::buildRspErrors($landlordRsp);
					//Helper::goToFrontPage();
					return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
				}else{
					$landlordId = $landlordRsp['data']->landlordId;
				}
			}else{
				$landlordId = $scrReqNode->field_api_entity_id->getValue()[0]['value'];
			}
		}

		// -----------------------------------------------------------------------
	    // GET/CREATE THE PROPERTY
		// -----------------------------------------------------------------------

		// GET PROPERTY ID FROM PROPERTY NODE
		$propertyNodeId = $appData['property']['id'];
		$propertyNode = Node::load($propertyNodeId);
		$propertyId = $propertyNode->field_api_entity_id->getValue()[0]['value'];

		// CREATE THE PROPERTY IF NOT EXISTS - MAY NOT REQUIRE IF THE FLOW IS CORRECT
		if(empty($propertyId) || $propertyId == 0 || $propertyId == null){
			$property = Helper::buildProperty($propertyNodeId);
			$propertyRsp = $theAPI->createProperty($landlordId,$property);
			// IF ANY VALIDATION ERRORS
			if(!empty($propertyRsp['error'])){
				Helper::buildRspErrors($propertyRsp,$form_state);
				return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
			}else{
				// GET PROPERTY ID FROM RSP
				$propertyId = $propertyRsp['data']->propertyId;
				$propertyNode->set('field_api_entity_id',[
					['value' => $propertyId]
				]);
				$propertyNode->save();			
			}
		}


		// -----------------------------------------------------------------------
		// CREATE THE SQR REQUEST
		// -----------------------------------------------------------------------
		$scrReqRsp = $theAPI->createScrReq($landlordId,$propertyId);	
		if(!empty($scrReqRsp['error'])){
			Helper::buildRspErrors($scrReqRsp);
			return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
		}else{
			$scrReqId = $scrReqRsp['data']->screeningRequestId;

			// CREATE THE REQUEST NODE
			$node = Node::create(['type' => 'screqs',
				'title' => 'Screening Request to '.$appData['renter']['email'],
				'field_first_name' => $appData['renter']['name'],
				'field_user' => [
					'target_id' => $appData['renter']['id']
				],
				'field_email' => $appData['renter']['email'],
				'field_api_entity_id' => [
					['value' => $landlordId],
					['value' => $propertyId],
					['value' => $scrReqId]
				],
				'field_property' => [
					'target_id' => $appData['property']['id']
				],
				'field_report_types' => 3,
				'field_payment_status' => 0,
				'field_payer' => 'Renter',
				'field_terms' => true,
				'field_updated' => false
			]);
			$node->save();


			// Create Notification Node
			$notification = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
				'title' => 'You have a new background report Request from '.Helper::getFullName($curUser),
				'description' =>  'Please generate/share your report here',
				'target_link' => 'internal:/screening/renter/',
				'uid' => $appData['renter']['id'], // The user that should recieve this notification.
			]);
			$notification->save();


			// SEND THE REQUEST EMAIL
			$mailManager = \Drupal::service('plugin.manager.mail');
			$params['data'] = $appData;
			$params['data']['link'] = '/screening/renter';
			$result = $mailManager->mail('bgcheck', 'landlord_send_request', $appData['renter']['email'], \Drupal::currentUser()->getPreferredLangcode(), $params, NULL, true);
			if ($result['result'] !== true) {
				drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
			}
			else {
				drupal_set_message(t('Request has been sent.'),'status');
			}
			drupal_set_message(t('Request has been sent.'),'status');
			\Drupal::logger('bgcheck')->notice('LandlordId: '.$landlordId.' PropertyId: '.$propertyId.' screeningRequestId: '.$scrReqId);
		}

		return new RedirectResponse(\Drupal::url('bgcheck.landlord-home'));
		//kint($appData); die();
        
    }

    // GET NECESSARY APPLICATION DATA
    public function getAppData($appNodeId){
    	$appNode = Node::load($appNodeId);
    	$propertyNode = $appNode->get('field_property')->first()->get('entity')->getTarget()->getValue();
    	//kint($propertyNode); die();
    	$renterUser = $appNode->getOwner();
    	$landlordUser = $propertyNode->getOwner();
    	$data = [
    		'renter' => [
    			'id' => $renterUser->id(),
    			'name' => Helper::getFullName($renterUser),
    			'email' => $renterUser->mail->value
    		],
    		'landlord' => Helper::getFullName($propertyNode->getOwner()),
    		'property' => [
    			'node' => $propertyNode,
    			'id' => $propertyNode->id(),
    			'name' => $propertyNode->getTitle()
    		]
    	]; 
    	return $data;
    }


    // CHECK IF SAME REQUEST EXISITS ALREADY
    public function checkIfReqExisits($renterUserId){
		// CHECK IF SAME REQUEST EXISTS
    	$landlordUserId = \Drupal::currentUser()->id();
        $query = \Drupal::entityQuery('node')->condition('type', 'screqs')
        	->condition('uid', $landlordUserId)
        	->condition('field_user.target_id', $renterUserId)
        	->condition('status', 1);
		$reqNodeIds = $query->execute();
		if(!empty($reqNodeIds)){
			return true;
		}
		return false;
    }


}