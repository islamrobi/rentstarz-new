<?php

namespace Drupal\bgcheck\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Stripe\Stripe;
use Stripe\PaymentIntent;
use Stripe\StripeClient;

/**
 * Defines HelloController class.
 */
class RenterController extends ControllerBase {

    public function __construct(){
    }

	// ------------------------------------------------------------
	// RENTER VIEW/CREATE REPORTS DASHBOARD
	// ------------------------------------------------------------
    public function viewReports() {


    	if(Helper::isLandlordorAgent()){
    		return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
    	}
        $renterUserId = \Drupal::currentUser()->id();
		$reportStatus = 'NotInitialized';
	    $renterUser = User::load($renterUserId);
		$data = [];

        // GET SCREENING NODE OF THIS RENTER 

    	$scrNode = Helper::getScrNode();

    	//kint($scrNode); die();

		if(is_null($scrNode)){

        	// IF NODE NOT CREATED TAKE TO THE SCRENNING FORM
        	return new RedirectResponse(\Drupal::url('node.add.renter', [
        		'node_type' => 'screening'
        	]));

        }else{

			$reportTypes = Helper::getReportTypes($scrNode);
			$paymentStatus = $scrNode->field_payment_status->value;
			$renterId = $scrNode->field_api_entity_id->getValue()[3]['value'];
			$scrReqRenterId = $scrNode->field_api_entity_id->getValue()[4]['value'];

		    // -----------------------------------------------
		    // REPORT STATUS UPDATE ON EVERY REFRESH
		    // -----------------------------------------------
	        $theAPI = new ShareAbleAPI();
	        $scrReqRenterRsp = $theAPI->getScrReqRenter($scrReqRenterId);
	        //kint($scrReqRenterRsp); die();
	        if(!empty($scrReqRenterRsp['error'])){
	            Helper::buildRspErrors($scrReqRenterRsp);
	            drupal_set_message('Please try again, or contact administrator','error');
	            return new RedirectResponse(\Drupal::url('<front>'));
	        } 			
	        // SAVE STATUS IN NODE
	        $reportStatus = $scrReqRenterRsp['data']->renterStatus;
	        $reportExpiry = $scrReqRenterRsp['data']->reportsExpireNumberOfDays;
	        $reportType = $scrReqRenterRsp['data']->bundleId;
	        // -----------------------------------------------

			// IF REPORT EXPIRES
			// $reportStatus = "ScreeningRequestExpired";
			if($reportStatus == "ScreeningRequestExpired"){
			
				// UNPUBLISH/DELETE ALL SCREQ NODES

				// SET PAYMENT STATUS TO 0
			
				// SEND TO EDIT FORM NEXT TIME 
			}

	        // SEND TO EXAM IF NEEDS TO BE VERIFIED
	        if($reportStatus == "IdentityVerificationPending"){
	            // GO TO VERIFICATION EXAM
	            return new RedirectResponse(\Drupal::url('bgcheck.renter-exam',[
	            	'nid' => $scrNode->id(), 		
	            ]));	        	
	        }

			// SEND TO PAYMENT OR GENERATE REPORT
			if($reportStatus == "ReadyForReportRequest"){
				if($paymentStatus == 2){
		        	return new RedirectResponse(\Drupal::url('bgcheck.renter-create-report', [
						'nid' => $scrNode->id(),
						'scrReqRenterId' => $scrReqRenterId,
						'renterId' => $renterId,
		        	]));	
		        }else{
		        	return new RedirectResponse(\Drupal::url('bgcheck.payment', []));
		        }
			}

			// CHECK IF PAYMENT IS MISSED SOMEHOW
			if($reportStatus == "ReportsDeliverySuccess" && $paymentStatus == 0){
	        	return new RedirectResponse(\Drupal::url('bgcheck.payment', []));					
			}

		    // -----------------------------------------------
	        // GET ALL REPORTS
		    // -----------------------------------------------
		    $reports = [];
		    if($reportStatus == "ReportsDeliverySuccess"){
			    $reports = [];
			    foreach ($reportTypes as $reportType) {
			        $reportRsp = $theAPI->getReport($scrReqRenterId,$reportType);
			        // IF ANY VALIDATION ERRORS
			        if(!empty($reportRsp['error'])){
			        	Helper::buildRspErrors($reportRsp);
		            	drupal_set_message('Please try again, or contact administrator','error');
		            	return new RedirectResponse(\Drupal::url('<front>'));
			        }
					$reports[$reportType] = $reportRsp['data'];

			    }
			}

			//$reportStatus = "RetryLimitExceeded"; // To check verification failed scenario
			$data['scr'] = [
				'nid' => $scrNode->id(),
				'renterId' => $renterId,
				'scrReqRenterId' => $scrReqRenterId,
				'renterName' => Helper::getFullName($renterUser),
				'reportStatus' => $reportStatus,
				'reportExpiry' => $reportExpiry,
				'paymentStatus' => $paymentStatus,
				'reportType' => $reportType,
				'reportTypes' => $reportTypes,
				'reports' => $reports
			];				

		}

		//kint($data); die();
        return [ '#theme' => 'renter_view_reports', '#data' => $data ];
	}


	// ------------------------------------------------------------
	// RENTER SHARE/UNSHARE REPORTS DASHBAORD
	// ------------------------------------------------------------
    public function shareReports() {

    	if(Helper::isLandlordorAgent()){
    		return new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
    	}

    	// Check report share limit
		$reportConfig = \Drupal::config('bgcheck.api_settings');
		$shareLimit = $reportConfig->get('bgc_share_limit');  

		// Get total shared so far  	
        $renterUserId = \Drupal::currentUser()->id();
	    $renterUser = User::load($renterUserId);

		// GET ALL THE APPLICATIONS
		$node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $query = \Drupal::entityQuery('node')->condition('type', 'screqs')->condition('status', 1);
		$orGroup = $query->orConditionGroup()->condition('field_user.target_id', $renterUserId)->condition('field_email', $renterUser->mail->value);
		$query->condition($orGroup);
		$reqIds = $query->execute();
		$scrReqNodes = $node_storage->loadMultiple($reqIds);

		$data = [];
		$landlords = [];

		$shareCount = 0;
		foreach ($scrReqNodes as $theId => $scrReqNode) {

			$user = User::load($scrReqNode->getOwnerId());
			$uroles = $user->getRoles();
			if(in_array('landlord',$uroles))$utype = 'landlord';
			if(in_array('agent',$uroles))$utype = 'agent';
			$isShared = $scrReqNode->field_updated->value;
			$isReportReady = false;
			if(isset($scrReqNode->field_api_entity_id->getValue()[4])){
				$isReportReady = true;
			}
			$landlords[] = [
				'id' => $user->uid->value,
				'reqNodeId' => $scrReqNode->id(),
				'name' => $user->field_first_name->value.' '.$user->field_last_name->value,
				'company' => $user->field_company_name->value,
				'type' => $utype,
				'shared' => $isShared,
				'isReportReady' => $isReportReady
			];

		}
		$data = [
			//'nid' => $scrNodeId,
			'shareCount' => $shareCount,
			'shareLimit' => $shareLimit,
			'viewers' => $landlords
		];	

		//kint($data); die();
        return [ '#theme' => 'renter_share_reports', '#data' => $data ];
	}


	// ------------------------------------------------------------
	// RENTER SHARE REPORT WITH A LONDLORD
	// ------------------------------------------------------------

    public function shareReport($reqNodeId){

        $reqNode = Node::load($reqNodeId);
        $reqNode->field_updated->value = true;
        $reqNode->save();	

		// Create Notification Node
		$curUser = User::load(\Drupal::currentUser()->id());
		$notification = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
			'title' => Helper::getFullName($curUser).' has shared his background report',
			'description' =>  'Please go the link below to view',
			'target_link' => 'internal:/screening/landlord/',
			'uid' => $reqNode->getOwnerId(), // The user that should recieve this notification.
		]);
		$notification->save();

        drupal_set_message('Report Shared','status');
	    return new RedirectResponse(\Drupal::url('bgcheck.renter-share-reports', [] ));

    }    

	// ------------------------------------------------------------
	// RENTER UNSHARE REPORT WITH A LONDLORD
	// ------------------------------------------------------------
    public function unshareReport($reqNodeId){

        $reqNode = Node::load($reqNodeId);
        $reqNode->field_updated->value = false;
        $reqNode->save();	

        drupal_set_message('Report UnShared','status');
	    return new RedirectResponse(\Drupal::url('bgcheck.renter-share-reports', [] ));

    }


	// ------------------------------------------------------------
	// RENTER INITIATES REPORT CREATION
	// ------------------------------------------------------------
	public function acceptRequest($reqNodeId){

		$reqNode = Node::load($reqNodeId);	
		$landlordId = $reqNode->field_api_entity_id->getValue()[0]['value'];
		$scrReqId = $reqNode->field_api_entity_id->getValue()[2]['value'];
        //$renterUserId = \Drupal::currentUser()->id();
	    //$renterUser = User::load($renterUserId);
		$theAPI = new ShareAbleAPI();
		if(!empty($reqNode)){

			$scrNode = Helper::getScrNode();

			if($scrNode != null){
				$package = $scrNode->field_report_types->value;
				$reportTypes = Helper::getReportTypes($scrNode);
				$paymentStatus = $scrNode->field_payment_status->value;
				$renterId = $scrNode->field_api_entity_id->getValue()[3]['value'];

				// APPLY THE MULTISHARE BUNDLE ID
				$multiSharePackage = $package + 5000;
				//kint($scrNode); die();

			    // CREATE THE SCREENING REQUEST FOR THIS PROPERTY
			    $scrReqRenterRsp = $theAPI->createScrReqRenter($scrReqId,$landlordId,$renterId,$multiSharePackage,30);

			    // IF ANY VALIDATION ERRORS
			    if(!empty($scrReqRenterRsp['error'])){
			    	Helper::buildRspErrors($scrReqRenterRsp);
			    	return new RedirectResponse(\Drupal::url('bgcheck.renter-share-reports', [] ));
			    }else{
			        $scrReqRenterId = $scrReqRenterRsp['data']->screeningRequestRenterId;

			        // Create the report
        			$renterUserId = \Drupal::currentUser()->id();
        			$person = Helper::buildPerson($scrNode->id());	
        			//kint($person); die();	            
        			$renterData = ['person' => $person];
		            // CREATE THE REPORT AND BACK
		            $reportRsp = $theAPI->createReport($scrReqRenterId,$renterData);
		            //kint($reportRsp); die();
		            if(!empty($reportRsp['error'])){
		                Helper::buildRspErrors($reportRsp);
		                return new RedirectResponse(\Drupal::url('bgcheck.renter-share-reports', [] ));
		            } 

			        $reqNode->field_api_entity_id->appendItem($renterId);
			        $reqNode->field_api_entity_id->appendItem($scrReqRenterId);
			        $reqNode->set('field_report_types',$package);
			        //$reqNode->set('field_updated',true);
			        $reqNode->set('field_user',['target_id' => $renterUserId]);
			        $reqNode->save();

				}

			}

		}
		drupal_set_message('Report generated and shared','status');
		return new RedirectResponse(\Drupal::url('bgcheck.renter-share-reports', [] ));

	}



	// ------------------------------------------------------------
	// RENTER INITIATES REPORT CREATION
	// ------------------------------------------------------------
	public function createReport($nid,$scrReqRenterId,$renterId){

        $renterUserId = \Drupal::currentUser()->id();
        $person = Helper::buildPerson($nid);

        //kint($person); die();

	    // TRANSUNION API CALL
        $theAPI = new ShareAbleAPI();
        $verifyRsp = $theAPI->verifyRenter($scrReqRenterId,$person);
        if(!empty($verifyRsp['error'])){
            Helper::buildRspErrors($verifyRsp);
            return new RedirectResponse(\Drupal::url('bgcheck.renter-home', [] ));
        }  

        // IF NO ERRORS
        if($verifyRsp['data']->status == "Verified"){
            
            $renterData = ['person' => $person];
            // CREATE THE REPORT AND BACK
            $reportRsp = $theAPI->createReport($scrReqRenterId,$renterData);
            if(!empty($reportRsp['error'])){
                Helper::buildRspErrors($reportRsp);
			}

			// // CHARGE PAYMENT IF IN PROCESSING
			// $scrNode = Helper::getScrNode();
			// if($scrNode != null){
			// 	$metadata = json_decode($scrNode->field_metadata->value);
			// 	if($metadata->paymentIntent->status == "requires_capture"){
			// 		$paymentConfig = \Drupal::config('bgcheck.payment_settings');
			// 		$stripe_secret_key = $paymentConfig->get('bgc_stripe_sec_key');
			// 		Stripe::setApiKey($stripe_secret_key);
			// 		$intent = PaymentIntent::retrieve($metadata->paymentIntent->id);
			// 		$pintent = $intent->capture();
	
			// 		$scrNode->set('field_metadata',json_encode($pintent));
			// 		$scrNode->save();
			// 	}
			// }

            drupal_set_message('Report Generation Process Initialized !','status');
            return new RedirectResponse(\Drupal::url('bgcheck.renter-home', [] ));            	

        }else{

            // GO TO VERIFICATION EXAM
            return new RedirectResponse(\Drupal::url('bgcheck.renter-exam',[
            	'nid' => $nid,
                //'scrReqRenterId' => $scrReqRenterId,
                //'renterId' => $renterId,
                //'examId' => $examId,       		
            ]));
        }

	}

}