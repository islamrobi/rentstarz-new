<?php

namespace Drupal\bgcheck\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;

/**
 * Defines HelloController class.
 */
class ReportController extends ControllerBase {

    private $theAPI;
    public $scrReqRenterId;
    public $reportType;

    public function __construct(){

        // INITIALIZE THE API
        $this->theAPI = new ShareAbleAPI();
        $this->scrReqRenterId = \Drupal::request()->get('scrReqRenterId');
        $this->reportType = \Drupal::request()->get('reportType');

    }

    /**
    * Display the markup.
    *
    * @return array
    *   Return markup array.
    */
    public function getReport() {

        // RETURN VALUE IN CASE OF ANY ERRORS
        $errPage = [
            '#type' => 'markup',
            '#markup' => $this->t('<div style="padding: 100px 0;">REPORTS CANNOT BE GENERATED !!</div>'),
        ];       
        //$reportSuccess = $this->theAPI->createReport($this->scrReqRenterId);

        // GET A REPORT
        $reportRsp = $this->theAPI->getReport($this->scrReqRenterId,$this->reportType);
        //$reportRsp = $this->theAPI->getReport($this->scrReqRenterId,'criminal');

        // IF ANY VALIDATION ERRORS
        if(!empty($reportRsp['error'])){
        	Helper::buildRspErrors($reportRsp);
        	return $errPage;
        }
        // IF NO ERRORS
        $reports = $reportRsp['data'];
        //drupal_set_message('Report generation successfull !!','status');

        return [
            //'#type' => 'markup',
            '#theme' => 'report',
            '#reports' => $reports,
            //'#markup' => $this->t('<div style="padding: 100px 0;">BGCheck HOME</div>'),
        ];
    }


    public function getPDFReport($scrReqRenterId, $reportType) {

        // RETURN VALUE IN CASE OF ANY ERRORS
        $errPage = [
            '#type' => 'markup',
            '#markup' => $this->t('<div style="padding: 100px 0;">REPORTS CANNOT BE GENERATED !!</div>'),
        ];       
        //$reportSuccess = $this->theAPI->createReport($this->scrReqRenterId);

        // GET A REPORT
        $reportRsp = $this->theAPI->getReport($scrReqRenterId,$reportType);
        //$reportRsp = $this->theAPI->getReport($this->scrReqRenterId,'criminal');

        // IF ANY VALIDATION ERRORS
        if(!empty($reportRsp['error'])){
        	Helper::buildRspErrors($reportRsp);
        	return $errPage;
        }
        // IF NO ERRORS
        $reports = $reportRsp['data'];
        //drupal_set_message('Report generation successfull !!','status');

        $reportHTML = $reports->reportResponseModelDetails[0]->reportData;
        //kint($reportHTML); die();

        //return $dompdf->stream();
        return $dompdf->stream($reportType."-Report-".time().".pdf", array("Attachment" => false));
/*
        return [
            //'#type' => 'markup',
            '#theme' => 'report',
            '#reports' => $reports,
            //'#markup' => $this->t('<div style="padding: 100px 0;">BGCheck HOME</div>'),
        ];
        */
    }



}