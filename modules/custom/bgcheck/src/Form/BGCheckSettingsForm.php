<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class BGCheckSettingsForm extends ConfigFormBase {

    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'bgcheck_api_settings';
    }

    /**
    * {@inheritdoc}
    */
    protected function getEditableConfigNames() {
        return [
            'bgcheck.api_settings',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $config = $this->config('bgcheck.api_settings');

        $form['bgc_api_info'] = array(
          '#type' => 'markup', '#markup' => t('<hr><h2>ShareAble API Settings</h2> (Provided by Transunion) <hr>'),
        );
        $form['bgc_partner_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Partner ID'),
          '#default_value' => $config->get('bgc_partner_id'),
        ];
        $form['bgc_client_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client ID'),
          '#default_value' => $config->get('bgc_client_id'),
        ];
        $form['bgc_api_key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('API Key'),
          '#default_value' => $config->get('bgc_api_key'),
        ];
        $form['bgc_api_base_url'] = [
          '#type' => 'url',
          '#title' => $this->t('ShareAble API Base URL'),
          '#default_value' => $config->get('bgc_api_base_url'),
        ];

        $form['bgc_report_info'] = array(
          '#type' => 'markup', '#markup' => t('<hr><h2>Report related settings</h2><hr>'),
        );
        $form['bgc_expiry_days'] = [
          '#type' => 'textfield', '#title' => $this->t('Report Expiry Days'),
          '#default_value' => $config->get('bgc_expiry_days'),
        ];
        $form['bgc_share_limit'] = [
          '#type' => 'textfield', '#title' => $this->t('Report Share Limit (Maximum No of user the report can be shared with at a time)'),
          '#default_value' => $config->get('bgc_share_limit'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('bgcheck.api_settings')
            ->set('bgc_partner_id', $form_state->getValue('bgc_partner_id'))
            ->set('bgc_client_id', $form_state->getValue('bgc_client_id'))
            ->set('bgc_api_key', $form_state->getValue('bgc_api_key'))
            ->set('bgc_api_base_url', $form_state->getValue('bgc_api_base_url'))
            ->set('bgc_expiry_days', $form_state->getValue('bgc_expiry_days'))
            ->set('bgc_share_limit', $form_state->getValue('bgc_share_limit'))
            ->save();
        parent::submitForm($form, $form_state);
    }

}