<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\Component\Serialization\Json;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ExamForm extends FormBase {

    public $scrReqRenterId;
    public $renterId;
    public $nid;
    private $theAPI;

    public function __construct(){

        $this->nid = \Drupal::request()->get('nid');
        $scrNode = Helper::getScrNode();
        //kint($scrNode); die();
        //$this->renterId = \Drupal::request()->get('renterId');
        //$this->scrReqRenterId = \Drupal::request()->get('scrReqRenterId');
        $this->scrReqId = $scrNode->field_api_entity_id->getValue()[2]['value'];
        $this->renterId = $scrNode->field_api_entity_id->getValue()[3]['value'];
        $this->scrReqRenterId = $scrNode->field_api_entity_id->getValue()[4]['value'];

    }

    /**
    * Build the simple form.
    *
    * A build form method constructs an array that defines how markup and
    * other form elements are included in an HTML form.
    *
    * @param array $form
    *   Default form array structure.
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    *   Object containing current form state.
    *
    * @return array
    *   The render array defining the elements of the form.
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

	    // INITIALIZE THE API
        $theAPI = new ShareAbleAPI();
        $person = Helper::buildPerson($this->nid);

        $verifyRsp = $theAPI->verifyRenter($this->scrReqRenterId,$person);
        //kint($verifyRsp, $person); die();
        if(!empty($verifyRsp['error'])){
            Helper::buildRspErrors($verifyRsp);
            return new RedirectResponse(\Drupal::url('<front>', [] ));
        }else{
        	if($verifyRsp['data']->status == "Verified"){
        		return new RedirectResponse(\Drupal::url('<front>', [] ));
        	}
        }

        // CREATES THE EXAM WITH THE RENTER INFO
        $examData = ["person" => $person,"externalReferenceNumber" => $this->renterId.'-'.$this->scrReqRenterId];
        
        $examRsp = $theAPI->createExam($this->scrReqRenterId,$examData);   
        //kint($examRsp); die();

        if(!empty($examRsp['error'])){
            Helper::buildRspErrors($examRsp);
            return new RedirectResponse(\Drupal::url('<front>', [] ));
        }
        
        // Check if manual verification is required
        //kint($examRsp); die();
        //$examRsp['data']->result = 'ManualVerificationRequired';
        $examResult = $examRsp['data']->result;
        if($examResult == 'ManualVerificationRequired'){
            $form['failed'] = [
                '#type' => 'item',
                '#title' => t('Identity Verification Failed'),
                '#markup' => '<div class="alert alert-warning">We are unable to verify your identity online.<br>Please call customer support at <a href="tel:1-833-458-6338">(833) 458-6338</a> for assistance with completing your screening over the phone.</div><p> For referrence please provide the ScreeningReqeustId <strong>('.$this->scrReqId.')</strong> to the support executive if required</p>',
            ];
            return $form;
        }

        // POPULATE THE FORM
        $examId = $examRsp['data']->examId;
        $examRspData = $examRsp['data'];

        $tempstore = \Drupal::service('user.private_tempstore')->get('bgcheck');
        $tempstore->set('examRspData', $examRspData);
        //$_SESSION['examRspData'] = $examRspData;

        // Pass the Exam Id through From submission
        $form['examId'] = [
          '#type' => 'hidden',
          '#required' => TRUE,
          '#default_value' => $examId,
        ];

        // SET UP THE QUESTIONS
        $questions = $examRspData->authenticationQuestions;
        foreach ($questions as $question) {
            $options = [];
            foreach ($question->choices as $choice) {
                $options[$choice->choiceKeyName] = $choice->choiceDisplayName;
            }
            $form['questions'][$question->questionKeyName] = [
                '#type' => 'radios',
                '#title' => $question->questionDisplayName,
                '#options' => $options,
                '#required' => TRUE,
            ];   
        }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Verify Identity')];
        $form['actions']['submit']['#attributes']['class'][] = 'btn';
        $form['actions']['submit']['#attributes']['class'][] = 'loader';
        $form['#theme'] = 'exam_form';
        return $form;
    }

   /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
    public function getFormId() {
        return 'exam_form';
    }

   /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        $answers = [ "answers" => [] ];

        // GET THE QUESTIONS
        $tempstore = \Drupal::service('user.private_tempstore')->get('bgcheck');
        $examRspData = $tempstore->get('examRspData');      

        //$examRspData = $_SESSION['examRspData'];
        $questions = $examRspData->authenticationQuestions;

        foreach ($questions as $question) {
            $answers["answers"][] = [
                "questionKeyName" => $question->questionKeyName, 
                "selectedChoiceKeyName" => $form_state->getValue($question->questionKeyName)
            ];  
        }

        // INITIALIZE THE API
        $theAPI = new ShareAbleAPI();
        $answerRsp = $theAPI->createAnswers($this->scrReqRenterId, $examRspData->examId, $answers);

        // IF ANY VALIDATION ERRORS
        if(!empty($answerRsp['error'])){
        	Helper::buildRspErrorsForms($answerRsp,$form_state);
        	return;
        }

        // CHECK IF EXAM IS PASSED
        if($answerRsp['data']->result == 'Failed'){
	        drupal_set_message('IDENTITY VERIFICATION FAILED, PLEASE TRY AGAIN !','error');
	        $form_state->setRedirect('bgcheck.renter-exam',[
	        	'nid' => $this->nid,
                //'scrReqRenterId' => $this->scrReqRenterId,
                //'renterId' => $this->renterId,     		
            ]);
            return;

/*            // GO TO VERIFICATION EXAM
            return new RedirectResponse(\Drupal::url('bgcheck.renter-exam',[
                'scrReqRenterId' => $this->scrReqRenterId,
                'renterId' => $this->renterId,
                //'examId' => $examId,       		
            ]));*/

        }else{
	        //drupal_set_message('IDENTITY VERIFICATION COMPLETE','status');
	        $form_state->setRedirect('bgcheck.renter-home',[]);         	
        }

        //Helper::prettyDump($answerRsp);
    }


  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function submitForm(array &$form, FormStateInterface $form_state) {  

        // drupal_set_message('IDENTITY VERIFICATION COMPLETE','status');
        // $form_state->setRedirect('bgcheck.renter-home',[]); 

    }

}
