<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;

/**
 * Defines a form that configures forms module settings.
 */
class LandlordSettingsForm extends ConfigFormBase {

    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'bgcheck_landlord_settings';
    }

    /**
    * {@inheritdoc}
    */
    protected function getEditableConfigNames() {
        return [
            'bgcheck.landlord_settings',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $config = $this->config('bgcheck.landlord_settings');

        $form['bgc_landlord_info'] = array(
          '#type' => 'markup',
          '#markup' => t('<hr><h2>API Static Data Settings - Landlord Detail</h2> <hr>'),
        );
        $form['bgc_landlord_id'] = [
          '#type' => 'textfield', '#title' => $this->t('Id'),
          '#default_value' => $config->get('bgc_landlord_id'),
          '#attributes' => ['readonly' => 'readonly'],
        ];        
        $form['bgc_landlord_email'] = [
          '#type' => 'textfield', '#title' => $this->t('Email'),
          '#default_value' => $config->get('bgc_landlord_email'),
        ];
        $form['bgc_landlord_fname'] = [
          '#type' => 'textfield', '#title' => $this->t('First Name'),
          '#default_value' => $config->get('bgc_landlord_fname'),
        ];
        $form['bgc_landlord_lname'] = [
          '#type' => 'textfield', '#title' => $this->t('Last Name'),
          '#default_value' => $config->get('bgc_landlord_lname'),
        ];
        $form['bgc_landlord_phone'] = [
          '#type' => 'textfield', '#title' => $this->t('Phone'),
          '#default_value' => $config->get('bgc_landlord_phone'),
        ];
        $form['bgc_landlord_address'] = [
          '#type' => 'textfield', '#title' => $this->t('Address'),
          '#default_value' => $config->get('bgc_landlord_address'),
        ];
        $form['bgc_landlord_city'] = [
          '#type' => 'textfield', '#title' => $this->t('City'),
          '#default_value' => $config->get('bgc_landlord_city'),
        ];        
        $form['bgc_landlord_state'] = [
          '#type' => 'textfield', '#title' => $this->t('State'),
          '#description' => $this->t('2 LETTER STATE CODE'),
          '#default_value' => $config->get('bgc_landlord_state'),
        ];
        $form['bgc_landlord_zip'] = [
          '#type' => 'textfield', '#title' => $this->t('Zip'),
          '#default_value' => $config->get('bgc_landlord_zip'),
        ];

        return parent::buildForm($form, $form_state);
    }

    public function validateForm(array &$form, FormStateInterface $form_state){

	    // INITIALIZE THE API
	    $theAPI = new ShareAbleAPI();
        $landlord = [
            //"landlordId" => $form_state->getValue('bgc_landlord_id'),
            "emailAddress" => $form_state->getValue('bgc_landlord_email'),
            "firstName" => $form_state->getValue('bgc_landlord_fname'),
            "lastName" => $form_state->getValue('bgc_landlord_lname'),
            "phoneNumber" => $form_state->getValue('bgc_landlord_phone'),
            "phoneType" => "Mobile",
            "businessName" => "business",
            "businessAddress" => [
                "addressLine1" => $form_state->getValue('bgc_landlord_address'),
                "addressLine2" => "",
                "addressLine3" => "",
                "addressLine4" => "",
                "locality" => $form_state->getValue('bgc_landlord_city'),
                "region" => $form_state->getValue('bgc_landlord_state'),
                "postalCode" => $form_state->getValue('bgc_landlord_zip'),
                "country" => "US"
            ],
            "acceptedTermsAndConditions" => true
        ];

    	// EDIT OR CREATE
    	if($form_state->getValue('bgc_landlord_id') && $form_state->getValue('bgc_landlord_id') > 0){
    		// EDIT LANDLORD BASED ON THE FORM DATA
    		$landlord['landlordId'] = $form_state->getValue('bgc_landlord_id');
		    $landlordRsp = $theAPI->editLandlord($landlord);   
		    if(!empty($landlordRsp['error'])){
		    	Helper::buildRspErrorsForms($landlordRsp,$form_state);
		    	return;
		    }      		
    	}else{
    		// CREATE A NEW LANDLORD
		    $landlordRsp = $theAPI->createLandlord($landlord);   
		    if(!empty($landlordRsp['error'])){
		    	Helper::buildRspErrorsForms($landlordRsp,$form_state);
		    	return;
		    }else{
		    	$form_state->setValue('bgc_landlord_id', $landlordRsp['data']->landlordId);
		    }
    	}

   
	    
	    //$form_state->setErrorByName('bgc_landlord_email','Test Error'); 	
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('bgcheck.landlord_settings')
            ->set('bgc_landlord_id', $form_state->getValue('bgc_landlord_id'))
            ->set('bgc_landlord_email', $form_state->getValue('bgc_landlord_email'))
            ->set('bgc_landlord_fname', $form_state->getValue('bgc_landlord_fname'))
            ->set('bgc_landlord_lname', $form_state->getValue('bgc_landlord_lname'))
            ->set('bgc_landlord_phone', $form_state->getValue('bgc_landlord_phone'))
            ->set('bgc_landlord_address', $form_state->getValue('bgc_landlord_address'))
            ->set('bgc_landlord_city', $form_state->getValue('bgc_landlord_city'))
            ->set('bgc_landlord_state', $form_state->getValue('bgc_landlord_state'))
            ->set('bgc_landlord_zip', $form_state->getValue('bgc_landlord_zip'))
            ->save();
        parent::submitForm($form, $form_state);
    }

}