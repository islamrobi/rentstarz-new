<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\Component\Serialization\Json;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Stripe\Stripe;
use Stripe\PaymentIntent;
use Stripe\StripeClient;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class PaymentForm extends FormBase {


    public function __construct(){

    }

    /**
    * Build the simple form.
    *
    * A build form method constructs an array that defines how markup and
    * other form elements are included in an HTML form.
    *
    * @param array $form
    *   Default form array structure.
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    *   Object containing current form state.
    *
    * @return array
    *   The render array defining the elements of the form.
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        //return new RedirectResponse(\Drupal::url('bgcheck.landlord-home',[]));

        $key = '';
        $email = '';
        // CHECK IF LANDLORD IS PAYING
        if(Helper::isLandlordorAgent()){
            if(isset($_GET['key'])){
                $key = $_GET['key'];
                $email = base64_decode($_GET['key']);
                $package = Helper::getPackageInfo($email);
                $reqNode = Helper::getReqNodeByEmail($form_state->getValue('email'));
            }else{
                Helper::backToHome();
            }
            $payer = "Landlord";
        }else{

            // CHECK IF PAYMENT IS ALREADY MADE BY LANDLORD
            $reqNode = Helper::getPayingReqNode(Helper::getEmail());
            if($reqNode != null){
                
				$metadata = json_decode($reqNode->field_metadata->value);
				if($metadata->paymentIntent->status == "requires_capture"){
					$paymentConfig = \Drupal::config('bgcheck.payment_settings');
					$stripe_secret_key = $paymentConfig->get('bgc_stripe_sec_key');
					Stripe::setApiKey($stripe_secret_key);
					$intent = PaymentIntent::retrieve($metadata->paymentIntent->id);
					$pintent = $intent->capture();
	
					$reqNode->set('field_metadata',json_encode($pintent));
					$reqNode->save();
                } 
                $scrNode = Helper::getScrNode();
                $scrNode->set('field_payment_status',2);
                $scrNode->save();
                // Redirect to next step
                return new RedirectResponse(\Drupal::url('bgcheck.renter-home',[]));
                // $rrsp = new RedirectResponse(\Drupal::url('bgcheck.renter-home',[]));
                // $rrsp->send();  

            }else{

                $package = Helper::getPackageInfo();
                $payer = "Renter";
            }
        }

        //kint($email); die();

    	// GET PACKAGE INFO
		$paymentConfig = \Drupal::config('bgcheck.payment_settings');
		if($package == 2){
			$data['price'] = $paymentConfig->get('bgc_pay_basic');
			$data['package_name'] = 'Criminal + Credit';
		}else{
			$data['price'] = $paymentConfig->get('bgc_pay_premium');
			$data['package_name'] = 'Criminal + Credit + Eviction';
		}
		$data['tax'] = $paymentConfig->get('bgc_pay_tax');
        $data['discount'] = $paymentConfig->get('bgc_pay_discount');
        $tax = $data['price'] * ($data['tax'] / 100);
        $discount = $data['price'] * ($data['discount'] / 100);

		$data['total'] = number_format( $data['price'] - $discount + $tax ,2);

		$form['payer'] = [
			'#type' => 'hidden',
            '#value' => $payer
		];
		$form['email'] = [
			'#type' => 'hidden',
            '#value' => $email
        ];
		$form['key'] = [
			'#type' => 'hidden',
            '#value' => $key,
            '#attributes' => ['id' => 'email_key']
        ];

		$form['selpackage'] = [
			'#type' => 'item',
			'#title' => t('Package - '),
			'#markup' => $data['package_name'],
		];

		$form['price'] = [
			'#type' => 'item',
			'#title' => t('Price - '),
			'#markup' => $data['price'],
		];
		$form['total'] = [
			'#type' => 'item',
			'#title' => t('Total - '),
			'#markup' => $data['total'],
		];
		$form['discount'] = [
			'#type' => 'item',
			'#title' => t('Discount - '),
			'#markup' => $data['discount'],
		];        
		$form['tax'] = [
			'#type' => 'item',
			'#title' => t('Tax - '),
			'#markup' => $data['tax'],
		]; 

        $form['first_name'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('First Name'),
        ];

        $form['middle_name'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Middle Name'),
        ];

        $form['last_name'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Last Name'),
        ];

        $form['address'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Address'),
        ];

        $form['city'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('City'),
        ];

        $form['state'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('State'),
        ];

        $form['zip'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Zip code'),
        ];

        $form['amount'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Amount'),
        	'#default_value' => $data['price'],
        ];
        $form['package'] = [
        	'#type' => 'textfield',
        	'#title' => $this->t('Package'),
        	'#default_value' => $package
        ];
        $form['metadata'] = [
        	'#type' => 'textarea',
        	'#title' => $this->t(' '),
        ];
        $form['metadata']['#attributes']['class'][] = 'hidden';

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Pay Now')];
        $form['actions']['submit']['#attributes']['class'][] = 'stripe_pay_btn';
        //$form['actions']['submit']['#attributes']['class'][] = 'loader';
        $form['#theme'] = 'payment_form';
        //$form['#data'] = $data;
        //kint($form); die();
        return $form;
    }

   /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
    public function getFormId() {
        return 'payment_form';
    }

   /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {


    }


  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function submitForm(array &$form, FormStateInterface $form_state) {  

    	// Address
    	$address = $form_state->getValue('address').' , '.$form_state->getValue('city').' '.$form_state->getValue('state').' '.$form_state->getValue('zip');

		// CREATE THE REQUEST NODE
		$payNode = Node::create(['type' => 'payments',
			'title' => 'Payment from '.\Drupal::currentUser()->getUsername(),
            'field_email' => $form_state->getValue('email'),
            'field_first_name' => $form_state->getValue('first_name'),
			'field_middle_name' => $form_state->getValue('middle_name'),
			'field_last_name' => $form_state->getValue('last_name'),
			'field_street' => $address,
			'field_package' => $form_state->getValue('package'),
			'field_amount' => $form_state->getValue('amount'),
			'field_metadata' => $form_state->getValue('metadata')
		]);
		$payNode->setPublished(true);
		$payNode->setOwnerId(\Drupal::currentUser()->id());
		$payNode->save();    	

        if($form_state->getValue('payer') == "Landlord"){
            $reqNode = Helper::getReqNodeByEmail($form_state->getValue('email'));
            if($reqNode != null){
                $reqNode->set('field_payment_status',2);
                $reqNode->set('field_metadata',$form_state->getValue('metadata'));
                $reqNode->save();
            }
            drupal_set_message('PAYMENT COMPLETE','status');

            // SEND EMAIL
            $name = $reqNode->field_first_name->value;
            $email = $reqNode->field_email->value;
            $propertyId = $reqNode->field_property->target_id;
            $property = Helper::buildProperty($propertyId);
        
            // GET USER INFORMATION
            $userId = \Drupal::currentUser()->id();
            $user = User::load($userId);	    
            $userName = Helper::getFullName($user);
            $reglink = '/registration/'.base64_encode($email).'?destination=/screening/renter';
            $emailData = [
                'landlord_name' => $userName,
                'renter_name' => $name,
                'renter_email' => $email,
                'property_name' => $property['propertyName'],
                'property_address' => $property['addressLine1'].', '.$property['locality'].' '.$property['region'].' '.$property['postalCode'],
                'reglink' => $reglink,
                'paid' => true,
            ];
            // Email With Payment news
            $mailManager = \Drupal::service('plugin.manager.mail');
            $params['data'] = $emailData;
            $langcode = \Drupal::currentUser()->getPreferredLangcode();
            $result = $mailManager->mail('bgcheck', 'landlord_send_invitation', $emailData['renter_email'], $langcode, $params, NULL, true);
            if ($result['result'] !== true) {
                drupal_set_message(t('There was a problem sending your message.'), 'error');
            }
            else {
                drupal_set_message(t('Invitation has been sent.'),'status');
            }
            $form_state->setRedirect('bgcheck.landlord-home',[]); 

        }else{ // renter

            $scrNode = Helper::getScrNode();
            if($scrNode != null){

				$metadata = json_decode($form_state->getValue('metadata'));
				if($metadata->paymentIntent->status == "requires_capture"){
					$paymentConfig = \Drupal::config('bgcheck.payment_settings');
					$stripe_secret_key = $paymentConfig->get('bgc_stripe_sec_key');
					Stripe::setApiKey($stripe_secret_key);
					$intent = PaymentIntent::retrieve($metadata->paymentIntent->id);
					$pintent = $intent->capture();
                    $scrNode->set('field_payment_status',2);
					$scrNode->set('field_metadata',json_encode($pintent));
					$scrNode->save();
                }

                //$scrNode->set('field_metadata',$form_state->getValue('metadata'));
                //$scrNode->save();                
			}

            drupal_set_message('PAYMENT COMPLETE','status');
            $form_state->setRedirect('bgcheck.renter-home',[]); 

        }
		// GET SCR NODE & SET PAYMENT STATUS


    }

}
