<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class PaymentSettingsForm extends ConfigFormBase {

    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'payment_api_settings';
    }

    /**
    * {@inheritdoc}
    */
    protected function getEditableConfigNames() {
        return [
            'bgcheck.payment_settings',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $config = $this->config('bgcheck.payment_settings');

        $form['bgc_stripe_info'] = array(
          '#type' => 'markup',
          '#markup' => t('<hr><h2>Payment Settings</h2> (Stripe & Princing) <hr>'),
          //'#description' => $data,
        );
        $form['bgc_stripe_sec_key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Secret Key'),
          '#default_value' => $config->get('bgc_stripe_sec_key'),
        ];
        $form['bgc_stripe_pub_key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Publishable Key'),
          '#default_value' => $config->get('bgc_stripe_pub_key'),
        ];
        $form['bgc_pay_rest_url'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Payment Rest URL'),
          '#default_value' => $config->get('bgc_pay_rest_url'),
        ];        
        $form['bgc_pay_basic'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Basic Package Price'),
          '#default_value' => $config->get('bgc_pay_basic'),
        ];
        $form['bgc_pay_premium'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Premium Package Price '),
          '#default_value' => $config->get('bgc_pay_premium'),
        ];
        $form['bgc_pay_tax'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Tax Amount (%)'),
          '#default_value' => $config->get('bgc_pay_tax'),
        ];        
        $form['bgc_pay_discount'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Discount Amount (%)'),
          '#default_value' => $config->get('bgc_pay_discount'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('bgcheck.payment_settings')
            ->set('bgc_stripe_sec_key', $form_state->getValue('bgc_stripe_sec_key'))
            ->set('bgc_stripe_pub_key', $form_state->getValue('bgc_stripe_pub_key'))
            ->set('bgc_pay_rest_url', $form_state->getValue('bgc_pay_rest_url'))
            ->set('bgc_pay_basic', $form_state->getValue('bgc_pay_basic'))
            ->set('bgc_pay_premium', $form_state->getValue('bgc_pay_premium'))
            ->set('bgc_pay_tax', $form_state->getValue('bgc_pay_tax'))
            ->set('bgc_pay_discount', $form_state->getValue('bgc_pay_discount'))
            ->save();
        parent::submitForm($form, $form_state);
    }

}