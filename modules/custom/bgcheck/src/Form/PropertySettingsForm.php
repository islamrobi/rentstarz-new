<?php

namespace Drupal\bgcheck\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bgcheck\Helpers\ShareAbleAPI;
use Drupal\bgcheck\Helpers\Helper;

/**
 * Defines a form that configures forms module settings.
 */
class PropertySettingsForm extends ConfigFormBase {

    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'bgcheck_property_settings';
    }

    /**
    * {@inheritdoc}
    */
    protected function getEditableConfigNames() {
        return [
            'bgcheck.property_settings',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $config = $this->config('bgcheck.property_settings');

        $form['bgc_property_info'] = array(
          '#type' => 'markup',
          '#markup' => t('<hr><h2>API Static Data Settings - Property Detail</h2> <hr>'),
        );

/*        $form['bgc_property_id'] = [
          '#type' => 'textfield', '#title' => $this->t('Id'),
          '#default_value' => $config->get('bgc_property_id'),
          '#attributes' => ['readonly' => 'readonly'],
        ]; 
        $form['bgc_scrreq_id'] = [
          '#type' => 'textfield', '#title' => $this->t('ScrReqId'),
          '#default_value' => $config->get('bgc_scrreq_id'),
          '#attributes' => ['readonly' => 'readonly'],
        ];
        $form['bgc_landlord_id'] = [
          '#type' => 'textfield', '#title' => $this->t('LandlordId'),
          '#default_value' => $config->get('bgc_landlord_id'),
          '#attributes' => ['readonly' => 'readonly'],
        ];*/        
        $form['bgc_property_name'] = [
          '#type' => 'textfield', '#title' => $this->t('Property Name'),
          '#default_value' => $config->get('bgc_property_name'),
        ];
        $form['bgc_property_rent'] = [
          '#type' => 'textfield', '#title' => $this->t('Rent Amount'),
          '#default_value' => $config->get('bgc_property_rent'),
        ];
        $form['bgc_property_deposit'] = [
          '#type' => 'textfield', '#title' => $this->t('Deposit Amount'),
          '#default_value' => $config->get('bgc_property_deposit'),
        ];
/*        $form['bgc_property_address'] = [
          '#type' => 'textfield', '#title' => $this->t('Address'),
          '#default_value' => $config->get('bgc_property_address'),
        ];
        $form['bgc_property_city'] = [
          '#type' => 'textfield', '#title' => $this->t('City'),
          '#default_value' => $config->get('bgc_property_city'),
        ];        
        $form['bgc_property_state'] = [
          '#type' => 'textfield', '#title' => $this->t('State'),
          '#description' => $this->t('2 LETTER STATE CODE'),
          '#default_value' => $config->get('bgc_property_state'),
        ];
        $form['bgc_property_zip'] = [
          '#type' => 'textfield', '#title' => $this->t('Zip'),
          '#default_value' => $config->get('bgc_property_zip'),
        ];*/


        return parent::buildForm($form, $form_state);
    }

/*    public function validateForm(array &$form, FormStateInterface $form_state){
        // Save the Landlord Info in API
	    // INITIALIZE THE API
	    $theAPI = new ShareAbleAPI();
        $property = [
            //"propertyId" => $form_state->getValue('bgc_property_id'),
            "propertyName" => $form_state->getValue('bgc_property_name'),
            "rent" => $form_state->getValue('bgc_property_rent'),
            "deposit" => $form_state->getValue('bgc_property_deposit'),
            "isActive" => true,
            "addressLine1" => $form_state->getValue('bgc_property_address'),
            "addressLine2" => "",
            "addressLine3" => "",
            "addressLine4" => "",
            "locality" => $form_state->getValue('bgc_property_city'),
            "region" => $form_state->getValue('bgc_property_state'),
            "postalCode" => $form_state->getValue('bgc_property_zip'),
            "country" => "US",
            "bankruptcyCheck" => true,
            "bankruptcyTimeFrame" => 120,
            "incomeToRentRatio" => 0
        ];

        if($form_state->getValue('bgc_property_id') != null && $form_state->getValue('bgc_property_id') > 0){
	        $property['propertyId'] = $form_state->getValue('bgc_property_id');
		    // EDIT PROPERTY BASED ON THE FORM DATA
	        $landlordId = $form_state->getValue('bgc_landlord_id');
		    $propertyRsp = $theAPI->editProperty($landlordId,$property);   
		    if(!empty($propertyRsp['error'])){
		    	Helper::buildRspErrors($propertyRsp);
		    	return;
		    } 
		}else{
		    // CREATE PROPERTY BASED ON THE FORM DATA
		    $landlordConfig = \Drupal::config('bgcheck.landlord_settings');
	        $landlordId = $landlordConfig->get('bgc_landlord_id');
	        if($landlordId > 0){
			    $propertyRsp = $theAPI->createProperty($landlordId,$property);   
			    if(!empty($propertyRsp['error'])){
			    	Helper::buildRspErrors($propertyRsp);
			    	return;
			    }else{
			    	// CREATE THE SCREENIGN REQUEST
			    	$propertyId = $propertyRsp['data']->propertyId; 
			    	$scrReqRsp = $theAPI->createScrReq($landlordId,$propertyId);
			    	if(!empty($scrReqRsp['error'])){
				    	Helper::buildRspErrors($scrReqRsp);
				    	return;
				    }else{
			    		$form_state->setValue('bgc_landlord_id', $landlordId);
			    		$form_state->setValue('bgc_property_id', $propertyId);
			    		$form_state->setValue('bgc_scrreq_id', $scrReqRsp['data']->screeningRequestId);
				    }
			    } 
			}else{
				$form_state->setErrorByName('bgc_landlord_id','Landlord settings missing');
			}
		}    
	    
	    //$form_state->setErrorByName('bgc_landlord_email','Test Error'); 	
    }
*/


    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('bgcheck.property_settings')

            //->set('bgc_property_id', $form_state->getValue('bgc_property_id'))
            //->set('bgc_scrreq_id', $form_state->getValue('bgc_scrreq_id'))
            //->set('bgc_landlord_id', $form_state->getValue('bgc_landlord_id'))
            ->set('bgc_property_name', $form_state->getValue('bgc_property_name'))
            ->set('bgc_property_rent', $form_state->getValue('bgc_property_rent'))            
            ->set('bgc_property_deposit', $form_state->getValue('bgc_property_deposit'))
            // ->set('bgc_property_address', $form_state->getValue('bgc_property_address'))
            // ->set('bgc_property_city', $form_state->getValue('bgc_property_city'))
            // ->set('bgc_property_state', $form_state->getValue('bgc_property_state'))
            // ->set('bgc_property_zip', $form_state->getValue('bgc_property_zip'))

            ->save();
        parent::submitForm($form, $form_state);
    }

}