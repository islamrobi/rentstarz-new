<?php

namespace Drupal\bgcheck\Helpers;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines Helper class.
 */
class Helper {

	public static function debugObj($obj){
        //echo '<pre>' . var_export($obj,true) . '</pre>'; die();
        kint($obj);
    } 

	public static function prettyDump($var){
		echo '<pre>' . var_export($var,true) . '</pre>';
		die();
	}

	// GENERIC ERROR MESSAGES
	public static function buildRspErrors($rsp){
		//drupal_set_message('Code : ' . $rsp['code'],'error');       		
        foreach ($rsp['error'] as $errKey => $errMsg) {
        	//drupal_set_message($errKey . ' : ' . $errMsg,'error');
        	drupal_set_message($errMsg,'error');
        }
	}

	// ERROR MESSAGES IN A FROM PAGE
	public static function buildRspErrorsForms($rsp,$form_state){
		//drupal_set_message('Code : ' . $rsp['code'],'error');
        foreach ($rsp['error'] as $errKey => $errMsg) {
        	//drupal_set_message($errKey . ' : ' . $errMsg,'error');
        	$form_state->setErrorByName($errKey,$errMsg);
        }	
	}

	//------------------------------------------------------------
	// BUILD A LANDLORD BASED ON USER DATA
	//------------------------------------------------------------
	public static function buildLandlord($userId = 0){

    	if($userId == 0)$userId = \Drupal::currentUser()->id();
    	$userNode = User::load($userId);
    	$address = $userNode->field_address->getValue()[0];

    	//kint($address); die();

        $landlord = [
            //"landlordId" => 0,
            "emailAddress" => $userNode->mail->value,
            "firstName" => $userNode->field_first_name->value,
            "lastName" => $userNode->field_last_name->value,
            "phoneNumber" => ($userNode->field_mobile->value != null) ? $userNode->field_mobile->value : '1123456789',
            "phoneType" => "Mobile",
            "businessName" => ($userNode->field_company_name->value != null) ? $userNode->field_company_name->value : 'Company',
            "businessAddress" => [
				"addressLine1" => $address['address_line1'],
				"addressLine2" => $address['address_line2'],
				"addressLine3" => "",
				"addressLine4" => "",
				"locality" => $address['locality'],
				"region" => $address['administrative_area'],
				"postalCode" => $address['postal_code'],
				"country" => $address['country_code'],
            ],
            "acceptedTermsAndConditions" => true
        ];  

        //kint($landlord); die();
        return $landlord;
	}

	//------------------------------------------------------------
	// BUILD A PROPERTY BASED ON PROPERTY ID
	//------------------------------------------------------------
	public static function buildProperty($propertyNodeId){

		$propertyNode = Node::load($propertyNodeId);
		$address = $propertyNode->field_property_address->getValue()[0];
		//kint($address); die();

		$property = [
			//"propertyId" => 0,
			"propertyName" => $propertyNode->title->value,
			"rent" => $propertyNode->field_monthly_rental->value,
			"deposit" => $propertyNode->field_deposit_amount->value,
			"isActive" => true,
			"addressLine1" => $address['address_line1'],
			"addressLine2" => $address['address_line2'],
			"addressLine3" => "",
			"addressLine4" => "",
			"locality" => $address['locality'],
			"region" => $address['administrative_area'],
			"postalCode" => $address['postal_code'],
			"country" => $address['country_code'],
			"bankruptcyCheck" => true,
			"bankruptcyTimeFrame" => 60,
			"incomeToRentRatio" => 1000			
		];

		//kint($property); die();

		return $property;	

	}

	//------------------------------------------------------------
	// BUILD A RENTER PERSON DATA FROM A SCREENING NODE
	//------------------------------------------------------------
    public static function buildPerson($nid){

        $scrNode = Node::load($nid); 
        if(empty($scrNode) || !isset($scrNode) || $scrNode == NULL){
        	return NULL;
        }

        $middle_name = $scrNode->field_middle_name->value;
        if($middle_name == NULL || !isset($middle_name))$middle_name = '';
         
        $address = $scrNode->field_address; 
        $person = [
            "personId" => $scrNode->field_api_entity_id->getValue()[3]['value'],
            "emailAddress" => $scrNode->field_email->value,
            "firstName" => $scrNode->field_first_name->value,
            "middleName" => $middle_name, //$scrNode->field_middle_name->value,
            "lastName" => $scrNode->field_last_name->value,
            "phoneNumber" => $scrNode->field_phone->value,
            "phoneType" => "Mobile",
            "socialSecurityNumber" => $scrNode->field_ssn->value,
            "dateOfBirth" => $scrNode->field_dob->value,
            "homeAddress" => [
                "addressLine1" => $address->address_line1,
                "addressLine2" => $address->address_line2,
                "addressLine3" => "",
                "addressLine4" => "",
                "locality" => $address->locality,
                "region" => $address->administrative_area,
                "postalCode" => $address->postal_code,
                "country" => $address->country_code,
            ],
            "acceptedTermsAndConditions" => true
        ];
        
        return $person;

    }

    // GET FULLNAME OF A USER 
    public static function getFullName($user){
    	return $user->field_first_name->value.' '.$user->field_middle_name->value.' '.$user->field_last_name->value;
    }

    // GET FULLNAME OF A USER 
    public static function getEmail($userId = 0){
    	if($userId == 0)$userId = \Drupal::currentUser()->id();
    	$userNode = User::load($userId);
    	return $userNode->get('mail')->value;
    }

    // GET FULLNAME OF A USER 
    public static function getUserByEmail($userEmail){
    	$userIds = \Drupal::entityQuery('user')
        	->condition('mail', $userEmail)->execute();
    	foreach ($userIds as $id => $userId) {
    		return $userId;
    	}
    	return 0;
    }


    // CHECK IF USER IS A LOANDLORD/AGENT
    public static function isLandlordorAgent($userId = 0){
    	if($userId == 0)$userId = \Drupal::currentUser()->id();
		$userNode = User::load($userId);
		$profileType = $userNode->field_profile_type->value;
		$uroles = $userNode->getRoles();
		if( in_array('landlord',$uroles) || in_array('agent',$uroles) || $profileType == "landlord" || $profileType == "agent" ){
			return true;
		}
		return false;
    }

	// ------------------------------------------------------------
	// GET PACKAGE & PAYEMNT INFO
	// ------------------------------------------------------------
	public static function getPaymentStatus(){

		$renterUserId = \Drupal::currentUser()->id();
		$query = \Drupal::entityQuery('node')->condition('type', 'screening')
			->condition('field_payment_status', 2)
			->condition('uid', $renterUserId)
			->condition('status', 1);	
		$scrNodeIds = $query->execute();	
		if(empty($scrNodeIds)){
			return 0;
		}
		foreach ($scrNodeIds as $id => $scrNodeId) {
			$scrNodeNode = Node::load($scrNodeId);
			return $scrNodeNode->field_report_types->value;
		}
		return 0;
	}


	// ------------------------------------------------------------
    // CHECK IF PAYMENT NODE ALREADY EXISTS
	// ------------------------------------------------------------
	public static function getPackageInfo($email = ''){

		$userId = \Drupal::currentUser()->id();
		if(!isset($userId))return 0;

		if(!empty($email)){ // LANDLORD

			$query = \Drupal::entityQuery('node')->condition('type', 'screqs')
				->condition('uid', $userId)
				->condition('field_email',$email)
				->accessCheck(false)
				->condition('status', 1);	
			$reqNodeIds = $query->execute();	
			if(empty($reqNodeIds)){
				return 0;
			}
			foreach ($reqNodeIds as $id => $reqNodeId) {
				$reqNode = Node::load($reqNodeId);
				return $reqNode->field_report_types->value;
			}
			
		}else{ // RENTER
			
			$query = \Drupal::entityQuery('node')->condition('type', 'screening')
				->condition('uid', $userId)
				->condition('status', 1);	
			$scrNodeIds = $query->execute();	
			if(empty($scrNodeIds)){
				return 0;
			}
			foreach ($scrNodeIds as $id => $scrNodeId) {
				$scrNode = Node::load($scrNodeId);
				return $scrNode->field_report_types->value;
			}
		}


		return 0;
	}

	// ------------------------------------------------------------
    // CHECK IF PAYMENT NODE ALREADY EXISTS
	// ------------------------------------------------------------
	public static function isPayemntExists($userId = 0){
		if($userId == 0)$userId = \Drupal::currentUser()->id();
		$query = \Drupal::entityQuery('node')->condition('type', 'payments')
			->condition('field_payment_status', 2)
			->condition('uid', $userId)
			->condition('status', 1);	
		$payNodeIds = $query->execute();
		foreach ($payNodeIds as $id => $payNodeId) {
			return $payNodeId;
		}
		return 0;				
	}



	// ------------------------------------------------------------
    // GET SCEENING NODE OF A RENTER 
	// ------------------------------------------------------------
	public static function getScrNode($renterUserId = 0){
		if($renterUserId == 0)$renterUserId = \Drupal::currentUser()->id();
		if(!isset($renterUserId))return NULL;
		$query = \Drupal::entityQuery('node')->condition('type', 'screening')
			->condition('uid', $renterUserId)
			->condition('status', 1);	
		$scrNodeIds = $query->execute();
		if(empty($scrNodeIds)){
			return NULL;
		}		
		foreach ($scrNodeIds as $id => $scrNodeId) {
			$scrNode = Node::load($scrNodeId);
			return $scrNode;
		}	
		return NULL;	
	}

	// ------------------------------------------------------------
    // GET REPORT TYPES FROM A SCEENING NODE
	// ------------------------------------------------------------
	public static function getReportTypes($scrNode){
		$reportTypesField = $scrNode->field_report_types;
		$reportTypeTxt = $reportTypesField->getFieldDefinition()->getFieldStorageDefinition()->getOptionsProvider('value', $reportTypesField->getEntity())->getPossibleOptions()[$reportTypesField->value];
		$reportTypes = explode('+',$reportTypeTxt);		
		return $reportTypes;		
	}

	// ------------------------------------------------------------
    // CHECK IF USER EXISTS WITH A REQ NODE
	// ------------------------------------------------------------
/*    public static function getScrReqNode($renterEmail){
		$ids = \Drupal::entityQuery('node')->condition('type', 'screqs')
			//->condition('uid', $userId) // disabled to stop sending more than 1 request from any landlord to this email
			->condition('field_email', $renterEmail)
			//->notExists('field_api_entity_id.3') // Check if renter Id exisits
			->condition('status', 1)
			->range(0, 1)->execute();
    	if(!empty($ids)){
    		//kint(array_values($ids)[0]); die();
    		//return true;
    		return $scrReqNode = Node::load(array_values($ids)[0]);
    	}
    	return NULL;
    }*/

	// ------------------------------------------------------------
    // GET REQ NODE BY RENTER EMAIL
	// ------------------------------------------------------------
    public static function getReqNodeByEmail($renterEmail){
		$ids = \Drupal::entityQuery('node')->condition('type', 'screqs')
			//->condition('uid', $userId) // disabled to stop sending more than 1 request from any landlord to this email
			->condition('field_email', $renterEmail)
			//->notExists('field_api_entity_id.3') // Check if renter Id exisits
			->condition('status', 1)
			->range(0, 1)->execute();
    	if(!empty($ids)){
    		//kint(array_values($ids)[0]); die();
    		//return true;
    		return $scrReqNode = Node::load(array_values($ids)[0]);
    	}
    	return NULL;
    }



	// ------------------------------------------------------------
    // CHECK IF USER EXISTS WITH A REQ NODE
	// ------------------------------------------------------------
    public static function getPayingReqNode($email){
		//if($userId == 0)$userId = \Drupal::currentUser()->id();
		$ids = \Drupal::entityQuery('node')->condition('type', 'screqs')
			->condition('field_email', $email)
			->condition('field_payer', 'Landlord')
			->condition('field_payment_status', '2')
			->condition('status', 1)
			->sort('changed' , 'DESC')
			->range(0, 1)->execute();
    	if(!empty($ids)){
    		return $reqNode = Node::load(array_values($ids)[0]);
    	}
    	return NULL;
    }


	// ------------------------------------------------------------
    // CHECK IF USER EXISTS
	// ------------------------------------------------------------
    public static function getUserScrReqNode($userId = 0){
    	if($userId == 0)$userId = \Drupal::currentUser()->id();
		$ids = \Drupal::entityQuery('node')->condition('type', 'screqs')
			->condition('uid', $userId) 
			->condition('status', 1)
			->range(0, 1)->execute();
    	if(!empty($ids)){
    		return $scrReqNode = Node::load(array_values($ids)[0]);
    	}
    	return NULL;
    }


	// ------------------------------------------------------------
    // ROUTE BACK TO HOME (LANDLORD/RENTER)
	// ------------------------------------------------------------
    public static function backToHome($message='Something is not right',$status='error'){

    	// IF USER NOT LOGGED IN
    	drupal_set_message($message,$status);
    	$userId = \Drupal::currentUser()->id();
    	if(!isset($userId) || empty($userId) || $userId == NULL){
    		$rrsp = new RedirectResponse(\Drupal::url('<front>'));
    		$rrsp->send();
    	}

    	// IF LOGGED IN, REDIRECT BASED ON ROLES
        $userNode = User::load($userId);
		$uroles = $userNode->getRoles();
		if(in_array('landlord',$uroles) || in_array('agent',$uroles)){
        	$rrsp = new RedirectResponse(\Drupal::url('bgcheck.landlord-home', [] ));
        	$rrsp->send(); 
		}
        $rrsp = new RedirectResponse(\Drupal::url('bgcheck.renter-home', [] )); 
        $rrsp->send();	
    }


    public static function goToFrontPage(){
    	new RedirectResponse(\Drupal::url('<front>'));
    }

}