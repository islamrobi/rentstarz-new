<?php

namespace Drupal\bgcheck\Helpers;

use Drupal\bgcheck\Helpers\Helper;

/**
 * Defines APITestController class.
 */
class ShareAbleAPI {

    private $API_BASE_URL;
    private $PARTNER_ID;
    private $API_KEY;
    private $CLIENT_ID;
    private $sequrityToken;
    private $authHeader;

    public function __construct(){

        $bgcheckConfig = \Drupal::config('bgcheck.api_settings'); 
        $this->PARTNER_ID = $bgcheckConfig->get('bgc_partner_id');
        $this->CLIENT_ID = $bgcheckConfig->get('bgc_client_id');
        $this->API_KEY = $bgcheckConfig->get('bgc_api_key');
        $this->API_BASE_URL = $bgcheckConfig->get('bgc_api_base_url');   

        $this->sequrityToken = $this->getSequrityToken();
        $this->authHeader = [ 
        	'authorization' => $this->sequrityToken, 
        	'content-type' => 'application/json' 
        ];
    }

    public function fails(){
        if($this->sequrityToken == null || !isset($this->sequrityToken)){
            return true;
        }
        return false;
    }

    // ---------------------------------------------------------------
    // BUILD RESPONSE TO RETURN
    // ---------------------------------------------------------------

    function buildResponse($res){

    	// GET THE RESPONSE BODY
        $response = json_decode($res->getBody(true));
        //Helper::prettyDump($response);
        $statusCode = $res->getStatusCode();
        $output = [ 'data' => null, 'error' => [], 'code' => $statusCode ];
        $httpErrorCodes = [400,401,403,404,422];
        $successCodes = [200,201];

        // IF ANY HTTP/CLIENT ERROS
        if(in_array($statusCode, $httpErrorCodes)){
	        // IF ANY NULL RSP ERRORS
	        if(empty($response)){
	        	$output['error']['Sorry'] = 'No Response, Please try again';
	        	return $output;
	        }        	
        	$output['error']['Reason'] = $response->message;
        	if(!empty($response->errors)){
                foreach ($response->errors as $err) {
                	if(isset($err->field)){
                    	$output['error'][$err->field] = $err->message;
                    }else{
                    	$output['error']['common'] = $err->message;
                    }
                }   
            }  
        }else if(in_array($statusCode, $successCodes)){
            $output['data'] = $response;
        }else{
        	$output['error']['Sorry'] = 'Something went wrong, Please try again';
        }
        return $output;        	
    }

    // ---------------------------------------------------------------
    // GET THE SECURITY TOKEN
    // ---------------------------------------------------------------
    public function getSequrityToken(){

        // PASS THE NECESSARY CREDENTIALS
        $token = null;
        $authData = [ "ClientId" => $this->CLIENT_ID, "apiKey" => $this->API_KEY ]; 

        try{
            // MAKE THE HTTP CALL FOR TOKEN
            $client = \Drupal::httpClient();
            $res = $client->request('POST',$this->API_BASE_URL."Tokens", [
                'json' => $authData,
                'Content-type' => 'application/json',
                'http_errors' => false
            ]);
            // GET AND RETURN THE TOKEN
            if($res->getStatusCode() == 200 || $res->getStatusCode() == 201){
                $tokenArr = json_decode($res->getBody()); $token = $tokenArr->token;
            }
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

        $this->sequrityToken = $token;
        return $token;      
    }


    // ---------------------------------------------------------------
    // GETS A SINGLE LOANDLORD DATA
    // ---------------------------------------------------------------
    public function getLandlord($landlordId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}", [
                'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            //Helper::prettyDump(json_decode($res->getBody(true))); 
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }     

    }

    // ---------------------------------------------------------------
    // CREATE A NEW LANDLORD DATA
    // ---------------------------------------------------------------
    public function createLandlord($landlord){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."Landlords", [
                'json' => $landlord, 'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }


    // ---------------------------------------------------------------
    // EDIT A LANDLORD DATA
    // ---------------------------------------------------------------
    public function editLandlord($landlord){

        try {
            $res = \Drupal::httpClient()->request('PUT',$this->API_BASE_URL."Landlords", [
                'json' => $landlord, 'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }


   	// ---------------------------------------------------------------
    // CREATE A NEW PROPERTY DATA
    // ---------------------------------------------------------------
    public function createProperty($landlordId,$property){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."Landlords/{$landlordId}/Properties", [
                'json' => $property, 'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }


    // ---------------------------------------------------------------
    // EDIT A PROPERTY DATA
    // ---------------------------------------------------------------
    public function editProperty($landlordId,$property){

        try {
            $res = \Drupal::httpClient()->request('PUT',$this->API_BASE_URL."Landlords/{$landlordId}/Properties", [
                'json' => $property, 'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }


    // ---------------------------------------------------------------
    // GET ALL PROPERTIES FOR A LANDLORD
    // ---------------------------------------------------------------
    public function getProperties($landlordId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}/Properties", [
                'http_errors' => false, 'headers' => $this->authHeader                
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }   

    }

    // ---------------------------------------------------------------
    // GET SCRENNING REQUESTS FOR THE LANDLORD
    // ---------------------------------------------------------------
    public function getLandlordScrReqs($landlordId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Landlords/{$landlordId}/ScreeningRequests", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }     

    }

    // ---------------------------------------------------------------
    // GET SCRENNING REQUESTS FOR A RENTER
    // ---------------------------------------------------------------
    public function getRenterScrReqs($renterId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Renters/{$renterId}/ScreeningRequests", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }     

    }


    // ---------------------------------------------------------------
    // CREATE A NEW SCREENING REQUEST DATA
    // ---------------------------------------------------------------
    public function createScrReq($landlordId,$propertyId){

        $scrReq = [
            //"landlordId" => $landlordId, "propertyId" => $propertyId, "initialBundleId" => 3, // 2 - Basic // 3 - Plus
            "landlordId" => $landlordId, "propertyId" => $propertyId, "initialBundleId" => 5003, // 5002 - Basic // 5003 - Plus
            //"screeningRequestRenters" => []
        ];

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."ScreeningRequests", [
                'json' => $scrReq, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }

    // ---------------------------------------------------------------
    // GET A RENTER DATA
    // ---------------------------------------------------------------
    public function getRenter($renterId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Renters/{$renterId}", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }        

    }

    // ---------------------------------------------------------------
    // CREATE A NEW RENTER DATA
    // ---------------------------------------------------------------
    public function createRenter($renter){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."Renters", [
                'json' => $renter, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }      

    }

    // ---------------------------------------------------------------
    // EDIT A RENTER DATA
    // ---------------------------------------------------------------
    public function editRenter($renter){

        try {
            $res = \Drupal::httpClient()->request('PUT',$this->API_BASE_URL."Renters", [
                'json' => $renter, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }      

    }

    // ---------------------------------------------------------------
    // CREATE A NEW SCREENING REQUEST RENTER DATA
    // ---------------------------------------------------------------
    public function createScrReqRenter($screeningRequestId,$landlordId,$renterId,$bundleId=5003,$expiryDays=30){

        $scrReqRenter = [
            "landlordId" => $landlordId, "renterId" => $renterId, 
            //"bundleId" => $bundleId, // 2 - Basic // 3 - Plus
            "bundleId" => $bundleId, // 5002 - Basic // 5003 - Plus
            "renterRole" => "Applicant", // Applicant, CoSigner
            //"renterStatus" => "IdentityVerificationPending", //"IdentityVerificationPending", 
            // IdentityVerificationPending, ScreeningRequestCanceled, ReadyForReportRequest, PaymentFailure, ReportsDeliveryInProgress, ReportsDeliveryFailed, ReportsDeliverySuccess, RetryLimitExceeded, ScreeningRequestExpired
            "reportsExpireNumberOfDays" => $expiryDays
        ];

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."ScreeningRequests/{$screeningRequestId}/ScreeningRequestRenters", [
                'json' => $scrReqRenter, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }       

    }

    // ---------------------------------------------------------------
    // GET A SCREENING REQUEST RENTER DATA
    // ---------------------------------------------------------------
    public function getScrReqRenter($screeningRequestRenterId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        } 

    }

    // ---------------------------------------------------------------
    // GET SCREENING REQUEST RENTER REPORT NAMES
    // ---------------------------------------------------------------
    function getScrReqRenterReportNames($screeningRequestRenterId){
      
        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports/Names", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        } 

    }


    // ---------------------------------------------------------------
    // CHECK IF THE RENTER IS VERIFIED
    // ---------------------------------------------------------------
    public function verifyRenter($screeningRequestRenterId,$person){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Validate", [
                'json' => $person, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        } 
     
    }

    // ---------------------------------------------------------------
    // CREATE A NEW EXAM DATA
    // ---------------------------------------------------------------
    public function createExam($screeningRequestRenterId,$examData){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Exams", [
                'json' => $examData, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        } 
     
    }

    // ---------------------------------------------------------------
    // CREATE ANSWERS
    // ---------------------------------------------------------------
    public function createAnswers($screeningRequestRenterId,$examId,$answers){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."ScreeningRequestRenters/{$screeningRequestRenterId}/Exams/{$examId}/Answers", [
                'json' => $answers, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        } 

    }

    // ---------------------------------------------------------------
    // CREATES A REPORT
    // ---------------------------------------------------------------
    public function createReport($screeningRequestRenterId,$renter){

        try {
            $res = \Drupal::httpClient()->request('POST',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports", [
                'json' => $renter, 'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }

    }

    // ---------------------------------------------------------------
    // GET REPORT NAMES
    // ---------------------------------------------------------------
    public function getReportNames($screeningRequestRenterId){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports/Names", [
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }
   
    }

    // ---------------------------------------------------------------
    // GET A REPORT DATA
    // ---------------------------------------------------------------
    public function getReport($screeningRequestRenterId,$reportType){

        try {
            $res = \Drupal::httpClient()->request('GET',$this->API_BASE_URL."Renters/ScreeningRequestRenters/{$screeningRequestRenterId}/Reports", [
            	'timeout' => 60000,
                'query' => [ 'requestedProduct' => $reportType ], 
                'http_errors' => false, 'headers' => $this->authHeader               
            ]);
            return $this->buildResponse($res);
        }catch (Exception $ex) {
            watchdog_exception('error', $ex);
        }
        //$reportArr = $response->reportResponseModelDetails; 
        //$output['reports'] = $reportArr;
   
    }


}