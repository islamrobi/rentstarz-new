<?php

namespace Drupal\bgcheck\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ModifiedResourceResponse;

use Drupal\node\Entity\Node;

/**
 * Provides a Report Status
 *
 * @RestResource(
 *   id = "manualauth_status",
 *   label = @Translation("ManualAuth Status"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/screening/manualauthentication/status"
 *   }
 * )
 */
class ManualAuth extends ResourceBase {


	/**
	* Responds to entity POST requests.
	* @return \Drupal\rest\ResourceResponse
	*/
    public function post($data){

		//return new ResourceResponse($data);
		//$response = ['data' => $data];

/*		// POSSIBLE RESPONSE
		{
			"ScreeningRequestRenterId": 1234, // Screening Request Renter ID (integer)
			"ManualAuthenticationStatus": "Passed" // Possible value: “UserAuthenticated” (string)
		}*/

		$rsp = [];

		if(array_key_exists('ScreeningRequestRenterId',$data) && array_key_exists('ManualAuthenticationStatus',$data)){

			// vars
			$scrReqRenterId = $data['ScreeningRequestRenterId'];
			$reportStatus = $data['ManualAuthenticationStatus'];

			// find screening data based on the scrreqrenterid
	        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
	        $query = \Drupal::entityQuery('node')->condition('type', 'screening')
	        	->condition('field_api_entity_id.4', $scrReqRenterId)
	        	->condition('status', 1);
			$scrNodeIds = $query->execute();
			$scrNodeId = array_values($scrNodeIds)[0];

			if($scrNodeId != null){

				$scrNode = Node::load($scrNodeId);

				// Create Notification Node
				$notification = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
					'title' => 'Manual Identity verification has been complete',
					'description' =>  'Manual Identity verification status updated. Please click here to view details',
					'target_link' => 'internal:/screening/renter/',
					'uid' => $scrNode->getOwnerId(), // The user that should recieve this notification.
				]);
				$notification->save();			
			}

		}

		// Build the response
		$response = $rsp;

		// Response without caching
		$build = array(
		  '#cache' => array(
		    'max-age' => 0,
		  ),
		);
		//return (new ResourceResponse($response))->addCacheableDependency($build);
		//return new ResourceResponse($response); 
		return new ModifiedResourceResponse($response);

	}

}