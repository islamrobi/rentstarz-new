<?php

namespace Drupal\bgcheck\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\bgcheck\Helpers\Helper;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides a Report Status
 *
 * @RestResource(
 *   id = "report_status",
 *   label = @Translation("Report Status"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/screening/reports/status"
 *   }
 * )
 */
class ReportStatus extends ResourceBase {


	/**
	* Responds to entity POST requests.
	* @return \Drupal\rest\ResourceResponse
	*/
    public function post($data){

		//return new ResourceResponse($data);
		//$response = ['data' => $data];
		// Write some codes later to update the report status

/*		// POSSIBLE RESPONSE
		{
			"ScreeningRequestRenterId": 1234, // Screening Request Renter ID (integer)
			"ReportsDeliveryStatus": "Success" // Possible values: “ReportCompleted”, “ReportUpdated” (string)
		}*/
		$rsp = [];

		if(array_key_exists('ScreeningRequestRenterId',$data) && array_key_exists('ReportsDeliveryStatus',$data)){

			// vars
			$scrReqRenterId = $data['ScreeningRequestRenterId'];
			$reportStatus = $data['ReportsDeliveryStatus'];

			// find screening data based on the scrreqrenterid
	        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
	        $query = \Drupal::entityQuery('node')->condition('type', 'screqs')
	        	->condition('field_api_entity_id.4', $scrReqRenterId)
	        	->condition('status', 1);
			$reqNodeIds = $query->execute();
			$reqNodeId = array_values($reqNodeIds)[0];

			// Screening Data Found
			if($reqNodeId != null){

				$reqNode = Node::load($reqNodeId);

				//echo $reqNode->field_user->getValue()[0]['target_id'];  die();

				// Create Notification Node
				$notification = \Drupal::entityTypeManager()->getStorage('inotify_notification')->create([
					'title' => 'Your Background Report is ready',
					'description' =>  'Background report status updated. Please click here to view details',
					'target_link' => 'internal:/screening/renter/',
					'uid' => $reqNode->field_user->getValue()[0]['target_id'], // The user that should recieve this notification.
				]);
				$notification->save();	

			}

		}

		
		// Build the response
		$response = $rsp;

		// Response without caching
		$build = array(
		  '#cache' => array(
		    'max-age' => 0,
		  ),
		);
		//return (new ResourceResponse($response))->addCacheableDependency($build);
		//return new ResourceResponse($response); 
		return new ModifiedResourceResponse($response);
	}

}