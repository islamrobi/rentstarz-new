<?php

namespace Drupal\bgcheck\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\user\Entity\User;
use Drupal\bgcheck\Helpers\Helper;

use Stripe\Stripe;
use Stripe\PaymentIntent;
use Stripe\StripeClient;

/**
 * Creates & returns a Stripe Intent
 *
 * @RestResource(
 *   id = "stripe_payment",
 *   label = @Translation("Stripe Payment"),
 *   uri_paths = {
 *     "canonical" = "/api/stripe/payment"
 *   }
 * )
 */
class StripePayment extends ResourceBase {

	/**
	* Responds to entity GET requests.
	* @return \Drupal\rest\ResourceResponse
	*/

	public function get(){

		// CHECK USER & GET INFO
        $userId = \Drupal::currentUser()->id();
        if(!isset($userId) || empty($userId)){
        	$response = ['success' => false, 'data' => null, 'error' => 'User Not Found'];
        	return new ResourceResponse($response); 
        }
	    $user = User::load($userId);
        if(!isset($user) || empty($user)){
        	$response = ['success' => false, 'data' => null, 'error' => 'User Not Found'];
        	return new ResourceResponse($response); 
		}	
		$userName = Helper::getFullName($user);
		$userEmail = $user->get('mail')->value;	

		// GET THE PACKAGE
		try{
			//return new ResourceResponse(['data' => $email]); 
			if(Helper::isLandlordorAgent()){
				$payer = 'Landlord';
				if(isset($_GET['key'])){
					$email = base64_decode($_GET['key']);								
					$package = Helper::getPackageInfo($email);
					//return new ResourceResponse(['data' => $email]); 
				}else{
					$response = ['success' => false, 'data' => null, 'error' => 'Package info not found'];
					return new ResourceResponse($response); 				
				}
			}else{
				$package = Helper::getPackageInfo();
				//return new ResourceResponse(['data' => $package]); 
			}
		}catch(Exception $ex){
			$response = ['success' => false, 'data' => null, 'error' => $ex];
			return new ResourceResponse($response); 			
		}

		// GET PAYMENT INFO
		$paymentConfig = \Drupal::config('bgcheck.payment_settings');
		$packages = [
			'2' => $paymentConfig->get('bgc_pay_basic'),
			'3' => $paymentConfig->get('bgc_pay_premium'),
		];
		$tax = $paymentConfig->get('bgc_pay_tax');
		$discount = $paymentConfig->get('bgc_pay_discount');
		$amount = $packages[$package];
		if($discount > 0)$amount = number_format($packages[$package] - ($packages[$package] * ($discount / 100)),2);	
		$taxAmount = ($tax / 100) * $amount;
		$totalAmount = 	$amount + $taxAmount;
		$stripe_secret_key = $paymentConfig->get('bgc_stripe_sec_key');

		// GET USER INFORMATION
		//if($payer == "Landlord"){}

	    // CREATE THE PAYMENT INTENT
		try{
			// Create the Intent
			Stripe::setApiKey($stripe_secret_key);
			$intent = PaymentIntent::create([
			    'amount' => round($totalAmount,2) * 100,
			    'currency' => 'usd',
				//'metadata' => ['integration_check' => 'accept_a_payment'],
				'payment_method_types' => ['card'],
				'capture_method' => 'manual', // only for landlord payments				
			]);
			$data = [
				'client_secret' => $intent->client_secret,
				//'payment_intent_id' => $intent->id,
				'name' => $userName,
				'email' => $userEmail,
				//'amount' => $intent->amount,
				//'created' => $intent->created,
				// 'totalAmount' => $totalAmount,
			];
			$response = ['success' => true, 'data' => $data, 'error' => null];
		}catch(Exception $ex){
			$error = $ex;
			$response = ['success' => false, 'data' => null, 'error' => $error];
		}

		// Response without caching
		$build = array(
		  '#cache' => array(
		    'max-age' => 0,
		  ),
		);
		//return (new ResourceResponse($response))->addCacheableDependency($build);

		//$rsp = new ResourceResponse($response);
		return new ModifiedResourceResponse($response);
        //$rsp->addCacheableDependency($response);
        //return $rsp;

		//return new ResourceResponse($response);  	
	}

}