<?php

namespace Drupal\bgcheck\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a System Status
 *
 * @RestResource(
 *   id = "system_status",
 *   label = @Translation("System Status"),
 *   uri_paths = {
 *     "canonical" = "/api/screening/System"
 *   }
 * )
 */
class SystemStatus extends ResourceBase {

	/**
	* Responds to entity GET requests.
	* @return \Drupal\rest\ResourceResponse
	*/

	public function get(){
		$response = ['status' => 'System is running!'];
		//return new ResourceResponse($response);  	

		// Response without caching
		$build = array(
		  '#cache' => array(
		    'max-age' => 0,
		  ),
		);
		return (new ResourceResponse($response))->addCacheableDependency($build);

	}

}