<?php

namespace Drupal\invited\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\invited\Helpers\Helper;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use \Drupal\node\Entity\Node;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class RegForm extends FormBase {

    //private $returnURL;
	private $email;
	private $access_key;

    public function __construct(){
		// Get the email from URL
        $email = \Drupal::request()->get('email');
        if(isset($email) && $email != null){
        	$this->email = base64_decode($email);
		}

		// Get the access key from URL
		$this->access_key = '';
		if(isset($_GET['access_key']) && $_GET['access_key']){
        	$this->access_key = $_GET['access_key'];
        }

    }

    /**
    * Build the simple form.
    *
    * A build form method constructs an array that defines how markup and
    * other form elements are included in an HTML form.
    *
    * @param array $form
    *   Default form array structure.
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    *   Object containing current form state.
    *
    * @return array
    *   The render array defining the elements of the form.
    */
    public function buildForm(array $form, FormStateInterface $form_state) {
		// CHECK IF USER ALREADY LOGGED IN
		if(\Drupal::currentUser()->isAuthenticated()){
			//throw new AccessDeniedHttpException();
			return new RedirectResponse(\Drupal::url('user.login',[]));
		}    	

    	// CHECK IF VALID EMAIL PASSED
    	if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {  
			throw new NotFoundHttpException();    		
    	}

    	// CHECK IF EMAIL IS ALREADY REGISTERED
    	if(Helper::checkUserExists($this->email)){
    		//throw new NotFoundHttpException();  
    		throw new AccessDeniedHttpException();
    	}

        // Invited email
        $form['email'] = [
			//'#type' => 'hidden',
			'#title' => $this->t('Email'),
			'#type' => 'email',
			'#required' => TRUE,
			'#attributes' => ['readonly' => 'readonly'],
			'#default_value' => $this->email,
			'#disabled' => TRUE,
			//'#default_value' => $uname,
        ];

        $form['fname'] = [
        	'#title' => $this->t('First Name'),
			'#type' => 'textfield',
			'#required' => TRUE,
        ];
        $form['lname'] = [
        	'#title' => $this->t('Last Name'),
			'#type' => 'textfield',
			'#required' => TRUE,
        ];
        $form['pass'] = [
        	'#type' => 'password_confirm',
			'#required' => TRUE,
        ];        
        
        $form['agree'] = [
        	'#title' => $this->t('I agree to the terms & conditions'),
			'#type' => 'checkbox',
			'#required' => TRUE,
        ];

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Register Now')];
        $form['#theme'] = 'reg_form';
        return $form;
    }

   /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
    public function getFormId() {
        return 'reg_form';
    }

   /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    	$pass = $form_state->getValue('pass');
    	$fname = $form_state->getValue('fname');
    	$lname = $form_state->getValue('lname');

    	// Check if names are null
    	if($fname == null || $lname == null){
    		$form_state->setErrorByName('fname','Please enter your name');
    	}

    	// Password Validation
    	$pass = $form_state->getValue('pass');
    	if(strlen($pass) < 6){
    		$form_state->setErrorByName('pass','Password should be at least 6 characters long');
    	}
    }


  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
    public function submitForm(array &$form, FormStateInterface $form_state) {  

       	// --------------------------------------------------------------
    	// CREATE THE USER
		// --------------------------------------------------------------
    	$pass = $form_state->getValue('pass');
    	$fname = $form_state->getValue('fname');
    	$lname = $form_state->getValue('lname');
    	$agree = $form_state->getValue('agree');

		$language = \Drupal::languageManager()->getCurrentLanguage()->getId();
		$user = User::create();

		// Mandatory settings
		$user->setPassword($pass);
		$user->enforceIsNew();
		$user->setEmail($this->email);

    	// Get the unique username
		$uname = Helper::cleanupUsername($this->email);
		$uname = Helper::getUniqueUsername($uname);
		$user->setUsername($uname); 

	    // $user->setPassword(user_password()); // dont need it
		$user->set("status",1);
		$user->set("field_verified",1);
		$user->set("field_i_agree_with_the_terms_and",$agree);
		$user->set("field_first_name",$fname);
		$user->set("field_last_name",$lname);
		
		$user->set("field_user_key", $this->access_key);

		// LANG SETTINGS
		//$user->set("init", 'email');
		$user->set("langcode", $language);
		$user->set("preferred_langcode", $language);
		$user->set("preferred_admin_langcode", $language);
		//$user->activate();

		// SAVE USER
		$res = $user->save();

		// Email With Welcome message
		$mailManager = \Drupal::service('plugin.manager.mail');
		$params['data'] = [
			'name' => $fname.' '.$lname
		];
		$langcode = \Drupal::currentUser()->getPreferredLangcode();
		$result = $mailManager->mail('invited', 'reg_confirm_email', $this->email, $langcode, $params, NULL, true);
		if ($result['result'] !== true) {
			drupal_set_message(t('There was a problem sending your message.'), 'error');
		}
		else {
			drupal_set_message(t('Registration complete'),'status');
		}


		// LOGIN
		if(!empty($user)) {
			user_login_finalize($user);
		}
    }

}
