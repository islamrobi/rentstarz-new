<?php

namespace Drupal\invited\Helpers;

use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Defines Helper class.
 */
class Helper {

    // CHECK IF USER EXISTS
    public static function checkUserExists($email){
    	$query = \Drupal::entityQuery('user');
    	$query->condition('mail', $email);
        // $orQuery = $query->orConditionGroup();
        // $orQuery->condition('name', $email);
        // $query->condition($orQuery);
        $userIds = $query->execute();
    	foreach ($userIds as $id => $userId) {
    		return true;
    	}
    	return false;
    }

    // GENERATE A UNIQUE USERNAME
	public static function getUniqueUsername($name, $uid = 0) {
		// Iterate until we find a unique name.
		$i = 0;
		$database = \Drupal::database();
		do {
			$new_name = empty($i) ? $name : $name . '_' . $i;
			$found = $database->queryRange("SELECT uid from {users_field_data} WHERE uid <> :uid AND name = :name", 0, 1, [
				':uid' => $uid, 
				':name' => $new_name
			])->fetchAssoc();
			$i++;
		} while (!empty($found));
		return $new_name;
	}

	// CLEANS UP USERNAME.
	public static function cleanupUsername($name) {

		// Take the first part of the email
		$name = strtolower(explode('@',$name)[0]);

		// Strip illegal characters.
		$name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);

		// Strip leading and trailing spaces.
		$name = trim($name);

		// Convert any other series of spaces to a single underscore.
		$name = preg_replace('/\s+/', '_', $name);

		// If there's nothing left use a default.
		$name = ('' === $name) ? t('user') : $name;

		// replace other special chracters to underscore
		//$name = preg_replace('/@.-*$/', '_', $name);
		$name = preg_replace('/[^a-zA-Z0-9\']/', '_', $name);

		// Truncate to a reasonable size.
		$name = (mb_strlen($name) > (UserInterface::USERNAME_MAX_LENGTH - 10)) ? mb_substr($name, 0, UserInterface::USERNAME_MAX_LENGTH - 11) : $name;
		return $name;
	}





}