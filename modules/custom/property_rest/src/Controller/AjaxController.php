<?php
namespace Drupal\property_rest\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends ControllerBase {

    public function call_ajax($nid, $action){
        $response = array();
        $current_user = \Drupal::currentUser();

        if($current_user->isAuthenticated()){
            $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
            $flag_id = 'favorite';
            $flag_service = \Drupal::service('flag');
            $flag = $flag_service->getFlagById($flag_id);

            if(isset($action) && $action == 'flag'){
                // Flag an entity with a specific flag.
                $flag_service->flag($flag, $node);
            }

            if(isset($action) && $action == 'unflag'){
                // Unflag an entity with a specific flag.
                $flag_service->unflag($flag, $node);
            }

            $response['nid'] = $nid;
        }else{
            $response['nid'] = 0;
        }

        return new JsonResponse($response);
    }

}