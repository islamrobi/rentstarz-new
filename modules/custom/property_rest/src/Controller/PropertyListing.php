<?php
namespace Drupal\property_rest\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field\Entity\FieldConfig;

/**
 * Provides route responses for the property_rest module.
 */
class PropertyListing extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
    public function listingPage() {
        $fields['beds'] = [];
        $fields['pets_allowed'] = [];
        $fields['property_type'] = [];
        $fields['rental_type'] = [];
        $fields['field_features'] = [];
        
        // Get field definition of 'field_bedrooms'
        $property_field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'property');
        $field_beds = [];

        if (isset($property_field_definitions['field_bedrooms'])) {
            $field_beds = $property_field_definitions['field_bedrooms']->getSetting('allowed_values');

            foreach($field_beds as $key=>$value){
                array_push($fields['beds'], $key);
            }
        }
        
        $terms_pets = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('pets');
        foreach ($terms_pets as $pets) {
            array_push($fields['pets_allowed'], ['id' => $pets->tid, 'name' => $pets->name]);
        }

        $property_type = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('property_type');
        foreach ($property_type as $type) {
            array_push($fields['property_type'], ['id' => $type->tid, 'name' => $type->name]);
        }

        $features = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('features');
        foreach ($features as $feature) {
            array_push($fields['field_features'], ['id' => $feature->tid, 'name' => $feature->name]);
        }

        return array(
            '#theme' => 'property_listing',
            '#fields' => $fields,
        );
    }

    function property_rest_entity_type_alter(array &$entity_types) {
        $form_modes = \Drupal::service('entity_display.repository')->getAllFormModes();
        print_r($form_modes);
    }

}