<?php
/**
 * @file
 * Contains \Drupal\property_rest\Form\LocationSearchForm.
 */
namespace Drupal\property_rest\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class LocationSearchForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_search_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['property_location'] = array(
      '#type' => 'textfield',
      '#placeholder' => $this->t('Search Location'),
      '#suffix' => '<i class="location-search-icon"></i>',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));
  }
}