<?php

namespace Drupal\property_rest\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the PropertyRestSettings form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class PropertyRestSettings extends ConfigFormBase {

   /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'property_rest.settings',
    ];
  }

   /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'property_rest_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('property_rest.settings');

    $form['default_price_min'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum Default Price'),
      '#default_value' => $config->get('default_price_min'),
      '#description' => $this->t('This is the default Minimum Price value for filtering the list of property in REST request'),
    ];

    $form['default_price_max'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Maximum Default Price'),
        '#default_value' => $config->get('default_price_max'),
        '#description' => $this->t('This is the default Maximum Price value for filtering the list of property in REST request'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('property_rest.settings')
        ->set('default_price_min', $form_state->getValue('default_price_min'))
        ->set('default_price_max', $form_state->getValue('default_price_max'))
        ->save();

    // Confirmation on form submission.
    drupal_set_message($this->t('The configuration options have been saved.'));
  }

}
