<?php
/**
 * @file
 * Contains \Drupal\property_rest\Plugin\Block\LocationSearchBlock.
 */
namespace Drupal\property_rest\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'location_search_block' block.
 *
 * @Block(
 *   id = "location_search_block",
 *   admin_label = @Translation("Location Search")
 * )
 */
class LocationSearchBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
    public function build() {
        $form = \Drupal::formBuilder()->getForm('Drupal\property_rest\Form\LocationSearchForm');
        return $form;
    }
}