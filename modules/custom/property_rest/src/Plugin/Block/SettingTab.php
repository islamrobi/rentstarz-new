<?php

/**
 * @file
 * Contains \Drupal\property_rest\Plugin\Block\SettingTab.
 */

namespace Drupal\property_rest\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'setting_tab' block.
 *
 * @Block(
 *   id = "setting_tab",
 *   admin_label = @Translation("Setting tab user page")
 * )
 */
class SettingTab extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path = explode("/", $current_path);
    $edit = $profile = $cancel = '';
    if ($path[3] == 'edit')
      $edit = 'active';
    elseif ($path[3] == 'cancel')
      $cancel = 'active';
    else
      $profile = 'active';

    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    $link = '';
    if (in_array('renter', $roles)) {
      $link = '<a href="/user/' . \Drupal::currentUser()->id() . '/edit" class="' . $edit . '">Settings</a> 
              <a href="/user/' . \Drupal::currentUser()->id() . '/cancel" class="' . $cancel . '">Delete Account</a>';
    } else {
      $link = 
          '<a href="/user/' . \Drupal::currentUser()->id() . '/edit" class="' . $edit . '">Settings</a>
          <a href="/user/' . \Drupal::currentUser()->id() . '" class="' . $profile . '">Profiles</a>
          <a href="/user/' . \Drupal::currentUser()->id() . '/cancel" class="' . $cancel . '">Delete Account</a>';
    }
    $data = '<header class="modal-page-header">
      				<div class="row">
        				<div class="col-md-12">
                            <div class="block block--rentstarz-page-title">
                                <h1 class="display-4">Settings</h1>
                            </div>
						</div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<div class="underline"></div>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<div class="title-rent-like-a-star">'.
      							$link
      						.'</div>
      					</div>
      				</div>
      			</header>';

    return [
        '#markup' => $this->t($data),
        '#cache' => array('max-age' => 0),
            //'#attributes' => array('class'  => 'page-layout-modal'),
    ];
  }

}
