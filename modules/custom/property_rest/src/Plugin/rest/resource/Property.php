<?php

namespace Drupal\property_rest\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Entity\Index;
use Drupal\webprofiler\Config\ConfigFactoryWrapper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Property REST API - GET
 *
 * @RestResource(
 *   id = "property",
 *   label = @Translation("Property"),
 *   uri_paths = {
 *     "canonical" = "/rest/api/property/{id}",
 *     "https://www.drupal.org/link-relations/create" = "/rest/api/property/{id}"
 *   }
 * )
 */

class Property extends ResourceBase {
    /**
     * A current user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;

    /**
     * The request object that contains the parameters.
    *
    * @var \Symfony\Component\HttpFoundation\Request
    */
    protected $request;

    /**
     * Drupal\webprofiler\Config\ConfigFactoryWrapper definition.
     *
     * @var \Drupal\webprofiler\Config\ConfigFactoryWrapper
     */
    protected $configFactory;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request object.
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     *   A current user instance.
     */

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user,
        Request $request, $config_factory) {
            parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
            $this->request = $request;
            $this->currentUser = $current_user;
            $this->configFactory = $config_factory;
        }
    
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('property'),
            $container->get('current_user'),
            $container->get('request_stack')->getCurrentRequest(),
            $container->get('config.factory')
        );
    }

    /**
     * Responds to GET requests.
     *
     * Returns a list of bundles for specified entity.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function get($nid) {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();
        
        $property = [];
        $view_count = 0;
        $own_property = false;
        $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
        $user = User::load(\Drupal::currentUser()->id());
        $co_applicant_nid = $user->field_co_applicant_id->value;
        $co_applicant_name = NULL;
        $co_applicant_email = NULL;

        // Check application data is exist or not
        $application_data_exist = false || $user->field_application_data_exist->getValue()[0]['value'];

        if(isset($co_applicant_nid) && !empty($co_applicant_nid)){
            $co_applicant_node = \Drupal::entityTypeManager()->getStorage('node')->load($co_applicant_nid);
            $field_about_co_applicant = $this->getParagraph($co_applicant_node, 'field_about_co_applicant');
            
            $co_applicant_name = $this->getParagraphFieldValue($field_about_co_applicant, 'field_first_name'). ' ' . $this->getParagraphFieldValue($field_about_co_applicant, 'field_last_name');
            $co_applicant_email = $this->getParagraphFieldValue($field_about_co_applicant, 'field_email');
        }

        /*
        $database = \Drupal::database();
        $query = $database->select('nodeviewcount', 'nvc')
            ->condition('nvc.nid', $nid);
        $node_counter = $query->execute();
        
        foreach ($node_counter as $records) {
            foreach($records as $record){
                $view_count = $record;
            }
        }
        

        $response = new ResourceResponse($node_counter);
        $response->addCacheableDependency($node_counter);
        return $response;
        die();
        */

        //Check the property is flagged or not
        $flags = array();
        $flagged = 0;
        $database = \Drupal::database();
        $query = $database->query("SELECT * FROM flagging WHERE entity_id = :nid", [
            ':nid' => $nid,
        ]);
        $results = $query->fetchAll();

        foreach($results as $result){
            array_push($flags, $result);
        }

        if(count($flags) > 0){
            $flagged = 1;
        }
        
        //Check the current user is the property owner or not
        if($node->getOwnerId() == \Drupal::currentUser()->id()){
            $own_property = true;
        }

        $property_owner = User::load($node->getOwnerId());
        $property_owner_verified = $property_owner->field_verified->getValue()[0]['value'];

        if(!$property_owner->user_picture->isEmpty()){
            $owner_pic = file_create_url($property_owner->user_picture->entity->getFileUri());
        }
        else {
            $owner_pic = ''; // get default picture
        }

        if($property_owner->field_first_name->isEmpty()){
            $owner_name = $property_owner->field_last_name->value;
        }elseif($property_owner->field_last_name->isEmpty()){
            $owner_name = $property_owner->field_first_name->value;
        }else{
            $owner_name = $property_owner->field_first_name->value.' '.$property_owner->field_last_name->value;
        }

        if($node->field_property_status->value == 'active'){
            if(is_object($node) && $property_owner_verified){
                $property['nid'] = $node->id();
                $property['uid'] = $node->getOwnerId();
                $property['own_property'] = $own_property;
                $property['owner_name'] = $owner_name;
                $property['owner_pic'] = $owner_pic;
                $property['owner_email'] = $property_owner->getEmail();
                $property['owner_company_name'] = $property_owner->field_company_name->value?$property_owner->field_company_name->value:'No Company';
                $property['owner_contact_naumber'] = $property_owner->field_mobile->value?$property_owner->field_mobile->value:'No Contact Number';
                $property['created'] = \Drupal::service('date.formatter')->formatInterval(REQUEST_TIME - $node->created->value, 3).' ago';
                //$property['total_views'] = $view_count;
                $property['title'] = $node->getTitle();
                $property['description'] = $node->body->value;
                $property['housing_type'] = $node->field_property_type->referencedEntities()[0]->name[0]->value;
                $property['amenities'] = $this->get_taxonomy_terms($node->field_features);
                $property['beds'] = $node->field_bedrooms->value;
                $property['bathrooms'] = $node->field_bathrooms->value;
                $property['property_area'] = $node->field_property_area->value;
                $property['available-from'] = $node->field_available_from->value;
                $property['lease_length'] = $node->field_lease_length->value;
                $property['monthly_rental'] = $node->field_monthly_rental->value;
                $property['deposit_amount'] = $node->field_deposit_amount->value;
                $property['property_pictures'] = $this->get_files($node->field_property_pictures);
                $property['property_video'] = $this->get_files($node->field_property_video)[0];
                $property['property_address'] = $node->field_property_address->getValue()[0];
                $property['lat'] = $node->field_location->getValue()[0]['lat'];
                $property['lng'] = $node->field_location->getValue()[0]['lng'];
                $property['location'] = $node->field_location->getValue()[0];
                $property['status'] = $node->status->value;
                $property['flagged'] = $flagged;
                $property['application_data_exist'] = $application_data_exist;

                if(isset($co_applicant_name) && !empty($co_applicant_name)){
                    $property['co_applicant_name'] = $co_applicant_name;
                }

                if(isset($co_applicant_email) && !empty($co_applicant_email)){
                    $property['co_applicant_email'] = $co_applicant_email;
                }
            }
        }

        $response = new ResourceResponse($property);
        $response->addCacheableDependency($property);
                
        return $response;
    }

    function get_taxonomy_terms($taxonomy){
        $taxonomy_terms = [];
        foreach ($taxonomy as $term) {
            $taxonomy_term = Term::load($term->target_id);
            $name = $taxonomy_term->getName();
            array_push($taxonomy_terms, $name);
        }
        return $taxonomy_terms;
    }

    function get_files($files){
        $img_urls = array();

        foreach ($files as $item) {
            if ($item->entity) {
                $img_urls[] = $item->entity->url();
            }
        }

        return $img_urls;
    }

    /**
    * Getter method for paragraph
    */
    public function getParagraph($entity, $field_name)
    {
        return Paragraph::load($entity->get($field_name)->getValue()[0]['target_id']);
    }

    public function getParagraphFieldValue($paragraph, $name)
    {
        return $paragraph->$name->value;
    }
}