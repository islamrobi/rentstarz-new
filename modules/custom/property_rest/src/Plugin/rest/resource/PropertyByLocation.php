<?php

namespace Drupal\property_rest\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Entity\Index;
use Drupal\webprofiler\Config\ConfigFactoryWrapper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Property REST API - GET
 *
 * @RestResource(
 *   id = "propertybylocation",
 *   label = @Translation("Property By Location"),
 *   uri_paths = {
 *     "canonical" = "/rest/api/propertybylocation",
 *     "https://www.drupal.org/link-relations/create" = "/rest/api/property/{id}"
 *   }
 * )
 */

class PropertyByLocation extends ResourceBase {
    /**
     * A current user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;

    /**
     * The request object that contains the parameters.
    *
    * @var \Symfony\Component\HttpFoundation\Request
    */
    protected $request;

    /**
     * Drupal\webprofiler\Config\ConfigFactoryWrapper definition.
     *
     * @var \Drupal\webprofiler\Config\ConfigFactoryWrapper
     */
    protected $configFactory;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request object.
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     *   A current user instance.
     */

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user,
        Request $request, $config_factory) {
            parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
            $this->request = $request;
            $this->currentUser = $current_user;
            $this->configFactory = $config_factory;
        }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('propertybylocation'),
            $container->get('current_user'),
            $container->get('request_stack')->getCurrentRequest(),
            $container->get('config.factory')
        );
    }

    /**
     * Responds to GET requests.
     *
     * Returns a list of bundles for specified entity.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function get() {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        $own_property = false;

        // Get the configuration object
        $config = $this->configFactory->get('property_rest.settings');

        /* /rest/api/property?type=10&pets=4 */
        $property = [];
        $lat = $this->request->get('lat');
        $lng = $this->request->get('lng');
        $current_property_id = $this->request->get('current');

        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', 'property')
            ->accessCheck(false);
        if((isset($lat) && !empty($lat)) && (isset($lng) && !empty($lng))){
            $group = $query->andConditionGroup()
                ->condition('field_location.lat', $lat)
                ->condition('field_location.lng', $lng);
            $query->condition($group);
        }

        $nids = $query->execute();

        $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

        foreach($nodes as $node){
            if($node->field_property_status->value == 'active' && $node->id() != $current_property_id){
                if($node->getOwnerId() == \Drupal::currentUser()->id()){
                    $own_property = true;
                }

                //Check the property is flagged or not
                $flags = array();
                $flagged = 0;
                $database = \Drupal::database();
                $query = $database->query("SELECT * FROM flagging WHERE entity_id = :nid", [
                    ':nid' => $node->id(),
                ]);
                $results = $query->fetchAll();

                foreach($results as $result){
                    array_push($flags, $result);
                }

                if(count($flags) > 0){
                    $flagged = 1;
                }

                $property_owner = User::load($node->getOwnerId());
                $property_owner_verified = $property_owner->field_verified->getValue()[0]['value'];

                if(is_object($node) && $property_owner_verified){
                    $response_result['nid'] = $node->id();
                    $response_result['uid'] = $node->getOwnerId();
                    $response_result['own_property'] = $own_property;
                    $response_result['title'] = $node->getTitle();
                    $response_result['description'] = $node->body->value;
                    $response_result['housing_type'] = $node->field_property_type->referencedEntities()[0]->name[0]->value;
                    $response_result['beds'] = $node->field_bedrooms->value;
                    $response_result['bathrooms'] = $node->field_bathrooms->value;
                    $response_result['property_area'] = $node->field_property_area->value;
                    $response_result['available-from'] = $node->field_available_from->value;
                    $response_result['lease_length'] = $node->field_lease_length->value;
                    $response_result['monthly_rental'] = $node->field_monthly_rental->value;
                    $response_result['deposit_amount'] = $node->field_deposit_amount->value;
                    $response_result['amenities'] = $this->get_taxonomy_terms($node->field_features);
                    $response_result['pets_allowed'] = $node->field_pets_allowed->referencedEntities()[0]->name[0]->value;
                    $response_result['property_pictures'] = $this->get_files($node->field_property_pictures);
                    $response_result['property_video'] = $this->get_files($node->field_property_video)[0];
                    $response_result['property_address'] = $node->field_property_address->getValue()[0];
                    $response_result['lat'] = $node->field_location->getValue()[0]['lat'];
                    $response_result['lng'] = $node->field_location->getValue()[0]['lng'];
                    $response_result['location'] = $node->field_location->getValue()[0];
                    $response_result['status'] = $node->status->value;
                    $response_result['flagged'] = $flagged;

                    array_push($property, $response_result);
                }
            }
        }

        $response = new ResourceResponse($property);
        $response->addCacheableDependency($property);

        return $response;
    }

    function get_taxonomy_terms($taxonomy){
        $taxonomy_terms = [];
        foreach ($taxonomy as $term) {
            $taxonomy_term = Term::load($term->target_id);
            $name = $taxonomy_term->getName();
            array_push($taxonomy_terms, $name);
        }
        return $taxonomy_terms;
    }

    function get_files($files){
        $img_urls = array();

        foreach ($files as $item) {
            if ($item->entity) {
                $img_urls[] = $item->entity->url();
            }
        }

        return $img_urls;
    }
}
