<?php

namespace Drupal\property_rest\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Entity\Index;
use Drupal\webprofiler\Config\ConfigFactoryWrapper;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Property REST API - GET
 *
 * @RestResource(
 *   id = "property_resource",
 *   label = @Translation("Property Resource"),
 *   uri_paths = {
 *     "canonical" = "/rest/api/property",
 *     "https://www.drupal.org/link-relations/create" = "/rest/api/property"
 *   }
 * )
 */

class PropertyResource extends ResourceBase {
    /**
     * A current user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;

    /**
     * The request object that contains the parameters.
    *
    * @var \Symfony\Component\HttpFoundation\Request
    */
    protected $request;

    /**
     * Drupal\webprofiler\Config\ConfigFactoryWrapper definition.
     *
     * @var \Drupal\webprofiler\Config\ConfigFactoryWrapper
     */
    protected $configFactory;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request object.
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     *   A current user instance.
     */

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user,
        Request $request, $config_factory) {
            parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
            $this->request = $request;
            $this->currentUser = $current_user;
            $this->configFactory = $config_factory;
        }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('property_rest'),
            $container->get('current_user'),
            $container->get('request_stack')->getCurrentRequest(),
            $container->get('config.factory')
        );
    }

    /**
     * Responds to GET requests.
     *
     * Returns a list of bundles for specified entity.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function get() {
        // Clear cache for this page everytime order to have updated data
        \Drupal::service('page_cache_kill_switch')->trigger();

        $own_property = false;

        // Get the configuration object
        $config = $this->configFactory->get('property_rest.settings');

        /* /rest/api/property?type=10&pets=4 */
        $property = [];
        $favorite_property = [];
        $type = $this->request->get('type') ? explode(',', $this->request->get('type')) : '';
        $pets = $this->request->get('pets') ? explode(',', $this->request->get('pets')) : '';
        $beds = $this->request->get('beds') ? explode(',', $this->request->get('beds')) : '';
        $amenities = $this->request->get('amenities') ? explode(',', $this->request->get('amenities')) : '';
        $price_min = $this->request->get('price_min') ? $this->request->get('price_min') : $config->get('default_price_min');
        $price_max = $this->request->get('price_max') ? $this->request->get('price_max') : $config->get('default_price_max');
        $lat_left = $this->request->get('lat_left');
        $lat_right = $this->request->get('lat_right');
        $lng_left = $this->request->get('lng_left');
        $lng_right = $this->request->get('lng_right');
        $area_min = $this->request->get('area_min') ? $this->request->get('area_min') : 0;
        $area_max = $this->request->get('area_max') ? $this->request->get('area_max') : 10000;
        $favorite = $this->request->get('favorite') ? $this->request->get('favorite') : false;
        $zip = $this->request->get('zip');
        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', 'property')
            ->condition('field_monthly_rental', array($price_min, $price_max), 'BETWEEN')
            ->condition('field_property_area', array($area_min, $area_max), 'BETWEEN')
            ->accessCheck(false);
        if(
            (isset($lat_left) && !empty($lat_left)) &&
            (isset($lat_right) && !empty($lat_right)) &&
            (isset($lng_left) && !empty($lng_left)) &&
            (isset($lng_right) && !empty($lng_right))
        ){
            $group = $query->andConditionGroup()
                ->condition('field_location.lat', array($lat_left, $lat_right), 'BETWEEN')
                ->condition('field_location.lng', array($lng_left, $lng_right), 'BETWEEN');
            $query->condition($group);
        }
        if(isset($zip) && !empty($zip)){
            $group = $query->andConditionGroup()
                ->condition('field_property_address.postal_code', $zip, 'IN');
            $query->condition($group);
        }
        if(isset($type) && !empty($type)){
            $group = $query->andConditionGroup()
                ->condition('field_property_type', $type, 'IN');
            $query->condition($group);
        }

        if(isset($pets) && !empty($pets)){
            $group = $query->andConditionGroup()
                ->condition('field_pets_allowed', $pets, 'IN');
            $query->condition($group);
        }

        if(isset($beds) && !empty($beds)){
            $group = $query->andConditionGroup()
                ->condition('field_bedrooms', $beds, 'IN');
            $query->condition($group);
        }

        if(isset($amenities) && !empty($amenities)){
            $group = $query->andConditionGroup()
                ->condition('field_features', $amenities, 'IN');
            $query->condition($group);
        }
		$query->sort('created', 'DESC');
		
        $nids = $query->execute();
		

        $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

        foreach($nodes as $node){
            if($node->field_property_status->value == 'active'){
                if($node->getOwnerId() == \Drupal::currentUser()->id()){
                    $own_property = true;
                }

                //Check the property is flagged or not
                $flags = array();
                $flagged = 0;
                $database = \Drupal::database();
                $query = $database->query("SELECT * FROM flagging WHERE entity_id = :nid AND uid = :uid", [
                    ':nid' => $node->id(),
                    ':uid' => \Drupal::currentUser()->id(),
                ]);
                $results = $query->fetchAll();

                foreach($results as $result){
                    array_push($flags, $result);
                }

                if(count($flags) > 0){
                    $flagged = 1;
                }

                $property_owner = User::load($node->getOwnerId());
                $property_owner_verified = $property_owner->field_verified->value;

                if(is_object($node) && $property_owner_verified){
                    $response_result['nid'] = $node->id();
                    $response_result['uid'] = $node->getOwnerId();
                    $response_result['own_property'] = $own_property;
                    $response_result['created'] = \Drupal::service('date.formatter')->formatInterval(REQUEST_TIME - $node->created->value, 1);
                    $response_result['title'] = $node->getTitle();
                    $response_result['description'] = $node->body->value;
                    if(isset($node->field_property_type->referencedEntities()[0])){
                        $response_result['housing_type'] = $node->field_property_type->referencedEntities()[0]->name[0]->value;
                    }

                    $response_result['beds'] = $node->field_bedrooms->value;
                    $response_result['bathrooms'] = $node->field_bathrooms->value;
                    $response_result['property_area'] = $node->field_property_area->value;
                    $response_result['available-from'] = $node->field_available_from->value;
                    $response_result['lease_length'] = $node->field_lease_length->value;
                    $response_result['monthly_rental'] = $node->field_monthly_rental->value;
                    $response_result['deposit_amount'] = $node->field_deposit_amount->value;
                    $response_result['zip'] = $node->field_property_address->postal_code;
                    $response_result['amenities'] = $this->get_taxonomy_terms($node->field_features);

                    if(isset($node->field_pets_allowed->referencedEntities()[0])){
                        $response_result['pets_allowed'] = $node->field_pets_allowed->referencedEntities()[0]->name[0]->value;
                    }
                    $response_result['property_pictures'] = $this->get_files($node->field_property_pictures);
                    if(isset($this->get_files($node->field_property_video)[0])){
                        $response_result['property_video'] = $this->get_files($node->field_property_video)[0];
                    }
                    if(isset($node->field_property_address->getValue()[0])){
                        $response_result['property_address'] = $node->field_property_address->getValue()[0];
                    }
                    if(isset($node->field_location->getValue()[0])){
                        $response_result['lat'] = $node->field_location->getValue()[0]['lat'];
                    }
                    if(isset($node->field_location->getValue()[0])){
                        $response_result['lng'] = $node->field_location->getValue()[0]['lng'];
                    }
                    if(isset($node->field_location->getValue()[0])){
                        $response_result['location'] = $node->field_location->getValue()[0];
                    }
                    $response_result['status'] = $node->status->value;

                    $response_result['flagged'] = $flagged;

                    array_push($property, $response_result);

                    if($favorite && $flagged){
                        array_push($favorite_property, $response_result);
                    }
                }
            }
        }

        if($favorite){
            $response = new ResourceResponse($favorite_property);
            $response->addCacheableDependency($favorite_property);
        }else{
            $response = new ResourceResponse($property);
            $response->addCacheableDependency($property);
        }

        return $response;
    }

    function get_taxonomy_terms($taxonomy){
        $taxonomy_terms = [];
        foreach ($taxonomy as $term) {
            $taxonomy_term = Term::load($term->target_id);
            $name = $taxonomy_term->getName();
            array_push($taxonomy_terms, $name);
        }
        return $taxonomy_terms;
    }

    function get_files($files){
        $img_urls = array();

        foreach ($files as $item) {
            if ($item->entity) {
                $img_urls[] = $item->entity->url();
            }
        }

        return $img_urls;
    }
}
