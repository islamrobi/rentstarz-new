<?php

namespace Drupal\user_redirection\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Builds an Access page.
 */
class AccessChecker implements AccessInterface {
  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.
    return AccessResult::allowedIf($this->checkUserRole());
  }

  public function checkUserRole() {
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    if (in_array('renter', $roles)) {
      return false;
    } else {
      return true;
    }
  }

}
