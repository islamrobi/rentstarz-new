<?php

namespace Drupal\user_redirection\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\user\UserInterface;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
   
      if ($route = $collection->get('user.page')) {
         $route->setRequirement('_custom_access', 'Drupal\user_redirection\Access\AccessChecker::access');
      }
      if ($route = $collection->get('entity.user.canonical')) {
        $route->setRequirement('_custom_access', 'Drupal\user_redirection\Access\AccessChecker::access');
      }
    
  }

}
