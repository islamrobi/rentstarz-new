/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/inotify.js":
/*!***************************!*\
  !*** ./src/js/inotify.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.inotify = {};

  Drupal.behaviors.inotify.RefreshNotificationsView = function () {
    // Find the notification view.
    $.each(Drupal.views.instances, function getInstance(index, element) {
      if (element.settings.view_name == 'notifications') {
        element.refreshViewAjax.progress = {
          type: 'none'
        };
        $('.js-view-dom-id-' + element.settings.view_dom_id).trigger('RefreshView');
        $(document).ajaxStop(function () {
          $('.inotify-opener .count').html($('.inotify-block-content .unread').length);
          $('.inotify-opener .count').show();
        });
      }
    });
  };

  Drupal.behaviors.NotificationsBlockEvents = {
    attach: function attach(context, settings) {
      // $('.inotify-opener').once().click(function () {
      //   $('.inotify-block-content').toggleClass('active');
      //   $('.inotify-opener .count').hide();
      // });
      $('.inotify-opener').once().click(function (e) {
        e.preventDefault();
        $('.inotify-block .inotify-list-content').fadeIn('fast', function () {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '0',
            'transition': 'all 500ms'
          });
        });
        $('body').addClass('overflow-hidden');
      });
      $('.inotify-block .inotify-list-content, .inotify-block .close').once().click(function (e) {
        if ($(e.target).hasClass('inotify-list-content') || $(e.target).hasClass('close')) {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '-600px',
            'transition': 'all 500ms'
          });
          $('.inotify-block .inotify-list-content').fadeOut('fast');
          $('body').removeClass('overflow-hidden');
        }
      });
    }
  }; // Drupal.Nodejs.callbacks.RefreshNotificationsViewCallBack = {
  //   callback: function (message) {
  //   	if (message.callback == 'RefreshNotificationsViewCallBack') {
  //       Drupal.behaviors.inotify.RefreshNotificationsView();
  //   	}
  //   }
  // };
})(jQuery, Drupal);

/***/ }),

/***/ 1:
/*!*********************************!*\
  !*** multi ./src/js/inotify.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\rentstarz\themes\rentstarz\src\js\inotify.js */"./src/js/inotify.js");


/***/ })

/******/ });