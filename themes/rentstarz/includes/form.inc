<?php
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;
use \Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use \Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @file
 * Theme and preprocess functions for forms.
 */

function rentstarz_user_register_submit(array $form, FormStateInterface $form_state){

}


function rentstarz_form_user_register_form_process_pass(&$element, FormStateInterface $form_state, &$complete_form) {
	$element = \Drupal\Core\Render\Element\PasswordConfirm::processPasswordConfirm($element, $form_state, $complete_form);

    $element['pass1']['#prefix'] = '<div class="row"><div class="col-md-6">';
    $element['pass1']['#suffix'] = '</div>';

    $element['pass2']['#prefix'] = '<div class="col-md-6">';
    $element['pass2']['#suffix'] = '</div></div>';

	return $element;
}
//Address field
function rentstarz_form_user_register_form_process_address(&$element, FormStateInterface $form_state, &$complete_form) {
    //kint($element);
    $element['country_code']['#prefix'] = '<span class="d-none">';
    $element['country_code']['#suffix']= '</span>';
    $element['address_line1']['#title'] = t('Business address');

    $element['locality']['#prefix'] = '<div class="row"><div class="col-md-6">';
    $element['locality']['#suffix'] = '</div>';

    $element['administrative_area']['#prefix'] = '<div class="col-md-6">';
    $element['administrative_area']['#suffix'] = '</div></div>';

    $element['postal_code']['#prefix'] = '<div class="row"><div class="col-md-6">';
    $element['postal_code']['#suffix'] = '</div></div>';

    return $element;
}

/*
function rentstarz_user_insert($account){
    if($account->getEntityType()->id() == 'user'){
        $access_key = $account->field_user_key->getValue()[0]['value'];

        if($access_key && isset($access_key)){
            $query = \Drupal::entityQuery('node')
                ->condition('type', 'application')
                ->condition('field_access_key', $access_key);

            $results = $query->execute();

            $application_id = NULL;
            foreach($results as $result){
                $application_id = $result;
            }

            if(isset($application_id)){
                $application =  Node::load($result);
                $application->field_co_applicant_user = $account->id();
                $application->save();
            }
        }
    }
}
*/

/**
 * Implements hook_form_alter().
 */
function rentstarz_form_alter(&$form, $form_state, $form_id) {
    //Customize the user registration form
    if($form_id =='user_register_form') {
        $form['field_user_key']['#prefix'] = '<span class="d-none">';
        $form['field_user_key']['#suffix'] = '</span>';
        $form['#title'] = 'Sign up';
        /*
        $form['field_user_key']['widget'][0]['value']['#default_value'] = NULL;

        if(isset($_GET['access_key']) && $_GET['access_key']){
            \Drupal::service('page_cache_kill_switch')->trigger();

            $query = \Drupal::entityQuery('node')
                ->condition('type', 'application')
                ->condition('field_access_key', $_GET['access_key']);

            $results = $query->execute();
            $application_id = NULL;
            foreach($results as $result){
                $application_id = $result;
            }
            if($application_id){
                $application = node::load($application_id);
                $field_about_me = getParagraph($application, 'field_about_co_applicant');

                $form['field_user_key']['widget'][0]['value']['#default_value'] = t($_GET['access_key']);
                $form['account']['mail']['#default_value'] = getParagraphFieldValue($field_about_me, 'field_email');
                $form['account']['mail']['#disabled'] = TRUE;
            }else{
                $response = new \Symfony\Component\HttpFoundation\RedirectResponse('/property-listing');
                $response->send();
            }
        }
        */


        $form['#prefix'] = '<section class="user-register">';
        $form['#suffix'] = '</section>';
        //Row 1
        $form['field_first_name']['#prefix'] = '<div class="row"><div class="col-md-6">';
        $form['field_first_name']['#suffix'] = '</div>';

        $form['field_last_name']['#prefix'] = '<div class="col-md-6">';
        $form['field_last_name']['#suffix'] = '</div></div>';

        //Row 2
        $form['account']['mail']['#prefix'] = '<div class="row"><div class="col-md-6">';
        $form['account']['mail']['#suffix'] = '</div>';

        $form['account']['pass']['#prefix'] = '<div class="col-md-6">';
        $form['account']['pass']['#suffix'] = '</div></div>';

        //row
        $form['account']['pass']['#process'][] = 'rentstarz_form_user_register_form_process_pass';

        //Row 3
        $form['field_profile_type']['#prefix'] = '<div class="row profile-type"><div class="col-md-6">';
        $form['field_profile_type']['widget']['#options']['_none'] = 'Are you a landlord or agent';
        $form['field_profile_type']['#suffix'] = '</div></div>';

        //Address
        $form['field_address']['widget'][0]['address']['#process'][] = ['Drupal\address\Element\Address', 'processAddress'];
        $form['field_address']['widget'][0]['address']['#process'][] = 'rentstarz_form_user_register_form_process_address';

        //Row 4
        $form['field_business_hours']['widget'][0]['from']['#prefix']= '<div class="row"><div class="col-md-6">';
        $form['field_business_hours']['widget'][0]['from']['#suffix']= '</div>';
        $form['field_business_hours']['widget'][0]['to']['#prefix']= '<div class="col-md-6">';
        $form['field_business_hours']['widget'][0]['to']['#suffix']= '</div></div>';

        //kint($form['field_business_hours']);

        //Row 5
        $form['field_years_as_an_agent']['#prefix']= '<div class="row"><div class="col-md-6">';
        $form['field_years_as_an_agent']['#suffix']= '</div>';
        $form['field_real_estate_id']['#prefix']= '<div class="col-md-6">';
        $form['field_real_estate_id']['#suffix']= '</div></div>';

        //Row 6
        $form['field_mobile']['#prefix']= '<div class="row"><div class="col-md-6">';
        $form['field_mobile']['#suffix']= '</div></div>';


        foreach (array_keys($form['actions']) as $action) {
            if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
                //$form['actions'][$action]['#submit'][] = 'rentstarz_user_register_submit';
            }
        }
    }

    //Customize the user edit form
    if($form_id =='user_form') {
        /*
        $form['#prefix'] = '<section class="user-edit">';
        $form['#suffix'] = '</section>';
        //kint($form);

        //Row 1
        $form['field_first_name']['#prefix'] = '<div class="row"><div class="col-md-6">';
        $form['field_first_name']['#suffix'] = '</div>';

        $form['field_last_name']['#prefix'] = '<div class="col-md-6">';
        $form['field_last_name']['#suffix'] = '</div></div>';

        //Row
        $form['account']['current_pass']['#prefix'] = '<div class="row"><div class="col-md-6">';
        $form['account']['current_pass']['#suffix'] = '</div>';

        $form['account']['mail']['#prefix'] = '<div class="col-md-6">';
        $form['account']['mail']['#suffix'] = '</div></div>';

        //row
        $form['account']['pass']['#process'][] = 'rentstarz_form_user_register_form_process_pass';

        //Row
        $form['field_date_of_birth']['#prefix']= '<div class="row"><div class="col-md-4">';
        $form['field_date_of_birth']['#suffix']= '</div>';

        $form['field_gender']['#prefix']= '<div class="col-md-4">';
        $form['field_gender']['#suffix']= '</div>';

        $form['field_profile_type']['#prefix']= '<div class="col-md-4">';
        $form['field_profile_type']['#suffix']= '</div></div>';

        //Row
        $form['field_company_name']['#prefix']= '<div class="row"><div class="col-md-3">';
        $form['field_company_name']['#suffix']= '</div>';

        $form['field_mobile']['#prefix']= '<div class="col-md-3">';
        $form['field_mobile']['#suffix']= '</div>';

        $form['field_real_estate_number']['#prefix']= '<div class="col-md-3">';
        $form['field_real_estate_number']['#suffix']= '</div>';

        $form['field_year_of_experience']['#prefix']= '<div class="col-md-3">';
        $form['field_year_of_experience']['#suffix']= '</div></div>';
        */
    }

    //Customize the Property Creation and Edit form
    if(($form_id =='node_property_form') || ($form_id == 'node_property_edit_form')) {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $user_profile_type = $user->field_profile_type->getValue()[0]['value'];

        if(empty($user_profile_type)){
            //Redirect map page and return status message
            $response = new RedirectResponse('/modal/user/agent-landloard/update');
            $response->send();
            drupal_set_message('Add your Agent or Landloard information to add listing.', 'status', TRUE);
        }

        $form['#prefix'] = '<section class="create-edit-property">';
        $form['#suffix'] = '</section>';

        //Row 1
        $form['title']['#prefix']= '<div class="row"><div class="col-md-6">';
        $form['title']['#suffix']= '</div><div class="col-md-6"></div></div>';

        //Row 3
        $form['field_bedrooms']['#prefix']= '<div class="row"><div class="col-md-3">';
        $form['field_bedrooms']['#suffix']= '</div>';

        $form['field_bathrooms']['#prefix']= '<div class="col-md-3">';
        $form['field_bathrooms']['#suffix']= '</div>';

        $form['field_property_area']['#prefix']= '<div class="col-md-3">';
        $form['field_property_area']['#suffix']= '</div>';

        $form['field_available_from']['#prefix']= '<div class="col-md-3">';
        $form['field_available_from']['#suffix']= '</div></div>';

        //Row 4
        $form['field_lease_length']['#prefix']= '<div class="row"><div class="col-md-3">';
        $form['field_lease_length']['#suffix']= '</div>';

        $form['field_monthly_rental']['#prefix']= '<div class="col-md-3">';
        $form['field_monthly_rental']['#suffix']= '</div>';

        $form['field_deposit_amount']['#prefix']= '<div class="col-md-3">';
        $form['field_deposit_amount']['#suffix']= '</div>';

        $form['field_pets_allowed']['#prefix']= '<div class="col-md-3">';
        $form['field_pets_allowed']['#suffix']= '</div></div>';

        //Unpublish property if the user is not verified
        $form['field_property_status']['#prefix'] = '<span class="d-none">';
        $form['field_property_status']['#suffix']= '</span>';

    }

    //Customize the Requirement Creation and Edit form
    if(($form_id =='node_requirement_form') || ($form_id == 'node_requirement_edit_form')) {
        $form['#prefix'] = '<section class="rent-like-a-star">';
		$form['#suffix'] = '</section>';

        //Row 1
        $form['title']['#prefix']= '<div class="row"><div class="col-md-4">';
        $form['title']['#suffix']= '</div>';

        $form['field_phone']['#prefix']= '<div class="col-md-4">';
        $form['field_phone']['#suffix']= '</div>';

        $form['field_email']['#prefix']= '<div class="col-md-4">';
        $form['field_email']['#suffix']= '</div></div>';

        //Row 2
        $form['field_income']['#prefix']= '<div class="row"><div class="col-md-4">';
        $form['field_income']['#suffix']= '</div>';

        $form['field_number_of_applicants']['#prefix']= '<div class="col-md-4">';
        $form['field_number_of_applicants']['#suffix']= '</div>';

        $form['field_number_of_children']['#prefix']= '<div class="col-md-4">';
        $form['field_number_of_children']['#suffix']= '</div></div>';

        //Row 5
        $form['field_floor_level']['#prefix']= '<div class="row"><div class="col-md-4">';
        $form['field_floor_level']['#suffix']= '</div>';

        $form['field_bedrooms']['#prefix']= '<div class="col-md-4">';
        $form['field_bedrooms']['#suffix']= '</div>';

        $form['field_bathrooms']['#prefix']= '<div class="col-md-4">';
        $form['field_bathrooms']['#suffix']= '</div></div>';

        //Row 6
        $form['field_monthly_rental']['#prefix']= '<div class="row"><div class="col-md-4">';
        $form['field_monthly_rental']['#suffix']= '</div>';

        $form['field_deposit_amount']['#prefix']= '<div class="col-md-4">';
        $form['field_deposit_amount']['#suffix']= '</div>';

        $form['field_lease_length']['#prefix']= '<div class="col-md-4">';
        $form['field_lease_length']['#suffix']= '</div></div>';
    }

    if($form_id =='user_login_form') {
        $form['#title'] = 'Welcome back';
        $form['name']['#weight'] = 1;
        $form['pass']['#weight'] = 2;
        $form['link'] = array('#markup' => \Drupal::l(t('Forgot password'), Url::fromUri('internal:/user/password')),'#weight' => 3);
        $form['persistent_login']['#weight'] = 4;
        $form['actions']['submit']['#value'] = 'Sign in';
        $form['actions']['sign_up'] = array('#markup' => \Drupal::l(t("Don't have an account. Sign up"), Url::fromUri('internal:/user/register')));
        $form['actions']['sign_up']['#prefix']= '<div class="sign-up-link">';
        $form['actions']['sign_up']['#suffix']= '</div></div>';
    }

    if($form_id =='node_property_form') {
        $form['#title'] = 'Add your listing';
    }
}

/**
 * Getter method for paragraph
 */
function getParagraph($node, $name){
    return Paragraph::load($node->get($name)->getValue()[0]['target_id']);
}

function getParagraphFieldValue($paragraph, $name){
    return $paragraph->$name->value;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
// function rentstarz_form_user_login_form_alter(&$form, FormStateInterface $form_state) {
//     $form['#submit'][] = 'rentstarz_user_login_submit';
// }

/**
 * Form submission handler for user_login_form().
 *
 * Redirects the user to the dashboard after logging in.
 */
// function rentstarz_user_login_submit(&$form, FormStateInterface $form_state) {
//     $user = \Drupal::currentUser();
//     kint($user->id());
//     exit;

//     $url = '/modal/co-applicant/edit';

//     // Check if a destination was set, probably on an exception controller.
//     // @see \Drupal\user\Form\UserLoginForm::submitForm()
//     $request = \Drupal::service('request_stack')->getCurrentRequest();
//     if (!$request->request->has('destination')) {
//       $form_state->setRedirectUrl($url);
//     }
//     else {
//       $request->query->set('destination', $request->request->get('destination'));
//     }
// }
