<?php
/**
 * @file
 * Theme and preprocess functions for views.
 */

function rentstarz_views_pre_render(&$view)
{
  if($view->id() == "applications") {

    /*
    if($view->current_display == 'page_applications_on_property'){
      $results = &$view->result;
      $view->field['nothing_2']->options['exclude'] = TRUE;
      $view->field['nothing_4']->options['exclude'] = TRUE;
      foreach ($results as $result) {
        if ($result->_entity->get('field_status')->value == 'pending') {
          $view->field['nothing_4']->options['exclude'] = FALSE;
        }
      }
    }
    */

    if ($view->current_display == 'page_my_applications') {
      $results = &$view->result;
      $view->field['field_status']->options['exclude'] = TRUE;
      $view->field['nothing_1']->options['exclude'] = TRUE;
      foreach ($results as $result) {
        if ($result->_entity->get('field_status')->value == 'requestinginfo') {
          $view->field['nothing_1']->options['exclude'] = FALSE;
        }

        if ($result->_entity->get('field_status')->value == 'pending') {
          $view->field['field_status']->options['exclude'] = FALSE;
        }
      }
    }

  }
}
