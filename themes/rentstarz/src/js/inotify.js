(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.inotify = {};
  Drupal.behaviors.inotify.RefreshNotificationsView = function(){
    // Find the notification view.
    $.each(Drupal.views.instances ,function getInstance(index, element) {
      if(element.settings.view_name == 'notifications') {
        element.refreshViewAjax.progress = {type:'none'};
        $('.js-view-dom-id-' + element.settings.view_dom_id).trigger('RefreshView');
        $( document ).ajaxStop(function() {
          $('.inotify-opener .count').html($('.inotify-block-content .unread').length);
          $('.inotify-opener .count').show();
        });
      }
    });
  };

  Drupal.behaviors.NotificationsBlockEvents = {
    attach: function (context, settings) {

      // $('.inotify-opener').once().click(function () {
      //   $('.inotify-block-content').toggleClass('active');
      //   $('.inotify-opener .count').hide();
      // });

      $('.inotify-opener').once().click(function (e) {
        e.preventDefault();
        $('.inotify-block .inotify-list-content').fadeIn('fast', function () {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '0',
            'transition': 'all 500ms'
          });
        });
        $('body').addClass('overflow-hidden');
      });

      $('.inotify-block .inotify-list-content, .inotify-block .close').once().click(function (e) {
        if ($(e.target).hasClass('inotify-list-content') || $(e.target).hasClass('close')) {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '-400px',
            'transition': 'all 500ms',
          });
          $('.inotify-block .inotify-list-content').fadeOut('fast');
          $('body').removeClass('overflow-hidden');
        }
      });
    }
  };

  // Drupal.Nodejs.callbacks.RefreshNotificationsViewCallBack = {
  //   callback: function (message) {
  //   	if (message.callback == 'RefreshNotificationsViewCallBack') {
  //       Drupal.behaviors.inotify.RefreshNotificationsView();
  //   	}
  //   }
  // };
})(jQuery, Drupal);
