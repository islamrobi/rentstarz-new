import 'popper.js';
import 'bootstrap';

(function ($, Drupal) {
  "use strict";

  //Drupal.behaviors.formsValidation = {
    //attach(context) {


      $(document).ready(function() {
        // Drupal.behaviors.helloWorld = {
        //   attach: function (context) {
        //console.log('Hello World');

        var property_id = 0;
        var map;
        var marker;
        var singleMarker;
        var nodeurl = '#';
        var markers = [];
        var filter = {};
        var rest_url = '/rest/api/property';
        var map_bounds;

        // Initiate Feather icon library
        feather.replace();

        //notificationList();

        // Create Google Map to browse property
        if ($('main').hasClass('property-listing-with-map')) {
          var i;

          /****************** Property Map Section ******************
           **********************************************************/
          filter.propertyType = [];
          filter.bedrooms = [];
          filter.petsAllowed = [];
          filter.amenities = [];
          filter.favorite = 0;
          map = new google.maps.Map(document.getElementById('property-map'));
          var singleMap = new google.maps.Map(document.getElementById('single-map'));
          var opt = {
            center: {
              lat: 40.709633,
              lng: -74.007511
            },
            zoom: 16,
            minZoom: 4,
            maxZoom: 20,
            scrollwheel: true,
            styles: [
              {
                "featureType": "administrative.neighborhood",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "administrative.neighborhood",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "color": "#01374f"
                  },
                  {
                    "visibility": "simplified"
                  }
                ]
              },
              {
                "featureType": "poi.attraction",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.government",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.government",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.medical",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.medical",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.place_of_worship",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.place_of_worship",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.school",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.school",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.sports_complex",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.sports_complex",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#f5f6f9"
                  }
                ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#c69c6d"
                  },
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "color": "#969696"
                  }
                ]
              },
              {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#f5f6f9"
                  },
                  {
                    "visibility": "on"
                  }
                ]
              },
              {
                "featureType": "road.highway.controlled_access",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.bus",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.bus",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.bus",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.rail",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.rail",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.rail",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.rail",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.station.rail",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#f5f6f9"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              }
            ]
          };
          map.setOptions(opt);
          singleMap.setOptions(opt);
          singleMap.scrollwheel = false;

          var propertyNotInitiated = true;

          google.maps.event.addListener(map, 'tilesloaded', function() {
            if (propertyNotInitiated) {
              filterProperty(rest_url, getCurrentMapBounds());
              propertyNotInitiated = false;
              map.currentZoom = map.getZoom();
            }
          });

          google.maps.event.addListener(map, 'dragend', function() {
            filterProperty(rest_url, getCurrentMapBounds());
            map.currentZoom = map.getZoom();
          });

          google.maps.event.addListener(map, 'place_changed', function() {
            filterProperty(rest_url, getCurrentMapBounds());
            map.currentZoom = map.getZoom();
          });

          google.maps.event.addListener(map, 'zoom_changed', function() {
            filterProperty(rest_url, getCurrentMapBounds());
            map.currentZoom = map.getZoom();
          });

          // Add a marker clusterer to manage the markers.
          var markerCluster = new MarkerClusterer(map, markers, {
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
            minimumClusterSize: 2
          });

          google.maps.event.addListener(markerCluster, 'clusteringend', function() {
            var allClusters = markerCluster.getClusters();
            $.each(allClusters, function(i, cluster) {
              if (cluster.markers_.length > 1) {
                $.each(cluster.markers_, function(j, marker) {
                  var markerId = marker.id;
                  $('.property-list .property-card#' + markerId).on('mouseenter', function(e) {
                    $(cluster.clusterIcon_.div_).addClass('active');
                  });

                  $('.property-list .property-card#' + markerId).on('mouseleave', function(e) {
                    $(cluster.clusterIcon_.div_).removeClass('active');
                  });
                });
              }
            });
          });

          /* Hover on marker cluster */
          google.maps.event.addListener(markerCluster, 'mouseover', function(cluster) {
            $(cluster.clusterIcon_.div_).addClass('active');
          });

          /* Hover Out on marker cluster */
          google.maps.event.addListener(markerCluster, 'mouseout', function(cluster) {
            $(cluster.clusterIcon_.div_).removeClass('active');
          });

          /* On Click marker cluster */
          google.maps.event.addListener(markerCluster, 'click', function(cluster) {
            var markers = cluster.getMarkers();
            var lats = [],
              lngs = [];

            markers.forEach(function(marker) {
              lats.push(marker.getPosition().lat());
              lngs.push(marker.getPosition().lng());
            });

            lats = !!lats.reduce(function(a, b) {
              return (a === b) ? a : NaN;
            });
            lngs = !!lngs.reduce(function(a, b) {
              return (a === b) ? a : NaN;
            });

            if (lats && lngs && map.currentZoom >= map.maxZoom) {
              markers = cluster.getMarkers();

              //console.log(markers);
              //var clusterIconPosition = $(cluster.clusterIcon_.div_).position();

              $.each(markers, function(i, marker) {
                //console.log('Marker: ', marker);
                $('.cluster-info-window ul').append(
                  '<li id="' + marker.id + '">' +
                  '<div class="property-image" style="background-image: url(' + marker.image + ')"></div>' +
                  '<div class="property-info">' +
                  '<div class="price">' + marker.price + ' | ' + marker.area + ' SQF</div>' +
                  '<div class="features">' + marker.beds + ' Bed, ' + marker.baths + ' Bath</div>' +
                  '</div>' +
                  '</li>'
                );
              });

              $('.cluster-info-window').show();
            }
          });

          $('.prop-filter #area_min').on('change', function(e) {
            filter.area_min = this.value;
            filterProperty(rest_url, map_bounds);
          });

          $('.prop-filter #area_max').on('change', function(e) {
            filter.area_max = this.value;
            filterProperty(rest_url, map_bounds);
          });

          $('.prop-filter .section-type input[type=checkbox]').change(function(e) {
            filter.propertyType = getFilterArray($(this), filter.propertyType);
            filterProperty(rest_url, map_bounds);
          });

          $('.prop-filter .section-bed input[type=checkbox]').change(function() {
            filter.bedrooms = getFilterArray($(this), filter.bedrooms);
            filterProperty(rest_url, map_bounds);
          });

          $('.prop-filter .section-pets input[type=checkbox]').change(function() {
            filter.petsAllowed = getFilterArray($(this), filter.petsAllowed);
            filterProperty(rest_url, map_bounds);
          });

          $('.prop-filter .section-amenities input[type=checkbox]').on('change', function() {
            filter.amenities = getFilterArray($(this), filter.amenities);
            filterProperty(rest_url, map_bounds);
          });

          $('body').on('mouseenter', '.property-list > ul > li', function(e) {
            var markerId = $(this).children('.property-card').attr('id');
            var marker = {};

            $(this).children('.property-card').addClass('active');
            $('.price-label-' + markerId).addClass('active');
          });

          $('body').on('mouseleave', '.property-list > ul > li', function(e) {
            var markerId = $(this).children('.property-card').attr('id');

            $(this).children('.property-card').removeClass('active');
            $('.price-label-' + markerId).removeClass('active');
          });

          $('body').on('mouseenter', '.price-label', function(e) {
            var markerId = $(this).attr('class').match(/\d+/)[0];

            $(this).addClass('active');
            $('.property-list > ul > li .property-card#' + markerId).addClass('active');
            $('html, body').animate({
              scrollTop: $('.property-list > ul > li .property-card#' + markerId).offset().top - 200
            }, 500);
          });

          $('body').on('mouseleave', '.price-label', function(e) {
            var markerId = $(this).attr('class').match(/\d+/)[0];

            $(this).removeClass('active');
            $('.property-list > ul > li .property-card#' + markerId).removeClass('active');
          });

          $('body').on('click', '.property-list > ul > li .property-card .property-image', function(e) {
            var nid = $(this).closest('.property-card').attr('id');
            showDetailModal(nid);
          });

          $('body').on('click', '.modal-property-details .similar-property ul li .img', function(e) {
            var nid = $(this).data('nid');
            showDetailModal(nid);
            scrollToTop('.modal-property-details');
          });

          $('body').on('click', '.modal-property-details .available-units .available-unit-link', function(e) {
            e.preventDefault();
            var nid = $(this).data('nid');
            showDetailModal(nid);
            scrollToTop('.modal-property-details');
          });

          /* Price Slider */
          var plainPrice = wNumb({
            thousand: ',',
            decimals: 2
          });

          var slider = document.getElementById('rangeslider');
          var priceMin = document.getElementById('price-field-min');
          var priceMax = document.getElementById('price-field-max');
          var priceFields = [priceMin, priceMax];
          var valueMin = document.getElementById('price-filter-min');
          var valueMax = document.getElementById('price-filter-max');


          //$('#rangeslider', context).once('myCustomBehaviorSlider').each(function () {
            // Apply the myCustomBehaviour effect to the elements only once.
            noUiSlider.create(slider, {
              start: [0, 10000],
              connect: true,
              step: 1,
              range: {
                'min': 0,
                'max': 10000
              },
              format: wNumb({
                decimals: 0,
                // thousand: ','
              })
            }, true);
          //});


          slider.noUiSlider.on('update', function(values, handle) {
            (handle ? valueMax : valueMin).innerHTML = values[handle];
            priceFields[handle].value = values[handle];
            $('.price-fields input[type="text"]').each(resizeInput);
          });

          // Listen to keydown events on the input field.
          priceFields.forEach(function(input, handle) {

            input.addEventListener('change', function() {
              slider.noUiSlider.setHandle(handle, this.value);
              $('.price-fields input[type="text"]').each(resizeInput);
            });

            input.addEventListener('keydown', function(e) {

              var values = slider.noUiSlider.get();
              var value = Number(values[handle]);

              // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
              var steps = slider.noUiSlider.steps();

              // [down, up]
              var step = steps[handle];

              var position;

              // 13 is enter,
              // 38 is key up,
              // 40 is key down.
              switch (e.which) {

                case 13:
                  slider.noUiSlider.setHandle(handle, this.value);
                  $('.price-fields input[type="text"]').each(resizeInput);
                  break;

                case 38:

                  // Get step to go increase slider value (up)
                  position = step[1];

                  // false = no step is set
                  if (position === false) {
                    position = 1;
                  }

                  // null = edge of slider
                  if (position !== null) {
                    $('.price-fields input[type="text"]').each(resizeInput);
                    slider.noUiSlider.setHandle(handle, value + position);
                  }

                  break;

                case 40:

                  position = step[0];

                  if (position === false) {
                    position = 1;
                  }

                  if (position !== null) {
                    $('.price-fields input[type="text"]').each(resizeInput);
                    slider.noUiSlider.setHandle(handle, value - position);
                  }

                  break;
              }
            });
          });

          //Price Filter on Slider Update
          slider.noUiSlider.on('end', function(values, handle) {
            filter.price_min = plainPrice.from(values[0]);
            filter.price_max = plainPrice.from(values[1]);
            filterProperty(rest_url, map_bounds);
          });
          /* EOF: Price Slider */

          /* Property Filter Section */
          var qfilterActive = false;
          var prevOption = '';

          $('section.property-list-map .filter-options button.qfilter').on('click', function(e) {
            var currentOption = $(this).data('filter');

            $('section.property-list-map .filter-options button.qfilter').removeClass('active');

            $('section.property-list-map .property-filter .filter-section').hide();
            $('section.property-list-map .property-filter .section-' + currentOption).show();

            if (qfilterActive && currentOption === prevOption) {
              $(this).removeClass('active');
              $('section.property-list-map .property-list .property-filter').removeClass('active');
              qfilterActive = false;
            } else {
              $(this).addClass('active');
              $('section.property-list-map .property-list .property-filter').addClass('active');
              qfilterActive = true;
              prevOption = currentOption;
            }
          });

          // Property Filter Modal
          $('section.property-list-map .filter-options button.open-filter').on('click', function(e) {
            $('.modal-property-filter').fadeIn('fast', function() {
              $('.modal-property-filter .content-wrapper').css({
                'left': '0',
                'transition': 'all 400ms'
              });
            });

            $('section.property-list-map .filter-options button.qfilter').removeClass('active');
            $('section.property-list-map .property-filter .filter-section').show();
            $('section.property-list-map .property-list .property-filter').removeClass('active');

            $('section.property-list-map .prop-filter .section-price').appendTo('.modal-property-filter .popup-section-price .placeholder');
            $('section.property-list-map .prop-filter .section-type').appendTo('.modal-property-filter .popup-section-type .placeholder');
            $('section.property-list-map .prop-filter .section-bed').appendTo('.modal-property-filter .popup-section-bed .placeholder');
            // $('.modal-property-filter').addClass('active');
            // $(this).fadeOut('fast');
            $('body').addClass('overflow-hidden');
          });

          $('.modal-property-filter .title').after().on('click', function(e) {
            // $('.modal-property-filter').fadeOut('fast', function(){
            var wrapperWidth = $('.modal-property-filter .content-wrapper').width();
            $('.modal-property-filter .content-wrapper').css({
              'left': '-' + wrapperWidth + 'px',
              'transition': 'all 400ms'
            });
            $('.modal-property-filter').fadeOut('fast');
            // $('.modal-property-filter').removeClass('active');
            // $('section.property-list-map .filter-options button.open-filter').fadeIn('fast');
            setTimeout(function() {
              $('.modal-property-filter .popup-section-price .placeholder .section-price').appendTo('section.property-list-map .property-filter');
              $('.modal-property-filter .popup-section-type .placeholder .section-type').appendTo('section.property-list-map .property-filter');
              $('.modal-property-filter .popup-section-bed .placeholder .section-bed').appendTo('section.property-list-map .property-filter');
            }, 600);
            $('body').removeClass('overflow-hidden');
            // });
          });
          /* EOF: Property Filter Section */

          $('#property-map').on('mousedown', function(e) {
            hideInfoWindow();
          });

          $('body').on('click', '.cluster-info-window ul li', function(e) {
            var id = $(this).attr('id');
            showDetailModal(id);
          });

          // Apply to this listing button handler
          $('.modal-property-details a.btn-apply').on('click', function(e){
            var applied = $(this).data('applied');
            var edit = $(this).data('edit');
            var coApplicantName = $(this).data('co-applicant-name');
            var coApplicantEmail = $(this).data('co-applicant-email');

            if(applied && !edit){
              e.preventDefault();
              var id = $(this).data('id');
              var name = $(this).data('name');
              var address = $(this).data('address');
              $('.apply-popup .property-name').html(name);
              $('.apply-popup .property-address').html(address);
              $('.apply-popup .property-co-applicant-name').html(coApplicantName);
              $('.apply-popup .property-co-applicant-email').html(coApplicantEmail);
              $('.apply-popup .direct-apply').attr('href', '/application/apply/direct/' + id);

              $('.apply-popup').fadeIn();
            }
          });

          $('.apply-popup .close-apply-popup, .apply-popup').on('click', function(e){
            if($(e.target).hasClass('apply-popup') || $(e.target).hasClass('close-apply-popup')){
              $('.apply-popup').fadeOut();
            }
          });

          $('.apply-popup #co-applicant-update-form').on('change', function(e){
            if($(this).is(':checked')){
              //console.log('Checked');
              $('.apply-popup .direct-apply').attr('href', '/application/apply/direct/' + property_id + '?co_applicant_update_form=true');
            }else{
              //console.log('Un-Checked');
              $('.apply-popup .direct-apply').attr('href', '/application/apply/direct/' + property_id);
            }
          });

          // Close property details modal on close click and outside of details area
          //$('.modal-property-details .close, .modal-property-details').on('click', function(e) {
          $('.modal-property-details').on('click', function(e) {
            //if ($(e.target).hasClass('close') || $(e.target).hasClass('close-img') || $(e.target).hasClass('modal-property-details')) {
            if ($(e.target).hasClass('modal-property-details')) {
              $('body').removeClass('overflow-hidden');
              $('.modal-property-details .content-wrapper').css({
                'left': '-60%',
                'transition': 'all 200ms'
              });
              setTimeout(function() {
                $('.modal-property-details').fadeOut('fast');
              }, 200);
            }
          });

          $('.modal-property-details .close').on('click', function(e) {
            if ($(e.target).hasClass('close')) {
              $('body').removeClass('overflow-hidden');
              $('.modal-property-details .content-wrapper').css({
                'left': '-60%',
                'transition': 'all 200ms'
              });
              setTimeout(function() {
                $('.modal-property-details').fadeOut('fast');
              }, 200);
            }
          });

          // Close Application details modal on close click and outside of details area
          $('.modal-application-details .close, .modal-application-details').on('click', function(e) {
            if ($(e.target).hasClass('close') || $(e.target).hasClass('close-img') || $(e.target).hasClass('modal-application-details')) {
              $('body').removeClass('overflow-hidden');
              $('.modal-application-details .content-wrapper').css({
                'left': '-60%',
                'transition': 'all 200ms'
              });
              setTimeout(function() {
                $('.modal-application-details').fadeOut('fast');
              }, 200);
            }
          });


          //$('#edit-property-location', context).once('myCustomBehavior').each(function() {

            /* Location Search */
            var searchInput = document.getElementById('edit-property-location');
            var searchBox = new google.maps.places.SearchBox(searchInput);
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', function(e) {
                var places = searchBox.getPlaces();
                if (places.length == 0)
                    return;
                markers.forEach(function(m) {
                    m.setMap(null);
                });

                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(p) {
                    // console.log(p);
                    if (!p.geometry)
                        return;
                    if (p.geometry.viewport) {
                        bounds.union(p.geometry.viewport);
                    } else {
                        bounds.extend(p.geometry.location);
                    }
                });
                map.fitBounds(bounds);
                var search_location = places[0].formatted_address.replace(/,/g, "+").trim();
                var request_url = "https://nominatim.openstreetmap.org/search.php?q=" + search_location + "&polygon_geojson=1&format=json";
                get_location_cord(request_url, map);
                filterProperty(rest_url, getCurrentMapBounds());
            });
            if ($.cookie("homeSearch")) {
                var homeSearchAddress = $.cookie("homeSearchAddress");
                $('#edit-property-location').val(homeSearchAddress);
                var places = searchBox.getPlaces();
                var service = new google.maps.places.PlacesService(map);
                // if (places.length == 0)
                //         return;
                var request = {
                    query: $('#edit-property-location').val()
                };

                if (request.query.length > 0) {
                    service.textSearch(request, function(places) {
                        searchBox.set('places', places || [])
                    });
                }
                $.cookie("homeSearchAddress", '');
                $.cookie("homeSearch", false)
            }
            // Apply the myCustomBehaviour effect to the elements only once.
        //});


          $('#location-search-form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
              e.preventDefault();
              return false;
            }
          });

          // Make item favorite
          $('body .property-list-map .item-wrapper, body .favorite-items .item-wrapper').on('click', '.favorite', function(e) {
            var nid = $(this).data('id');

            if($('body .property-list-map .item-wrapper .favorite-' + nid).hasClass('flagged-1')){
              $.ajax({
                url: "/call/property_rest/ajax/" + nid + '/unflag',
                method: 'GET',
                dataType: "json",
                success: function(response) {
                  if(response.nid){
                    $('body .property-list-map .item-wrapper .favorite-' + response.nid).removeClass('flagged-1');
                    $('body .favorite-items li.item_'+response.nid).remove();
                  }
                }
              });
            }else{
              $.ajax({
                url: "/call/property_rest/ajax/" + nid + '/flag',
                method: 'GET',
                dataType: "json",
                success: function(response) {
                  if(response.nid){
                    $('body .property-list-map .item-wrapper .favorite-' + response.nid).addClass('flagged-1');
                  }
                }
              });
            }
          });

          /****************** End Property Map Section ******************
           ***************************************************************/

        }

        // Make the agent verification message vissible
        if($('.my-property-row .views-row').hasClass('Disabled')){
          $('.agent-not-verified').show();
        }

        // Property Details Modal Popup Image Viewer
        $('.modal-property-details .property-img').magnificPopup({
          delegate: 'a',
          type: 'image',
          gallery: {
            enabled: true
          },
          callbacks: {
            open: function() {
              $(".mfp-gallery").swipe( {
                swipeLeft:function(event, direction, distance, duration, fingerCount) {
                  var magnificPopup = $.magnificPopup.instance;
                  magnificPopup.next();
                },
                swipeRight:function(event, direction, distance, duration, fingerCount) {
                  var magnificPopup = $.magnificPopup.instance;
                  magnificPopup.prev();
                }
              });
            }
          }
        });

        //Search field in the home page
        var homeSearchInput = (document.getElementById('home_location_search') !== null) ? document.getElementById('home_location_search') : '';
        // If you are at the home page.
        //  if (homeSearchInput !== null){
        google.maps.event.addDomListener(window, 'load', homesearch);

        function homesearch() {
          var options = {
            types: ['(regions)'],
            componentRestrictions: {country: 'us'}
          };
          if (homeSearchInput != ''){
            new google.maps.places.Autocomplete(homeSearchInput,options);
          }
        }

        $('.home_location_search_btn').on('click', function(e){
          $.cookie("homeSearchAddress", $('#home_location_search').val());
          $.cookie("homeSearch", true);

          window.location = "/property-listing";
        });
        // }

        $('body').on('click', 'section.favorite-items .property-card .property-image', function(e) {
          var nid = $(this).closest('.property-card').attr('id');
          showDetailModal(nid, '500px', '400ms');
        });

        $('.toggle-favorite').on('click', function(e) {
          $('body').addClass('overflow-hidden');
          $('section.favorite-items').fadeIn('fast', function() {
            $('section.favorite-items .favorite-items-container').css({
              'left': '0',
              'transition': 'all 400ms'
            });
          });

          makeFavoriteList();
        });

        $('section.favorite-items , section.favorite-items .close').on('click', function(e){
          /*$('section.favorite-items').css({
              'left': '-600px',
              'transition': 'all 200ms'
          });*/
          if($(e.target).hasClass('favorite-items') || $(e.target).hasClass('close')){
            $('section.favorite-items .favorite-items-container').css({
              'left': '-600px',
              'transition': 'all 400ms',
            });
            $('section.favorite-items').fadeOut('fast');

            $('body').removeClass('overflow-hidden');

            $('.modal-property-details .content-wrapper').css({
              'left': '-60%',
              'transition': 'all 400ms'
            });

            setTimeout(function() {
              $('.modal-property-details').fadeOut('fast');
            }, 400);
          }
        });

        function makeFavoriteList(){
          if ($('body .favorite-items').length){
            $.getJSON(rest_url + '?favorite=1', function(all_property) {
              $('.favorite-items .item-wrapper').empty();

              $.each(all_property, function(i, property) {
                var data = {
                  'property': property,
                  'countryCode': property.property_address.country_code,
                  'state': property.property_address.administrative_area,
                  'city': property.property_address.locality,
                  'postalCode': property.property_address.postal_code,
                  'imageURL': '/sites/devrentstarz.dd/files/2019-06/shutterstock_416414848.jpg'
                };

                if (property.property_pictures) {
                  data.imageURL = property.property_pictures[0];
                }

                appendItemIntoList('.favorite-items .item-wrapper', data);
              });
            });
          }
        }

        function getFilterArray(targetElm, filterArray) {
          var ischecked = targetElm.is(':checked');

          if (ischecked && (filterArray.indexOf(targetElm.val()) > -1) == false) {
            filterArray.push(targetElm.val());
          }

          if (!ischecked && filterArray.indexOf(targetElm.val()) > -1) {
            filterArray = filterArray.filter(function(value, index, arr) {
              return value != targetElm.val();
            });
          }

          return filterArray;
        }

        function filterProperty(rest_url, map_bounds) {
          var price_min = filter.price_min ? filter.price_min : 0;
          var price_max = filter.price_max ? filter.price_max : 500000;
          var area_min = filter.area_min ? filter.area_min : 0;
          var area_max = filter.area_max ? filter.area_max : 10000;
          var lat_left = map_bounds.lat_left ? map_bounds.lat_left : 0;
          var lat_right = map_bounds.lat_right ? map_bounds.lat_right : 0;
          var lng_left = map_bounds.lng_left ? map_bounds.lng_left : 0;
          var lng_right = map_bounds.lng_right ? map_bounds.lng_right : 0;
          var favorite = filter.favorite;


          // filter.propertyType = [];
          // filter.bedrooms = [];
          // filter.petsAllowed = [];
          // filter.amenities = [];
          var propertyType = '';
          $.each(filter.propertyType, function(i, type) {
            propertyType += ',' + type;
          });

          var bedrooms = '';
          $.each(filter.bedrooms, function(i, rooms) {
            bedrooms += ',' + rooms;
          });

          var pets = '';
          $.each(filter.petsAllowed, function(i, pet) {
            pets += ',' + pet;
          });
          var amenities = '';
          $.each(filter.amenities, function(i, amenity) {
            amenities += ',' + amenity;
          });

          var params = '?area_min=' + area_min +
            '&area_max=' + area_max +
            '&price_min=' + price_min +
            '&price_max=' + price_max +
            '&lat_left=' + lat_left +
            '&lat_right=' + lat_right +
            '&lng_left=' + lng_left +
            '&lng_right=' + lng_right +
            '&type=' + propertyType.replace(/^,/, '') +
            '&beds=' + bedrooms +
            '&pets=' + pets.replace(/^,/, '') +
            '&amenities=' + amenities.replace(/^,/, '') +
            '&favorite=' + favorite;

          //?list_a=1&list_a=2&list_a=3&list_b[]=1&list_b[]=2&list_b[]=3&list_c=1,2,3
          //console.log(params);
          $.getJSON(rest_url + params, function(result) {
            deleteMarkers();
            addMarkers(result);
          });
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
          }
          markers.length = 0;
          markerCluster.clearMarkers();
        }

        function addMarkers(data) {
          $('.property-list-map .item-wrapper li').remove();
          if(data.length > 0){
            $('.property-list-map .item-wrapper .no-item-text').remove();
            $.each(data, function(i, property) {
              var price = 0;

              if (property.nid) {
                nodeurl = "#" + property.nid;
              }

              if (property.monthly_rental) {
                price = property.monthly_rental;
              }

              marker = new MarkerWithLabel({
                map: map,
                position: new google.maps.LatLng(property.location.lat, property.location.lng),
                icon: '/themes/rentstarz/css/images/empty-g-marker.png',
                labelContent: '$' + formatNumber(Math.round(price)),
                labelClass: "price-label price-label-" + property.nid, // the CSS class for the label
                url: '',
                labelAnchor: new google.maps.Point(35, 35)
              });

              marker.id = property.nid;
              marker.price = '$' + formatNumber(Math.round(price));
              marker.image = property.property_pictures[0];
              marker.beds = property.beds;
              marker.baths = property.bathrooms;
              marker.area = property.property_area;

              google.maps.event.addListener(marker, 'click', function(e) {
                //window.location.href = this.url;
                showDetailModal(property.nid);
                //console.log("Node ID:", nodeurl);
              });

              markers.push(marker);
              markerCluster.addMarker(marker);

              //addProperty(title, view, image, price, details, beds, pets, area)
              addProperty(property);
            });
          }else{
            $('.property-list-map .item-wrapper').append('<li class="no-item-text">Sorry we have no listing available for the options or location you’ve selected, we are working on it</li>');
          }
        }

        /* function to create item in the property list */
        function addProperty(property) {
          var data = {
            'property': property,
            'countryCode': property.property_address.country_code,
            'state': property.property_address.administrative_area,
            'city': property.property_address.locality,
            'postalCode': property.property_address.postal_code,
            'imageURL': '/sites/devrentstarz.dd/files/2019-06/shutterstock_416414848.jpg'
          };

          if (property.property_pictures) {
            data.imageURL = property.property_pictures[0];
          }

          // var countryCode = property.property_address.country_code;
          // var state = property.property_address.administrative_area;
          // var city = property.property_address.locality;
          // var postalCode = property.property_address.postal_code;
          // var imageURL = '/sites/devrentstarz.dd/files/2019-06/shutterstock_416414848.jpg';

          appendItemIntoList('.property-list-map .item-wrapper', data);
          /*
          $('.property-list-map .item-wrapper').append(
              '<li>' +
              '<div class="property-card" id="' + property.nid + '">' +
              '<div class="details">' +
              '<div class="time"><span><i class="fa fa-clock-o" aria-hidden="true"></i> ' + property.created + ' ago</span></div>' +
              '<div class="property-image" style="background-image: url(' + imageURL + ')"></div>' +
              '<div class="property-details">' +
              '<div class="price">$' + formatNumber(Math.round(property.monthly_rental)) + '</div>' +
              '<div class="address line-1">' + property.property_address.address_line1 + '</div>' +
              '<div class="address line-2">' + city.charAt(0).toUpperCase() + city.slice(1) + ', ' + state + ' ' + postalCode + '</div>' +
              '<ul class="options">' +
              '<li class="bed">' + property.beds + '  Beds</li>' +
              '<li class="pets">' + property.bathrooms + ' Baths</li>' +
              '<li class="size">' + property.property_area + ' SQF</li>' +
              '</ul>' +
              // '<a href="#">contact</a>'+
              '<div class="favorite favorite-' + property.nid + ' flagged-' + property.flagged + '" data-id="' + property.nid + '"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg></div>' +
              // '<div class="favorite"></div>'+
              '</div>' +
              '</div>' +
              '</div>' +
              '</li>'
          );
          */
        }
        /* EOF: function to create item in the property list */

        /* Create item to push into property listing */
        function appendItemIntoList(itemWrapper, data){
          var beds = 0;
          if (data.property.beds == '5'){
            beds = '4+ beds';
          }else{
            if (data.property.beds > 1){
              beds = data.property.beds + ' Beds';
            }else{
              beds = data.property.beds + ' Bed';
            }
          }

          $(itemWrapper).append(
            '<li class="item_'+data.property.nid+'">' +
            '<div class="property-card" id="' + data.property.nid + '">' +
            '<div class="details">' +
            '<div class="time"><span><i class="fa fa-clock-o" aria-hidden="true"></i> ' + data.property.created + ' ago</span></div>' +
            '<div class="property-image" style="background-image: url(' + data.imageURL + ')"></div>' +
            '<div class="property-details">' +
            '<div class="price">$' + formatNumber(Math.round(data.property.monthly_rental)) + '</div>' +
            '<div class="address line-1">' + data.property.property_address.address_line1 + '</div>' +
            '<div class="address line-2">' + data.city.charAt(0).toUpperCase() + data.city.slice(1) + ', ' + data.state + ' ' + data.postalCode + '</div>' +
            '<ul class="options">' +
            '<li class="bed">' + beds + '  Beds</li>' +
            '<li class="pets">' + data.property.bathrooms + ' Baths</li>' +
            '<li class="size">' + data.property.property_area + ' SQF</li>' +
            '</ul>' +
            // '<a href="#">contact</a>'+
            '<div class="favorite favorite-' + data.property.nid + ' flagged-' + data.property.flagged + '" data-id="' + data.property.nid + '"><svg viewBox="0 0 24 24" width="28" height="28" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg></div>' +
            // '<div class="favorite"></div>'+
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>'
          );
        }
        /* Create item to push into property listing */

        function hideInfoWindow() {
          $('.cluster-info-window').hide();
          $('.cluster-info-window ul li').remove();
        }

        function formatNumber(num) {
          return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        function showDetailModal(nid, positionLeft = '0', duration = '200ms') {
          $.getJSON('/rest/api/property/' + nid, function(property) {
            property_id = nid;
            var beds = 0;
            if (property.beds == '5') {
              beds = '4+ beds';
            } else {
              if (property.beds > 1) {
                beds = property.beds + ' Beds';
              } else {
                beds = property.beds + ' Bed';
              }
            }

            $('.modal-property-details span.name').html(property.title);
            $('.modal-property-details span.price').html('$' + formatNumber(Math.round(property.monthly_rental)));

            $('.modal-property-details .time-ago').html(property.created);

            /* Property Image */
            $('.modal-property-details .property-img a').remove(); // Remove existing images

            // Adding new images for property
            $.each(property.property_pictures, function(i, l) {
              $('.modal-property-details .property-img').append('<a href="' + property.property_pictures[i] + '" class="img img' + i + '" style="background-image: url(' + property.property_pictures[i] + ')"></a>');
            });
            /* EOF: Property Image */

            $('.presented-by .line-2').html(property.owner_name);
            $('.presented-by .photo img').attr('src', property.owner_pic);
            $('.presented-by .line-1').html(property.owner_company_name);
            $('.presented-by .call span').html(property.owner_contact_naumber);

            $('.modal-property-details .address').html(property.property_address.address_line1);
            $('.modal-property-details .details').html(property.description);
            $('.modal-property-details .beds').html(beds);
            $('.modal-property-details .baths').html(property.bathrooms+' Bath');
            $('.modal-property-details .area').html(property.property_area + ' SQF');
            // $('.modal-property-details .total-views').html(property.total_views + ' Views');

            if (property.own_property) {
              $('.modal-property-details a.btn-apply').text('Edit Property');
              $('.modal-property-details a.btn-apply').attr('href', '/node/' + property.nid + '/edit');
              $('.modal-property-details a.btn-apply').attr('data-edit', true);
            } else {
              $('.modal-property-details a.btn-apply').text('Apply');
              $('.modal-property-details a.btn-apply').attr('href', '/modal/application/apply/' + property.nid);
              $('.modal-property-details a.btn-apply').attr('data-id', property.nid);
              $('.modal-property-details a.btn-apply').attr('data-name', property.title);
              $('.modal-property-details a.btn-apply').attr('data-address', property.property_address.address_line1);
              $('.modal-property-details a.btn-apply').attr('data-applied', property.application_data_exist);
              $('.modal-property-details a.btn-apply').attr('data-co-applicant-name', property.co_applicant_name);
              $('.modal-property-details a.btn-apply').attr('data-co-applicant-email', property.co_applicant_email);
              $('.modal-property-details a.btn-apply').attr('data-edit', false);
            }

            if (property.pets_allowed == 'No Pets') {
              $('.modal-property-details .pets').hide();
            } else {
              $('.modal-property-details .pets').show();
            }

            $.each(property.amenities, function(i, l) {
              //console.log(property.amenities[i]);
              $('.modal-property-details ul.features').append('<li>' + property.amenities[i] + '</li>');
            });

            if (property.property_video) {
              $('.modal-property-details .video-tour').show();
              $('.modal-property-details .video-tour .property_video').attr('src', property.property_video.replace('watch', 'embed'));
            } else {
              $('.modal-property-details .video-tour').hide();
            }

            $('.modal-property-details .rent').html('$' + formatNumber(Math.round(property.monthly_rental)));
            $('.modal-property-details .deposit-amount').html('$' + formatNumber(Math.round(property.deposit_amount)));
            $('.modal-property-details .lease-length').html(property.lease_length);
            $('.modal-property-details .salary-range').html('$0 - $0');

            $('.modal-property-details .map-section h4 span.address').html(property.property_address.address_line1);
            if (typeof(singleMarker) !== 'undefined') {
              //singleMarker.setMap(null);
            }

            singleMarker = new google.maps.Marker({
              map: singleMap,
              position: new google.maps.LatLng(property.lat, property.lng)
            });

            singleMap.setCenter(singleMarker.getPosition());

            $.getJSON('/rest/api/propertybylocation?lat=' + property.lat + '&lng=' + property.lng + '&current=' + property.nid, function(rproperty) {
              $('.modal-property-details .available-units-wrapper .available-unit .row').remove();
              if (rproperty.length > 1) {
                $('.modal-property-details .available-units-wrapper').show();
                $.each(rproperty, function(i, l) {
                  if (rproperty[i].nid != nid) {
                    $('.modal-property-details .available-units-wrapper .available-unit').append('<div class="row">' +
                      '<div class="col-md-4"><a style="background-image:url(' + rproperty[i].property_pictures[0] + ')" data-nid="' + rproperty[i].nid + '" href="" alt="" class="available-unit-link"></a></div>' +
                      '<div class="col-md-8">' +
                      '<div class="title">' + rproperty[i].title + '</div>' +
                      '<div class="price">$' + formatNumber(Math.round(rproperty[i].monthly_rental)) + '</div>' +
                      '<div class="specifications"><span class="beds">' + rproperty[i].beds + '</span> beds <span class="baths">' + rproperty[i].bathrooms + '</span> bath <span class="area">' + rproperty[i].property_area + '</span>SQ</div>' +
                      '<div class="details">' + rproperty[i].description + '</div>' +
                      '<div><a href="#">more...</a></div>' +
                      '</div>');
                    // '<div class="col-md-3">' +
                    // '<div class="favorite"><img src="/themes/rentstarz/css/images/icon-favorite.png" class="img-fluid"></div>' +
                    // '<div class="actions">' +
                    // '<a href="#">Apply</a><a href="#">Contact</a>' +
                    // '</div>' +
                    // '</div>' +
                    // '<hr></div>');
                  }
                });


              } else {
                $('.modal-property-details .available-units-wrapper').hide();
              }
            });

            $('.modal-property-details .similar-property ul li').remove();
            $.getJSON('/rest/api/relatedproperty?beds=' + property.beds + '&baths=' + property.bathrooms + '&current=' + property.nid, function(rdata) {

              if (rdata.length > 0) {
                $('.modal-property-details .similar-property').show();
              } else {
                $('.modal-property-details .similar-property').hide();
              }

              $.each(rdata, function(i, rproperty) {
                var pmarkup = '<li>' +
                  '<div data-nid="' + rproperty.nid + '" class="img" style="background-image: url(' + rproperty.property_pictures[0] + ')"></div>' +
                  '<div class="content">' +
                  '<div class="price-features"><span class="price">$' + formatNumber(Math.round(rproperty.monthly_rental)) + '</span><span class="related-beds">' + rproperty.beds + '</span> Beds</div>' +
                  '<div class="addressline">' + rproperty.property_address.address_line1 + '</div>' +
                  '<div class="city">' + rproperty.property_address.locality + ', ' + rproperty.property_address.administrative_area + '</div>' +
                  '</div>' +
                  '</li>';
                $('.modal-property-details .similar-property ul').append(pmarkup);
              });
            });

          });

          $('.modal-property-details').fadeIn('fast', function() {
            $('.modal-property-details .content-wrapper').css({
              'left': positionLeft,
              'transition': 'all '+duration
            });
          });

          $('body').addClass('overflow-hidden');
        }

        function showApplicationDetailModal(nid) {
          $.getJSON('/rest/api/view-property-application/' + nid, function(application) {
            //console.log(application);
            // About Me
            $('.modal-application-details .about-me .first-name').html(application.first_name);
            $('.modal-application-details .about-me .phone-number').html(application.cell_phone);
            $('.modal-application-details .about-me .email').html(application.email);
            $('.modal-application-details .about-me .driver-license').html(application.drivers_licence);
            $('.modal-application-details .about-me .date-of-birth').html(application.birth_date);

            // Residence
            $('.modal-application-details .current-residence .residence-type').html(application.residence.type);
            $('.modal-application-details .current-residence .movin-date').html(application.residence.movin_date);
            $('.modal-application-details .current-residence .city').html(application.residence.city);
            $('.modal-application-details .current-residence .state').html(application.residence.state);
            $('.modal-application-details .current-residence .zip-code').html(application.residence.zip_code);
            $('.modal-application-details .current-residence .rent').html(application.residence.rent);
            $('.modal-application-details .current-residence .landlord-name').html(application.residence.landlord_name);
            $('.modal-application-details .current-residence .reason-for-leaving').html(application.residence.reason_for_leaving);

            // Occupation
            $('.modal-application-details .current-occupation .occupation-type').html(application.occupation);
            $('.modal-application-details .current-occupation .employer-name').html(application.current_occupation.employer_name);
            $('.modal-application-details .current-occupation .job-title').html(application.current_occupation.job_title);
            $('.modal-application-details .current-occupation .total-monthly-income').html(application.current_occupation.total_monthly_income);
            $('.modal-application-details .current-occupation .work-address').html(application.current_occupation.work_address);
            $('.modal-application-details .current-occupation .city').html(application.current_occupation.city);
            $('.modal-application-details .current-occupation .zip').html(application.current_occupation.zip);
            $('.modal-application-details .current-occupation .work-type').html(application.current_occupation.work_type);
            $('.modal-application-details .current-occupation .start-date').html(application.current_occupation.start_date);
            $('.modal-application-details .current-occupation .supervisor-name').html(application.current_occupation.supervisor_name);
            $('.modal-application-details .current-occupation .phone-number').html(application.current_occupation.phone_number);

            // Previous Occupation
            $('.modal-application-details .previous-occupation .employer-name').html(application.previous_occupation.employer_name);
            $('.modal-application-details .previous-occupation .job-title').html(application.previous_occupation.job_title);
            $('.modal-application-details .previous-occupation .total-monthly-income').html(application.previous_occupation.total_monthly_income);
            $('.modal-application-details .previous-occupation .work-address').html(application.previous_occupation.work_address);
            $('.modal-application-details .previous-occupation .city').html(application.previous_occupation.city);
            $('.modal-application-details .previous-occupation .zip').html(application.previous_occupation.zip);
            $('.modal-application-details .previous-occupation .work-type').html(application.previous_occupation.work_type);
            $('.modal-application-details .previous-occupation .start-date').html(application.previous_occupation.start_date);
            $('.modal-application-details .previous-occupation .supervisor-name').html(application.previous_occupation.supervisor_name);
            $('.modal-application-details .previous-occupation .phone-number').html(application.previous_occupation.phone_number);

            //Reference
            $.each(application.reference, function(i, reference) {
              $('.modal-application-details .reference').append(
                '<div class="row">' +
                '<div class="col-md-4"><div class="val">' + reference.name + '</div></div>' +
                '<div class="col-md-4"><div class="val">' + reference.phone_number + '</div></div>' +
                '<div class="col-md-4"><div class="val"><span class="glyphicon glyphicon-envelope"></span> ' + reference.email + '</div></div>' +
                '</div>'
              );
            });

            //Rental Documents
            $.each(application.rental_documents, function(i, document) {
              $('.modal-application-details .rental-docs').append(
                '<div class="row"><div class="col-md-12">' +
                '<div class="val"><a href="' + document.url + '"><span class="glyphicon glyphicon-file"></span> ' + document.name + '</a></div>' +
                '</div></div>'
              );
            })

          });

          $('.modal-application-details').fadeIn('fast', function() {
            $('.modal-application-details .content-wrapper').css({
              'left': '0%',
              'transition': 'all 200ms'
            });
          });

          $('body').addClass('overflow-hidden');
        }

        function scrollToTop(elem) {
          $(elem).animate({
            scrollTop: 0
          }, 500);
        }

        function resizeInput() {
          $(this).attr('size', $(this).val().length);
        }

        //Get current visibale area of google map
        function getCurrentMapBounds() {
          var bounds = map.getBounds();
          var ne = bounds.getNorthEast();
          var sw = bounds.getSouthWest();
          map_bounds = {
            'lat_left': sw.lat(),
            'lat_right': ne.lat(),
            'lng_left': sw.lng(),
            'lng_right': ne.lng()
          };
          return map_bounds;
        }

        /* Popup Menu */
        $('.menu-item-dashboard').on('click', function(e){
          e.preventDefault();
          $('body').addClass('overflow-hidden');
          $('.popup-menu').fadeIn('fast', function() {
            $('.popup-menu .popup-container').css({
              'right': '0',
              'transition': 'all 500ms'
            });
          });

        });

        $('.popup-menu , .popup-menu .close').on('click', function(e){
          if($(e.target).hasClass('popup-menu') || $(e.target).hasClass('close')){
            $('.popup-menu .popup-container').css({
              'right': '-400px',
              'transition': 'all 500ms',
            });
            $('.popup-menu').fadeOut('fast');
            $('body').removeClass('overflow-hidden');
          }
        });

        /* Popup Notification */
        /*
        $('.menu-item-notifications').on('click', function(e){
          e.preventDefault();
          $('.popup-notification').fadeIn('fast', function() {
            $('.popup-notification .popup-container').css({
              'right': '0',
              'transition': 'all 500ms'
            });
          });
          $('body').addClass('overflow-hidden');

          notificationList();
        });

        $('.popup-notification , .popup-notification .close').on('click', function(e){
          if($(e.target).hasClass('popup-notification') || $(e.target).hasClass('close')){
            $('.popup-notification .popup-container').css({
              'right': '-600px',
              'transition': 'all 500ms',
            });
            $('.popup-notification').fadeOut('fast');
            $('body').removeClass('overflow-hidden');
          }
        });
        */

        /*Property application from*/
        $('.application-form-page .add-co-applicant').on('click', function(e){
          $('.application-form-page .sections-body > .section').hide();
        });

        // $('.application-form-page .start-co-applicant-form').on('click', function(){
        //     $('.application-form-page .co-applicant-form-wrapper').show();
        // });

        $('.application-form-page .co-applicant-selection input[type=radio]').on('change', function(e){
          if($(this).val() == 'off'){
            $('.application-form-page .co-applicant-form-wrapper').show();
          }else{
            $('.application-form-page .co-applicant-form-wrapper').hide();
          }
        });

        var currentSectionId = 1;
        $('.application-form-page ul.tab-menu li').on('click', function(e) {
          var sectionId = $(this).data('section-id');
          goToSection(sectionId);
        });

        $('.application-form-page .action-buttons .btn-next').on('click', function(e) {
          e.preventDefault();
          if (currentSectionId < 9) {
            goToSection(currentSectionId + 1);
          }
        });

        $('.application-form-page .action-buttons .btn-prev').on('click', function(e) {
          e.preventDefault();
          if (currentSectionId > 1) {
            goToSection(currentSectionId - 1);
          }
        });

        function goToSection(id) {
          currentSectionId = id;

          $('#myapplication-area .sections-body > .section').hide();
          $('#myapplication-area .sections-body > .section-' + id).show();

          $('#myapplication-area ul.tab-menu li').removeClass('active');
          $('#myapplication-area ul.tab-menu li.nav-' + id).addClass('active');

          $('.application-form-page .action-buttons .btn-next').show();
          $('.application-form-page .action-buttons .btn-prev').show();

          if (currentSectionId == 9) {
            $('.application-form-page .action-buttons .btn-next').hide();
            $('.application-form-page .action-buttons .btn-prev').show();
            $('.application-form-page .action-buttons .form-submit').show();
          }

          if (currentSectionId == 1) {
            $('.application-form-page .action-buttons .btn-prev').hide();
            $('.application-form-page .action-buttons .btn-next').show();
          }
        }

        // Send an email to co-applicant to fillup the form
        $('.email-to-co-applicant').on('click', function(e) {
          var access_key = $('#access_key').val();
          var data = {
            'email':'dmrobi89@gmail.com',
            'access_key': access_key
          };

          getCsrfToken(function (csrfToken) {
            sendEmail(csrfToken, data);
          });
        });

        //Ajax method to send email to co-applicant
        function sendEmail(csrfToken, data) {
          $.ajax({
            url: '/call/ajax_email',
            method: 'POST',
            headers: {
              'Content-Type': 'application/hal+json',
              'X-CSRF-Token': csrfToken
            },
            data: JSON.stringify(data),
            success: function (response) {
              //console.log(response);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Status: " + textStatus);
              console.log("Error: " + errorThrown);
            }
          });
        }


        //CSRF token to create POST request
        function getCsrfToken(callback) {
          $.get(Drupal.url('rest/session/token'))
            .done(function (data) {
              var csrfToken = data;
              callback(csrfToken);
            });
        }
        //   }
        // };

        //Make Radio button unchecked possible
        $(".action-type .action-label").on('click', function(e){
          console.log('boom');
          var checked = $(this).hasClass('active');

          $(".action-type .action-label").removeClass('active');
          $('.email-to-co-applicant').css({'display':'none'});

          $('.application-form-page #edit-submit').show();
          $('.application-form-page .btn-start-now').hide();
          $('.application-form-page #edit-submit').attr('disabled', false);

          if(checked){
            $(this).removeClass('active');
            $('#'+$(this).data('id')).prop('checked', false);
          }else{
            $(this).addClass('active');
            $('#'+$(this).data('id')).prop('checked', true);
          }

          if($('.edit-sign-up-required-on').hasClass('active')){
            $('.email-to-co-applicant').css({'display':'flex'});

            if(!$('#edit-co-email').val()){
              $('.application-form-page #edit-submit').attr('disabled', true);
            }
          }

          if($('.edit-sign-up-required-off').hasClass('active')){
            $('.application-form-page #edit-submit').hide();
            $('.application-form-page .btn-start-now').show();
          }

          $('#edit-co-email').on('keyup', function(e){
            var coEmail = $('#edit-co-email').val();

            if(coEmail && IsEmail(coEmail)){
              $('.application-form-page #edit-submit').attr('disabled', false);
            }else{
              $('.application-form-page #edit-submit').attr('disabled', true);
            }
          });


          $('.application-form-page .btn-start-now').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            $('.application-form-page #myapplication-area').hide();
            $('.application-form-page .page-title').text('Create Co-Applicant');
            $('.application-form-page #edit-submit').show();
            $('.application-form-page .action-buttons .btn-prev').hide();

            $('.application-form-page .btn-prev-to-applicant').show();
            $('.application-form-page #mycoapplication-area').show();
          });

          $('.application-form-page .btn-prev-to-applicant').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            $('.application-form-page #mycoapplication-area').hide();

            $('.application-form-page .btn-start-now').show();
            $('.application-form-page .btn-start-now').text('Edit Co-Applicant');
            $('.application-form-page #myapplication-area').show();
            $('.application-form-page .page-title').text('Create Applicant');
            $('.application-form-page #edit-submit').show();
            $('.application-form-page .action-buttons .btn-prev').show();
          });

          function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)) {
              return false;
            }else{
              return true;
            }
          }
        });
      });

      if ($('body').hasClass('page-node-type-property')) {
        var property_single_map_lat = Number($('#property-single-map').data('lat'));
        var property_single_map_lng = Number($('#property-single-map').data('lng'));
        // The location of Uluru
        var uluru = {lat: property_single_map_lat, lng: property_single_map_lng};
        // The map, centered at Uluru
        var property_single_map = new google.maps.Map(
          document.getElementById('property-single-map'), {zoom: 15, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: property_single_map});
      }

      /*
      function notificationList(){
        $.getJSON('/rest/api/notifications', function(notifications) {
          $('.popup-notification .notifications ul').empty();

          $.each(notifications, function(i, notification) {
            $('.popup-notification .notifications ul').append(
              '<li>'+
              '<div class="profile-photo"><img src="'+ notification.user_picture +'" class="rounded-circle" alt="Cinque Terre"></div>'+
              '<div class="notification-details">'+ notification.title +'</div>'+
              '</li>'
            );
          });
        });
      }
      */

      $('.my-property-listing .views-row').each(function(i){
        var nid = $(this).data('nid');

        $.getJSON('/rest/api/count/'+nid, function(data) {
          $('.my-property-'+ nid +' .total-application-count').text(data.total_application);
          $('.my-property-'+ nid +' .total-nodeview-count').text(data.total_view);
        });
      });


    //} //End of attach(context)
  //}; //End of Drupal.behaviors.formsValidation


  var flightPath = new google.maps.Polyline({});
  function get_location_cord(url,map){
    var coordinates = [];
    // flightPath.setMap(null);
    $.get(url, function(data){
      remove_polygone(flightPath);
      var cords = data[0].geojson.coordinates[0];
      for(var i = 0; i < cords.length; i++){
        coordinates.push({lng: cords[i][0], lat: cords[i][1]});
      }
      flightPath = new google.maps.Polyline({
        path: coordinates,
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2
      });
      // map.Polyline.clear();
      draw_polygone(flightPath,map);
      return flightPath;
    });
  }

  function draw_polygone(flightPath,map){
    flightPath.setMap(map);
  }

  function remove_polygone(flightPath){
    flightPath.setMap(null);
  }

})(jQuery, Drupal);


/*
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.inotify = {};

  Drupal.behaviors.inotify.RefreshNotificationsView = function () {
    // Find the notification view.
    $.each(Drupal.views.instances, function getInstance(index, element) {
      if (element.settings.view_name == 'notifications') {
        element.refreshViewAjax.progress = { type: 'none' };
        $('.js-view-dom-id-' + element.settings.view_dom_id).trigger('RefreshView');
        $(document).ajaxStop(function () {
          $('.inotify-opener .count').html($('.inotify-block-content .unread').length);
          $('.inotify-opener .count').show();
        });
      }
    });
  };

  Drupal.behaviors.NotificationsBlockEvents = {
    attach: function (context, settings) {

      $('.inotify-opener').once().click(function () {
        console.log('Boom');
        $('.inotify-block-content').toggleClass('active');
        $('.inotify-opener .count').hide();
      });

      $('.inotify-opener').once().click(function (e) {
        e.preventDefault();
        $('.inotify-block .inotify-list-content').fadeIn('fast', function () {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '0',
            'transition': 'all 500ms'
          });
        });
        $('body').addClass('overflow-hidden');
      });

      $('.inotify-block .inotify-list-content, .inotify-block .close').once().click(function (e) {
        if ($(e.target).hasClass('inotify-list-content') || $(e.target).hasClass('close')) {
          $('.inotify-block .inotify-list-content-container').css({
            'right': '-600px',
            'transition': 'all 500ms',
          });
          $('.inotify-block .inotify-list-content').fadeOut('fast');
          $('body').removeClass('overflow-hidden');
        }
      });
    }
  };
})(jQuery, Drupal);
*/
