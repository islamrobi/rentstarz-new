(function($) {

    'use strict';

    $(document).ready(function() {
        
        //console.log('Hello World');
        if($(".my-property-listing").hasClass("inactive")){
              $( "body" ).addClass( "my-inactive-property-listing" );
        }

        // Main page min height
        $("main").css("min-height", $( window ).height() - 120 );

        // Share report search
        $(".path-screening .dataTables_wrapper .dataTables_filter input").attr("placeholder", "Search");

        //Add active class to update applocation menu.
        if($("body.path-modal .sub-menu a:last-child").hasClass("active")){
              $( "body.path-modal .main-menu a:nth-child(2)" ).addClass( "active" );
        }

        //My listings no result
        //Add active class to update applocation menu.
        if($("body.path-dashboard form h3").hasClass("no-result")){
              $( "body" ).addClass( "no-listing" );
        }

        //Add user profile class to body.
        if($("body.user-logged-in.path-user article").hasClass("user-profile")){
              $( "body" ).addClass( "user-profile" );
        }


        // map and listing switching for mobile and ipad
        $('.property-list-map .filter-options .map').on('click', function(e){
          e.preventDefault();
          $('.property-list-map .property-list ul.item-wrapper').css({'display': 'none'});
          $('.property-list-map .property-map').css({'opacity': '1'});
        });

        $('.property-list-map .filter-options .list').on('click', function(e){
          e.preventDefault();
          $('.property-list-map .property-list ul.item-wrapper').css({'display': 'block'});
          $('.property-list-map .property-map').css({'opacity': '0'});
        });
        

        //landing page logo placement and floating menu for small devices.
        if ($(window).width() <= 991.98) {

            //if(!$("body").hasClass("path-property-listing")){
              //if ( $(".mr-auto.ml-auto").parents(".navbar-collapse").length == 1 ) { 
                //$( ".navbar .navbar-collapse .mr-auto.ml-auto" ).insertAfter( ".navbar .navbar-toggler" );
              //}
            //}

            $(".navbar button[data-toggle]").removeAttr("data-toggle");

            //All labet will be placeholder in mobile and tablet
            $("form :input").each(function(index, elem) {
              var eId = $(elem).attr("id");
              var type = $(elem).attr( "type" );
              var label = null;
              if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1 && (type == "text" || type == "email" || type == "password")) {
                $(elem).attr("placeholder", $(label).text());
                $(label).remove();
              }
            });

            //$( "<span class='ipad-notifications'></span>" ).insertBefore( ".navbar .navbar-collapse" );

        }

        $(window).resize(function () {
          // Main page min height
          $("main").css("min-height", $( window ).height() - 120 );

          if ($(window).width() <= 991.98) {

            // Branding logo arrangement for mobile and ipad
            //if(!$("body").hasClass("path-property-listing")){
              //if ( $(".mr-auto.ml-auto").parents(".navbar-collapse").length == 1 ) { 
                //$( ".navbar .navbar-collapse .mr-auto.ml-auto" ).insertAfter( ".navbar .navbar-toggler" );
              //}
            //}

            // Notification icon for mobile and ipad
            //if(!$('.navbar .mr-auto.ml-auto').next().is('.ipad-notifications') && !$("body").hasClass("path-property-listing")){
                //$( "<span class='ipad-notifications'></span>" ).insertAfter( ".navbar .mr-auto.ml-auto" );
            //}

            $(".navbar button[data-toggle]").removeAttr("data-toggle");
              
          }else if($('.navbar .navbar-toggler').next().is('.mr-auto.ml-auto')){
              //$('.navbar .mr-auto.ml-auto').insertBefore(".navbar .block--rentstarz-main-menu");
              //$(".ipad-notifications").remove();
              $('.property-list-map .property-map').css({'opacity': '1'});
          }
        });

        //landing page logo placement and floating menu for small devices.
        //if ($(window).width() <= 1200) {

            //if($("body").hasClass("path-dashboard")){
              //$( ".navbar .navbar-collapse .mr-auto.ml-auto" ).insertAfter( ".ipad-dashboard-menu.mobile-menu" );
              //$( "<span class='ipad-notifications'></span>" ).insertAfter( ".ipad-dashboard-menu.mobile-menu" );
              
            //}
        //}

        if($("body").hasClass("path-dashboard")){
            $(".property-details .d-flex li:first-child p").each(function(index, elem) {
              var bed = $(elem).text().split(' ')[0];
              $(elem).text(bed);
            });
        }

        /* Popup Menu for mobile and ipad */
        $('.navbar-toggler').on('click', function(e){
          e.preventDefault();
          $('body').addClass('overflow-hidden');
          $('.popup-menu').fadeIn('fast', function() {
            $('.popup-menu .popup-container').css({
              'right': '0',
              'transition': 'all 500ms'
            });
          });

        });

        /* Popup Notifications 
        $('.ipad-notifications').on('click', function(e){
            e.preventDefault();
            $('.popup-notification').fadeIn('fast', function() {
              $('.popup-notification .popup-container').css({
                  'right': '0',
                  'transition': 'all 500ms'
              });
            });
            $('body').addClass('overflow-hidden');
        });
        */
        /* My Application page*/
         $( "body.View-application .application-details-view header" ).insertAfter( "body.View-application .application-details-view article .application__field-property" );

        /*Featurs popup close*/
        $('.modal-property-filter').on('click', function(e){
          if($(e.target).hasClass('modal-property-filter')){
            $('.modal-property-filter .title .close').trigger( "click" );
          }
        });


        // Property image Carosel
        $('.carousel .carousel-item').each(function(){
            var minPerSlide = 1;
            var next = $(this).next();
            if (!next.length) {
            next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().addClass('clone-1').appendTo($(this));
            
            for (var i=0;i<minPerSlide;i++) {
                next=next.next();
                if (!next.length) {
                  next = $(this).siblings(':first');
                }                
                next.children(':first-child').clone().addClass('clone-2').appendTo($(this));
              }
        });

        // Property Details Modal Popup Image Viewer
        $('#propertyImageCarousel .carousel-item .col-md-6:not(.clone-1 , .clone-2)').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            },
            callbacks: {
              open: function() {
                $(".mfp-gallery").swipe( {
                  swipeLeft:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.next(); 
                  },
                  swipeRight:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.prev(); 
                  }
                });
              }
            }
        });

        $('#propertyImageCarousel .carousel-item .clone-1').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            },
            callbacks: {
              open: function() {
                $(".mfp-gallery").swipe( {
                  swipeLeft:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.next(); 
                  },
                  swipeRight:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.prev(); 
                  }
                });
              }
            }
        });

        $('#propertyImageCarousel .carousel-item .clone-2').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            },
            callbacks: {
              open: function() {
                $(".mfp-gallery").swipe( {
                  swipeLeft:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.next(); 
                  },
                  swipeRight:function(event, direction, distance, duration, fingerCount) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.prev(); 
                  }
                });
              }
            }
        });

        //Make fieldset Collapsible
        if($("article").hasClass("applicant--details") || $("article").hasClass("co-applicant--details") || $("article").hasClass("application--details")){
	        $('fieldset .card-body').hide();
	        $('.card-header').click(function(){
	            $(this).parent().find('.card-body').slideToggle("slow");
	            $(this).parent().toggleClass('active');
	        });
    	}

        $('.expand-all').click(function(){
          $("fieldset").each(function() {
            $(this).find('.card-body').slideDown("slow");
            $(this).addClass('active');
          });
        });

        $('.collapse-all').click(function(){
          $("fieldset").each(function() {
            $(this).find('.card-body').slideUp("slow");
            $(this).removeClass('active');
          });
        });

        //property list ul min-height
        if ($('main').hasClass('property-listing-with-map')) {
        	$('section.property-list-map .property-list ul.item-wrapper').css('min-height',$('main').height() - 30)
    	}

    	// Create application collapsible for mobile
    	//Make fieldset Collapsible
        if($("#myapplication-area").hasClass("myApplication") && $(window).width() <= 767){
	        $('#myapplication-area .section .row,#myapplication-area .section h3,#myapplication-area .section #ajax-wrapper').hide();
	        $('#myapplication-area .section h1').click(function(){
	            $(this).parent().find('.row').slideToggle("slow");
	            $(this).parent().find('h3').slideToggle("slow");
	            $(this).parent().find('#ajax-wrapper').slideToggle("slow");
	            $(this).parent().toggleClass('active');
	        });
    	}

    	$('.app-expand-all').click(function(){
          $("#myapplication-area .section").each(function() {
            $(this).find('.row').slideDown("slow");
            $(this).find('h3').slideDown("slow");
            $(this).find('#ajax-wrapper').slideDown("slow");
            $(this).addClass('active');
          });
        });

        $('.app-collapse-all').click(function(){
          $("#myapplication-area .section").each(function() {
            $(this).find('.row').slideUp("slow");
            $(this).find('h3').slideUp("slow");
            $(this).find('#ajax-wrapper').slideUp("slow");
            $(this).removeClass('active');
          });
        });
         
    });
})(jQuery, Drupal);